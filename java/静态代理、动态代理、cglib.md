>三者都是对目标类的增强，区别在于，静态代理和动态代理的目标类都要去实现同一个接口，静态代理在编译时就生成了代理类，代理类必须是这个接口的实现，而动态代理在运行时生成代理类，生成的代理类基本和静态代理差不多，只不过在运行时动态生成。而cglib也和动态代理差不多，只不过cglib代理生成的类是继承的目标类，这样目标类就不用实现接口了，动态生成的通过调用父类的方法来实现。

~~~ java
//动态代理：生成代理的工厂类需要实现InvercationHandler接口创建代理对象：

Proxy.newProxyInstance(target.getClass.getClassLoader(),target.getClass.getInterfaces,new InvercationHandler(){})

//cglib:生成代理的工厂类需要实现MethodInterceptor接口创建代理对象：
 //1. ⼯具类
 Enhancer en = new Enhancer();
 //2. 设置⽗类
 en.setSuperclass(target.getClass());
 //3. 设置回调函数
 en.setCallback(this);this工厂类
 //4. 创建⼦类(代理对象)
 en.create()
~~~