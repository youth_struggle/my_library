# java常用知识

## 1. 时间类
java的关于日期时间的类有：
* java.util.Date
* java.sql.Date
* java.sql.Time
* java.sql.Timestamp
* java.text.SimpleDateFormat
* java.util.Calendar

### 1. Calendar的使用
~~~ java
//获取当前时间
Calendar calendar=Calendar.getInstance();
Date date=calendar.getTime();
~~~

### 2. LocalDate的使用（）
~~~ java
//对字符串时间进行转换
DateTimeFormatter formatter= DateTimeFormatter.ofPattern("yyyy-MM-dd");
LocalDate localDate=LocalDate.parse("2022-10-16",formatter);
//对当前时间按格式转换
LocalDate localDate = LocalDate.now();
//按照yyyyMMdd样式进行更改
localDate.format(DateTimeFormatter.BASIC_ISO_DATE)
//按照yyyy-MM-dd样式进行更改
localDate.format(DateTimeFormatter.ISO_DATE)
//自定义修改
DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
String format = localDate.format(pattern);
//两个时间做差
LocalDate start = LocalDate.of(2021,5,1);
LocalDate end = LocalDate.now();
Period next = Period.between(start,end);
next.getDays();//相差天数
next.getMonths();//相差月份
next.getYears();//相差年份
//时间加一天
localDate.plusDays(1);
//获取当前日期，在当年的第几周，已当年的1号为第一周
LocalDate.now().get(WeekFields.of(DayOfWeek.MONDAY, 1).weekOfWeekBasedYear());
~~~

### @ResponseBody和@RequestBody
二者都是将对象转换成json字符串，前者是将返回的对象转换成json,后者是将请求的json转换为对象

### 当springMVC接收的对象中有内部类时，这个内部类必须是静态内部类
例如下面这个类：
~~~ java
@Data
public class ThresholdRule {
    private String freq;
    private Integer status;
    private List<Rule> rules;
    @Data
    public static class Rule {
        private String indicator;
        private String expr;
        private Double value;
    }
}
~~~

### 可以使用Gson将复杂的json转换成List对象
~~~ java
List<ThresholdRule> rules = gson.fromJson(thresholdRule, new TypeToken<List<ThresholdRule>>() {}.getType());
~~~

### 获取指定字符在最后一次出现的位置
~~~ java
String path="/home/work/log/server.log"
int index=path.lastIndexof("/")
~~~

### 获取指定字符在最后一次出现的位置
~~~ java
String path="/home/work/log/server.log"
int index=path.lastIndexof("/")
~~~

### 使用mybatis来生成动态sql
~~~java
        Configuration configuration = sqlSessionFactory.getConfiguration();
        MappedStatement mappedStatement = configuration.getMappedStatement(HighLoadCellPredictMapper.class.getName() + ".hightLoadPredictiveConfig");
        Map<String, PredictiveChangeRecordsVo> params = new HashMap<>();
        params.put("query", predictiveChangeRecordsVo);
        BoundSql boundSql = mappedStatement.getBoundSql(params);
        predictiveChangeRecordsDomain.setRuleSql(boundSql.getSql());
~~~

### feign调用出现问题时可以查看 AbstractLoadBalancerAwareClient.executeWithLoadBalancer 方法


### feign的 SynchronousMethodHandler.executeAndDecode 方法的第一句代码就是执行请求拦截方法


### 在Java中，如果以每年1号为第一周，获取当天是一年中的第几周.
> 必须是以每周的最后一天来算
~~~ java
        LocalDate lastDayOfWeek = LocalDate.now().with(DayOfWeek.SUNDAY);
        int week = lastDayOfWeek.get(WeekFields.of(DayOfWeek.MONDAY, 1).weekOfWeekBasedYear());
        int year = lastDayOfWeek.getYear();
        System.out.println(year + " " + week)
~~~

### easeExcel中有Date格式化的常量，com.alibaba.excel.util.DateUtils


### 查看mybatis加载xml文件，可以查看 com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties.getResources 方法


### mybatis在 org.apache.ibatis.mapping.MappedStatement.getBoundSql() 为sql设置参数。


### springmvc 返回参数序列化时间问题
> 在一个请求接收和返回参数时，涉及到时间转换，例如：LocalDateTime、Date、Timestamp 统一转换成yyyy-MM-dd HH:mm:ss 这种格式的字符串，我们可以全局统一配置。其中Timestamp没有默认的序列化实现类，可以自己写一个。

~~~ java
@Configuration
public class DateCovertors implements WebMvcConfigurer{
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        boolean tag = true;
        Iterator<HttpMessageConverter<?>> iterator = converters.iterator();
        while (iterator.hasNext()) {
            HttpMessageConverter<?> converter = iterator.next();
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                if (tag) {
                    MappingJackson2HttpMessageConverter jacksonConverter = (MappingJackson2HttpMessageConverter) converter;
                    SimpleModule module = new SimpleModule();
                    //反序列化
                    module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateFormat.DATE_TIME_FORMAT));
                    //序列化
                    module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateFormat.DATE_TIME_FORMAT));
                    module.addSerializer(Timestamp.class, new TimestampSerializer(DateFormat.DATE_TIME_FORMAT));
                    jacksonConverter.getObjectMapper().registerModule(module);
                    tag = false;
                } else {
                    iterator.remove();
                }
            }
        }
    }
}

public class TimestampSerializer extends JsonSerializer<Timestamp> {
    private static DateTimeFormatter DEFAULT_FORMATTER = DateFormat.DATE_TIME_FORMAT;

    public TimestampSerializer(DateTimeFormatter dateTimeFormat) {
        DEFAULT_FORMATTER = dateTimeFormat;
    }

    @Override
    public void serialize(Timestamp value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(value.toLocalDateTime().format(DEFAULT_FORMATTER));
    }
}
~~~