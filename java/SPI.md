# java的spi技术（server provider interface）
> 方式2有问题
使用方式：
1. 定义一个接口DLL
~~~ java
package com.qingzheng
//方式1：
public interface Dll{
    String ver(String version);
}
//方式2：
@SPI
public interface Dll{
     String ver(String version);
}
~~~
2. 对接口进行实现，可以有多个实现类，这里假设有两个实现DllImplA、DllImplB

```java
package com.qingzheng
public class DllImplA implements Dll{
    @Override
    String ver(String version){
        return version;
    }
}

public class DllImplB implements Dll{
    @Override
    String ver(String version){
        return version;
    }
}
```

3. 在resource下创建文件/META-INF/services/**com.qingzheng.Dll**(接口全限定类名)，写如内容

   ```properties
   方式1：
   com.qingzheng.DllImplA
   com.qingzheng.DllImplB
   方式2：
   a=com.qingzheng.DllImplA
   b=com.qingzheng.DllImplB
   ```

4. 主方法

   ```java
   public static void main(args[]){
       /*
       方式1：会对接口的所有类对象进行实例化，只需要某个实例对象时需要遍历，并且存在线程不安全问题
       */
       ServiceLoader<Dll> serviceLoader = ServiceLoader.load(Dll.class);
       Iterator<Serializer> iterator = serviceLoader.iterator();
       while (iterator.hasNext()) {
           Dll dll= iterator.next();
           System.out.println(dll.getClass().getName());
       }
       
       /*
       方式2：避免了上面的问题
       */
   
       ExtensionLoader<Dll> loader = ExtensionLoader.getLoader(Dll.class);
       Dll dll = loader.getExtension("a");
       System.out.println(dll.getClass().getName());
   }
   ```

   

