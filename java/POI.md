# Apache POI操作 excel、world
> HSSF:读写03版的excel,后缀.xls  
> XSSF:读写07版excel,后缀.xlsx  
> HWPF:读写word  
> HSLF:读写幻灯片  
> HDGF:读写visio格式文件  

## 1. 导入依赖
~~~ xml
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi</artifactId>
    <version>3.8</version>
</dependency>
<!-- 07版excel -->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-ooxml</artifactId>
    <version>3.8</version>
</dependency>
<!-- word文件读取 -->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-scratchpad</artifactId>
    <version>3.8</version>
</dependency>
~~~

## 2. java代码读取excel
~~~ java
public List<Area> importXLS(){

    ArrayList<Area> list = new ArrayList<>();
    try {
　　　　　//1、获取文件输入流
　　　　　InputStream inputStream = new FileInputStream("/Users/Shared/区域数据.xls");
　　　　　//2、获取Excel工作簿对象
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
　　　　　//3、得到Excel工作表对象
        HSSFSheet sheetAt = workbook.getSheetAt(0);
        //4、循环读取表格数据
　　　　 for (Row row : sheetAt) {
　　　　　　　//首行（即表头）不读取
            if (row.getRowNum() == 0) {
                continue;
            }
            //读取当前行中单元格数据，索引从0开始
　　　　　   String areaNum = row.getCell(0).getStringCellValue();
            String province = row.getCell(1).getStringCellValue();
            String city = row.getCell(2).getStringCellValue();
            String district = row.getCell(3).getStringCellValue();
            String postcode = row.getCell(4).getStringCellValue();

            Area area = new Area();
            area.setCity(city);
            area.setDistrict(district);
            area.setProvince(province);
　　　　　　 area.setPostCode(postcode);
            list.add(area);
        }
　　　　 //5、关闭流
        workbook.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
　　return list;
}
~~~

## 3. java读取word文档
### 1. doc文件读取
~~~ java
 public static void readAndWriterTest3() throws IOException {
         File file = new File("C:\Users\tuzongxun123\Desktop\aa.doc");
         String str = "";
         try {
             FileInputStream fis = new FileInputStream(file);
             HWPFDocument doc = new HWPFDocument(fis);
             String doc1 = doc.getDocumentText();
             System.out.println(doc1);
             StringBuilder doc2 = doc.getText();
             System.out.println(doc2);
             Range rang = doc.getRange();
             String doc3 = rang.text();
             System.out.println(doc3);
             fis.close();
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
~~~
### 2. docx文件读取
~~~ java
 public static void readAndWriterTest4() throws IOException {
         File file = new File("C:\Users\tuzongxun123\Desktop\aa.docx");
         String str = "";
         try {
             FileInputStream fis = new FileInputStream(file);
             XWPFDocument xdoc = new XWPFDocument(fis);
             XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
             String doc1 = extractor.getText();
             System.out.println(doc1);
             fis.close();
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
~~~
## 4. java向word写入表格
![表格](/resources/1.bmp)
## 4. EasyExcel的使用
[参考easyExcel官方文档](https://gitee.com/mirrors/easyexcel?_from=gitee_search)