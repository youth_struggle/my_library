## jvm参数

- -Xmx：指定JVM堆内存的最大值。例如，-Xmx2g表示将堆内存最大限制设置为2GB。

- -Xms：指定JVM堆内存的初始大小。例如，`-Xms512m`表示将堆内存初始大小设置为512MB。

- -Xss：指定每个线程的栈大小。默认值根据操作系统的不同而不同。例如，`-Xss1m`表示将线程栈大小设置为1MB。

- -XX:MaxMetaspaceSize：指定Metaspace（

  [^元数据空间]: Java虚拟机用来存储类元数据的内存区域，Java 8及以上版本中，元数据空间取代了永久代，类元数据是描述Java类结构的数据，包括类的名称、父类、接口、方法、字段等信息。元数据空间特点：元数据空间的大小不再由-Xmx和-XX:MaxPermSize参数限制，而是由-XX:MaxMetaspaceSize参数控制、元数据空间使用的是本地内存而不是Java堆内存

  ）的最大大小。例如，`-XX:MaxMetaspaceSize=256m`表示将Metaspace的最大大小设置为256MB。

- -XX:PermSize和-XX:MaxPermSize：用于指定永久代（Permanent Generation）的初始大小和最大大小。在Java 8及以上的版本中，永久代已被Metaspace所取代，这两个参数已经不再生效。

## jvm指令使用

### 1. jsp

~~~shell
# 查询java应用信息
jps [-l|-lv|-q|-m] [hostid| grep xxx]
~~~

### 2. jstat

可以查看堆内存各部分的使用量，以及加载类的数量。命令的格式如下：jstat [-命令选项] [vmid] [间隔时间/毫秒] [查询次数]

详细信息：[jstat命令详解 - 南湖公明 - 博客园 (cnblogs.com)](https://www.cnblogs.com/lizhonghua34/p/7307139.html)

- vmid是Java应用的进程id
- 单位kb、s

- 命令选项：-class、-complier、-gc(垃圾回收统计)、-gcutil(总结垃圾回收统计)、-gccapacity(堆内存统计)

  ~~~ shell
  jstat -gcutil 7732 1000
  # S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT 
  
  # S0：表示Survivor 0区的使用率，即Survivor 0区已使用的比例。
  # 
  # S1：表示Survivor 1区的使用率，即Survivor 1区已使用的比例。
  # 
  # E：表示Eden区的使用率，即Eden区已使用的比例。
  # 
  # O：表示老年代的使用率，即老年代已使用的比例。
  # 
  # M：表示元数据区（Metaspace）的使用率，即元数据区已使用的比例。
  # 
  # CCS：表示压缩类空间的使用率，即压缩类空间已使用的比例。
  # 
  # YGC：表示年轻代垃圾回收的次数。
  # 
  # YGCT：表示年轻代垃圾回收的时间，以秒为单位。
  # 
  # FGC：表示Full GC的次数。
  # 
  # FGCT：表示Full GC的时间，以秒为单位。
  # 
  # GCT：表示垃圾回收的总时间，以秒为单位。
  ~~~

### 3. jmap
查看指定应用的堆使用情况
~~~ shell
  jmap -heap <pid>
~~~