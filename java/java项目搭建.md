# 项目构建

- <packaging>：
- <scope>标签
  - `compile`（默认）：该依赖项在所有阶段都可见，包括编译、测试、运行和打包阶段。这是最常用的作用域。
  - `runtime`：该依赖项在运行和打包阶段可见，但在编译和测试阶段不可见。这意味着它不会参与编译和测试，但在运行时会被加载和使用。
  - `test`：该依赖项只在测试阶段可见，在编译、运行和打包阶段不可见。它用于在测试代码中使用的依赖项，不会被打包到最终的应用程序中。
  - `provided`：该依赖项在编译和测试阶段可见，但在运行和打包阶段由目标环境（如Java EE容器）提供。它适用于那些在编译时需要，但在运行时由目标环境提供的依赖项。
- mapper.xml初始文件：

    ~~~ xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
            "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
    <mapper namespace="com.dtsw.mapper.ShareMapper">
    
    </mapper>
    ~~~




## 1、springboot、mybatis、mysql、druid

1. pom文件

   ~~~ xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
       <modelVersion>4.0.0</modelVersion>
       <parent>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-parent</artifactId>
           <version>2.1.18.RELEASE</version>
       </parent>
   
       <groupId>com.dtsw</groupId>
       <artifactId>dynamic-mybatis</artifactId>
       <version>1.0.0</version>
       <name>dynamic-mybatis</name>
       <packaging>jar</packaging>
       <description>动态mybtsi</description>
   
   
       <dependencies>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-test</artifactId>
               <scope>test</scope>
           </dependency>
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>8.0.28</version>
           </dependency>
           <dependency>
               <groupId>org.mybatis.spring.boot</groupId>
               <artifactId>mybatis-spring-boot-starter</artifactId>
               <version>2.1.1</version>
           </dependency>
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>druid-spring-boot-starter</artifactId>
               <version>1.1.10</version>
           </dependency>
       </dependencies>
       <build>
           <outputDirectory>target</outputDirectory>
           <plugins>
               <plugin>
                   <groupId>org.springframework.boot</groupId>
                   <artifactId>spring-boot-maven-plugin</artifactId>
               </plugin>
           </plugins>
       </build>
   </project>
   ~~~

   

2. yml配置

~~~ yaml
server:
  port: 8080

spring:
  application:
    name: dynamic-mybatis
  datasource:
    url: jdbc:mysql://101.43.99.219:3306/mybatis?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true
    username: mybatis
    password: xiechao88
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      initial-size: 5
      max-active: 20
      min-idle: 5
      max-wait: 60000
      validation-query: SELECT 1
      test-on-borrow: true
      test-on-return: false
      test-while-idle: true
      time-between-eviction-runs-millis: 60000
      min-evictable-idle-time-millis: 300000
      pool-prepared-statements: true
      max-pool-prepared-statement-per-connection-size: 20
      filters: stat,wall
      connection-properties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
mybatis:
  mapper-locations: classpath:mappers/*.xml
  configuration:
    map-underscore-to-camel-case: true
~~~

