## spring、springboot中的注解的使用

### @AutoConfigureBefore
> 用于控制自动配置类的加载顺序，value和name可以多个，不同的前者指定类后者是指定全限定类名，效果相同。
> 例如：
~~~ java
@AutoConfigureBefore(value = DataSourceAutoConfiguration.class, name = "com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure")
~~~

### @Import
> 将一个或多个外部配置类导入到当前的配置类中，将它们的配置信息合并到当前配置中。
~~~ java
@Import({DruidDynamicDataSourceConfiguration.class, DynamicDataSourceCreatorAutoConfiguration.class, DynamicDataSourceAopConfiguration.class, DynamicDataSourceAssistConfiguration.class})
~~~

