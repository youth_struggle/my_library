# 内存淘汰策略
1. noeviction: 不删除策略。当达到最大内存限制时, 如果需要使用更多内存,则直接返回错误信息。（redis默认淘汰策略）
2. allkeys-lru: 在所有key中优先删除最近最少使用(less recently used ,LRU) 的 key。
3. allkeys-random: 在所有key中随机删除一部分 key。
4. volatile-lru: 在设置了超时时间（expire ）的key中优先删除最近最少使用(less recently used ,LRU) 的 key。
5. volatile-random: 在设置了超时时间（expire）的key中随机删除一部分 key。
6. volatile-ttl: 在设置了超时时间（expire ）的key中优先删除剩余时间(time to live,TTL) 短的key。

# 过期策略
> 注意：并不是一次运行就检查所有的库，所有的键，而是随机检查一定数量的键。
1. 惰性删除：Redis的惰性删除策略由 db.c/expireIfNeeded 函数实现，所有键读写命令执行之前都会调用 expireIfNeeded 函数对其进行检查，如果过期，则删除该键，然后执行键不存在的操作；未过期则不作操作，继续执行原有的命令。

2. 定期删除：由redis.c/activeExpireCycle 函数实现，函数以一定的频率运行，每次运行时，都从一定数量的数据库中取出一定数量的随机键进行检查，并删除其中的过期键。