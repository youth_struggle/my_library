local str = "lua语法正则匹配1.703,325|这个字符串中的数字"
local numStr = ""
-- 遍历字符串的每个字符
for i = 1, #str do
    local char = string.sub(str, i, i)

    -- 如果字符是数字，将其添加到numStr中
    if tonumber(char) then
        numStr = numStr .. char
    end
end
-- 将拼接好的numStr转换为数字
local result = tonumber(numStr)
-- 输出结果
print(result)
