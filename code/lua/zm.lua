QMPlugin = {}
--[[****************************版权所有,请勿修改!****************************]]
--插件作者: 紫猫学院
--联系方式: 345911220@qq.com
--官方网址: zimaoxy.com
--招生推广: 紫猫编程学院专注按键精灵电脑版、安卓版、VB、VBS、Lua、易语言等编程教学，欢迎咨询入学。
--教学模式: QQ群视频现场直播远程授课+上千集超清VIP视频+全面系统学习计划+终身制学习+创业扶持+有问必答
--报名方式: 联系QQ345911220
--使用方法: 在按键精灵中采用 Import "zm.lua" 导入插件后，再用 zm.函数名 即可调用
--[[****************************版权所有,请勿修改!****************************]]
-- print(string.format("%18.0f",124768747512312313123456))
--CHECK
--LuaAuxLib.Check()

--ELEMENT.GETALL
--LuaAuxLib.GetUiElement()
--ELEMENT.GETALLINJSON
--LuaAuxLib.GetFullUiElement()
--ELEMENT.GETTEXT
--LuaAuxLib.GetElementText(%1)
--ELEMENT.FIND
--LuaAuxLib.FindUiElement(%1, %2)

--ELF.GETGENERALPARAMS
--LuaAuxLib.ELF_GetGeneralParams()
--INITELFKEYS
--LuaAuxLib.InitElfKeys(%1, %2)

--FIXCOORDINATE
--LuaAuxLib.FixCoordinate()

--GETKVALUE
--LuaAuxLib.GetKeyValueFromWeb(%1,%2)
--SETKVALUE
--LuaAuxLib.PutKeyValueToWeb(%1,%2,%3)

--NOTIFYAPP
--LuaAuxLib.NotifyApp(%1)

--SMARTOCR
--LuaAuxLib.SmartOcr(%1,%2,%3,%4,%5)

--SETCONTROLBARPOS
--LuaAuxLib.SetControlBarPos(%1, %2, %3)

--SYS.CLEARALLPHOTOS
--LuaAuxLib.ClearAllPhotos()

--SAVEIMAGETOALBUM
--LuaAuxLib.SaveImageToAlbum(tostring(%1))

--REMOTERUNLOG
--LuaAuxLib.SpecialFunc(LuaAuxLib.Encode_GetJsonLib():encode(%1))

--4.21xx
--ElementRotation加入旋转指定坐标功能
--读取指定行加入随机读取某一行功能
--快速判断文件编码
--arrayremove参数传值调用是否写反了
--获取ip加入随机数实现跳过缓存功能
--swipeex加入字符串坐标功能
--todo: 检查lua-lockbox是否有更新
--[[**************************************************************************]]
--[[****************************** 全局数据 **********************************]]
--[[**************************************************************************]]
local _zmm           = {}
local zmm            = {}
QMPlugin             = zmm      -- 所有表zmm里面的键名都会被作为插件函数使用

_zmm._VERSION_       = "4.2118" -- 对外版本号
_zmm._VER_           = 13       -- 内部版本号, 每次修改至少+1
_zmm._URL_analytics_ = "http://ia.51.la/go1?id=20749581&pvFlag=1"
_zmm._URL_ELF_       = "http://ia.51.la/go1?id=20746653&pvFlag=1"
_zmm._URL_           = "https://text-1251668224.file.myqcloud.com/zmlua_aj.json" -- 对外版本号
_zmm._NEW_VERSION_   = ""
_zmm._NEW_VER_       = 0
_zmm._INFO_          = {}
_zmm._CONFIG_FILE_   = "zmplugin.json"                                                           -- 插件配置文件
_zmm._URL_DOWNLOAD_  = "https://zimaoxy.com/b/t-77-1-1.html"                                     -- 插件下载地址
_zmm._URL_DOCUMENT_  = "https://zimaoxy.com/m/"                                                  -- 帮助文档
_zmm._QQ_GROUP_      = "7333555"                                                                 -- 交流QQ群
_zmm._PLUGIN_DIR_    = "zmplugin"                                                                -- 插件配置文件夹名
_zmm._GLOBAL_PATH_   = (__MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__ or "") .. _zmm._PLUGIN_DIR_ .. "/" -- 当前全局路径
--_ZM_._PLUGIN_DIR_    = "" -- 测试抹机影响
--_ZM_._GLOBAL_PATH_   = __MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__      -- 测试抹机影响
_zmm._VIP_           = false


_zmm.TickCount = function() return LuaAuxLib.GetTickCount() end
_zmm.CreatePluginDir = function()
	LuaAuxLib.DIR_Create(_zmm._GLOBAL_PATH_)
	os.execute("mkdir -p " .. _zmm._GLOBAL_PATH_)
	_zmm.CreatePluginDir = function()
	end
end

--[[**************************************************************************]]
--[[****************************** 基础函数 **********************************]]
--[[**************************************************************************]]
-- 在自动识别Lua与按键精灵的输出，带前缀"紫猫插件："
local function traceprint(...)
	if QMPlugin then
		print(...)
	else
		local line = select(1, ...)
		if type(line) == "string" and line:match("^%_%d+$") then
			LuaAuxLib.TracePrint(line .. ":", table.concat({ ... }, " ", 2, select("#", ...)))
		elseif line == ":" and #{ ... } > 1 then
			LuaAuxLib.TracePrint(":", table.concat({ ... }, " ", 2, select("#", ...)))
		else
			LuaAuxLib.TracePrint(":", "紫猫插件：", ...)
		end
	end
end

--[[
@fname:UUID
@cname:生成UUID
@tname:生成UUID
@format:无
@note:生成唯一不重复的36位UUID, 建议在脚本开头执行一次zm.Init()后使用本命令
@ret:UUID, 字符串, 成功返回一个标准UUID, 失败返回null
@arg:格式, 数值型, 可选, 0表示标准小写uuid, 1表示标准大写写uuid, 省略默认为0
@exp:Dim uuid = zm.UUID()  //随机生成一个UUID, 建议脚本开头执行一次zm.Init()后使用本命令
--]]
function zmm.UUID(mode)
	-- 初始化时需要生成UUID, 故不加入统计
	-- 由于try需要调用uuid(), 所以不使用try
	local f
	local isok, errors = pcall(
		function()
			local uuid
			f = io.open("/proc/sys/kernel/random/uuid", "r")
			if f then
				uuid = f:read()
				f:close()
			else
				local seed = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }
				local tb = {}
				for _ = 1, 32 do
					tb[#tb + 1] = seed[math.random(1, 16)]
				end
				uuid = tb[1] .. tb[2] .. tb[3] .. tb[4] .. tb[5] .. tb[6] .. tb[7] .. tb[8] .. "-" ..
					tb[9] .. tb[10] .. tb[11] .. tb[12] .. "-" ..
					tb[13] .. tb[14] .. tb[15] .. tb[16] .. "-" ..
					tb[17] .. tb[18] .. tb[19] .. tb[20] .. "-" ..
					tb[21] ..
					tb[22] ..
					tb[23] .. tb[24] .. tb[25] .. tb[26] .. tb[27] .. tb[28] .. tb[29] .. tb[30] .. tb[31] .. tb[32]
			end
			if mode == 1 then uuid = uuid:upper() end
			return uuid
		end
	)
	if isok then
		if io.type(f) == "file" then f:close() end
		return errors
	else
		traceprint("发生运行时错误！错误代码：zm.UUID()，错误信息：", errors)
	end
end

function _zmm.ASCIIToVariable(s)
	if #s > 2 and s:sub(1, 2) == "id" then
		local t = {}
		for i = 3, #s, 2 do
			t[#t + 1] = string.char(tonumber(s:sub(i, i + 1), 16))
		end
		return table.concat(t)
	end
end

-- return table/nil
function _zmm.readzmconfig()
	local f
	local isok, errors = pcall(
		function()
			local path = (_zmm._GLOBAL_PATH_ or "./") .. _zmm._CONFIG_FILE_
			local text, json
			f = io.open(path, "r")
			if f then
				text = f:read("*all")
				json = LuaAuxLib.Encode_GetJsonLib():decode(text)
				f:close()
			end
			return json
		end
	)
	if isok then
		if io.type(f) == "file" then f:close() end
		return errors
	end
end

-- return string/nil
function _zmm.writezmconfig(json)
	local f
	local isok, errors = pcall(
		function()
			local path = (_zmm._GLOBAL_PATH_ or "./") .. _zmm._CONFIG_FILE_
			local text
			f = io.open(path, "w")
			if f then
				text = LuaAuxLib.Encode_GetJsonLib():encode(json)
				f:write(text)
				f:close()
			end
			return text
		end
	)
	if isok then
		if io.type(f) == "file" then f:close() end
		return errors
	end
end

function _zmm.FunctionToASCII(s)
	local t = { string.byte(s:upper(), 1, -1) }
	for k, v in ipairs(t) do
		t[k] = string.format("%02x", v)
	end
	local str = "id" .. table.concat(t)
	return str
end

-- 统计使用人次 http://monster.gostats.cn/summary.xml?id=492573
-- 2017-05-30: 加入开关判断，保证一次运行只调用一次，加快执行效率
-- 2017-09-09: 借鉴山海师插件，让函数执行一次后修改为空函数
-- 2018-04-27: 通过文件判断每天只执行一次
-- 2018-05-19: 改为apsx网站统计
-- 2018-06-04: 加入包名统计
-- 2018-07-18: 仅统计按键精灵用户, 忽略小精灵
-- 2020-04-08: 更换统计地址
function _zmm.analytics()
	local f
	local isok, errors = pcall(
		function()
			_zmm.CreatePluginDir()

			local json = _zmm.readzmconfig()
			if json then
				if json.cur_ver == _zmm._VER_ then
					if json.date == os.date("%Y-%m-%d") then
						--todo: 读取本地配置检查更新
						zmm.CheckForUpdates(1) -- 检查插件更新
						return 2
					else
						json.date = os.date("%Y-%m-%d")
						_zmm.writezmconfig(json)
					end
				else
					json.date = os.date("%Y-%m-%d")
					json.cur_ver = _zmm._VER_
					_zmm.writezmconfig(json)
					--                    _ZM_.writezmconfig( {date=os.date("%Y-%m-%d"), cur_ver=_ZM_._VER_} )
				end
			else
				_zmm.writezmconfig({ date = os.date("%Y-%m-%d"), cur_ver = _zmm._VER_ })
			end

			if not (_zmm._GLOBAL_PATH_:match("MobileAnJian") or _zmm._GLOBAL_PATH_:match("mobileanjian")) then
				traceprint("小精灵端已跳过检查插件更新")
				LuaAuxLib.URL_OperationGet(_zmm._URL_ELF_, 3)
				return 3
			end

			_zmm.Init()
			zmm.CheckForUpdates(2) -- 检查插件更新
			LuaAuxLib.URL_OperationGet(_zmm._URL_analytics_, 3)
			return 4
		end
	)

	_zmm.analytics = function() return 1 end
	if io.type(f) == "file" then f:close() end

	--    traceprint(isok, errors)
	if isok then
		return errors
	else
		return 0
	end
end

local function try(block)
	local tablejoin = function(...)
		local result = {}
		for _, t in ipairs({ ... }) do
			if type(t) == "table" then
				for k, v in pairs(t) do
					if type(k) == "number" then
						table.insert(result, v)
					else
						result[k] = v
					end
				end
			else
				table.insert(result, t)
			end
		end
		return result
	end

	_zmm.analytics()

	-- get the try function
	local try = block[1]
	assert(try)

	-- get catch and finally functions
	local funcs = tablejoin(block[2] or {}, block[3] or {})

	-- try to call it
	local result_error = {}
	local results = { pcall(try) }
	if not results[1] then
		-- run the catch function
		if funcs and funcs.catch then
			result_error = { funcs.catch(results[2]) }
		end
	end

	-- run the finally function
	if funcs and funcs.finally then
		local result_fin = { funcs.finally(table.unpack(results)) }
		if #result_fin > 0 then
			return table.unpack(result_fin)
		end
	end

	-- ok?
	if results[1] and #results > 1 then
		return table.unpack(results, 2, #results)
	else
		if #result_error > 0 then
			return table.unpack(result_error)
		else
			return nil
		end
	end
end
local function catch(block)
	-- get the catch block function
	return { catch = block[1] }
end
local function finally(block)
	-- get the finally block function
	return { finally = block[1] }
end


function _zmm.clearbom(str)
	str = str:gsub("^" .. string.char(0xEF, 0xBB, 0xBF), "")
	return str
end

function _zmm.filewrite(path, ...)
	local args, f = { path = path, texts = { ... } }
	local isok, errors = pcall(
		function()
			f = io.open(args.path, "w")
			if f == nil then
				return nil
			end

			if type(args.texts[1]) == "table" then
				args.texts = args.texts[1]
				args.sep = "\n"
			end

			args.text = table.concat(args.texts, args.sep)

			f:write(args.text)
			f:close()
			return args.texts
		end
	)
	if io.type(f) == "file" then f:close() end
	if isok then
		return errors
	else
		traceprint("发生运行时错误！错误代码：filewrite()，错误信息：", errors)
	end
end

function _zmm.fileread(path, isdel)
	local f

	local isok, errors = pcall(
		function()
			f = io.open(path, "r")
			if f == nil then
				return nil
			end
			local ret = f:read("*all")
			f:close()
			if isdel then
				os.remove(path)
			end
			return ret
		end
	)
	if io.type(f) == "file" then f:close() end
	if isok then
		return errors
	else
		traceprint("发生运行时错误！错误代码：fileread()，错误信息：", errors)
	end
end

--[[
@fname:SetDelay
@cname:设置超级延时
@tname:设置超级延时
@format:属性表
@note:设置超级延时zm.Delay()的默认值, 设置一次后, 所有zm.Delay()均受影响
@ret:属性表, table, 返回设置的表
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值$n 例如{“min”:-50, “max”:50}表示所有zm.Delay()函数随机浮动-50到50之间$n 详细属性如下:$n min: 数值型, 随机浮动最小值, 省略默认0$n max: 数值型, 随机浮动最大值, 省略默认为0$n time: 数值型, 默认延时时间, 省略默认为0
@exp:zm.SetDelay({"min":-30, "max":40})$n zm.Delay(100)  //表示随机延时(100-30)到(100+40)毫秒范围
@exp:zm.SetDelay({"min":-30, "max":40})$n zm.Delay(100, 200)  //表示随机延时100到200毫秒范围, 不受默认值影响
@exp:zm.SetDelay({"min":-30, "max":40})$n zm.Delay(100, true)  //表示固定延时100毫秒, 不要随机
@exp:zm.SetDelay({"min":-300, "max":400, "time":1000})$n zm.Delay()   //表示随机延迟(1000-300)到(1000+400)毫秒范围
--]]
_zmm.DelayAttr = {}
_zmm.DelayAttr.default = { min = 0, max = 0, time = 0, fun = false, params = {} }
_zmm.DelayAttr.mt = { __index = _zmm.DelayAttr.default }
function zmm.SetDelay(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.DelayAttr.default[k:lower()] ~= nil then
					_zmm.DelayAttr.default[k:lower()] = v
				end
			end
			return _zmm.DelayAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetDelay()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:PCall
@cname:保护执行函数
@tname:以保护模式执行函数 $1
@format:执行函数名[, 任意数量参数][, 错误处理函数名[, 任意数量参数][, 必定执行函数名[, 任意数量参数] ] ]
@note:以保护模式调用执行函数, 避免由于异常错误导致整个脚本停止结束. 支持错误处理函数与必定执行函数, 支持任意数量参数, 效果类似于其他语言的try...catch...finally功能. 也可以实现调用按键函数免写可选参数功能
@ret:返回函数自身返回值
@arg:执行函数名, function, 需要保护执行的函数名, 注意这里填写的是函数名, 不要在后面加括号变成调用函数!
@arg:任意数量参数, 需要传入前面函数的参数, 多个依次以英文逗号隔开
@arg:错误处理函数名, function, 可选, 如果前面执行函数内部出现异常错误导致无法执行完毕情况时, 将会执行本处理函数, 如果没有前面执行函数没有出错, 那么就不会执行本处理函数
@arg:任意数量参数, 需要传入前面函数的参数, 多个依次以英文逗号隔开
@arg:必定执行函数名, function, 可选, 不管前面函数执行是否出错, 本函数都将必定会执行一次.
@arg:任意数量参数, 需要传入前面函数的参数, 多个依次以英文逗号隔开
@exp:代码过多, 请参考命令帮助文档.
--]]
function zmm.PCall(...)
	local args = { ... }

	return try {
		function()
			local foo = {
				"try",
				"catch",
				"finally",
				try = { function()
				end },
				catch = {},
				finally = { function()
				end }
			}
			local i = 0
			if #args == 0 then
				return nil
			elseif type(args[1]) == "table" then
				foo.try = args[1].t or args[1].try or foo.try
				foo.catch = args[1].c or args[1].catch or foo.catch
				foo.finally = args[1].f or args[1].finally or foo.finally
			else
				if type(args[1]) ~= "function" then
					error("保护执行第一个参数必须是函数名, 而不是调用函数命令")
					return nil
				end

				for _, v in ipairs(args) do
					if type(v) == "function" then
						i = i + 1
						foo[foo[i]] = { v }
					else
						table.insert(foo[foo[i]], v)
					end
				end
			end
			return try {
				function()
					return foo.try[1](table.unpack(foo.try, 2))
				end,
				catch {
					function(errors)
						errors = errors:gsub("id%w+", function(s) return _zmm.ASCIIToVariable(s) end)
						if #foo.catch == 0 then
							traceprint("PCall捕获异常:", errors)
						else
							return foo.catch[1](table.unpack(foo.catch, 2))
						end
					end
				},
				finally {
					function()
						return foo.finally[1](table.unpack(foo.finally, 2))
					end
				}
			}
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.PCall()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:TranslateError
@cname:翻译错误
@tname:翻译错误 $1
@format:错误文本
@note:可以将运行时错误内容中的id部分翻译成对应变量名或函数名, 例如 attempt to index global 'ide7b4abe78cabe5ada6e999a2' (a null value), 将翻译并输出 attempt to index global '紫猫学院' (a number value)
@ret:无
@arg:错误文本, 字符串, 含'idxxxxxx'的错误文本内容
@exp:Dim 紫猫学院, a, b$nzm.TranslateErrorID "当前脚本第6行：发生运行时错误！错误代码：2，错误行号：6，错误信息： attempt to index global 'ide7b4abe78cabe5ada6e999a2' (a null value)"$n紫猫学院(0) = a + b
--]]
function zmm.TranslateError(errors)
	return try {
		function()
			local err = errors:gsub("id%w+", function(s) return _zmm.ASCIIToVariable(s) end)
			traceprint("id错误翻译 >> ", err)
			return err
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TranslateError()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:Delay
@cname:超级延时
@tname:超级延时 $1 到 $2 毫秒
@format:[延时毫秒][,浮动毫秒][, 禁用随机]
@note:防检测随机延迟一段时间, 受zm.SetDelay()影响
@ret:随机时间, 数值型, 成功返回延时的实际时间, 失败返回null
@arg:延时毫秒, 数值型, 可选, 要延时的时间, 单位毫秒, 省略默认为zm.SetDelay()的时间
@arg:浮动毫秒, 数值型, 可选, 与延时毫秒参数组成随机数范围, 省略默认为zm.SetDelay()的随机浮动
@arg:禁用随机, 布尔型, 可选, 是否禁用随机延时, 省略默认为false
@exp:zm.Delay(100)        //固定延迟100毫秒
@exp:zm.Delay(100, 200)   //表示随机延迟100到200毫秒
--]]
function zmm.Delay(...)
	local args = { ... }
	return try {
		function()
			for k, v in ipairs(args) do
				if args.fun then
					table.insert(args.params, v)
				else
					if type(v) == "function" then
						args.fun = v
						args.params = {}
					elseif type(v) == "string" then
						v = tonumber(v)
						if v then
							args[k] = v
						end
					elseif type(v) == "number" then
						if args.time == nil then
							args.time = v
						elseif args.rndmin == nil then
							args.min = args.time > v and v or args.time
							args.max = args.time > v and args.time or v
							args.time = 0
						end
					elseif type(v) == "boolean" then
						args.nornd = v
					end
				end
			end

			setmetatable(args, _zmm.DelayAttr.mt)

			if not args.nornd then
				args.time = args.time + math.random(args.min, args.max)
			end

			args.time = args.time < 0 and 0 or args.time

			LuaAuxLib.Sleep(args.time)

			if not (args.fun and _zmm._VIP_) then
				return args.time
			elseif type(args.fun) == "string" then
				args.fun = _G[_zmm.FunctionToASCII(args.fun)]
			end

			return args.fun(table.unpack(args.params))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Delay()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:ShowMessage
@cname:超级浮窗
@tname:超级浮窗 $1 到 $2 毫秒
@format:信息[, 显示时间][, x, y][, 倒计间隔]
@note:与自带的命令相比, 加入了依次显示信息和倒计时功能
@ret:无
@arg:信息, 字符串或表, 浮窗显示的内容, 如果是字符串, 当包含"#时间"时将启用倒计时功能; 如果是表, 则在显示时间内依次遍历显示
@arg:显示时间, 数值型, 可选, 在屏幕上显示总时长, 单位毫秒, 省略默认3000毫秒
@arg:x, 数值型, 可选, 设置显示框在屏幕上的左上角X坐标, 省略默认为屏幕中间
@arg:y, 数值型, 可选, 设置显示框在屏幕上的左上角Y坐标, 省略默认为屏幕中间
@arg:倒计间隔, 数值型, 可选, 填写必须小于0, 表示倒计时步长, 省略默认为-1000毫秒
@exp:zm.ShowMessage("脚本将在 #时间 秒后启用", 5000)        //倒计时5秒
@exp:zm.ShowMessage({"1.先打开app", "2.点击登录", "3.输入帐号密码", "4.点击确定"}, 10*1000)   //遍历循环显示内容, 总耗时10秒
--]]
function zmm.ShowMessage(msg, ...)
	local args = { ... }
	return try {
		function()
			local num = {}
			for _, v in ipairs(args) do
				if type(v) == "number" then
					if v < 0 then
						args.step = v
					else
						num[#num + 1] = v
					end
				end
			end

			if #num == 1 then
				args.time = num[1]
			elseif #num == 2 then
				args.x, args.y = num[1], num[2]
			elseif #num == 3 then
				args.time, args.x, args.y = num[1], num[2], num[3]
			end

			args.time = args.time or 3000
			args.step = args.step or -1000

			if type(msg) == "table" then
				for _, v in ipairs(msg) do
					v = tostring(v)
					LuaAuxLib.ShowMessage(v, args.time / #msg, args.x, args.y)
					LuaAuxLib.Sleep(args.time / #msg)
				end
			else
				msg = tostring(msg)
				if msg:match("#时间") then
					for i = args.time, 0, args.step do
						LuaAuxLib.ShowMessage(msg:gsub("#时间", tostring(i / 1000)), -args.step, args.x, args.y)
						LuaAuxLib.Sleep(-args.step)
					end
				else
					LuaAuxLib.ShowMessage(msg, args.time, args.x, args.y)
				end
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ShowMessage()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:CheckForUpdates
@cname:检查更新
@tname:检查紫猫插件更新
@format:无
@note:检查是否有新版插件可以更新, 每次运行最多调用1次.
@ret:是否有更新, 布尔型, 有更新返回true, 无更新返回false, 失败返回null
@arg:无
@exp:zm.CheckForUpdates()        //检查更新
--]]
function zmm.CheckForUpdates(mode)
	local isok, errors = pcall(
		function()
			local str = ""
			if mode == 1 or mode == 2 then
				if mode == 1 then
					str = " (来自本地数据)"
				else
					str = " (来自网络数据)"
				end
				local json = _zmm.readzmconfig()
				if json then
					_zmm._NEW_VERSION_ = json.newversion or _zmm._NEW_VERSION_
					_zmm._NEW_VER_ = json.newver or _zmm._NEW_VER_
					_zmm._INFO_ = json.info or _zmm._INFO_
				end
			else
				if _zmm.Init() then
					str = " (来自网络数据)"
				else
					str = " (来自本地数据)"
				end
			end
			--traceprint("check",_ZM_._NEW_VER_)
			if tonumber(_zmm._NEW_VER_) ~= 0 then
				if tonumber(_zmm._VER_) >= tonumber(_zmm._NEW_VER_) then
					--                    traceprint("已是最新版本V" .. _ZM_._VERSION_ .. str)
					return false
				else
					traceprint("发现紫猫插件有更新, 欢迎下载新版 V" ..
					_zmm._NEW_VERSION_ .. str .. ". 注: 小精灵不检测插件更新.")
					LuaAuxLib.TracePrint(":", _zmm._URL_DOWNLOAD_)
					--                    traceprint("可以加入QQ群  " .. _ZM_._QQ_GROUP_ .. "  下载最新版插件")
					--                    traceprint("也可以右击复制下面的网址, 在浏览器中粘贴打开网页下载最新版插件")
					return true
				end
			else
				--                traceprint("当前插件版本为：V" .. _ZM_._VERSION_ .. str)
				--                traceprint("由于缓存或网络原因等导致插件检查更新失败, 这不影响脚本执行效果")
				--                traceprint("欢迎加入QQ群  " .. _ZM_._QQ_GROUP_ .. "  查看是否有新版插件")
				--                traceprint("也可以右击复制下面的网址, 在浏览器中粘贴打开网页查看是否有新版插件")
				--                LuaAuxLib.TracePrint(":", _ZM_._URL_DOWNLOAD_)
				return nil
			end
		end
	)
	if not isok then
		--        traceprint("当前版本为：V" .. _ZM_._VERSION_)
		--        traceprint("由于 " .. errors .. " 造成插件检查更新失败, 这不影响脚本执行效果")
		--        traceprint("欢迎加入QQ群  " .. _ZM_._QQ_GROUP_ .. "  查看是否有新版插件")
		--        traceprint("也可以右击复制下面的网址, 在浏览器中粘贴打开网页查看是否有新版插件")
		--        LuaAuxLib.TracePrint(":", _ZM_._URL_DOWNLOAD_)
	end
	if type(_zmm._INFO_) == "table" then
		for _, v in ipairs(_zmm._INFO_) do
			traceprint(v)
		end
	end
	zmm.CheckForUpdates = function() return errors end
	return errors
end

--[[
@fname:GetVersion
@cname:获取插件版本
@tname:获取紫猫插件版本号
@format:无
@note:获取当前紫猫插件版本号
@ret:插件版本, 字符串, 当前插件版本号
@arg:无
@exp:TracePrint zm.GetVersion()        //获取插件版本号
--]]
function zmm.GetVersion()
	return _zmm._VERSION_, _zmm._VER_
end

--[[
@fname:GetLuaVersion
@cname:获取Lua环境版本
@tname:获取当前Lua环境版本
@format:无
@note:获取当前运行的Lua环境版本号
@ret:插件版本, 字符串, 当前Lua版本号
@arg:无
@exp:TracePrint zm.GetLuaVersion()        //获取Lua版本号
--]]
function zmm.GetLuaVersion()
	return _VERSION
end

--[[
@fname:Type
@cname:获取数据类型
@tname:获取 $1 的数据类型
@format:数据
@note:获取参数的数据类型, 有以下几种类型$n number, string, boolean, nil, table, userdata
@ret:数据类型, 字符串, 得到参数的数据类型
@arg:数据, 任意类型, 待获取数据类型的参数
@exp:TracePrint zm.Type("123")        //返回number数据类型
--]]
zmm.Type = type

--[[
@fname:GetScriptMemory
@cname:获取脚本占用内存
@tname:获取脚本占用内存
@format:无
@note:获取当前脚本所占用的内存, 单位kb
@ret:内存容量, 数值型, 返回当前脚本占用的内存大小, 单位kb
@arg:无
@exp:TracePrint zm.GetScriptMemory()        //返回当前内存大小
--]]
function zmm.GetScriptMemory()
	return try {
		function()
			return collectgarbage("count")
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetScriptMemory()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:CollectGarbage
@cname:清理脚本内存
@tname:清理脚本内存
@format:[操作类型[, 附加参数] ]
@note:Lua的垃圾回收机制接口, 原型是collectgarbage()函数, 可通过第一个参数实现不同的操作$n 通常用于释放内存作用, 但是不建议频繁调用, 会降低脚本执行效率, 一般在加载大型资源前调用一次, 例如读取很大的一个文件之类的
@ret:执行结果, 数值型, 由参数1决定返回内容
@arg:操作类型, 字符串, 可选, 省略默认为执行一个完整的垃圾回收周期, 详细请百度搜索collectgarbage
@arg:附加参数, 数值型, 可选, 改变回收步骤周期等, 详细请百度搜索collectgarbage
@exp:TracePrint zm.CollectGarbage()        //执行一遍完整的垃圾回收周期, 释放内存
--]]
function zmm.CollectGarbage(opt, arg)
	return try {
		function()
			return collectgarbage(opt, arg)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.CollectGarbage()，错误信息：", errors)
			end
		}
	}
end

--[[
@fname:About
@cname:关于
@tname:获取 zm.$1() 命令的帮助网址
@format:[函数名]
@note:介绍插件更新记录与获取指定插件函数的帮助网址
@ret:网址, 字符串, 得到指定插件函数的帮助网址
@arg:函数名, 字符串, 可选, 得到该函数的帮助网址
@exp:TracePrint zm.About()        //自动输出最新版更新介绍
@exp:TracePrint zm.About("Type")  //得到zm.Type()的在线帮助网址
--]]
function zmm.About(kwd)
	return try {
		function()
			local url = _zmm._URL_DOCUMENT_
			if kwd then
				url = _zmm._URL_DOCUMENT_ .. "post/zm." .. string.lower(kwd) .. "/"
				traceprint("zm." .. kwd .. "() 的在线教程网址(右击下面复制)：")
				LuaAuxLib.TracePrint(":", url)
			else
				traceprint("这是由紫猫编程学院倾力打造的安卓全能插件，目前正在不断更新添加功能中。")
				traceprint("如果大家有什么好的建议或意见，欢迎联系QQ: 345911220，交流QQ群: 7333555")
				traceprint("插件的在线教程网址(右击下面复制)")
				LuaAuxLib.TracePrint(":", _zmm._URL_DOCUMENT_)
			end
			return url
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.About()，错误信息：", errors)
			end
		}
	}
end

function _zmm._tabledump(t)
	local str = "{"
	local kk = function(k)
		if type(k) == "string" then
			return "\034" .. k .. "\034"
		else
			return tostring(k)
		end
	end

	for k, v in pairs(t) do
		if type(v) == "table" and k ~= "__index" then
			str = str .. kk(k) .. ":" .. _zmm._tabledump(v)
		elseif type(v) == "string" then
			str = str .. kk(k) .. ":" .. "\034" .. v .. "\034" .. ","
		else
			str = str .. kk(k) .. ":" .. tostring(v) .. ","
		end
	end
	str = #str > 1 and str:sub(1, -2) .. "}," or str .. "},"
	return str
end

function _zmm.tabledump(t)
	return _zmm._tabledump(t):sub(1, -2)
end

--[[
@fname:VarInfo
@cname:获取参数信息
@tname:获取 $1 等参数的详细信息
@format:[参数1][,参数2...]
@note:获取参数的长度, 数据类型与值, 可以直接获取表的值
@ret:详细信息, 字符串, 得到所有参数的详细信息内容
@arg:参数, 任意类型, 可选, 不限参数数量, 表示要获取信息的参数
@exp:TracePrint zm.VarInfo("Hello")        //输出数据类型, 长度, 字符串内容
@exp:TracePrint zm.VarInfo({1,2,3,"紫猫":"学院"})  //输出table的长度与详细内容
--]]
function zmm.VarInfo(...)
	-- 防止无法获取nil参数
	local paramCount = select("#", ...)
	local varType, printStr, t = "", "", {}
	for i = 1, paramCount do
		local v = select(i, ...)
		try {
			function()
				local jq = function(v)
					if #v > 65536 then
						v = v:sub(1, 65536) .. "...(紫猫插件:由于内容长度超过65536,请使用其他方式输出查看.)"
					end
					return v
				end

				varType = type(v)
				if varType == "table" then
					try {
						function()
							printStr = "【" ..
							varType .. " " .. tostring(#v) .. "】" .. jq(LuaAuxLib.Encode_GetJsonLib():encode(v))
						end,
						catch {
							try {
								function()
									printStr = "【" .. varType .. " " .. tostring(#v) .. "】" .. jq(_zmm.tabledump(v))
								end
							}
						}
					}
				elseif varType == "number" or varType == "string" then
					printStr = "【" .. varType .. " " .. tostring(#tostring(v)) .. "】" .. jq(tostring(v))
				elseif varType == "boolean" or varType == "null" then
					printStr = "【" .. varType .. "】" .. tostring(v)
				else
					printStr = "【" .. varType .. "】 unknown"
				end
				table.insert(t, #t + 1, printStr)
			end,
			catch {
				function(errors)
					traceprint("发生运行时错误！错误代码：zm.VarInfo()，错误信息：", errors)
				end
			}
		}
	end
	printStr = table.concat(t, ", ")
	return printStr
end

--[=[
@fname:PosExclude
@cname:排除指定范围坐标
@tname:在 $1 坐标中排除 $2 $3 $4 $5 范围坐标
@format:原坐标数组, x1, y1[, 半径或x2[, y2]]
@note:在二维坐标数组中排除指定范围的坐标, 支持圆形范围和矩形范围
@ret:坐标数组, table, 得到排除后剩下的坐标
@arg:原坐标数组, table, 待筛选的坐标数组, 格式为{{x,y},{x,y},{xy},...}, 兼容超级图色里的返回找到所有坐标格式
@arg:x1, 数值型, 表示左上角x坐标或圆心x坐标
@arg:y1, 数值型, 表示左上角y坐标或圆心y坐标
@arg:半径或x2, 数值型, $n 当不填写参数y2时, 表示圆心范围的半径, $n 当填写参数y2时表示矩形右下角x坐标
@arg:y2, 数值型, 可选, 矩形右下角y坐标
@exp:Dim xy = zm.PosExclude({{10,20},{30,40}}, 15, 15, 10)    //以圆形15,15, 半径10排除坐标, 最终得到{{30,40}}
@exp:Dim xy = zm.PosExclude({{10,20},{30,40}}, 5, 5, 25, 25)  //以范围5,5,25,25排除坐标, 最终得到{{30,40}}
--]=]
function zmm.PosExclude(pos, ...)
	local args = { ... }
	return try {
		function()
			local t = {}
			if #args == 3 then
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						if (v[#v - 1] - args[1]) ^ 2 + (v[#v] - args[2]) ^ 2 > args[3] ^ 2 then
							t[#t + 1] = v
						end
					end
				end
			elseif #args == 4 then
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						if not (args[1] <= v[#v - 1] and v[#v - 1] <= args[3] and args[2] <= v[#v] and v[#v] <= args[4]) then
							t[#t + 1] = v
						end
					end
				end
			else
				error("参数数量有误!")
			end
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.PosExclude()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:PosInclude
@cname:获取包含范围坐标
@tname:在 $1 坐标中获取包含 $2 $3 $4 $5 范围坐标
@format:原坐标数组, x1, y1[, 半径或x2[, y2]]
@note:在二维坐标数组中获取包含范围的坐标, 支持圆形范围和矩形范围
@ret:坐标数组, table, 得到包含指定范围的坐标
@arg:原坐标数组, table, 待筛选的坐标数组, 格式为{{x,y},{x,y},{xy},...}, 兼容超级图色里的返回找到所有坐标格式
@arg:x1, 数值型, 表示左上角x坐标或圆心x坐标
@arg:y1, 数值型, 表示左上角y坐标或圆心y坐标
@arg:半径或x2, 数值型, $n 当不填写参数y2时, 表示圆心范围的半径, $n 当填写参数y2时表示矩形右下角x坐标
@arg:y2, 数值型, 可选, 矩形右下角y坐标
@exp:Dim xy = zm.PosInclude({{10,20},{30,40}}, 15, 15, 10)    //以圆形15,15, 半径10获取坐标, 最终得到{{10,20}}
@exp:Dim xy = zm.PosInclude({{10,20},{30,40}}, 5, 5, 25, 25)  //以范围5,5,25,25获取坐标, 最终得到{{10,20}}
--]=]
function zmm.PosInclude(pos, ...)
	local args = { ... }
	return try {
		function()
			local t = {}
			if #args == 3 then
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						if (v[#v - 1] - args[1]) ^ 2 + (v[#v] - args[2]) ^ 2 <= args[3] ^ 2 then
							t[#t + 1] = v
						end
					end
				end
			elseif #args == 4 then
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						if args[1] <= v[#v - 1] and v[#v - 1] <= args[3] and args[2] <= v[#v] and v[#v] <= args[4] then
							t[#t + 1] = v
						end
					end
				end
			else
				error("参数数量有误!")
			end
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.PosInclude()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:PosSort
@cname:坐标排序
@tname:对 $1 坐标按远近排序
@format:坐标数组[, x, y][, 是否降序]
@note:对二维坐标数组按远近进行排序, 支持升序, 降序
@ret:坐标数组, table, 得到排序后的坐标数组
@arg:坐标数组, table, 等待排序的坐标数组, 格式为{{x,y},{x,y},{xy},...}, 传址调用, 兼容超级图色里的返回找到所有坐标格式
@arg:x, number, 可选, 表示用于比较远近的x坐标, 省略默认为0
@arg:y, number, 可选, 表示用于比较远近的y坐标, 省略默认为0
@arg:是否降序, boolean, 可选, 填写true表示从远到近, 填写false表示从近到远, 省略为false
@exp:Dim xy = zm.PosSort({{30,25},{10,20}})    //按照与坐标0,0的远近排序, 最终得到{{10,20},{30,25}}
@exp:Dim xy = zm.PosSort({{30,25},{10,20}}, true)    //按照与坐标0,0的远近降序, 最终得到{{30,25},{10,20}}
@exp:Dim xy = zm.PosSort({{10,20},{30,40}}, 15, 15)  //按照与坐标15,15的远近排序, 最终得到{{10,20},{30,40}}
--]=]
function zmm.PosSort(pos, ...)
	local args = { ... }
	return try {
		function()
			local isDesc, xy = false, {}
			for _, v in pairs(args) do
				if type(v) == "boolean" then
					isDesc = v
				elseif type(v) == "number" then
					xy[#xy + 1] = v
				end
			end
			if #xy == 0 then xy = { 0, 0 } end

			local comp = function(a, b)
				if isDesc then
					return (b[#b - 1] - xy[1]) ^ 2 + (b[#b] - xy[2]) ^ 2 > (a[#a - 1] - xy[1]) ^ 2 + (a[#a] - xy[2]) ^ 2
				else
					return (b[#b - 1] - xy[1]) ^ 2 + (b[#b] - xy[2]) ^ 2 < (a[#a - 1] - xy[1]) ^ 2 + (a[#a] - xy[2]) ^ 2
				end
			end

			table.sort(pos, comp)
			return pos
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.PosSort()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Calculator
@cname:计算器
@tname:得到 $1 算式结果
@format:算式
@note:得到数学算式的计算结果
@ret:计算结果, number, 得到算式的计算结果
@arg:算式, string, 等待计算的数学算式, 支持加减乘除括号等运算
@exp:Dim ret = zm.Calculator("1+2*3")    //结果是7
--]=]
function zmm.Calculator(equation)
	return try {
		function()
			return load("return " .. equation)()
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Calculator()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------日期时间函数---------------------------------------

--[=[
@fname:Timestamp
@cname:转时间戳
@tname:将 $1 转为时间戳数值
@format:日期时间
@note:将时间格式的字符串转换成时间戳, 支持2017年7月23日 23:29:31, 2017-07-23 23:29:38等格式
@ret:时间戳, number, 成功返回时间戳数值, 失败返回null
@arg:日期时间, string, 可选, 待转换的日期时间, 支持2017年7月23日 23:29:31, 2017-07-23 23:29:38等格式, 省略默认为当前本地时间
@exp:Dim t = zm.Timestamp("2018年7月16日 14:03:26")
--]=]
function zmm.Timestamp(s)
	return try {
		function()
			local t = {}
			if s then
				local i, tt = 0, { "year", "month", "day", "hour", "min", "sec" }
				for k in s:gmatch("%d+") do
					i = i + 1
					t[tt[i]] = k
				end
			else
				t = nil
			end

			return os.date(os.time(t))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Timestamp()，错误信息：", errors)
			end
		},
	}
end

--[=[
@fname:TimeDiff
@cname:计算时间差
@tname:计算 $1 - $2 的时间差
@format:时间1[, 时间2][, 单位][, 是否取整]
@note:计算时间1减去时间2的时间差, 返回单位以第3个参数为准
@ret:时间差, number, 成功返回两者时间差, 单位由参数3决定
@arg:时间1, number或string, 第1个时间, 字符串时支持"2017年7月23日 23:29:31", "2017-07-23 23:29:38"等格式, 数值时表示时间戳
@arg:时间2, number或string, 可选, 第2个时间, 字符串时支持"2017年7月23日 23:29:31", "2017-07-23 23:29:38"等格式, 数值时表示时间戳, 省略默认为当前时间
@arg:单位, string, 可选, 表示计算返回的时间单位, 可填写["年", "月", "日", "天", "时", "分", "秒"]或["Y", "m", "d", "H", "M", "S"], 省略默认为"秒"
@arg:是否取整, boolean, 可选, 表示返回值是否只保留整数, 省略默认为false
@exp://计算参数1减去参数2, 返回单位秒数$n TracePrint zm.TimeDiff("2019年8月3日 14:03:26", "2018-08-03 22:06:04")$n //计算2018-12-01 00:00:00减去当前时间, 返回单位天数, 并取整$n TracePrint zm.TimeDiff("2018-12-01", "日", true)
--]=]
function zmm.TimeDiff(t1, t2, mode, isint)
	return try {
		function()
			if type(t1) == "string" then
				t1 = zmm.Timestamp(t1)
			end

			local args = { t2, mode, isint, t2 = zmm.Timestamp(), mode = "秒" }
			for _, v in ipairs(args) do
				if type(v) == "boolean" then
					args.isint = v
				elseif type(v) == "number" then
					args.t2 = v
				elseif type(v) == "string" then
					if v:len() == 1 or v:len() == 3 then
						args.mode = v
					else
						args.t2 = zmm.Timestamp(v)
					end
				end
			end

			local s = t1 - args.t2

			if args.mode == "M" or args.mode == "分" then
				s = s / 60
			elseif args.mode == "H" or args.mode == "时" then
				s = s / 60 / 60
			elseif args.mode == "d" or args.mode == "日" or args.mode == "天" then
				s = s / 60 / 60 / 24
			elseif args.mode == "m" or args.mode == "月" then
				s = s / 60 / 60 / 24 / (365 / 12)
			elseif args.mode == "Y" or args.mode == "年" then
				s = s / 60 / 60 / 24 / 365
			end

			if args.isint then
				s = math.modf(s)
			end

			return s
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TimeDiff()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TimeAdd
@cname:时间计算
@tname:计算时间 $1 + $2 , 单位 $3
@format:[时间, ]数值[, 单位]
@note:计算时间加减后的数值, 数值单位由参数3决定
@ret:计算结果, number, 返回时间加减计算后的时间戳结果
@arg:时间, number或string, 可选, 待计算的时间, 字符串时支持"2017年7月23日 23:29:31", "2017-07-23 23:29:38"等格式, 数值时表示时间戳, 省略默认为当前时间
@arg:数值, number, 表示要加减的具体数值, 支持正负数
@arg:单位, string, 可选, 表示数值的计算单位, 可填写["年", "月", "日", "天", "时", "分", "秒"]或["Y", "m", "d", "H", "M", "S"], 省略默认为"秒"
@exp://计算15天后的时间日期$n TracePrint zm.TimeAdd("2019年8月3日 14:03:26", 15, "日")$n //计算当前时间往后20个月的时间$n TracePrint zm.TimeAdd(20, "月")
--]=]
function zmm.TimeAdd(t, num, mode)
	return try {
		function()
			local args = { t, num, mode }
			for _, v in ipairs(args) do
				if type(v) == "number" then
					if args.num == nil then
						args.num = v
					else
						args.t, args.num = args.num, v
					end
				elseif type(v) == "string" then
					if v:len() == 1 or v:len() == 3 then
						args.mode = v
					else
						args.t = zmm.Timestamp(v)
					end
				end
			end

			args.t = args.t or zmm.Timestamp()
			args.date = os.date("*t", args.t)
			args.mode = args.mode or "秒"

			if args.mode == "S" or args.mode == "秒" then
				args.date.sec = args.date.sec + args.num
			elseif args.mode == "M" or args.mode == "分" then
				--                args.num = args.num * 60
				args.date.min = args.date.min + args.num
			elseif args.mode == "H" or args.mode == "时" then
				--                args.num = args.num * 60 * 60
				args.date.hour = args.date.hour + args.num
			elseif args.mode == "d" or args.mode == "日" or args.mode == "天" then
				--                args.num = args.num * 60 * 60 * 24
				args.date.day = args.date.day + args.num
			elseif args.mode == "m" or args.mode == "月" then
				--                args.num = args.num * 60 * 60 * 24 * 30
				args.date.month = args.date.month + args.num
			elseif args.mode == "Y" or args.mode == "年" then
				--                args.num = args.num * 60 * 60 * 24 * 365
				args.date.year = args.date.year + args.num
			end

			--            local s = args.t + args.num
			return os.time(args.date)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TimeAdd()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:GetNetworkTime
@cname:获取精准网络时间
@tname:以格式 $1 获取精准网络时间
@format:[格式[, 接口]]
@note:通过多个接口获取毫秒级网络时间, 内置淘宝/苏宁/香港/按键精灵/腾讯等多个网络时间接口
@ret:网络时间, number或string, 成功返回时间戳或格式化时间字符串, 失败返回null
@arg:格式, number或string, 可选, 表示返回的格式, 当数值型时, 0表示返回时间戳数值, 1表示返回"年-月-日 时:分:秒", 2表示返回"年月日时分秒",$n 当字符串时表示自定义格式化, 与DateTime.Format()的第一个参数格式相同, 省略默认为0
@arg:接口, number, 可选, 取值-1到5, 省略默认为0, 其中0时表示随机接口, 1为淘宝接口, 2为苏宁接口, 3为香港接口, 4为按键自身接口, 5为腾讯接口, -1表示遍历所有接口, 成功立即返回
@exp:Dim t = zm.GetNetworkTime()
--]=]
function zmm.GetNetworkTime(mode, id)
	return try {
		function()
			mode = mode or 0
			local api = {
				taobao = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp",
				suning = "http://quan.suning.com/getSysTime.do",
				hko = "http://www.hko.gov.hk/cgi-bin/gts/time5a.pr?t=" .. math.random(),
				qqgame = "https://apps.game.qq.com/CommArticle/app/reg/gdate.php"
			}
			local time
			local gettime = {}
			gettime[#gettime + 1] = function()
				local html = LuaAuxLib.URL_OperationGet(api.taobao, 3)
				if html and html:match("接口调用成功") then
					local t = LuaAuxLib.Encode_GetJsonLib():decode(html)
					time = tonumber(t.data.t) / 1000
					return time
				end
			end

			gettime[#gettime + 1] = function()
				local html = LuaAuxLib.URL_OperationGet(api.suning, 2)
				if html and html:match("sysTime1") then
					local t = LuaAuxLib.Encode_GetJsonLib():decode(html)
					time = zmm.Timestamp(t.sysTime2)
					return time
				end
			end

			gettime[#gettime + 1] = function()
				local html = LuaAuxLib.URL_OperationGet(api.hko, 2)
				if html and html:match("0=") then
					time = tonumber(html:sub(3, -1)) / 1000
					return time
				end
			end

			gettime[#gettime + 1] = function()
				local html = LuaAuxLib.DEVICE_GetDeviceInfo(4)
				if type(html) == "string" and html:match("%d+") then
					time = zmm.Timestamp(html)
					return time
				end
			end

			gettime[#gettime + 1] = function()
				local html = LuaAuxLib.URL_OperationGet(api.qqgame, 2)
				if html then
					local t = html:match("%d%d%d%d%-%d%d%-%d%d %d%d:%d%d:%d%d")
					time = zmm.Timestamp(t)
					return time
				end
			end

			id = id or 0
			if id == 0 then
				id = zmm.RndNum(1, #gettime)
			end

			if id > 0 and id <= #gettime then
				time = gettime[id]()
			else
				for _, v in pairs(gettime) do
					time = v()
					if time then
						break
					end
				end
			end

			if time then
				if mode == 1 then
					time = os.date("%Y-%m-%d %H:%M:%S", time)
				elseif mode == 2 then
					time = os.date("%Y%m%d%H%M%S", time)
				elseif type(mode) == "string" then
					time = os.date(mode, time)
				end
			end

			return time
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetNetworkTime()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[******************************  字符串  **********************************]]
--[[**************************************************************************]]
--[=[
@fname:LTrim
@cname:删除前导字符
@tname:删除 $1 左边的 $2 连续字符
@format:原字符串[, 删除字符]
@note:删除左边的指定连续字符集合
@ret:新字符串, 字符串, 成功返回删除字符后的内容, 失败返回null
@arg:原字符串, 字符串, 待删除字符的原字符串
@arg:删除字符, 字符串, 可选, 要删除的连续字符内容, 省略默认为" \r\n\t"
@exp:TracePrint zm.LTrim(" 紫猫编程学院  ") //左边的空格被删除了, 右边保留
@exp:TracePrint zm.LTrim("bdacee紫猫编程学院acbed", "abcdefg") //左边连续的abcdefg这几个字符被删除了
--]=]
function zmm.LTrim(str, filt)
	return try {
		function()
			filt = filt or "\r\n\t "
			local retstr = str
			for i = 1, string.len(str) do
				if string.find(filt, string.sub(str, i, i), 1, true) == nil then
					retstr = string.sub(str, i, -1)
					break
				end
			end
			return retstr
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.LTrim()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RTrim
@cname:删除后导字符
@tname:删除 $1 右边的 $2 连续字符
@format:原字符串[, 删除字符]
@note:删除右边的指定连续字符集合
@ret:新字符串, 字符串, 成功返回删除字符后的内容, 失败返回null
@arg:原字符串, 字符串, 待删除字符的原字符串
@arg:删除字符, 字符串, 可选, 要删除的连续字符内容, 省略默认为" \r\n\t"
@exp:TracePrint zm.RTrim(" 紫猫编程学院  ") //右边的空格被删除了, 左边保留
@exp:TracePrint zm.RTrim("bdacee紫猫编程学院acbed", "abcdefg") //右边连续的abcdefg这几个字符被删除了
--]=]
function zmm.RTrim(str, filt)
	return try {
		function()
			filt = filt or "\r\n\t "
			local retstr = str
			for i = string.len(str), 1, -1 do
				if string.find(filt, string.sub(str, i, i), 1, true) == nil then
					retstr = string.sub(str, 1, i)
					break
				end
			end
			return retstr
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RTrim()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Trim
@cname:删除前后导字符
@tname:删除 $1 左右两边的 $2 连续字符
@format:原字符串[, 删除字符]
@note:删除左右两边的指定连续字符集合
@ret:新字符串, 字符串, 成功返回删除字符后的内容, 失败返回null
@arg:原字符串, 字符串, 待删除字符的原字符串
@arg:删除字符, 字符串, 可选, 要删除的连续字符内容, 省略默认为" \r\n\t"
@exp:TracePrint zm.Trim(" 紫猫编程学院  ") //左右两边空格都被删除
@exp:TracePrint zm.Trim("bdacee紫猫编程学院acbed", "abcdefg") //左右两边连续的abcdefg都被删除了
--]=]
function zmm.Trim(str, filt)
	return try {
		function()
			local tmpstr = zmm.LTrim(str, filt)
			return zmm.RTrim(tmpstr, filt)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Trim()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Execute
@cname:执行execute
@tname:以execute方式执行 $1
@format:命令行
@note:执行execute命令行并返回结果
@ret:执行结果, 字符串, 成功返回执行后的内容, 失败返回null
@arg:命令行, 字符串, 要执行的命令行代码
@exp:TracePrint zm.Execute("ls")  //获取当前目录下所有文件名
--]=]
function zmm.Execute(cmd)
	local f, tFile
	return try {
		function()
			tFile = _zmm._GLOBAL_PATH_ .. os.tmpname():match("[^/]+$")
			if tFile then
				cmd = cmd .. " > " .. tFile
				os.execute(cmd)
				local result = _zmm.fileread(tFile, true)
				return result and zmm.Trim(result) or ""
			end
			return nil
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Execute()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then
					f:close()
					os.remove(tFile)
				end
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 数组与表 **********************************]]
--[[**************************************************************************]]
-- 改变table的默认nil值
--local function setTableDefalut(t, d)
--    local mt = {__index=function () return d end}
--    setmetatable(t, mt)
--end

--[=[
@fname:TableIsSame
@cname:表成员是否相同
@format:table表1, table表2
@tname:表成员是否相同
@note:判断两个table的成员是否相同, 例如t1={1,2}, t2={1,2}即相同, 而t1={1,2}, t2={1,2,3}为不相同
@ret:比较结果, boolean, 相同返回真, 不同返回假, 出错返回null
@arg:table表1, table, 要判断的table表
@arg:table表2, table, 要判断的table表
@exp:Dim ret = zm.TableIsSame({1, 2}, {1, 2})
--]=]
function zmm.TableIsSame(t1, t2)
	return try {
		function()
			local cjson = require("cjson")
			return cjson.encode(t1) == cjson.encode(t2)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TableIsSame()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TableIsEmpty
@cname:table是否为空
@format:table表
@tname:$1 是否为空表
@note:判断一个table表数据是否为{}, 因为键值对无法用Ubound来判断, 也不能直接与{}比较, 而用for each循环判断一遍也太麻烦, 所以才有本函数
@ret:是否为空表, boolean, 是空表{}返回true, 不是空表{}返回false
@arg:table表, table, 要判断的table表
@exp:Dim ret = zm.TableIsEmpty({12})
--]=]
function zmm.TableIsEmpty(t)
	return try {
		function()
			return next(t) == nil
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TableIsEmpty()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TableClear
@cname:清空表
@tname:清空表 $1
@format:表
@note:将传入的参数表全部清空为null
@ret:表, table, 成功返回长度为-1的表, 失败返回null
@arg:表, table, 待清空的键值对表
@exp:Dim t = zm.TableClear({1,2,3})  //得到长度-1的表t
--]=]
function zmm.TableClear(t)
	return try {
		function()
			for k in next, t do
				rawset(t, k, nil)
			end
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TableClear()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TableUnpack
@cname:数组解包
@tname:数组解包 $1
@format:[数组, ...][, 起始[, 终止]]
@note:返回传入参数数组的所有数组元素, 原型是Lua中的table.unpack()$n 目前手机版变量赋值只能接收一个元素, 但可选参数的函数命令可以接收多个元素, 详见示例
@ret:多个元素值, 任意类型, 返回表中的多个元素, 变量赋值只能接收第一个元素
@arg:数组, table, 待处理的数组, 支持传入多个数组
@arg:起始, number, 可选, 截取表的起始位置, 从1开始计算, 省略默认为1
@arg:终止, number, 可选, 截取表的终止位置, 从1开始计算, -1表示最后一个元素位置, 省略默认为-1
@exp:Dim t = {"a":"hello", 345911220, true, "紫猫"}$n //可以打印输出所有返回值, 因为TracePrint支持多个可选参数$n //没有显示键名a的元素, 因为只支持数组内容部分$n TracePrint zm.TableUnpack(t)$n //可以利用{}语法合并两个数组元素到新的数组中$n Dim t1 = {1, 2, 3}$n Dim newT = {zm.TableUnpack(t, t1)}$n TracePrint zm.VarInfo(newT)$n //可以解决超级多点找色或多点比色参数过长问题$n Dim 颜色1 = {"0DD200","4|2|FFFFFF,3|7|211003,27|1|1592FF"}$n Dim 颜色2 = {"123456","40|27|FFFFFF,31|5|211003"}$n //下面这两句代码实现效果完全相同$n zm.FindMultiColor zm.TableUnpack(颜色1, 颜色2), "显示"$n zm.FindMultiColor "0DD200","4|2|FFFFFF,3|7|211003,27|1|1592FF","123456","40|27|FFFFFF,31|5|211003","显示"
--]=]
function zmm.TableUnpack(...)
	local args = { ... }
	return try {
		function()
			local t, i, j = {}
			for _, v in ipairs(args) do
				if type(v) == "number" then
					if i == nil then
						i = v
					else
						j = v
					end
				end
			end

			i = i or 1
			j = j or -1

			for _, v in pairs(args) do
				if type(v) == "table" then
					j = (j ~= -1 and j <= #v and j > 0) and j or #v
					for id = i, j do
						t[#t + 1] = v[id]
					end
				end
			end

			return table.unpack(t)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TableUnpack()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Clone
@cname:超级克隆
@tname:超级克隆 $1
@format:表
@note:拷贝table或数组,且不影响旧表,支持嵌套表或嵌套数组的拷贝
@ret:表, table, 成功返回克隆后的新表, 失败返回null
@arg:表, table, 待克隆的键值对表
@exp:Dim oldT = {1,2,3}$n Dim newT = zm.Clone(oldT)  //得到成员值一样, 但相互独立的表newT
--]=]
function zmm.Clone(orig)
	return try {
		function()
			local orig_type = type(orig)
			local copy
			if orig_type == "table" then
				copy = {}
				for orig_key, orig_value in next, orig, nil do
					copy[zmm.Clone(orig_key)] = zmm.Clone(orig_value)
				end
				setmetatable(copy, zmm.Clone(getmetatable(orig)))
			else -- number, string, boolean, etc
				copy = orig
			end
			return copy
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Clone()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ClearBOM
@cname:清除BOM字符串
@tname:清除 $1 中的BOM字符串
@format:文本
@note:清除读取文本文件中包含的BOM字符串, 即文本开头的问号内容.
@ret:文本内容, string, 返回清除BOM后的文本内容
@arg:文本, string, 待清除的文本内容
@exp:Dim text = File.Read("/sdcard/文本文件.txt")//假设读取开头有问号内容$n text = zm.ClearBOM(text)$n TracePrint text
--]=]
zmm.ClearBOM = _zmm.clearbom

--[=[
@fname:Split
@cname:超级分割
@tname:对 $1 按 $2 进行超级分割
@format:原字符串, 分隔符[, 模式匹配[, 转换类型]]
@note:按指定分隔符或长度对原内容进行分割, 支持模式匹配, 支持智能转换成员为数值型
@ret:一维数组, table, 成功返回分割后的一维数组, 失败返回null
@arg:原字符串, string, 待分割的原字符串内容
@arg:分隔符, string或number, 分割规则$n 当是字符串时, 表示按此分隔符进行分割$n 当是数值时, 表示按此长度进行分割
@arg:模式匹配, boolean, 可选, 是否开启模式匹配, 省略默认为false
@arg:转换类型, boolean, 可选, 是否智能转换数据类型, 省略默认为false
@exp:Dim t = zm.Split("1,2,3,4,5", ",") //按照逗号分割字符串成一维数组
@exp:Dim t = zm.Split("紫猫编程学院", 1) //按照长度1进行分割
@exp:Dim t = zm.Split("这234句话里45645646有605数字", "%d+", true) //开启模式匹配, 按照匹配文字分割
@exp:Dim t = zm.Split("1,true,a,3,b", ",", false, true) //开启智能转换数据类型, 分割后, 1,3成员是数值型, a,b成员是字符串型, true是布尔型
--]=]
function zmm.Split(str, delim, plain, changetype)
	return try {
		function()
			str = type(str) == "number" and tostring(str) or str
			assert(type(str) == "string", "第1个参数不是字符串型")
			if type(delim) == "string" then
				local pos, result, s, e, l = 1, {}, 0, 0, str:len()
				plain = not (plain)
				if str:find(delim, pos, plain) == nil or delim == "" then
					if changetype then
						if tonumber(str) then
							str = tonumber(str)
						elseif str:lower() == "true" then
							str = true
						elseif str:lower() == "false" then
							str = false
						end
					end
					return { str }
				end

				while true do
					s, e = str:find(delim, pos, plain)
					if s then
						local ss = str:sub(pos, s - 1)
						if changetype then
							if tonumber(ss) then
								ss = tonumber(ss)
							elseif ss:lower() == "true" then
								ss = true
							elseif ss:lower() == "false" then
								ss = false
							end
						end
						table.insert(result, ss)
						pos = e + 1
					else
						local ss = str:sub(pos, l)
						if changetype then
							if tonumber(ss) then
								s = tonumber(ss)
							elseif str:lower() == "true" then
								ss = true
							elseif str:lower() == "false" then
								ss = false
							end
						end
						table.insert(result, ss)
						break
					end
				end
				return result
			else
				local t, l = {}
				if type(delim) == "number" and delim > 0 then
					l = delim
				else
					l = 1
				end
				for i = 1, LuaAuxLib.UTF8_Length(str), l do
					local ss = LuaAuxLib.UTF8_Mid(str, i, l)
					if changetype then
						if tonumber(ss) then
							ss = tonumber(ss)
						elseif ss:lower() == "true" then
							ss = true
						elseif ss:lower() == "false" then
							ss = false
						end
					end
					table.insert(t, ss)
				end
				return t
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Split()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArrayInsert
@cname:插入数组成员
@format:原数组, 插入值[, 下标][, 传值调用]
@tname:在数组 $1 的 $3 位置插入值 $2
@note:插入元素到一维数组中
@ret:一维数组, table, 成功返回插入新值的一维数组, 失败返回null
@arg:原数组, table, 待插入内容的原数组
@arg:插入值, 任意类型, 要插入的数据
@arg:下标, number, 可选, 要插入的下标位置, 省略默认为追加到原数组末尾
@arg:传值调用, boolean, 可选, 是否要传值调用数组, 为true时不修改原数组, false修改原数组, 省略默认为false
@exp:Dim t = zm.ArrayInsert({1,2,3}, "紫猫") //在数组末尾追加字符串"紫猫"
--]=]
function zmm.ArrayInsert(t, value, pos, byVal)
	return try {
		function()
			local p, bv
			if type(pos) == "boolean" then
				p, bv = #t, pos
			else
				p, bv = pos or #t, byVal
			end
			if bv then
				local tt = zmm.Clone(t)
				table.insert(tt, p + 1, value)
				return tt
			else
				table.insert(t, p + 1, value)
				return t
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArrayInsert()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArrayRemove
@cname:删除数组成员
@format:原数组, 下标[, 传值调用]
@tname:删除数组 $1 的下标 $2 成员
@note:删除一维数组中的指定下标成员
@ret:一维数组, table, 成功返回删除成员后的一维数组, 失败返回null
@arg:原数组, table, 待删除成员的原数组
@arg:下标, number, 要删除的下标位置
@arg:传值调用, boolean, 可选, 是否要传值调用数组, 省略默认为false
@exp:Dim t = zm.ArrayRemove({1,2,3}, 1) //删除下标为1的成员
--]=]
function zmm.ArrayRemove(list, pos, byVal)
	return try {
		function()
			if byVal then
				local t = zmm.Clone(list)
				table.remove(t, pos + 1)
				return t
			else
				table.remove(list, pos + 1)
				return list
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArrayRemove()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArraySort
@cname:数组排序
@format:原数组[, 是否降序[, 是否转数值[, 传值调用]]]
@tname:对数组 $1 进行排序
@note:对一维数组进行排序
@ret:一维数组, table, 成功返回排序后的一维数组, 失败返回null
@arg:原数组, table, 待排序的原数组
@arg:是否降序, boolean, 可选, 是否要降序排序, 省略默认false
@arg:是否转数值, boolean, 可选, 是否要把成员转换成数值型, 省略默认false
@arg:传值调用, boolean, 可选, 是否要传值调用数组, 省略默认为false
@exp:Dim t = zm.ArraySort({12,2,63,55}) //以升序形式排序数组元素
@exp:Dim t = zm.ArraySort({12,2,63,55}, false) //以降序形式排序数组元素
@exp:Dim t = zm.ArraySort({"12","2","63","55"}, true, true) //将成员转为数值型并以升序形式排序数组
--]=]
function zmm.ArraySort(list, isDesc, istonumber, byVal)
	return try {
		function()
			local comp, t
			if byVal then
				t = zmm.Clone(list)
			else
				t = list
			end
			if istonumber then
				for i = 1, #list do
					t[i] = tonumber(t[i])
				end
			end
			if isDesc then
				comp = function(a, b) return b < a end
			end
			table.sort(t, comp)
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArraySort()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArraySearch
@cname:搜索数组
@format:原数组, 查找内容[, 模糊匹配[, 忽略大小写[, 查找模式]]]]
@tname:查找数组 $1 中的 $2 位置
@note:在一维数组中搜索元素位置
@ret:下标位置, number或table, 成功返回查找的结果, 数据类型由参数查找模式决定, 失败返回null
@arg:原数组, table, 被查找的原数组
@arg:查找内容, 任意类型, 要查找的内容, 注意数据类型
@arg:模糊匹配, boolean, 可选, 是否要模糊搜索, 省略为false
@arg:忽略大小写, boolean, 可选, 是否要忽略大小写, 省略默认false
@arg:查找模式, number, 可选, 决定返回值类型, 省略默认为null$n null表示返回第一个找到的下标, 没找到返回-1$n 0为返回所有下标组成的数组, 没找返回长度-1的数组$n 其他数值为返回该数量的下标组成的数组, 没找返回长度-1的数组
@exp:Dim n = zm.ArraySearch({"紫猫","编程","学院"}, "编程") //找到返回下标1
--]=]
function zmm.ArraySearch(list, value, mode, comp, nums)
	return try {
		function()
			local t = {}
			local s1, s2, compare
			for i = 1, #list do
				s1, s2 = list[i], value
				if comp then
					s1, s2 = string.upper(s1), string.upper(s2)
				end
				if mode then
					compare = string.find(s1, s2, 1, true)
				else
					compare = s1 == s2
				end
				if compare then
					if nums then
						if #t <= nums or nums == 0 then
							table.insert(t, i - 1)
						end
					else
						return i - 1
					end
				end
			end
			if nums then
				return t
			else
				return -1
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArraySearch()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArrayRemoveDuplicate
@cname:数组去重复
@format:原数组[, 忽略大小写[, 传值调用]]
@tname:对数组 $1 进行去重复
@note:删除一维数组中重复的成员
@ret:一维数组, table, 成功返回去重复后的数组, 失败返回null
@arg:原数组, table, 待去重复的一维数组
@arg:忽略大小写, boolean, 可选, 是否要忽略大小写, 省略默认false
@arg:传值调用, boolean, 可选, 是否要传值调用数组, 省略默认为false
@exp:Dim t = zm.ArrayRemoveDuplicate({"紫猫","编程","紫猫","学院"}) //返回{"紫猫","编程","学院"}
--]=]
function zmm.ArrayRemoveDuplicate(list, comp, byVal)
	return try {
		function()
			local t = {}
			for _, v in pairs(list) do
				local add = true
				for _, vv in pairs(t) do
					if v == vv then
						add = false
						break
					elseif comp then
						local vc = type(v) == "string" and v:lower() or v
						local vvc = type(vv) == "string" and vv:lower() or vv
						if vc == vvc then
							add = false
							break
						end
					end
				end
				if add then
					table.insert(t, v)
				end
			end

			if not (byVal) then
				zmm.TableClear(list)
				for _, v in pairs(t) do
					table.insert(list, v)
				end
			end
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArrayRemoveDuplicate()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ArrayReplace
@cname:替换数组元素
@format:原数组, 查找内容, 替换内容[, 模糊匹配[, 忽略大小写[, 替换数量]]]
@tname:把数组 $1 中的 $2 替换成 $3
@note:搜索并替换一维数组中的指定元素内容
@ret:一维数组, table, 成功返回替换后的数组内容, 失败返回null
@arg:原数组, table, 被查找替换的原数组
@arg:查找内容, 任意类型, 要查找的内容, 注意数据类型
@arg:替换内容, 任意类型, 要替换后的内容, 注意数据类型
@arg:模糊匹配, boolean, 可选, 是否要模糊搜索, 省略为false
@arg:忽略大小写, boolean, 可选, 是否要忽略大小写, 省略默认false
@arg:替换数量, number, 可选, -1表示全部替换, 其他数值为替换次数, 省略默认为-1
@exp:Dim t = zm.ArraySearch({"紫猫", "老师", "QQ是多少"}, "QQ", "345911220", true) //返回{"紫猫","老师","345911220"}
--]=]
function zmm.ArrayReplace(list, value, revalue, mode, comp, nums)
	return try {
		function()
			local t = {}
			nums = nums or -1
			local recount = 0
			local s1, s2, compare
			for i = 1, #list do
				s1, s2 = list[i], value
				if comp then
					s1, s2 = string.upper(s1), string.upper(s2)
				end

				if mode then
					compare = string.find(s1, s2, 1, true)
				else
					compare = s1 == s2
				end

				if compare and (nums > recount or nums == -1) then
					t[i] = revalue
					recount = recount + 1
				else
					t[i] = list[i]
				end
			end
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ArrayReplace()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 数据转换 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:ConvBase
@cname:任意进制转换
@format:转换内容, 起始进制, 目标进制
@tname:把 $1 从 $2 进制转换成 $3 进制
@note:从2进制到62进制任意转换, 注意返回是string类型
@ret:转换结果, string, 成功返回转换后的字符串, 失败返回null
@arg:转换内容, string或number, 等待转换进制的内容
@arg:起始进制, number, 原来的进制, 填写范围2到62
@arg:目标进制, number, 要转换后的进制, 填写范围2到62
@exp:TracePrint zm.ConvBase(10, 8, 16) //把八进制的10转换成16进制, 返回8
--]=]
function zmm.ConvBase(number, frombase, tobase)
	return try {
		function()
			local digits, num, ptr, n, c
			num = 0; ptr = ""
			digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

			assert(62 >= frombase and frombase >= 2, "起始进制超过2至62进制范围。")
			assert(62 >= tobase and tobase >= 2, "目标进制超过2至62进制范围。")

			number = tostring(number)
			assert(type(number) == "string", "待转换内容参数不是字符串型")
			n = string.len(number)

			for i = 1, n do
				c = string.sub(number, i, i)
				if (c >= "0") and (c <= "9") then
					c = c - "0"
				elseif (c >= "A") and (c <= "Z") then
					if frombase <= 36 and tobase <= 36 then
						c = string.byte(c) - string.byte("A") + 10
					else
						c = string.byte(c) - string.byte("A") + 36
					end
				elseif (c >= "a") and (c <= "z") then
					c = string.byte(c) - string.byte("a") + 10
				else
					c = frombase
				end

				if c < frombase then
					num = num * frombase + c
				end
			end

			repeat
				ptr = ptr .. string.sub(digits, num % tobase + 1, num % tobase + 1)
				num = math.floor(num / tobase)
			until num == 0

			ptr = string.reverse(ptr)
			return ptr
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvBase()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ConvCoding
@cname:转换任意编码
@format:转换内容, 原始编码, 目标编码
@tname:把 $1 从 $2 编码转换成 $3 编码
@note:使用iconv进行编码转换, 常用于将gb2312的网页源码转换为utf8编码内容
@ret:转换结果, string, 成功返回转换后的字符串, 失败返回null
@arg:转换内容, string, 等待转换编码的内容
@arg:原始编码, string, 转换前的编码, 常见有gb2312, gbk, utf-8等
@arg:目标编码, string, 转换后的编码, 常见有gb2312, gbk, utf-8等
@exp:Dim str = zm.ConvCoding(text, "gb2312", "utf-8") //读取电脑文件内容后转码成utf8
--]=]
function zmm.ConvCoding(text, from, to)
	return try {
		function()
			local iconv = require("iconv")
			if from == "电脑" then from = "gb18030" end
			if to == "电脑" then to = "gb18030" end

			local cd = iconv.new(to, from)
			local ostr, err = cd:iconv(text)
			if err == iconv.ERROR_INCOMPLETE then
				error("参数不完整")
			elseif err == iconv.ERROR_INVALID then
				error("参数无效")
			elseif err == iconv.ERROR_NO_MEMORY then
				error("内存无法分配")
			elseif err == iconv.ERROR_UNKNOWN then
				error("未知错误")
			end
			return ostr
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvCoding()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ConvUnicodeToUTF16
@cname:Unicode转UTF16
@format:Unicode码
@tname:把Unicode码 $1 转为UTF16
@note:Unicode转UTF16字符串, 可用于静态界面的emoji代码, 失败返回null$n emoji整理网址: https://apps.timwhitlock.info/emoji/tables/unicode
@ret:转换结果, string, 返回以\uXXXX表示的UTF16字符串
@arg:Unicode码, string或number, 要转换的Unicode值, 数值型表示十进制, 字符串型表示十六进制
@exp:TracePrint zm.ConvUnicodeToUTF16("1F601") //显示\ud83d\ude01
--]=]
function zmm.ConvUnicodeToUTF16(num)
	return try {
		function()
			local ret
			if type(num) == "string" then
				num = tonumber(zmm.ConvBase(num:match("%x+"), 16, 10))
			end
			if num > 65535 then
				local n = num - 0x10000
				local n2 = ("0000000000" .. zmm.ConvBase(n, 10, 2))
				n2 = n2:sub(n2:len() - 19)
				local m1, m2 = n2:sub(1, 10), n2:sub(11, 20)
				local h1 = 0xD800 + tonumber(zmm.ConvBase(m1, 2, 10))
				local h2 = 0xDC00 + tonumber(zmm.ConvBase(m2, 2, 10))
				ret = string.format("\\u%04x", h1) .. string.format("\\u%04x", h2)
			else
				ret = string.format("\\u%04x", num)
			end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvUnicodeToUTF16()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ConvUnicodeToUTF8
@cname:Unicode转UTF8
@format:Unicode码
@tname:把Unicode码 $1 转为UTF8
@note:Unicode转UTF8字符串, 可用于动态界面的emoji代码, 失败返回null$n emoji整理网址: https://apps.timwhitlock.info/emoji/tables/unicode
@ret:转换结果, string, 返回UTF8字符串
@arg:Unicode码, string或number, 要转换的Unicode值, 数值型表示十进制, 字符串型表示十六进制, 也支持\uXXXX格式字符串
@exp:Dim 笑脸 = zm.ConvUnicodeToUTF8(&H1F601)$n UI.NewLayout "layout1"$n UI.AddTextView "layout1", "emoji", 笑脸$n UI.Show "layout1"$n TracePrint zm.ConvUTF8ToUnicode(笑脸)
--]=]
function zmm.ConvUnicodeToUTF8(code)
	return try {
		function()
			local ucode, st = {}, {}

			if type(code) == "string" then
				--                code = tonumber(zmm.ConvBase(code:match("%x+"), 16, 10))
				for w in code:gmatch("%x+") do
					ucode[#ucode + 1] = tonumber(zmm.ConvBase(w, 16, 10))
				end
			else
				ucode[1] = code
			end

			for _, v in ipairs(ucode) do
				local t, h = {}, 128
				while v >= h do
					t[#t + 1] = 128 + v % 64
					v = math.floor(v / 64)
					h = h > 32 and 32 or h / 2
				end
				t[#t + 1] = 256 - 2 * h + v
				st[#st + 1] = string.char(table.unpack(t)):reverse()
			end

			return table.concat(st)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvUnicodeToUTF8()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ConvUTF8ToUnicode
@cname:UTF8转Unicode
@format:UTF8
@tname:把UTF8 $1 转为Unicode
@note:将UTF8转为Unicode
@ret:转换结果, number, 返回Unicode码
@arg:UTF8, string, 要转换的UTF8
@exp:Dim 笑脸 = zm.ConvUnicodeToUTF8(&H1F601)$n UI.NewLayout "layout1"$n UI.AddTextView "layout1", "emoji", 笑脸$n UI.Show "layout1"$n TracePrint zm.ConvUTF8ToUnicode(笑脸)
--]=]
function zmm.ConvUTF8ToUnicode(utf8str, pos)
	return try {
		function()
			pos = pos or 1
			local code, size = utf8str:byte(pos), 1
			if code >= 0xC0 and code < 0xFE then
				local mask = 64
				code = code - 128
				repeat
					local next_byte = utf8str:byte(pos + size) or 0
					if next_byte >= 0x80 and next_byte < 0xC0 then
						code, size = (code - mask - 2) * 64 + next_byte, size + 1
					else
						code, size = utf8str:byte(pos), 1
					end
					mask = mask * 32
				until code < mask
			end
			return code, size
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvUTF8ToUnicode()，错误信息：", errors)
			end
		}
	}
end

local map_1252_to_unicode = {
	[0x80] = 0x20AC,
	[0x81] = 0x81,
	[0x82] = 0x201A,
	[0x83] = 0x0192,
	[0x84] = 0x201E,
	[0x85] = 0x2026,
	[0x86] = 0x2020,
	[0x87] = 0x2021,
	[0x88] = 0x02C6,
	[0x89] = 0x2030,
	[0x8A] = 0x0160,
	[0x8B] = 0x2039,
	[0x8C] = 0x0152,
	[0x8D] = 0x8D,
	[0x8E] = 0x017D,
	[0x8F] = 0x8F,
	[0x90] = 0x90,
	[0x91] = 0x2018,
	[0x92] = 0x2019,
	[0x93] = 0x201C,
	[0x94] = 0x201D,
	[0x95] = 0x2022,
	[0x96] = 0x2013,
	[0x97] = 0x2014,
	[0x98] = 0x02DC,
	[0x99] = 0x2122,
	[0x9A] = 0x0161,
	[0x9B] = 0x203A,
	[0x9C] = 0x0153,
	[0x9D] = 0x9D,
	[0x9E] = 0x017E,
	[0x9F] = 0x0178,
	[0xA0] = 0x00A0,
	[0xA1] = 0x00A1,
	[0xA2] = 0x00A2,
	[0xA3] = 0x00A3,
	[0xA4] = 0x00A4,
	[0xA5] = 0x00A5,
	[0xA6] = 0x00A6,
	[0xA7] = 0x00A7,
	[0xA8] = 0x00A8,
	[0xA9] = 0x00A9,
	[0xAA] = 0x00AA,
	[0xAB] = 0x00AB,
	[0xAC] = 0x00AC,
	[0xAD] = 0x00AD,
	[0xAE] = 0x00AE,
	[0xAF] = 0x00AF,
	[0xB0] = 0x00B0,
	[0xB1] = 0x00B1,
	[0xB2] = 0x00B2,
	[0xB3] = 0x00B3,
	[0xB4] = 0x00B4,
	[0xB5] = 0x00B5,
	[0xB6] = 0x00B6,
	[0xB7] = 0x00B7,
	[0xB8] = 0x00B8,
	[0xB9] = 0x00B9,
	[0xBA] = 0x00BA,
	[0xBB] = 0x00BB,
	[0xBC] = 0x00BC,
	[0xBD] = 0x00BD,
	[0xBE] = 0x00BE,
	[0xBF] = 0x00BF,
	[0xC0] = 0x00C0,
	[0xC1] = 0x00C1,
	[0xC2] = 0x00C2,
	[0xC3] = 0x00C3,
	[0xC4] = 0x00C4,
	[0xC5] = 0x00C5,
	[0xC6] = 0x00C6,
	[0xC7] = 0x00C7,
	[0xC8] = 0x00C8,
	[0xC9] = 0x00C9,
	[0xCA] = 0x00CA,
	[0xCB] = 0x00CB,
	[0xCC] = 0x00CC,
	[0xCD] = 0x00CD,
	[0xCE] = 0x00CE,
	[0xCF] = 0x00CF,
	[0xD0] = 0x00D0,
	[0xD1] = 0x00D1,
	[0xD2] = 0x00D2,
	[0xD3] = 0x00D3,
	[0xD4] = 0x00D4,
	[0xD5] = 0x00D5,
	[0xD6] = 0x00D6,
	[0xD7] = 0x00D7,
	[0xD8] = 0x00D8,
	[0xD9] = 0x00D9,
	[0xDA] = 0x00DA,
	[0xDB] = 0x00DB,
	[0xDC] = 0x00DC,
	[0xDD] = 0x00DD,
	[0xDE] = 0x00DE,
	[0xDF] = 0x00DF,
	[0xE0] = 0x00E0,
	[0xE1] = 0x00E1,
	[0xE2] = 0x00E2,
	[0xE3] = 0x00E3,
	[0xE4] = 0x00E4,
	[0xE5] = 0x00E5,
	[0xE6] = 0x00E6,
	[0xE7] = 0x00E7,
	[0xE8] = 0x00E8,
	[0xE9] = 0x00E9,
	[0xEA] = 0x00EA,
	[0xEB] = 0x00EB,
	[0xEC] = 0x00EC,
	[0xED] = 0x00ED,
	[0xEE] = 0x00EE,
	[0xEF] = 0x00EF,
	[0xF0] = 0x00F0,
	[0xF1] = 0x00F1,
	[0xF2] = 0x00F2,
	[0xF3] = 0x00F3,
	[0xF4] = 0x00F4,
	[0xF5] = 0x00F5,
	[0xF6] = 0x00F6,
	[0xF7] = 0x00F7,
	[0xF8] = 0x00F8,
	[0xF9] = 0x00F9,
	[0xFA] = 0x00FA,
	[0xFB] = 0x00FB,
	[0xFC] = 0x00FC,
	[0xFD] = 0x00FD,
	[0xFE] = 0x00FE,
	[0xFF] = 0x00FF,
}

--[=[
@fname:ConvUTF8ToCP1252
@cname:UTF8转CP1252
@format:UTF8
@tname:把UTF8 $1 转为CP1252
@note:UTF8转CP1252码
@ret:转换结果, number, 返回CP1252码
@arg:UTF8, string, 要转换的UTF8
@exp://ISO-8859-1里的欧元符号$n Dim str1252 = "1\128"$n TracePrint str1252$n //以UTF8输出欧元符号$n Dim strutf8 = zm.ConvCP1252ToUTF8(str1252)$n TracePrint strutf8$n //UTF8转为CP1252$n Dim str1252_2 = zm.ConvUTF8ToCP1252(strutf8)$n TracePrint str1252_2
--]=]
function zmm.ConvUTF8ToCP1252(utf8str)
	return try {
		function()
			local map_unicode_to_1252 = {}
			for code1252, code in pairs(map_1252_to_unicode) do
				map_unicode_to_1252[code] = code1252
			end
			local pos, result_1252 = 1, {}
			while pos <= #utf8str do
				local code, size = zmm.ConvUTF8ToUnicode(utf8str, pos)
				pos = pos + size
				code = code < 128 and code or map_unicode_to_1252[code] or string.byte('?')
				table.insert(result_1252, string.char(code))
			end
			return table.concat(result_1252)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvUTF8ToCP1252()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ConvCP1252ToUTF8
@cname:CP1252转UTF8
@format:CP1252码
@tname:把CP1252 $1 转为UTF8
@note:CP1252码转UTF8
@ret:转换结果, number, 返回UTF8
@arg:CP1252码, string, 要转换的CP1252码
@exp://ISO-8859-1里的欧元符号$n Dim str1252 = "1\128"$n TracePrint str1252$n //以UTF8输出欧元符号$n Dim strutf8 = zm.ConvCP1252ToUTF8(str1252)$n TracePrint strutf8$n //UTF8转为CP1252$n Dim str1252_2 = zm.ConvUTF8ToCP1252(strutf8)$n TracePrint str1252_2
--]=]
function zmm.ConvCP1252ToUTF8(str1252)
	return try {
		function()
			local result_utf8 = {}
			for pos = 1, #str1252 do
				local code = str1252:byte(pos)
				table.insert(result_utf8, zmm.ConvUnicodeToUTF8(map_1252_to_unicode[code] or code))
			end
			return table.concat(result_utf8)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ConvUTF8ToCP1252()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 随机函数 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:RndInitSeed
@cname:初始化随机种子
@format:[种子]
@tname:初始化随机种子 $1
@note:使用socket库初始化随机种子, 防止规律随机, 建议省略参数, zm.Init()内部已经执行此函数, 无需重复调用
@ret:种子值, number, 返回随机数种子
@arg:种子, number, 可选, 要初始化的种子, 省略默认将对当前时间计算获取种子值
@exp:zm.RndInitSeed() //如果你开头执行过zm.Init()命令, 则无需再调用本命令
--]=]
function zmm.RndInitSeed(seed)
	-- 初始化函数不加统计
	local isok, errors = pcall(
		function()
			local rst
			if seed then
				rst = seed
			else
				local socket = require("socket") -- 需要用到luasocket库
				local t = string.format("%f", socket.gettime())
				local st = string.sub(t, string.find(t, "%.") + 1, -1)
				rst = tonumber(string.reverse(st))
			end
			math.randomseed(rst)
			return rst
		end
	)
	if isok then
		return errors
	else
		local rst = tostring(os.time()):reverse():sub(1, 7)
		math.randomseed(rst)
		return rst
	end
end

--[=[
@fname:RndNum
@cname:随机数字
@format:[数值a][, 数值b]
@tname:在 $1, $2 范围内随机取值
@note:获取指定范围的随机数, 两个参数不区分大小顺序。
@ret:随机结果, number, 返回得到的随机数
@arg:数值a, number, 可选, 随机范围最大值或最小值, 省略默认为0
@arg:数值b, number, 可选, 随机范围最大值或最小值, 省略默认为0
@exp:TracePrint zm.RndNum(100, 10) //随机10到100$n TracePrint zm.RndNum(10, 100) //随机10到100$n TracePrint zm.RndNum(100)     //随机0到100
--]=]
function zmm.RndNum(m, n)
	return try {
		function()
			m = tonumber(m) or 0
			n = tonumber(n) or 0
			local max = (m > n) and m or n
			local min = (m < n) and m or n
			return math.random(min, max)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndNum()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RndPos
@cname:随机坐标
@format:[x, y, ...][坐标数组, ...]
@tname:随机获取一个坐标
@note:从多个坐标中随机获取一个坐标与对应的序号, 序号从0开始$n 参数二选一, 可以传入数组, 也可以传入数字
@ret:随机坐标, table, 返回序号与坐标, 序号从0开始, 格式为{id, x, y}
@arg:x, number, 横坐标数值, 与坐标数组参数二选一, 详见例子
@arg:y, number, 纵坐标数值, 与坐标数组参数二选一, 详见例子
@arg:坐标数组, table, 由坐标组成的数组, 格式为{x,y}, 与坐标数值参数二选一, 详见例子
@exp://------------数值坐标参数例子------------//$n //支持任意数量的坐标, 只要按照x,y格式顺序填写即可$n //不可以与坐标数组参数同时填写!$n //下面例子是从(100,200),(300,400),(500,600)这3个坐标中随机获取一个坐标$n Dim xy = zm.RndPos(100,200,300,400,500,600)$n TracePrint "序号:" & xy(0), ", x坐标=" & xy(1), ", y坐标=" & xy(2)$n $n //------------坐标数组参数例子------------//$n //支持任意数量的坐标数组, 不可以与数值坐标参数同时填写!$n //下面例子是从(100,200),(300,400),(500,600)这3个坐标中随机获取一个坐标$n Dim xxyy = zm.RndPos({100,200},{300,400},{500,600})$n TracePrint "序号:" & xxyy(0), ", x坐标=" & xxyy(1), ", y坐标=" & xxyy(2)
--]=]
function zmm.RndPos(...)
	local args = { ... }
	return try {
		function()
			local n = zmm.RndNum(1, #args)
			if type(args[1]) == "table" then
				return { n - 1, args[n][1], args[n][2], id = n - 1, x = args[n][1], y = args[n][2] }
			elseif type(args[1]) == "number" then
				local id = math.modf(n / 2)
				if n % 2 == 0 then
					return { id - 1, args[n - 1], args[n], id = id - 1, x = args[n - 1], y = args[n] }
				else
					return { id, args[n], args[n + 1], id = id, x = args[n], y = args[n + 1] }
				end
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndPos()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RndArrElement
@cname:随机数组成员
@format:原数组[, 起始下标[, 终止下标]]
@tname:从数组 $1 中随机下标 $2 到 $3 元素
@note:从一维数组的指定下标范围随机取元素
@ret:数组成员, 任意类型, 返回随机到的数组成员
@arg:原数组, table, 待随机取值的一维数组
@arg:起始下标, number, 可选, 表示下标随机范围, 当终止下标省略时, 随机0到起始下标。
@arg:终止下标, number, 可选, 表示下标随机范围, 当起始下标省略时, 随机0到最大下标。
@exp:TracePrint zm.RndArrElement({"紫", "猫", "编", "程", "学", "院", 345911220}) //随机获取一个元素值
--]=]
function zmm.RndArrElement(t, m, n)
	return try {
		function()
			assert(type(t) == "table", "参数1必须是数组")
			if m == nil and n == nil then
				m, n = 1, #t
			else
				m = tonumber(m)
				if n == nil then
					n = m < #t and (m + 1) or #t
					m = 1
				else
					n = tonumber(n)
					m, n = math.min(m, n) + 1, math.max(m, n) + 1
					m = (m < 1 or m > #t) and 1 or m
					n = (n < 1 or n > #t) and #t or n
				end
			end
			return t[zmm.RndNum(m, n)]
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndArrElement()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RndUniqueArr
@cname:随机不重复数组成员
@format:[一维数组][最小值, 最大值][, 个数]
@tname:随机不重复数组成员
@note:从一维数组或指定数值范围中随机取指定个数的不重复结果
@ret:数组, table, 返回随机到的不重复内容
@arg:一维数组, table, 可选, 一维数组, 待随机的数组, 与最小值最大值参数二选一
@arg:最小值, number, 可选, 随机最小值, 与数组参数二选一
@arg:最大值, number, 可选, 随机最大值, 与数组参数二选一
@exp:Dim t = zm.RndUniqueArr({"紫", "猫", "编", "程", "学", "院", 345911220}, 3) //随机获取3个不重复元素
@exp:Dim t = zm.RndUniqueArr(1, 100, 20) //从1到100中随机获取20个不重复数值
--]=]
function zmm.RndUniqueArr(t, m, n)
	return try {
		function()
			if type(t) ~= "table" then
				local min = t < m and t or m
				local max = t > m and t or m
				t = {}
				for i = min, max do
					table.insert(t, i)
				end
			else
				n = m
			end
			local s, tt = {}, zmm.Clone(t)
			local i
			n = ((n or #tt) >= #tt) and #tt or n
			while #s < n do
				i = math.random(#tt)
				table.insert(s, tt[i])
				table.remove(tt, i)
			end
			return s
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndUniqueArr()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RndStr
@cname:随机字符串
@format:个数[, 模式[, 是否不重复]]
@tname:随机获取 $1 个字符
@note:随机指定范围的字符串内容
@ret:随机结果, string, 返回随机到的字符串内容
@arg:个数, number, 随机字符个数
@arg:模式, number, 可选, 表示随机内容, 可取以下数值, 并且能相加组合, 省略默认为1+2+4$n 1: 随机0到9数字字符$n 2: 随机a到z的小写字母$n 4: 随机A到Z的大写字母$n 8: 随机标点符号!#$%&()*+,-./:;<=>?@[\]^_`{|}~'
@arg:是否不重复, boolean, 可选, 是否取不重复随机内容, 省略默认为false
@exp:TracePrint zm.RndStr(5) //随机长度为5的字符串
@exp:TracePrint zm.RndStr(6, 2) //随机长度为6的小写字母
@exp:TracePrint zm.RndStr(10, 1 + 4, true) //随机长度为10的不重复数字+大写字母
--]=]
function zmm.RndStr(n, mode, isNoRepeat)
	return try {
		function()
			local t, tt = {}, {}
			n = n or 1
			mode = mode or (1 + 2 + 4)
			local m = string.sub("0000" .. zmm.ConvBase(mode, 10, 2), -4, -1)
			if string.sub(m, 1, 1) == "1" then
				local s = "!#$%&()*+,-./:;<=>?@[\\]\"^_`{|}~'"
				for i = 1, #s do
					table.insert(t, string.sub(s, i, i))
				end
			end
			if string.sub(m, 2, 2) == "1" then
				for i = 65, 90 do
					table.insert(t, string.char(i))
				end
			end
			if string.sub(m, 3, 3) == "1" then
				for i = 97, 122 do
					table.insert(t, string.char(i))
				end
			end
			if string.sub(m, 4, 4) == "1" then
				for i = 0, 9 do
					table.insert(t, tostring(i))
				end
			end
			if isNoRepeat then
				tt = zmm.RndUniqueArr(t, n)
			else
				for _ = 1, n do
					table.insert(tt, zmm.RndArrElement(t))
				end
			end
			return table.concat(tt)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndStr()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RndName
@cname:随机姓名
@format:[性别]
@tname:随机取名
@note:随机取姓名, 可以指定性别
@ret:姓名, string, 返回随机到的姓名
@arg:性别, number, 可选, 0表示女性姓名, 1表示男性姓名, 省略默认为null, 表示全随机
@exp:TracePrint zm.RndName() //随机获取一个姓名
--]=]
function zmm.RndName(gender)
	return try {
		function()
			local FirstNameArr = { "赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "楮", "卫",
				"蒋", "沈", "韩", "杨", "朱", "秦", "尤", "许", "何", "吕", "施", "张", "孔", "曹", "严",
				"华", "金", "魏", "陶", "姜", "戚", "谢", "邹", "喻", "柏", "水", "窦", "章", "云", "苏",
				"潘", "葛", "奚", "范", "彭", "郎", "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞",
				"任", "袁", "柳", "酆", "鲍", "史", "唐", "费", "廉", "岑", "薛", "雷", "贺", "倪", "汤",
				"滕", "殷", "罗", "毕", "郝", "邬", "安", "常", "乐", "于", "时", "傅", "皮", "卞", "齐",
				"康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄", "和", "穆", "萧", "尹", "姚", "邵",
				"湛", "汪", "祁", "毛", "禹", "狄", "米", "贝", "明", "臧", "计", "伏", "成", "戴", "谈",
				"宋", "茅", "庞", "熊", "纪", "舒", "屈", "项", "祝", "董", "梁", "杜", "阮", "蓝", "闽",
				"席", "季", "麻", "强", "贾", "路", "娄", "危", "江", "童", "颜", "郭", "梅", "盛", "林",
				"刁", "锺", "徐", "丘", "骆", "高", "夏", "蔡", "田", "樊", "胡", "凌", "霍", "虞", "万",
				"支", "柯", "昝", "管", "卢", "莫", "经", "房", "裘", "缪", "干", "解", "应", "宗", "丁",
				"宣", "贲", "邓", "郁", "单", "杭", "洪", "包", "诸", "左", "石", "崔", "吉", "钮", "龚",
				"程", "嵇", "邢", "滑", "裴", "陆", "荣", "翁", "荀", "羊", "於", "惠", "甄", "麹", "家",
				"封", "芮", "羿", "储", "靳", "汲", "邴", "糜", "松", "井", "段", "富", "巫", "乌", "焦",
				"巴", "弓", "牧", "隗", "山", "谷", "车", "侯", "宓", "蓬", "全", "郗", "班", "仰", "秋",
				"仲", "伊", "宫", "宁", "仇", "栾", "暴", "甘", "斜", "厉", "戎", "祖", "武", "符", "刘",
				"景", "詹", "束", "龙", "叶", "幸", "司", "韶", "郜", "黎", "蓟", "薄", "印", "宿", "白",
				"怀", "蒲", "邰", "从", "鄂", "索", "咸", "籍", "赖", "卓", "蔺", "屠", "蒙", "池", "乔",
				"阴", "郁", "胥", "能", "苍", "双", "闻", "莘", "党", "翟", "谭", "贡", "劳", "逄", "姬",
				"申", "扶", "堵", "冉", "宰", "郦", "雍", "郤", "璩", "桑", "桂", "濮", "牛", "寿", "通",
				"边", "扈", "燕", "冀", "郏", "浦", "尚", "农", "温", "别", "庄", "晏", "柴", "瞿", "阎",
				"充", "慕", "连", "茹", "习", "宦", "艾", "鱼", "容", "向", "古", "易", "慎", "戈", "廖",
				"庾", "终", "暨", "居", "衡", "步", "都", "耿", "满", "弘", "匡", "国", "文", "寇", "广",
				"禄", "阙", "东", "欧", "殳", "沃", "利", "蔚", "越", "夔", "隆", "师", "巩", "厍", "聂",
				"晁", "勾", "敖", "融", "冷", "訾", "辛", "阚", "那", "简", "饶", "空", "曾", "毋", "沙",
				"乜", "养", "鞠", "须", "丰", "巢", "关", "蒯", "相", "查", "后", "荆", "红", "游", "竺",
				"权", "逑", "盖", "益", "桓", "公", "万俟", "司马", "上官", "欧阳", "夏侯", "诸葛",
				"闻人", "东方", "赫连", "皇甫", "尉迟", "公羊", "澹台", "公冶", "宗政", "濮阳", "淳于",
				"单于", "太叔", "申屠", "公孙", "仲孙", "轩辕", "令狐", "锺离", "宇文", "长孙",
				"慕容", "鲜于", "闾丘", "司徒", "司空", "丌官", "司寇", "仉", "督", "子车", "颛孙",
				"端木", "巫马", "公西", "漆雕", "乐正", "壤驷", "公良", "拓拔", "夹谷", "宰父", "谷梁",
				"晋", "楚", "阎", "法", "汝", "鄢", "涂", "钦", "段干", "百里", "东郭", "南门", "呼延",
				"归", "海", "羊舌", "微生", "岳", "帅", "缑", "亢", "况", "后", "有", "琴", "梁丘",
				"左丘", "东门", "西门", "商", "牟", "佘", "佴", "伯", "赏", "南宫", "墨", "哈", "谯",
				"笪", "年", "爱", "阳", "佟", "第五", "言", "福" }
			local MaleArr = { "君豪", "哲宇", "承钰", "承熙", "翰辰", "少杰", "天泽", "毅康", "俊杰",
				"凌豪", "岱熹", "希伦", "亭旭", "明曜", "禹杰", "梓轩", "昊宇", "辰熙", "梓泽",
				"昀锦", "奕嘉", "烁宇", "嘉铭", "广翰", "渝皓", "梓超", "翊翰", "徍烨", "杭骏", "智豪",
				"梓豪", "嘉豪", "熙钦", "泽毅", "硕杰", "翌熙", "炜堃", "诗崧", "昀昕", "元翰",
				"哲晗", "锦浩", "宇涵", "嘉昱", "子颢", "宇轩", "豪哲", "昊晗", "翰宇", "哲豪", "锦博",
				"锦灏", "宇杰", "文韬", "哲帆", "睿颖", "云恺", "灏轩", "思哲", "乐诚", "梓丞",
				"奕骁", "宇宸", "熙祥", "郅仑", "梓轩", "维翰", "嘉伟", "宇博", "大维", "梓明", "熙哲",
				"锦诚", "昊轩", "梓昕", "冠声", "梓林", "宝琛", "煜恒", "浩翔", "子晋", "毅诚",
				"奕恺", "天毅", "梓扬", "展豪", "昕哲", "博晗", "镇熙", "昊銘", "景豪", "仲轩", "梓谦",
				"宇翔", "浩行", "辰屹", "睿康", "昕哲", "子昂", "恺宇", "腾宇", "浩洋", "恺源",
				"天睿", "翌宸", "逸歆", "文琛", "智翔", "辰屹", "书宇", "逸天", "思睿", "奕哲", "辰恺",
				"闻涛", "子羽", "皓昇", "闻启", "宇瀚", "鼎恒", "嘉平", "彦川", "峥琪", "智行",
				"宇宸", "博涵", "杰恺", "闻韬", "圣祺", "梓恒", "智涵", "云帆", "灏宁", "奕辉", "凯扬",
				"文轩", "佳航", "天宇", "智权", "涵宇", "鑫哲", "涵韬", "嘉辉", "晨韬", "沁宇",
				"浩聪", "哲峰", "承俊", "鸿阳", "锦硕", "昱尧", "一鸣", "奕辰", "思宇", "嘉聪", "严涵",
				"文杰", "易鑫", "明琪", "逸航", "翌晨", "剑豪", "彦昕", "佳奕", "翔宇", "浩天",
				"承昊", "逸枫", "宇航", "宁悦", "玮杰", "骏杰", "哲涵", "恩硕", "亦瑄", "昊明", "佳毅",
				"缪瑄", "屹恒", "睿恒", "熠恒", "文烨", "添奕", "浩伦", "昊天", "曦宇", "子恒",
				"浩轩", "承辉", "明煜", "屹昕", "灏哲", "瑞聪", "嘉成", "睿哲", "铭鸿", "博怀", "博恺",
				"锦添", "鑫程", "昊泽", "晗瑜", "佳树", "浩然", "嘉临", "佳铭", "鼎宸", "博琛",
				"宇翱", "铭熙", "奕陶", "青云", "熙雨", "岳轩", "英赫", "盈叡", "卓瑄", "奕轩", "昕辰",
				"轩宇", "宇欣", "天奕", "宇赫", "皓涵", "成骢", "冰卓", "宇哲", "欣宇", "楚晗",
				"天琦", "瀚雄", "佳楠", "卓瀚", "誉乾", "羿凡", "泽昊", "昕宇", "潇洋", "嘉纯", "煜华",
				"琦硕", "俊乔", "俊言", "皓天", "麒暄", "乔哲", "嘉旭", "钰坤", "凡琪", "少祺",
				"宇昂", "珈睿", "泽晗", "彦鑫", "欣哲", "皓麒", "易灿", "昊南", "佳逸", "潇汉", "天翔",
				"佳翌", "佳玮", "潇阳", "承毓", "言恺", "嘉舟", "佑琪", "亦扬", "健坤", "哲睿",
				"伊铭", "皓楠", "煜哲", "嘉琛", "毅恒", "之麟", "晨昱", "嘉润", "宾鑫", "梓涵", "鑫烨",
				"铭恩", "睿麟", "彦伟", "景宸", "天麒", "沐阳", "奕可", "昱铭", "初微", "智钧",
				"祺敏", "佳轩", "宸铭", "卓玮", "佳皓", "羿臻", "梓晔", "歆昱", "祎博", "天瑜", "睿晗",
				"怡葳", "屹伦", "瀚文", "嘉鑫", "煜奇", "浩敏", "君涵", "宏宇", "若昕", "博雅",
				"行易", "佳栋", "舒阳", "欣奇", "天佑", "子程", "云宇", "梓暄", "珂贤", "煜烜", "易凡",
				"丰伟", "朝辉", "子俊", "正屹", "青阳", "宇诚", "宇豪", "昱哲", "臻宇", "夏宇",
				"紫霖", "天翊", "子岩", "天辰", "祎敏", "志成", "铭翔", "煜豪", "晨豪", "晨阳", "天浩",
				"怡辰", "雨昕", "曈欣", "衍杰", "思晨", "宏杰", "祎欣", "宇阳", "俊哲", "哲怡",
				"瑾祎", "宇帆", "正扬", "慕飞", "梓成", "仁浩", "家璐", "梓言", "诚熠", "铭皓", "舟宁",
				"伟杰", "泽阳", "晨祥", "羿青", "天易", "浩泽", "乐晨", "金豪", "天意", "杨奕",
				"子炜", "艺渊", "天一", "思昊", "昕远", "梓俊", "宸溪", "炜豪", "诗鸣", "钰航", "耀楠",
				"熠昊", "昱欣", "宸逸", "奕晨", "佳豪", "宇韬", "启铭", "宇辰", "世琪", "旻辉",
				"骏哲", "烨恒", "晨希", "奕凡", "嘉毅", "仕炜", "仕泓", "彦轩", "天宸", "益帆", "毅伟",
				"加易", "弘烨", "梓阳", "语彦", "星楠", "天博", "宸俞", "嘉伊", "诗喻", "浩枫",
				"宏昇", "旖烨", "嘉文", "颢铭", "紫杰", "怡聪", "浩峰", "佳俊", "辰煜", "奕旻", "峥玮",
				"炜霆", "子晗", "晨佳", "佩琪", "唯诣", "奕葳", "诗奕", "煜聪", "皓晖", "屹晨",
				"明远", "睿扬", "思杰", "奕聪", "佳骏", "铭欣", "晨星", "怡青", "俊寒", "舒翔", "闻哲",
				"俊袆", "睿熙", "世祺", "逸杰", "煜琪", "弘洋", "珺熠", "钰杰", "子寒", "书弈",
				"铭扬", "家奕", "睿健", "俊祎", "奕诚", "嘉煜", "晨翊", "行健", "奕佳", "燕楠", "澄浩",
				"宇乐", "仕翔", "昱诚", "奕琛", "海韬", "晔豪", "智淳", "昊铭", "熠晨", "成卓",
				"晟勋", "暄毅", "梓珏", "明烨", "驭涵", "诚炜", "凯杰", "昱文", "智健", "腾辉", "鑫海",
				"卓钰", "钰佳", "炜成", "鑫伟", "玮程", "毅峰", "宸毅", "鸣洋", "敏诚", "羿涵",
				"晨熠", "璟航", "奕隆", "诗玮", "烨铭", "嘉议", "志玮", "佳诚", "鸿诚", "懿帆", "奕玮",
				"智楷", "梓炜", "鑫璐", "奕腾", "羿飞", "文翰", "晨羿", "书瑾", "旻宇", "俊翊",
				"奕凯", "欣睿", "志宏", "嘉葳", "佩文", "昀轩", "硕成", "崇德", "佑哲", "孟哲", "诗淳",
				"焯涵", "志鸿", "楷峰", "彦成", "郁宏", "郁昇", "哲玮", "凯钧", "佑隆", "嘉容",
				"旻桦", "思咏", "峻纬", "昱彰", "峻贤", "嘉翔", "英翔", "俊曜", "嘉伦", "旻轩", "昱颖",
				"梓忆", "益昇", "彦锋", "钰翔", "奕翔", "卓谦", "玮伦", "卓峰", "泓伟", "子渊",
				"铭烨", "子巍", "鑫侃", "文凯", "哲晨", "嘉雨", "鑫彤", "昱阳", "晨皓", "诗灏", "青筠",
				"安邦", "安福", "安歌", "安国", "安和", "安康", "安澜", "安民", "安宁", "安平",
				"安然", "安顺", "安翔", "安晏", "安宜", "安怡", "安易", "安志", "昂然", "昂雄", "宾白",
				"宾鸿", "宾实", "彬彬", "彬炳", "彬郁", "斌斌", "斌蔚", "滨海", "波光", "波鸿",
				"波峻", "波涛", "博瀚", "博超", "博达", "博厚", "博简", "博明", "博容", "博赡", "博涉",
				"博实", "博涛", "博文", "博学", "博雅", "博延", "博艺", "博易", "博远", "才捷",
				"才良", "才艺", "才英", "才哲", "才俊", "成和", "成弘", "成化", "成济", "成礼", "成龙",
				"成仁", "成双", "成天", "成文", "成业", "成益", "成荫", "成周", "承安", "承弼",
				"承德", "承恩", "承福", "承基", "承教", "承平", "承嗣", "承天", "承望", "承宣", "承颜",
				"承业", "承悦", "承允", "承运", "承载", "承泽", "承志", "德本", "德海", "德厚",
				"德华", "德辉", "德惠", "德容", "德润", "德寿", "德水", "德馨", "德曜", "德业", "德义",
				"德庸", "德佑", "德宇", "德元", "德运", "德泽", "德明", "飞昂", "飞白", "飞飙",
				"飞掣", "飞尘", "飞沉", "飞驰", "飞光", "飞翰", "飞航", "飞翮", "飞鸿", "飞虎", "飞捷",
				"飞龙", "飞鸾", "飞鸣", "飞鹏", "飞扬", "飞文", "飞翔", "飞星", "飞翼", "飞英",
				"飞宇", "飞羽", "飞雨", "飞语", "飞跃", "飞章", "飞舟", "风华", "丰茂", "丰羽", "刚豪",
				"刚洁", "刚捷", "刚毅", "高昂", "高岑", "高畅", "高超", "高驰", "高达", "高澹",
				"高飞", "高芬", "高峯", "高峰", "高歌", "高格", "高寒", "高翰", "高杰", "高洁", "高峻",
				"高朗", "高丽", "高邈", "高旻", "高明", "高爽", "高兴", "高轩", "高雅", "高扬",
				"高阳", "高义", "高谊", "高逸", "高懿", "高原", "高远", "高韵", "高卓", "光赫", "光华",
				"光辉", "光济", "光霁", "光亮", "光临", "光明", "光启", "光熙", "光耀", "光誉",
				"光远", "国安", "国兴", "国源", "冠宇", "冠玉", "晗昱", "晗日", "涵畅", "涵涤", "涵亮",
				"涵忍", "涵容", "涵润", "涵涵", "涵煦", "涵蓄", "涵衍", "涵意", "涵映", "涵育",
				"翰采", "翰池", "翰飞", "翰海", "翰翮", "翰林", "翰学", "翰音", "瀚玥", "翰藻", "瀚海",
				"瀚漠", "昊苍", "昊昊", "昊空", "昊乾", "昊穹", "昊然", "昊天", "昊焱", "昊英",
				"浩波", "浩初", "浩大", "浩宕", "浩荡", "浩歌", "浩广", "浩涆", "浩瀚", "浩浩", "浩慨",
				"浩旷", "浩阔", "浩漫", "浩淼", "浩渺", "浩邈", "浩气", "浩然", "浩穰", "浩壤",
				"浩思", "浩言", "和蔼", "和安", "和璧", "和昶", "和风", "和歌", "和光", "和平", "和洽",
				"和惬", "和顺", "和硕", "和颂", "和泰", "和悌", "和通", "和同", "和煦", "和雅",
				"和宜", "和怡", "和玉", "和裕", "和豫", "和悦", "和韵", "和泽", "和正", "和志", "鹤轩",
				"弘博", "弘大", "弘方", "弘光", "弘和", "弘厚", "弘化", "弘济", "弘阔", "弘亮",
				"弘量", "弘深", "弘盛", "弘图", "弘伟", "弘文", "弘新", "弘雅", "弘扬", "弘业", "弘义",
				"弘益", "弘毅", "弘懿", "弘致", "弘壮", "宏伯", "宏博", "宏才", "宏畅", "宏达",
				"宏大", "宏放", "宏富", "宏峻", "宏浚", "宏恺", "宏旷", "宏阔", "宏朗", "宏茂", "宏邈",
				"宏儒", "宏深", "宏胜", "宏盛", "宏爽", "宏硕", "宏伟", "宏扬", "宏义", "宏逸",
				"宏毅", "宏远", "宏壮", "鸿宝", "鸿波", "鸿博", "鸿才", "鸿彩", "鸿畅", "鸿畴", "鸿达",
				"鸿德", "鸿飞", "鸿风", "鸿福", "鸿光", "鸿晖", "鸿朗", "鸿文", "鸿熙", "鸿羲",
				"鸿禧", "鸿信", "鸿轩", "鸿煊", "鸿煊", "鸿雪", "鸿羽", "鸿远", "鸿云", "鸿运", "鸿哲",
				"鸿祯", "鸿振", "鸿志", "鸿卓", "华奥", "华采", "华彩", "华灿", "华藏", "华池",
				"华翰", "华皓", "华晖", "华辉", "华茂", "华美", "华清", "华荣", "华容", "嘉赐", "嘉德",
				"嘉福", "嘉良", "嘉茂", "嘉木", "嘉慕", "嘉纳", "嘉年", "嘉平", "嘉庆", "嘉荣",
				"嘉容", "嘉瑞", "嘉胜", "嘉石", "嘉实", "嘉树", "嘉澍", "嘉熙", "嘉禧", "嘉祥", "嘉歆",
				"嘉许", "嘉勋", "嘉言", "嘉谊", "嘉懿", "嘉颖", "嘉佑", "嘉玉", "嘉誉", "嘉悦",
				"嘉运", "嘉泽", "嘉珍", "嘉祯", "嘉志", "嘉致", "坚白", "坚壁", "坚秉", "坚成", "坚诚",
				"建安", "建白", "建柏", "建本", "建弼", "建德", "建华", "建明", "建茗", "建木",
				"建树", "建同", "建修", "建业", "建义", "建元", "建章", "建中", "健柏", "金鑫", "锦程",
				"瑾瑜", "晋鹏", "经赋", "经亘", "经国", "经略", "经纶", "经天", "经武", "经业",
				"经义", "经艺", "景澄", "景福", "景焕", "景辉", "景辉", "景龙", "景明", "景山", "景胜",
				"景铄", "景天", "景同", "景曜", "靖琪", "君昊", "君浩", "俊艾", "俊拔", "俊弼",
				"俊才", "俊驰", "俊楚", "俊达", "俊德", "俊发", "俊风", "俊豪", "俊健", "俊杰", "俊捷",
				"俊郎", "俊力", "俊良", "俊迈", "俊茂", "俊美", "俊民", "俊名", "俊明", "俊楠",
				"俊能", "俊人", "俊爽", "俊悟", "俊晤", "俊侠", "俊雄", "俊逸", "俊英", "俊友", "俊语",
				"俊誉", "俊远", "俊哲", "俊喆", "俊智", "峻熙", "季萌", "季同", "开畅", "开诚",
				"开宇", "开济", "开霁", "开朗", "凯安", "凯唱", "凯定", "凯风", "凯复", "凯歌", "凯捷",
				"凯凯", "凯康", "凯乐", "凯旋", "凯泽", "恺歌", "恺乐", "康安", "康伯", "康成",
				"康德", "康复", "康健", "康乐", "康宁", "康平", "康胜", "康盛", "康时", "康适", "康顺",
				"康泰", "康裕", "乐安", "乐邦", "乐成", "乐池", "乐和", "乐家", "乐康", "乐人",
				"乐容", "乐山", "乐圣", "乐水", "乐天", "乐童", "乐贤", "乐心", "乐欣", "乐逸", "乐意",
				"乐音", "乐咏", "乐游", "乐语", "乐悦", "乐湛", "乐章", "黎昕", "黎明", "力夫",
				"力强", "力勤", "力行", "力学", "力言", "立诚", "立果", "立人", "立辉", "立轩", "立群",
				"良奥", "良弼", "良才", "良材", "良策", "良畴", "良工", "良翰", "良吉", "良骥",
				"良俊", "良骏", "良朋", "良平", "良哲", "理群", "理全", "茂才", "茂材", "茂德", "茂典",
				"茂实", "茂学", "茂勋", "茂彦", "敏博", "敏才", "敏达", "敏叡", "敏学", "敏智",
				"明诚", "明达", "明德", "明辉", "明杰", "明俊", "明朗", "明亮", "明旭", "明煦", "明轩",
				"明远", "明哲", "明喆", "明知", "明志", "明智", "明珠", "朋兴", "朋义", "彭勃",
				"彭薄", "彭湃", "彭彭", "彭魄", "彭越", "彭泽", "彭祖", "鹏池", "鹏飞", "鹏赋", "鹏海",
				"鹏鲸", "鹏举", "鹏鹍", "鹏鲲", "鹏涛", "鹏天", "鹏翼", "鹏云", "鹏运", "濮存",
				"溥心", "璞玉", "璞瑜", "浦和", "浦泽", "奇略", "奇迈", "奇胜", "奇水", "奇思", "奇邃",
				"奇伟", "奇玮", "奇文", "奇希", "奇逸", "奇正", "奇志", "奇致", "祺福", "祺然",
				"祺祥", "祺瑞", "琪睿", "庆生", "荣轩", "锐达", "锐锋", "锐翰", "锐进", "锐精", "锐立",
				"锐利", "锐思", "锐逸", "锐意", "锐藻", "锐泽", "锐阵", "锐志", "锐智", "睿才",
				"睿诚", "睿慈", "睿聪", "睿达", "睿德", "睿范", "睿广", "睿好", "睿明", "睿识", "睿思",
				"绍辉", "绍钧", "绍祺", "绍元", "升荣", "圣杰", "晟睿", "思聪", "思淼", "思源",
				"思远", "思博", "斯年", "斯伯", "泰初", "泰和", "泰河", "泰鸿", "泰华", "泰宁", "泰平",
				"泰清", "泰然", "天材", "天成", "天赋", "天干", "天罡", "天工", "天翰", "天和",
				"天华", "天骄", "天空", "天禄", "天路", "天瑞", "天睿", "天逸", "天佑", "天宇", "天元",
				"天韵", "天泽", "天纵", "同方", "同甫", "同光", "同和", "同化", "同济", "巍昂",
				"巍然", "巍奕", "伟博", "伟毅", "伟才", "伟诚", "伟茂", "伟懋", "伟祺", "伟彦", "伟晔",
				"伟泽", "伟兆", "伟志", "温纶", "温茂", "温书", "温韦", "温文", "温瑜", "文柏",
				"文昌", "文成", "文德", "文栋", "文赋", "文光", "文翰", "文虹", "文华", "文康", "文乐",
				"文林", "文敏", "文瑞", "文山", "文石", "文星", "文轩", "文宣", "文彦", "文曜",
				"文耀", "文斌", "文彬", "文滨", "向晨", "向笛", "向文", "向明", "向荣", "向阳", "翔宇",
				"翔飞", "项禹", "项明", "晓博", "心水", "心思", "心远", "欣德", "欣嘉", "欣可",
				"欣然", "欣荣", "欣怡", "欣怿", "欣悦", "新翰", "新霁", "新觉", "新立", "新荣", "新知",
				"信鸿", "信厚", "信鸥", "信然", "信瑞", "兴安", "兴邦", "兴昌", "兴朝", "兴德",
				"兴发", "兴国", "兴怀", "兴平", "兴庆", "兴生", "兴思", "兴腾", "兴旺", "兴为", "兴文",
				"兴贤", "兴修", "兴学", "兴言", "兴业", "兴运", "星波", "星辰", "星驰", "星光",
				"星海", "星汉", "星河", "星华", "星晖", "星火", "星剑", "星津", "星阑", "星纬", "星文",
				"星宇", "星雨", "星渊", "星洲", "修诚", "修德", "修杰", "修洁", "修谨", "修筠",
				"修明", "修能", "修平", "修然", "修为", "修伟", "修文", "修雅", "修永", "修远", "修真",
				"修竹", "旭尧", "炫明", "学博", "学海", "学林", "学民", "学名", "学文", "学义",
				"学真", "雪松", "雪峰", "雪风", "雅昶", "雅畅", "雅达", "雅惠", "雅健", "雅珺", "雅逸",
				"雅懿", "雅志", "炎彬", "阳飙", "阳飇", "阳冰", "阳波", "阳伯", "阳成", "阳德",
				"阳华", "阳晖", "阳辉", "阳嘉", "阳平", "阳秋", "阳荣", "阳舒", "阳朔", "阳文", "阳曦",
				"阳夏", "阳旭", "阳煦", "阳炎", "阳焱", "阳曜", "阳羽", "阳云", "阳泽", "阳州",
				"烨赫", "烨华", "烨磊", "烨霖", "烨然", "烨烁", "烨伟", "烨烨", "烨熠", "烨煜", "毅然",
				"逸仙", "逸明", "逸春", "宜春", "宜民", "宜年", "宜然", "宜人", "宜修", "意远",
				"意蕴", "意致", "意智", "熠彤", "英飙", "英博", "英才", "英达", "英发", "英范", "英光",
				"英豪", "英华", "英杰", "英朗", "英锐", "英睿", "英叡", "英韶", "英卫", "英武",
				"英悟", "英勋", "英彦", "英耀", "英奕", "英逸", "英毅", "英哲", "英喆", "英卓", "英资",
				"英纵", "永怡", "永春", "永安", "永昌", "永长", "永丰", "永福", "永嘉", "永康",
				"永年", "永宁", "永寿", "永思", "永望", "永新", "永言", "永逸", "永元", "永贞", "咏德",
				"咏歌", "咏思", "咏志", "勇男", "勇军", "勇捷", "勇锐", "勇毅", "宇达", "宇航",
				"宇寰", "宇文", "宇荫", "雨伯", "雨华", "雨石", "雨信", "雨星", "雨泽", "玉宸", "玉成",
				"玉龙", "玉泉", "玉山", "玉石", "玉书", "玉树", "玉堂", "玉轩", "玉宇", "玉韵",
				"玉泽", "煜祺", "元白", "元德", "元化", "元基", "元嘉", "元甲", "元驹", "元凯", "元恺",
				"元魁", "元良", "元亮", "元龙", "元明", "元青", "元思", "元纬", "元武", "元勋",
				"元正", "元忠", "元洲", "远航", "苑博", "苑杰", "越彬", "蕴涵", "蕴和", "蕴藉", "展鹏",
				"哲瀚", "哲茂", "哲圣", "哲彦", "振海", "振国", "正诚", "正初", "正德", "正浩",
				"正豪", "正平", "正奇", "正青", "正卿", "正文", "正祥", "正信", "正雅", "正阳", "正业",
				"正谊", "正真", "正志", "志诚", "志新", "志勇", "志明", "志国", "志强", "志尚",
				"志专", "志文", "志行", "志学", "志业", "志义", "志用", "志泽", "致远", "智明", "智鑫",
				"智勇", "智敏", "智志", "智渊", "子安", "子晋", "子民", "子明", "子默", "子墨",
				"子平", "子琪", "子石", "子实", "子真", "子濯", "子昂", "子轩", "子瑜", "自明", "自强",
				"作人", "自怡", "自珍", "曾琪", "泽宇", "泽语", "嘉博", "伟宇", "维博", "子明",
				"熙诚", "锦诚", "奕轩", "昊昕", "梓声", "冠林", "宝琛", "煜恒", "浩翔", "大晋", "梓毅",
				"天恺", "展毅", "梓扬", "銘豪", "昕哲", "梓晗", "镇熙", "昊哲" }
			local FeMaleArr = { "懿婕", "馨媛", "雨熙", "若涵", "馨瑜", "瑾涵", "羽欣", "琪悦", "逸菲",
				"馨瑜", "雨婷", "昕妤", "婉婷", "梦琪", "馨月", "佳瑜", "晓琦", "婷瑛", "诗琪",
				"瑾瑜", "艺琳", "雨婷", "欣怡", "诗雨", "佳琪", "悦涵", "昕瑶", "蓓佳", "诗萌", "曼如",
				"梦瑜", "若兮", "宛玉", "嘉淇", "莼菁", "雨涵", "诗懿", "雨彤", "璎琪", "婧怡",
				"姿蒨", "怡璐", "雨霏", "熙婷", "梓妍", "诗逸", "雯琪", "佳馨", "佳琪", "薇婷", "欣雨",
				"静茹", "欣怡", "蕙妍", "梓洁", "欣妍", "祺玥", "晨璐", "思懿", "佳蕊", "淑涵",
				"佳琦", "悦颖", "可馨", "思盈", "芷静", "欣卓", "璐怡", "嘉怡", "怡倩", "慧婷", "慧琳",
				"依婷", "雪菲", "雨妍", "语彤", "雨琪", "柏妍", "夏媛", "晓蝶", "若溪", "静雯",
				"涵诗", "心怡", "艾萌", "昕妍", "沈慧", "思颖", "靖雯", "梓慧", "欣桐", "伊嘉", "恬瑶",
				"靖怡", "鑫怡", "嘉雨", "歆玥", "诗雨", "诗怡", "语霏", "语悠", "雯慧", "欣予",
				"思琦", "蓓洁", "梓萌", "千媛", "玥涵", "彦瑶", "铭娴", "隽语", "依霏", "敏欣", "欣媛",
				"欣纯", "静霏", "雅楠", "昱涵", "婉毓", "熙婕", "莘瑶", "雅琪", "奕颖", "晗璐",
				"静楠", "佳瑶", "可心", "乐颜", "婉宜", "怡彤", "馨悦", "佳涵", "奕舒", "诗芮", "佳彦",
				"琚瑶", "晗瑜", "忆萱", "晨雁", "蕊嘉", "思妤", "子馨", "锦雯", "佳钰", "奕陶",
				"馨莹", "梓馨", "琬懿", "芷仪", "淑娴", "熙雨", "昕媛", "钰涵", "紫欣", "舒扬", "靖宜",
				"伊琳", "昱霏", "舒瑶", "梓蘅", "梓琪", "思予", "芷欣", "诗颖", "雨嫣", "懿真",
				"懿珊", "伊柔", "宇欣", "天奕", "奕菲", "予馨", "雨馨", "熙潆", "雅雯", "欣荣", "雨菲",
				"慧娴", "依睿", "雯欣", "艺璇", "蕊欣", "颖初", "欣宇", "歆乐", "文萱", "雅菲",
				"明婧", "雨杉", "萌露", "婷萱", "佳祺", "馨文", "雪婷", "怡嘉", "佳楠", "添艾", "欣羽",
				"芷晗", "馨尹", "语琦", "欣瑜", "馨妍", "雨乔", "诗涵", "煜华", "净萱", "哲娴",
				"奕心", "欣奕", "南颖", "思怡", "雨歆", "欣雯", "家琦", "嫣怡", "可新", "佳惠", "佳逸",
				"梓玥", "雨荷", "静怡", "思婷", "歆媛", "诗佳", "梓旋", "佑琪", "晨欣", "慧涵",
				"睿茜", "静仪", "雅然", "雨情", "冰倩", "语馨", "涵钰", "晓云", "枫滢", "熙妍", "雨茜",
				"曦涵", "宾鑫", "怡萱", "梓涵", "雅馨", "翊萱", "依涵", "昱欣", "珏铭", "乐彤",
				"芷馨", "冰滢", "静涵", "芷涵", "子茜", "唯伊", "馨月", "奕可", "佳缘", "雯昕", "祺敏",
				"奕璇", "梓熙", "歆昱", "韵涵", "雨宸", "欣平", "哓璐", "天瑜", "思雯", "伊颖",
				"佳璐", "睿晗", "昕颖", "嘉慧", "星玥", "艺涵", "煜琪", "馨蕾", "君涵", "若昕", "艺娴",
				"梓婷", "如昱", "熠婷", "欣奇", "紫沐", "馨竹", "圆婷", "语玥", "哲雯", "千慧",
				"佳诺", "恬易", "心奕", "懿饶", "怡辰", "雨昕", "曈欣", "懿倩", "宇馨", "凡琪", "鑫蕾",
				"钰沣", "乐怡", "叶馨", "梓言", "书晴", "舒文", "韵竹", "乐晨", "诗言", "佳言",
				"舒瑞", "诗蕊", "家彦", "佳瑶", "晗雁", "忆晨", "瑜萱", "嘉蕊", "瑜婷", "怡婷", "瑜洁",
				"晴语", "逸婷", "敏佳", "晓雪", "钰航", "佳玥", "雪晴", "怡雯", "奕瑶", "怡昕",
				"思媛", "依慧", "思蕊", "圣涵", "语尚", "心羽", "宇雯", "晴雨", "晨希", "煜婷", "辰萌",
				"昳涵", "语彦", "星楠", "嘉伊", "乐妍", "秀懿", "静蕾", "旖烨", "雯舒", "筱鹂",
				"宇萱", "佳敏", "诗怡", "奕婷", "逸萱", "奕洁", "采奕", "懿萱", "佳凝", "佳懿", "晨佳",
				"惠婷", "翌萌", "欣瑶", "煜祺", "诗奕", "梓怡", "珏妍", "奕柔", "怡晨", "乐薇",
				"雪怡", "铭欣", "佳韵", "卓语", "素妍", "怡青", "嘉旎", "昕瑜", "卓玲", "馨翊", "煜琪",
				"卓钰", "晓萱", "璐奕", "莞琪", "婉佳", "歆妍", "雨朦", "妙卓", "奕佳", "歆仪",
				"妤熙", "露瑶", "筱静", "智妤", "妤欣", "蕊怡", "筱晴", "燕楠", "钰婷", "裕湉", "涵玥",
				"奕琪", "瑾瑶", "思彤", "鑫雨", "若楠", "嘉懿", "淏妍", "宇晴", "雯怡", "彦琳",
				"诗琪", "誉菲", "郡瑶", "欣宜", "诗琦", "鑫月", "艾媛", "诗瑜", "思璇", "静宜", "懿文",
				"舒雯", "雯萱", "婉钰", "颖茜", "伊璇", "婧懿", "馨源", "佳奇", "昱琪", "梦馨",
				"宇菲", "梓忆", "卓妍", "嘉议", "逸璇", "佳莹", "书婕", "敏嘉", "懿帆", "慧雯", "佳湄",
				"慧怡", "欣婕", "馨宇", "鑫璐", "星语", "佳欣", "书瑾", "睿婕", "韵歆", "欣睿",
				"铭珠", "玫馨", "慕洁", "郁茹", "瑞珊", "姿婷", "欣容", "宜璇", "梓珏", "郁珊", "淑婷",
				"怡君", "旻珊", "珏如", "姿娴", "喻萱", "钰佳", "毓珊", "奕妤", "雯珊", "筱涵",
				"玮婷", "芳仪", "思咏", "馨瑶", "诗媛", "筱珺", "昱颖", "诗祺", "咏彤", "紫琪", "梓玲",
				"卓盈", "昱珊", "梦琪", "忆柳", "之桃", "慕青", "问兰", "尔岚", "元香", "初夏",
				"沛菡", "傲珊", "曼文", "乐菱", "痴珊", "恨玉", "惜文", "香寒", "新柔", "语蓉", "海安",
				"夜蓉", "涵柏", "水桃", "醉蓝", "春儿", "语琴", "从彤", "傲晴", "语兰", "又菱",
				"碧彤", "元霜", "怜梦", "紫寒", "妙彤", "曼易", "南莲", "紫翠", "雨寒", "易烟", "如萱",
				"若南", "寻真", "晓亦", "向珊", "慕灵", "以蕊", "寻雁", "映易", "雪柳", "孤岚",
				"笑霜", "海云", "凝天", "沛珊", "寒云", "冰旋", "宛儿", "绿真", "盼儿", "晓霜", "碧凡",
				"夏菡", "曼香", "若烟", "半梦", "雅绿", "冰蓝", "灵槐", "平安", "书翠", "翠风",
				"香巧", "代云", "梦曼", "幼翠", "友巧", "听寒", "梦柏", "醉易", "访旋", "亦玉", "凌萱",
				"访卉", "怀亦", "笑蓝", "春翠", "靖柏", "夜蕾", "冰夏", "梦松", "书雪", "乐枫",
				"念薇", "靖雁", "寻春", "恨山", "从寒", "忆香", "觅波", "静曼", "凡旋", "以亦", "念露",
				"芷蕾", "千兰", "新波", "代真", "新蕾", "雁玉", "冷卉", "紫山", "千琴", "恨天",
				"傲芙", "盼山", "怀蝶", "冰兰", "山柏", "翠萱", "恨松", "问旋", "从南", "白易", "问筠",
				"如霜", "半芹", "丹珍", "冰彤", "亦寒", "寒雁", "怜云", "寻文", "乐丹", "翠柔",
				"谷山", "之瑶", "冰露", "尔珍", "谷雪", "涵菡", "海莲", "傲蕾", "青槐", "冬儿", "易梦",
				"惜雪", "宛海", "之柔", "夏青", "亦瑶", "妙菡", "春竹", "痴梦", "紫蓝", "晓巧",
				"幻柏", "元风", "冰枫", "访蕊", "南春", "芷蕊", "凡蕾", "凡柔", "安蕾", "天荷", "含玉",
				"书兰", "雅琴", "书瑶", "春雁", "从安", "夏槐", "念芹", "怀萍", "代曼", "幻珊",
				"谷丝", "秋翠", "白晴", "海露", "代荷", "含玉", "书蕾", "听白", "访琴", "灵雁", "秋春",
				"雪青", "乐瑶", "含烟", "涵双", "平蝶", "雅蕊", "傲之", "灵薇", "绿春", "含蕾",
				"从梦", "从蓉", "初丹", "听兰", "听蓉", "语芙", "夏彤", "凌瑶", "忆翠", "幻灵", "怜菡",
				"紫南", "依珊", "妙竹", "访烟", "怜蕾", "映寒", "友绿", "冰萍", "惜霜", "凌香",
				"雁卉", "迎梦", "元柏", "代萱", "紫真", "千青", "凌寒", "紫安", "寒安", "怀蕊", "秋荷",
				"涵雁", "以山", "凡梅", "盼曼", "翠彤", "谷冬", "新巧", "冷安", "千萍", "冰烟",
				"雅阳", "友绿", "南松", "诗云", "飞风", "寄灵", "书芹", "幼蓉", "以蓝", "笑寒", "忆寒",
				"秋烟", "芷巧", "水香", "映之", "醉波", "幻莲", "夜山", "芷卉", "向彤", "小玉",
				"幼南", "凡梦", "尔曼", "念波", "迎松", "青寒", "笑天", "碧菡", "映秋", "盼烟", "忆山",
				"以寒", "寒香", "小凡", "代亦", "梦露", "映波", "友蕊", "寄凡", "怜蕾", "雁枫",
				"水绿", "曼荷", "笑珊", "寒珊", "谷南", "慕儿", "夏岚", "友儿", "小萱", "紫青", "妙菱",
				"冬寒", "曼柔", "语蝶", "青筠", "夜安", "觅海", "问安", "晓槐", "雅山", "访云",
				"翠容", "寒凡", "晓绿", "以菱", "冬云", "含玉", "访枫", "含卉", "夜白", "冷安", "灵竹",
				"醉薇", "元珊", "幻波", "盼夏", "元瑶", "迎曼", "水云", "访琴", "谷波", "乐之",
				"笑白", "之山", "妙海", "紫霜", "平夏", "凌旋", "孤丝", "怜寒", "向萍", "凡松", "青丝",
				"翠安", "如天", "凌雪", "绮菱", "代云", "南莲", "寻南", "春文", "香薇", "冬灵",
				"凌珍", "采绿", "天春", "沛文", "紫槐", "幻柏", "采文", "春梅", "雪旋", "盼海", "映梦",
				"映雪", "安雁", "映容", "凝阳", "访风", "天亦", "平绿", "盼香", "觅风", "小霜",
				"雪萍", "半雪", "山柳", "谷雪", "靖易", "白薇", "梦菡", "飞绿", "如波", "又晴", "友易",
				"香菱", "冬亦", "问雁", "妙春", "海冬", "半安", "平春", "幼柏", "秋灵", "凝芙",
				"念烟", "白山", "从灵", "尔芙", "迎蓉", "念寒", "翠绿", "翠芙", "靖儿", "妙柏", "千凝",
				"小珍", "天巧", "妙旋", "雪枫", "夏菡", "元绿", "痴灵", "绮琴", "雨双", "听枫",
				"觅荷", "凡之", "晓凡", "雅彤", "香薇", "孤风", "从安", "绮彤", "之玉", "雨珍", "幻丝",
				"代梅", "香波", "青亦", "元菱", "海瑶", "飞槐", "听露", "梦岚", "幻竹", "新冬",
				"盼翠", "谷云", "忆霜", "水瑶", "慕晴", "秋双", "雨真", "觅珍", "丹雪", "从阳", "元枫",
				"痴香", "思天", "如松", "妙晴", "谷秋", "妙松", "晓夏", "香柏", "巧绿", "宛筠",
				"碧琴", "盼兰", "小夏", "安容", "青曼", "千儿", "香春", "寻双", "涵瑶", "冷梅", "秋柔",
				"思菱", "醉波", "醉柳", "以寒", "迎夏", "向雪", "香莲", "以丹", "依凝", "如柏",
				"雁菱", "凝竹", "宛白", "初柔", "南蕾", "书萱", "梦槐", "香芹", "南琴", "绿海", "沛儿",
				"晓瑶", "听春", "凝蝶", "紫雪", "念双", "念真", "曼寒", "凡霜", "飞雪", "雪兰",
				"雅霜", "从蓉", "冷雪", "靖巧", "翠丝", "觅翠", "凡白", "迎波", "丹烟", "梦旋", "书双",
				"念桃", "夜天", "海桃", "青香", "恨风", "安筠", "觅柔", "初南", "秋蝶", "千易",
				"安露", "诗蕊", "山雁", "友菱", "香露", "晓兰", "白卉", "语山", "冷珍", "秋翠", "夏柳",
				"如之", "忆南", "书易", "翠桃", "寄瑶", "如曼", "问柳", "香梅", "幻桃", "又菡",
				"春绿", "醉蝶", "亦绿", "诗珊", "听芹", "新之", "易巧", "念云", "晓灵", "静枫", "夏蓉",
				"如南", "幼丝", "秋白", "冰安", "秋白", "南风", "醉山", "初彤", "凝海", "紫文",
				"凌晴", "香卉", "傲安", "傲之", "初蝶", "寻桃", "代芹", "诗霜", "春柏", "绿夏", "碧灵",
				"诗柳", "夏柳", "采白", "慕梅", "乐安", "冬菱", "紫安", "宛凝", "雨雪", "易真",
				"安荷", "静竹", "代柔", "丹秋", "绮梅", "依白", "凝荷", "幼珊", "忆彤", "凌青", "之桃",
				"芷荷", "听荷", "代玉", "念珍", "梦菲", "夜春", "千秋", "白秋", "谷菱", "飞松",
				"初瑶", "惜灵", "恨瑶", "梦易", "新瑶", "曼梅", "碧曼", "友瑶", "雨兰", "夜柳", "香蝶",
				"盼巧", "芷珍", "香卉", "含芙", "夜云", "依萱", "凝雁", "以莲", "易容", "元柳",
				"安南", "幼晴", "尔琴", "飞阳", "白凡", "沛萍", "向卉", "采文", "乐珍", "寒荷", "觅双",
				"白桃", "安卉", "迎曼", "盼雁", "乐松", "涵山", "恨寒", "问枫", "以柳", "含海",
				"秋春", "翠曼", "忆梅", "涵柳", "梦香", "海蓝", "晓曼", "代珊", "春冬", "恨荷", "忆丹",
				"静芙", "绮兰", "梦安", "紫丝", "千雁", "凝珍", "香萱", "梦容", "冷雁", "飞柏",
				"天真", "翠琴", "寄真", "秋荷", "代珊", "初雪", "雅柏", "怜容", "如风", "南露", "紫易",
				"冰凡", "海雪", "语蓉", "碧玉", "翠岚", "语风", "盼丹", "痴旋", "凝梦", "从雪",
				"白枫", "傲云", "白梅", "念露", "慕凝", "雅柔", "盼柳", "半青", "从霜", "怀柔", "怜晴",
				"夜蓉", "代双", "以南", "若菱", "芷文", "寄春", "南晴", "恨之", "梦寒", "初翠",
				"灵波", "巧春", "问夏", "凌春", "惜海", "亦旋", "沛芹", "幼萱", "白凝", "初露", "迎海",
				"绮玉", "凌香", "寻芹", "秋柳", "尔白", "映真", "含雁", "寒松", "友珊", "寻雪",
				"忆柏", "秋柏", "巧风", "恨蝶", "青烟", "问蕊", "灵阳", "春枫", "又儿", "雪巧", "丹萱",
				"凡双", "孤萍", "紫菱", "寻凝", "傲柏", "傲儿", "友容", "灵枫", "尔丝", "曼凝",
				"若蕊", "问丝", "思枫", "水卉", "问梅", "念寒", "诗双", "翠霜", "夜香", "寒蕾", "凡阳",
				"冷玉", "平彤", "语薇", "幻珊", "紫夏", "凌波", "芷蝶", "丹南", "之双", "凡波",
				"思雁", "白莲", "从菡", "如容", "采柳", "沛岚", "惜儿", "夜玉", "水儿", "半凡", "语海",
				"听莲", "幻枫", "念柏", "冰珍", "思山", "凝蕊", "天玉", "问香", "思萱", "向梦",
				"笑南", "夏旋", "之槐", "元灵", "以彤", "采萱", "巧曼", "绿兰", "平蓝", "问萍", "绿蓉",
				"靖柏", "迎蕾", "碧曼", "思卉", "白柏", "妙菡", "怜阳", "雨柏", "雁菡", "梦之",
				"又莲", "乐荷", "寒天", "凝琴", "书南", "映天", "白梦", "初瑶", "恨竹", "平露", "含巧",
				"慕蕊", "半莲", "醉卉", "天菱", "青雪", "雅旋", "巧荷", "飞丹", "恨云", "若灵",
			}
			if gender == nil then
				gender = math.random(0, 1)
			end
			local Name
			if gender == 0 then
				Name = FirstNameArr[math.random(1, #FirstNameArr)] .. FeMaleArr[math.random(1, #FeMaleArr)]
			else
				Name = FirstNameArr[math.random(1, #FirstNameArr)] .. MaleArr[math.random(1, #MaleArr)]
			end
			return Name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RndName()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 加解密类 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:EncodeURL
@cname:URL编码
@format:明文
@tname:对 $1 进行URL编码
@note:对网址进行URL编码, 空格转+, 保留-_.~这几个符号, 例如紫猫编码后就是%E7%B4%AB%E7%8C%AB
@ret:密文, string, 返回URL编码后的内容, 失败返回null
@arg:明文, string, 待编码的内容
@exp:TracePrint zm.EncodeURL("紫猫") //输出%E7%B4%AB%E7%8C%AB
--]=]
function zmm.EncodeURL(str)
	return try {
		function()
			if str then
				str = string.gsub(str, "\n", "\r\n")
				str = string.gsub
					(
						str, "([^%w %-%_%.%~])",
						function(c)
							return string.format("%%%02X", string.byte(c))
						end
					)
				str = string.gsub(str, " ", "+")
			end
			return str
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeURL()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DecodeURL
@cname:URL解码
@format:密文
@tname:对 $1 进行URL解码
@note:对网址进行URL解码, +转空格, 保留-_.~这几个符号, 例如%E7%B4%AB%E7%8C%AB解码后就是紫猫
@ret:明文, string, 返回URL解码后的内容, 失败返回null
@arg:密文, string, 待解码的内容
@exp:TracePrint zm.EncodeURL("%E7%B4%AB%E7%8C%AB") //输出紫猫
--]=]
function zmm.DecodeURL(str)
	return try {
		function()
			str = string.gsub(str, "+", " ")
			str = string.gsub
				(
					str, "%%(%x%x)",
					function(h)
						return string.char(tonumber(h, 16))
					end
				)
			str = string.gsub(str, "\r\n", "\n")
			return str
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeURL()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeMD5File
@cname:计算文件MD5值
@format:路径
@tname:计算 $1 的MD5值
@note:计算一个文件的MD5值
@ret:MD5, string, 返回指定文件的MD5值, 失败返回null
@arg:路径, string, 要计算MD5值的文件路径
@exp:TracePrint zm.EncodeMD5File(GetTempDir() & "Plugin/File.lua") //输出文件md5值
--]=]
function zmm.EncodeMD5File(path)
	return try {
		function()
			local ret = zmm.Execute("md5sum \"" .. path .. "\"")
			return ret:match("[^ ]+")
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeMD5File()，错误信息：", errors)
			end
		}
	}
end

_zmm.getBase64Code = function()
	local lua_text = [=[--[[**************************************************************************]]
-- base64.lua
-- Copyright 2014 Ernest R. Ewert
--
--  This Lua module contains the implementation of a Lua base64 encode
--  and decode library.
--
--  The library exposes these methods.
--
--      Method      Args
--      ----------- ----------------------------------------------
--      encode      String in / out
--      decode      String in / out
--
--      encode      String, function(value) predicate
--      decode      String, function(value) predicate
--
--      encode      file, function(value) predicate
--      deocde      file, function(value) predicate
--
--      encode      file, file
--      deocde      file, file
--
--      alpha       alphabet, term char
--

--------------------------------------------------------------------------------
-- known_base64_alphabets
--
--
local known_base64_alphabets=
{
    base64= -- RFC 2045 (Ignores max line length restrictions)
    {
        _alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        _strip="[^%a%d%+%/%=]",
        _term="="
    },

    base64noterm= -- RFC 2045 (Ignores max line length restrictions)
    {
        _alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        _strip="[^%a%d%+%/]",
        _term=""
    },

    base64url= -- RFC 4648 'base64url'
    {
        _alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_",
        _strip="[^%a%d%+%-%_=]",
        _term=""
    },
}
local c_alpha=known_base64_alphabets.base64
local pattern_strip

--[[**************************************************************************]]
--[[****************************** Encoding **********************************]]
--[[**************************************************************************]]


-- Precomputed tables (compromise using more memory for speed)
local b64e    -- 6 bit patterns to ANSI 'char' values
local b64e_a  -- ready to use
local b64e_a2 -- byte addend
local b64e_b1 -- byte addend
local b64e_b2 -- byte addend
local b64e_c1 -- byte addend
local b64e_c  -- ready to use


-- Tail padding values
local tail_padd64=
{
    "==",   -- two bytes modulo
    "="     -- one byte modulo
}


--------------------------------------------------------------------------------
-- e64
--
--  Helper function to convert three eight bit values into four encoded
--  6 (significant) bit values.
--
--                 7             0 7             0 7             0
--             e64(a a a a a a a a,b b b b b b b b,c c c c c c c c)
--                 |           |           |           |
--  return    [    a a a a a a]|           |           |
--                        [    a a b b b b]|           |
--                                    [    b b b b c c]|
--                                                [    c c c c c c]
--
local function e64( a, b, c )
    -- Return pre-calculated values for encoded value 1 and 4
    -- Get the pre-calculated extractions for value 2 and 3, look them
    -- up and return the proper value.
    --
    return  b64e_a[a],
            b64e[ b64e_a2[a]+b64e_b1[b] ],
            b64e[ b64e_b2[b]+b64e_c1[c] ],
            b64e_c[c]
end


--------------------------------------------------------------------------------
-- encode_tail64
--
--  Send a tail pad value to the output predicate provided.
--
local function encode_tail64( out, x, y )
    -- If we have a number of input bytes that isn't exactly divisible
    -- by 3 then we need to pad the tail
    if x ~= nil then
        local a,b,r = x,0,1

        if y ~= nil then
            r = 2
            b = y
        end

        -- Encode three bytes of info, with the tail byte as zeros and
        -- ignore any fourth encoded ASCII value. (We should NOT have a
        -- forth byte at this point.)
        local b1, b2, b3 = e64( a, b, 0 )

        -- always add the first 2 six bit values to the res table
        -- 1 remainder input byte needs 8 output bits
        local tail_value = string.char( b1, b2 )

        -- two remainder input bytes will need 18 output bits (2 as pad)
        if r == 2 then
            tail_value=tail_value..string.char( b3 )
        end

        -- send the last 4 byte sequence with appropriate tail padding
        out( tail_value .. tail_padd64[r] )
    end
end


--------------------------------------------------------------------------------
-- encode64_io_iterator
--
--  Create an io input iterator to read an input file and split values for
--  proper encoding.
--
local function encode64_io_iterator(file)

    assert( io.type(file)  == "file", "argument must be readable file handle" )
    assert( file.read ~= nil,         "argument must be readable file handle" )

    local ii = { } -- Table for the input iterator

    setmetatable(ii,{ __tostring=function() return "base64.io_iterator" end})

    -- Begin returns an input read iterator
    --
    function ii.begin()
        local sb  = string.byte

        -- The iterator returns three bytes from the file for encoding or nil
        -- when the end of the file has been reached.
        --
        return function()
            s = file:read(3)
            if s ~= nil and #s == 3 then
                return sb(s,1,3)
            end
            return nil
        end
    end

    -- The tail method on the iterator allows the routines to run faster
    -- because each sequence of bytes doesn't have to test for EOF.
    --
    function ii.tail()
        -- If one or two "overflow" bytes exist, return those.
        --
        if s ~= nil then return s:byte(1,2) end
    end

    return ii
end


--------------------------------------------------------------------------------
-- encode64_with_ii
--
--      Convert the value provided by an encode iterator that provides a begin
--      method, a tail method, and an iterator that returns three bytes for
--      each call until at the end. The tail method should return either 1 or 2
--      tail bytes (for source values that are not evenly divisible by three).
--
local function encode64_with_ii( ii, out )
    local sc=string.char

    for a, b, c in ii.begin() do
        out( sc( e64( a, b, c ) ) )
    end

    encode_tail64( out, ii.tail() )

end


--------------------------------------------------------------------------------
-- encode64_with_predicate
--
--      Implements the basic raw data --> base64 conversion. Each three byte
--      sequence in the input string is converted to the encoded string and
--      given to the predicate provided in 4 output byte chunks. This method
--      is slightly faster for traversing existing strings in memory.
--
local function encode64_with_predicate( raw, out )
    local rem=#raw%3     -- remainder
    local len=#raw-rem   -- 3 byte input adjusted
    local sb=string.byte -- Mostly notational (slight performance)
    local sc=string.char -- Mostly notational (slight performance)

    -- Main encode loop converts three input bytes to 4 base64 encoded
    -- ACSII values and calls the predicate with the value.
    for i=1,len,3 do
        -- This really isn't intended as obfuscation. It is more about
        -- loop optimization and removing temporaries.
        --
        out( sc( e64( sb( raw ,i , i+3 ) ) ) )
        --   |   |    |
        --   |   |    byte i to i + 3
        --   |   |
        --   |   returns 4 encoded values
        --   |
        --   creates a string with the 4 returned values
    end

    -- If we have a number of input bytes that isn't exactly divisible
    -- by 3 then we need to pad the tail
    if rem > 0 then
        local x, y = sb( raw, len+1 )

        if rem > 1 then
            y = sb( raw, len+2 )
        end

        encode_tail64( out, x, y )
    end
end


--------------------------------------------------------------------------------
-- encode64_tostring
--
--      Convenience method that accepts a string value and returns the
--      encoded version of that string.
--
local function encode64_tostring(raw)

    local sb={} -- table to build string

    local function collection_predicate(v)
        sb[#sb+1]=v
    end

    -- Test with an 818K string in memory. Result is 1.1M of data.
    --
    --      lua_base64      base64 (gnu 8.21)
    --      202ms           54ms
    --      203ms           48ms
    --      204ms           50ms
    --      203ms           42ms
    --      205ms           46ms
    --
    encode64_with_predicate( raw, collection_predicate )

    return table.concat(sb)
end


--[[**************************************************************************]]
--[[****************************** Decoding **********************************]]
--[[**************************************************************************]]


-- Precomputed tables (compromise using more memory for speed)
local b64d    -- ANSI 'char' to right shifted bit pattern
local b64d_a1 -- byte addend
local b64d_a2 -- byte addend
local b64d_b1 -- byte addend
local b64d_b2 -- byte addend
local b64d_c1 -- byte addend
local b64d_z  -- zero


--------------------------------------------------------------------------------
-- d64
--
--  Helper function to convert four six bit values into three full eight
--  bit values. Input values are the integer expression of the six bit value
--  encoded in the original base64 encoded string.
--
--     d64( _ _1 1 1 1 1 1,
--             |       _ _ 2 2 2 2 2 2,
--             |           |       _ _ 3 3 3 3 3 3,
--             |           |           |       _ _ 4 4 4 4 4 4)
--             |           |           |           |
--  return ', [1 1 1 1 1 1 2 2]        |           |
--         ',                 [2 2 2 2 3 3 3 3]    |
--         '                                  [3 3 4 4 4 4 4 4]
--
local function d64( b1, b2, b3, b4 )
    -- We can get away with addition instead of anding the values together
    -- because there are no  overlapping bit patterns.
    --
    return
        b64d_a1[b1] + b64d_a2[b2],
        b64d_b1[b2] + b64d_b2[b3],
        b64d_c1[b3] + b64d[b4]
end


--------------------------------------------------------------------------------
-- decode_tail64
--
--  Send the end of stream bytes that didn't get decoded via the main loop.
--
local function decode_tail64( out, e1, e2 ,e3, e4 )

    if tail_padd64[2] == "" or e4 == tail_padd64[2]:byte() then
        local n3 = b64d_z

        if e3 ~= nil and e3 ~= tail_padd64[2]:byte() then
            n3 = e3
        end

        -- Unpack the six bit values into the 8 bit values
        local b1, b2 = d64( e1, e2, n3, b64d_z )

        -- And add them to the res table
        if e3 ~= nil and e3 ~= tail_padd64[2]:byte() then
            out( string.char( b1, b2 ) )
        else
            out( string.char( b1 ) )
        end
    end
end


--------------------------------------------------------------------------------
-- decode64_io_iterator
--
--  Create an io input iterator to read an input file and split values for
--  proper decoding.
--
local function decode64_io_iterator( file )

    local ii = { }

    -- An enumeration coroutine that handles the reading of an input file
    -- to break data into proper pieces for building the original string.
    --
    local function enummerate( file )
        local sc=string.char
        local sb=string.byte
        local ll="" -- last line storage
        local len
        local yield = coroutine.yield

        -- Read a "reasonable amount" of data into the line buffer. Line by
        -- line is not used so that a file with no line breaks doesn't
        -- cause an inordinate amount of memory usage.
        --
        for cl in file:lines(2048) do
            -- Reset the current line to contain valid chars and any previous
            -- "leftover" bytes from the previous read
            --
            cl = ll .. cl:gsub(pattern_strip,"")
            --   |     |
            --   |     Remove "Invalid" chars (white space etc)
            --   |
            --   Left over from last line
            --
            len = (#cl-4)-(#cl%4)

            -- see the comments in decode64_with_predicate for a rundown of
            -- the results of this loop (sans the coroutine)
            for i=1,len,4 do
                yield( sc( d64( sb( cl, i, i+4 ) ) ) )
            end

            ll = cl:sub( len +1, #cl )
        end

        local l = #ll

        if l >= 4 and ll:sub(-1) ~= tail_padd64[2] then
            yield( sc( d64( sb( ll, 1, 4 ) ) ) )
            l=l-4
        end

        if l > 0 then

            local e1,e2,e3,e4 = ll:byte( 0 - l, -1 )

            if e1 ~= nil then
                decode_tail64( function(s) yield( s ) end, e1, e2, e3, e4 )
            end
        end

    end

    -- Returns an input iterator that is implemented as a coroutine. Each
    -- yield of the co-routine sends reconstructed bytes to the loop handling
    -- the iteration.
    --
    function ii.begin()
        local co = coroutine.create( function() enummerate(file) end )

        return function()
            local code,res = coroutine.resume(co)
            assert(code == true)
            return res
        end
    end

    return ii
end


--------------------------------------------------------------------------------
-- decode64_with_ii
--
--      Convert the value provided by a decode iterator that provides a begin
--      method, a tail method, and an iterator that returns four (usable!) bytes
--      for each call until at the end.
--
local function decode64_with_ii( ii, out )

    -- Uses the iterator to pull values. Each reconstructed string
    -- is sent to the output predicate.
    --
    for l in ii.begin() do out( l ) end

end


--------------------------------------------------------------------------------
-- decode64_with_predicate
--
-- Decode an entire base64 encoded string in memory using the predicate for
-- output.
--
local function decode64_with_predicate( raw, out )
    -- Sanitize the input to strip characters that are not in the alphabet.
    --
    -- Note: This is a deviation from strict implementations where "bad data"
    --       in the input stream is unsupported.
    --
    local san = raw:gsub(pattern_strip,"")
    local len = #san-#san%4
    local rem = #san-len
    local sc  = string.char
    local sb  = string.byte

    if san:sub(-1,-1) == tail_padd64[2] then
        rem = rem + 4
        len = len - 4
    end

    for i=1,len,4 do
        out( sc( d64( sb( san, i, i+4 ) ) ) )
    end

    if rem > 0 then
        decode_tail64( out, sb( san, 0-rem, -1 ) )
    end
end


--------------------------------------------------------------------------------
-- decode64_tostring
--
--  Takes a string that is encoded in base64 and returns the decoded value in
--  a new string.
--
local function decode64_tostring( raw )

    local sb={} -- table to build string

    local function collection_predicate(v)
        sb[#sb+1]=v
    end

    decode64_with_predicate( raw, collection_predicate )

    return table.concat(sb)
end


--------------------------------------------------------------------------------
-- set_and_get_alphabet
--
--  Sets and returns the encode / decode alphabet.
--
--
local function set_and_get_alphabet(alpha,term)

    if alpha ~= nil then
        local magic=
        {
    --        ["%"]="%%",
            [" "]="% ",
            ["^"]="%^",
            ["$"]="%$",
            ["("]="%(",
            [")"]="%)",
            ["."]="%.",
            ["["]="%[",
            ["]"]="%]",
            ["*"]="%*",
            ["+"]="%+",
            ["-"]="%-",
            ["?"]="%?",
        }

        c_alpha=known_base64_alphabets[alpha]
        if c_alpha == nil then
            c_alpha={ _alpha=alpha, _term=term }
        end

        assert( #c_alpha._alpha == 64,    "The alphabet ~must~ be 64 unique values."  )
        assert( #c_alpha._term  <=  1,    "Specify zero or one termination character.")

        b64d={}  -- Decode table alpha          -> right shifted int values
        b64e={}  -- Encode table 0-63 (6 bits)  -> char table
        local s=""
        for i = 1,64 do
            local byte = c_alpha._alpha:byte(i)
            local str  = string.char(byte)
            b64e[i-1]=byte
            assert( b64d[byte] == nil, "Duplicate value '"..str.."'" )
            b64d[byte]=i-1
            s=s..str
        end


        local ext --Alias for extraction routine that avoids extra table lookups

        if bit32 then
            ext = bit32.extract -- slight speed, vast visual (IMO)
        elseif bit then
            local band = bit.band
            local rshift = bit.rshift
            ext =
                function(n, field, width)
                    width = width or 1
                    return band(rshift(n, field), 2^width-1)
                end
        else
            error("Neither Lua 5.2 bit32 nor LuaJit bit library found!")
        end

        -- preload encode lookup tables
        b64e_a  = {}
        b64e_a2 = {}
        b64e_b1 = {}
        b64e_b2 = {}
        b64e_c1 = {}
        b64e_c  = {}

        for f = 0,255 do
            b64e_a  [f]=b64e[ext(f,2,6)]
            b64e_a2 [f]=ext(f,0,2)*16
            b64e_b1 [f]=ext(f,4,4)
            b64e_b2 [f]=ext(f,0,4)*4
            b64e_c1 [f]=ext(f,6,2)
            b64e_c  [f]=b64e[ext(f,0,6)]
        end

        -- preload decode lookup tables
        b64d_a1 = {}
        b64d_a2 = {}
        b64d_b1 = {}
        b64d_b2 = {}
        b64d_c1 = {}
        b64d_z  = b64e[0]

        for k,v in pairs(b64d) do
            -- Each comment shows the rough C expression that would be used to
            -- generate the returned triple.
            --
            b64d_a1 [k] = v*4                   -- ([b1]       ) << 2
            b64d_a2 [k] = math.floor( v / 16 )  -- ([b2] & 0x30) >> 4
            b64d_b1 [k] = ext( v, 0, 4 ) * 16   -- ([b2] & 0x0F) << 4
            b64d_b2 [k] = math.floor( v / 4 )   -- ([b3] & 0x3c) >> 2
            b64d_c1 [k] = ext( v, 0, 2 ) * 64   -- ([b3] & 0x03) << 6
        end

        if c_alpha._term ~= "" then
            tail_padd64[1]=string.char(c_alpha._term:byte(),c_alpha._term:byte())
            tail_padd64[2]=string.char(c_alpha._term:byte())
        else
            tail_padd64[1]=""
            tail_padd64[2]=""
        end

        local esc_term

        if magic[c_alpha._term] ~= nil then
            esc_term=c_alpha._term:gsub(magic[c_alpha._term],function (s) return magic[s] end)
        elseif c_alpha._term == "%" then
            esc_term = "%%"
        else
            esc_term=c_alpha._term
        end

        if not c_alpha._strip then
            local p=s:gsub("%%",function (s) return "__unique__" end)
            for k,v in pairs(magic)
            do
                p=p:gsub(v,function (s) return magic[s] end )
            end
            local mr=p:gsub("__unique__",function() return "%%" end)

            c_alpha._strip = string.format("[^%s%s]",mr,esc_term)
        end

        assert( c_alpha._strip )

        pattern_strip = c_alpha._strip

        local c =0 for i in pairs(b64d) do c=c+1 end

        assert( c_alpha._alpha == s,        "Integrity error." )
        assert( c == 64,                    "The alphabet must be 64 unique values." )
        if esc_term ~= "" then
            assert( not c_alpha._alpha:find(esc_term), "Tail characters must not exist in alphabet." )
        end

        if known_base64_alphabets[alpha] == nil then
            known_base64_alphabets[alpha]=c_alpha
        end
    end

    return c_alpha._alpha,c_alpha._term
end


--------------------------------------------------------------------------------
-- encode64
--
--  Entry point mode selector.
--
--
local function encode64(i,o)
    local method

    if o ~= nil and io.type(o) == "file" then
        local file_out = o
        o = function(s) file_out:write(s) end
    end

    if type(i) == "string" then
        if type(o) == "function" then
            method = encode64_with_predicate
        else
            assert( o == nil, "unsupported request")
            method = encode64_tostring
        end
    elseif io.type(i) == "file" then
        assert( type(o) == "function", "file source requires output predicate")
        i      = encode64_io_iterator(i)
        method = encode64_with_ii
    else
        assert( false, "unsupported mode" )
    end

    return method(i,o)
end


--------------------------------------------------------------------------------
-- decode64
--
--  Entry point mode selector.
--
--
local function decode64(i,o)
    local method

    if o ~= nil and io.type(o) == "file" then
        local file_out = o
        o = function(s) file_out:write(s) end
    end

    if type(i) == "string" then
        if type(o) == "function" then
            method = decode64_with_predicate
        else
            assert( o == nil, "unsupported request")
            method = decode64_tostring
        end
    elseif io.type(i) == "file" then
        assert( type(o) == "function", "file source requires output predicate")
        i      = decode64_io_iterator(i)
        method = decode64_with_ii
    else
        assert( false, "unsupported mode" )
    end

    return method(i,o)
end

set_and_get_alphabet("base64")

--[[**************************************************************************]]
--[[******************************  Module  **********************************]]
--[[**************************************************************************]]
return
{
    encode      = encode64,
    decode      = decode64,
    alpha       = set_and_get_alphabet,
}
]=]
	return lua_text
end

_zmm.setBase64Code = function(lua_path, json_path, version)
	local cjson = require("cjson")
	local lua_text = _zmm.getBase64Code()
	_zmm.filewrite(lua_path, lua_text)
	_zmm.filewrite(json_path, cjson.encode({ version = version }))
end

--[=[
@fname:InitBase64
@cname:初始化Base64模式
@format:[字符表[, 填充符]]
@tname:初始化Base64
@note:初始化设置Base64的加解密模式, 支持URL安全Base64, 默认为标准Base64, 已集成到zm.Init 初始化插件环境中
@ret:字符表, string, 返回设定的base64字符表, 失败返回null
@arg:字符表, string或number, 可选, 省略默认为标准Base64加解密$n 数值1为base64标准加解密$n 数值2为base64url安全加解密$n 数值3为base64noterm标准无填充加解密$n 填写长度64的不重复字符串时表示自定义编码表
@arg:填充符, string, 可选, 末尾填充字符, 省略默认为标准Base64加解密
@exp:TracePrint zm.InitBase64() //标准Base64编码
@exp:TracePrint zm.InitBase64(2) //Base64URL安全编码
--]=]
_zmm.BASE64 = {}
_zmm.BASE64E = {}
function zmm.InitBase64(alpha, term)
	-- 初始化不带统计
	_zmm.CreatePluginDir()
	local json_path = _zmm._GLOBAL_PATH_ .. "zm_base64.json"
	local isok, errors = pcall(
		function()
			local version = "1.1"
			local lua_path = _zmm._GLOBAL_PATH_ .. "zm_base64.lua"
			local text = _zmm.fileread(json_path)
			if text then
				local json = LuaAuxLib.Encode_GetJsonLib():decode(text)
				if json["version"] ~= version then
					_zmm.setBase64Code(lua_path, json_path, version)
				end
			else
				_zmm.setBase64Code(lua_path, json_path, version)
			end

			_zmm.BASE64[1] = require(_zmm._PLUGIN_DIR_ .. ".zm_base64")
			_zmm.BASE64[2] = alpha
			_zmm.BASE64[3] = term
			if alpha == 1 then
				alpha = "base64"
			elseif alpha == 2 then
				alpha = "base64url"
			elseif alpha == 3 then
				alpha = "base64noterm"
			elseif string.len(alpha) ~= 64 then
				alpha = "base64"
			end
			_zmm.BASE64E[1] = require(_zmm._PLUGIN_DIR_ .. ".zm_base64")
			_zmm.BASE64E[1].alpha("base64")
			return _zmm.BASE64[1].alpha(alpha, term)
		end
	)
	if isok then
		return errors
	else
		os.remove(json_path)
		traceprint("初始化zm.InitBase64()失败, 可尝试重启手机解决, 对云识别和Base64相关函数有影响, 错误信息：",
			errors)
	end
end

--[=[
@fname:EncodeBase64
@cname:快速Base64文本加密
@format:原文
@tname:对 $1 进行Base64加密
@note:对一串文本内容进行快速Base64加密, 可通过zm.InitBase64()设置加密模式
@ret:密文, string, 返回Base64加密后的内容, 失败返回null
@arg:原文, string, 待加密的原文本内容
@exp:TracePrint zm.EncodeBase64("紫猫编程学院") //对紫猫编程学院进行BASE64加密, 得到57Sr54yr57yW56iL5a2m6Zmi
--]=]
function zmm.EncodeBase64(i, o)
	return try {
		function()
			if _zmm.BASE64[1] == nil then
				zmm.InitBase64()
			end
			return _zmm.BASE64[1].encode(i, o)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeBase64()，错误信息：", errors)
			end
		}
	}
end

function _zmm.EncodeBase64(i, o)
	return try {
		function()
			if _zmm.BASE64E[1] == nil then
				zmm.InitBase64()
			end
			return _zmm.BASE64E[1].encode(i, o)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：EncodeBase64()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeBase64File
@cname:快速Base64文件加密
@format:原文件路径, 加密保存路径
@tname:对 $1 文件进行Base64加密保存到 $2 中
@note:对一个文件进行快速Base64加密, 可通过zm.InitBase64()设置加密模式
@ret:密文, string, 返回文件二进制base64的内容, 失败返回null
@arg:原文件路径, string, 待加密的文件路径, 文件必须存在
@arg:加密保存路径, string, 加密文件保存的路径
@exp:zm.EncodeBase64File("/sdcard/1.txt", "/sdcard/2.txt") //把1.txt加密成2.txt, 注意, 1.txt文件必须存在!
--]=]
function zmm.EncodeBase64File(i, o)
	local ii, oo
	return try {
		function()
			ii = io.open(i, "rb")
			oo = io.open(o, "wb")
			local str = zmm.EncodeBase64(ii, oo)
			ii:close()
			oo:close()
			return str
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeBase64File()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(ii) == "file" then ii:close() end
				if io.type(oo) == "file" then oo:close() end
			end
		}
	}
end

--[=[
@fname:DecodeBase64
@cname:快速Base64文本解密
@format:密文
@tname:对 $1 进行Base64解密
@note:对一串文本内容进行快速Base64解密, 可通过zm.InitBase64()设置加密模式
@ret:明文, string, 返回Base64解密后的内容, 失败返回null
@arg:密文, string, 待解密的内容
@exp:TracePrint zm.DecodeBase64("57Sr54yr57yW56iL5a2m6Zmi") //对57Sr54yr57yW56iL5a2m6Zmi进行BASE64解密, 得到紫猫编程学院
--]=]
function zmm.DecodeBase64(i, o)
	return try {
		function()
			if _zmm.BASE64[1] == nil then
				zmm.InitBase64()
			end
			return _zmm.BASE64[1].decode(i, o)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeBase64()，错误信息：", errors)
			end
		}
	}
end

function _zmm.DecodeBase64(i, o)
	return try {
		function()
			if _zmm.BASE64E[1] == nil then
				zmm.InitBase64()
			end
			return _zmm.BASE64E[1].decode(i, o)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：DecodeBase64()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DecodeBase64File
@cname:快速Base64文件解密
@format:加密文件路径, 解密保存路径
@tname:对 $1 文件进行Base64解密保存到 $2 中
@note:对一个文件进行快速Base64解密, 可通过zm.InitBase64()设置加密模式
@ret:明文, string, 返回文件二进制的内容, 失败返回null
@arg:加密文件路径, string, Base64文件路径, 文件必须存在
@arg:解密保存路径, string, 解密文件后保存的路径
@exp:zm.DecodeBase64File("/sdcard/1.txt", "/sdcard/2.txt") //把1.txt解密成2.txt, 注意, 1.txt文件必须存在!
--]=]
function zmm.DecodeBase64File(i, o)
	local ii, oo
	return try {
		function()
			ii = io.open(i, "rb")
			oo = io.open(o, "wb")
			local str = zmm.DecodeBase64(ii, oo)
			ii:close()
			oo:close()
			return str
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeBase64File()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(ii) == "file" then ii:close() end
				if io.type(oo) == "file" then oo:close() end
			end
		}
	}
end

_zmm.getLockBoxCode = function()
	local luatext = {}
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox.lua"] = [=[return require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.init")]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/init.lua"] = [=[local Lockbox = {};
Lockbox.ALLOW_INSECURE = true;

Lockbox.insecure = function()
	assert(Lockbox.ALLOW_INSECURE,"This module is insecure!  It should not be used in production.  If you really want to use it, set Lockbox.ALLOW_INSECURE to true before importing it");
end

return Lockbox;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/aes128.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");


local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

local SBOX = {
 [0]=0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
 0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
 0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
 0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
 0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
 0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
 0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
 0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
 0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
 0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
 0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
 0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
 0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
 0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
 0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
 0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16};

local ISBOX = {
 [0]=0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
 0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
 0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
 0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
 0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
 0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
 0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
 0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
 0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
 0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
 0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
 0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
 0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
 0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
 0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
 0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D};

local ROW_SHIFT =  {  1,  6, 11, 16,  5, 10, 15,  4,  9, 14,  3,  8, 13,  2,  7, 12,};
local IROW_SHIFT = {  1, 14, 11,  8,  5,  2, 15, 12,  9,  6,  3, 16, 13, 10,  7,  4,};

local ETABLE = {
 [0]=0x01, 0x03, 0x05, 0x0F, 0x11, 0x33, 0x55, 0xFF, 0x1A, 0x2E, 0x72, 0x96, 0xA1, 0xF8, 0x13, 0x35,
 0x5F, 0xE1, 0x38, 0x48, 0xD8, 0x73, 0x95, 0xA4, 0xF7, 0x02, 0x06, 0x0A, 0x1E, 0x22, 0x66, 0xAA,
 0xE5, 0x34, 0x5C, 0xE4, 0x37, 0x59, 0xEB, 0x26, 0x6A, 0xBE, 0xD9, 0x70, 0x90, 0xAB, 0xE6, 0x31,
 0x53, 0xF5, 0x04, 0x0C, 0x14, 0x3C, 0x44, 0xCC, 0x4F, 0xD1, 0x68, 0xB8, 0xD3, 0x6E, 0xB2, 0xCD,
 0x4C, 0xD4, 0x67, 0xA9, 0xE0, 0x3B, 0x4D, 0xD7, 0x62, 0xA6, 0xF1, 0x08, 0x18, 0x28, 0x78, 0x88,
 0x83, 0x9E, 0xB9, 0xD0, 0x6B, 0xBD, 0xDC, 0x7F, 0x81, 0x98, 0xB3, 0xCE, 0x49, 0xDB, 0x76, 0x9A,
 0xB5, 0xC4, 0x57, 0xF9, 0x10, 0x30, 0x50, 0xF0, 0x0B, 0x1D, 0x27, 0x69, 0xBB, 0xD6, 0x61, 0xA3,
 0xFE, 0x19, 0x2B, 0x7D, 0x87, 0x92, 0xAD, 0xEC, 0x2F, 0x71, 0x93, 0xAE, 0xE9, 0x20, 0x60, 0xA0,
 0xFB, 0x16, 0x3A, 0x4E, 0xD2, 0x6D, 0xB7, 0xC2, 0x5D, 0xE7, 0x32, 0x56, 0xFA, 0x15, 0x3F, 0x41,
 0xC3, 0x5E, 0xE2, 0x3D, 0x47, 0xC9, 0x40, 0xC0, 0x5B, 0xED, 0x2C, 0x74, 0x9C, 0xBF, 0xDA, 0x75,
 0x9F, 0xBA, 0xD5, 0x64, 0xAC, 0xEF, 0x2A, 0x7E, 0x82, 0x9D, 0xBC, 0xDF, 0x7A, 0x8E, 0x89, 0x80,
 0x9B, 0xB6, 0xC1, 0x58, 0xE8, 0x23, 0x65, 0xAF, 0xEA, 0x25, 0x6F, 0xB1, 0xC8, 0x43, 0xC5, 0x54,
 0xFC, 0x1F, 0x21, 0x63, 0xA5, 0xF4, 0x07, 0x09, 0x1B, 0x2D, 0x77, 0x99, 0xB0, 0xCB, 0x46, 0xCA,
 0x45, 0xCF, 0x4A, 0xDE, 0x79, 0x8B, 0x86, 0x91, 0xA8, 0xE3, 0x3E, 0x42, 0xC6, 0x51, 0xF3, 0x0E,
 0x12, 0x36, 0x5A, 0xEE, 0x29, 0x7B, 0x8D, 0x8C, 0x8F, 0x8A, 0x85, 0x94, 0xA7, 0xF2, 0x0D, 0x17,
 0x39, 0x4B, 0xDD, 0x7C, 0x84, 0x97, 0xA2, 0xFD, 0x1C, 0x24, 0x6C, 0xB4, 0xC7, 0x52, 0xF6, 0x01};

local LTABLE = {
 [0]=0x00, 0x00, 0x19, 0x01, 0x32, 0x02, 0x1A, 0xC6, 0x4B, 0xC7, 0x1B, 0x68, 0x33, 0xEE, 0xDF, 0x03,
 0x64, 0x04, 0xE0, 0x0E, 0x34, 0x8D, 0x81, 0xEF, 0x4C, 0x71, 0x08, 0xC8, 0xF8, 0x69, 0x1C, 0xC1,
 0x7D, 0xC2, 0x1D, 0xB5, 0xF9, 0xB9, 0x27, 0x6A, 0x4D, 0xE4, 0xA6, 0x72, 0x9A, 0xC9, 0x09, 0x78,
 0x65, 0x2F, 0x8A, 0x05, 0x21, 0x0F, 0xE1, 0x24, 0x12, 0xF0, 0x82, 0x45, 0x35, 0x93, 0xDA, 0x8E,
 0x96, 0x8F, 0xDB, 0xBD, 0x36, 0xD0, 0xCE, 0x94, 0x13, 0x5C, 0xD2, 0xF1, 0x40, 0x46, 0x83, 0x38,
 0x66, 0xDD, 0xFD, 0x30, 0xBF, 0x06, 0x8B, 0x62, 0xB3, 0x25, 0xE2, 0x98, 0x22, 0x88, 0x91, 0x10,
 0x7E, 0x6E, 0x48, 0xC3, 0xA3, 0xB6, 0x1E, 0x42, 0x3A, 0x6B, 0x28, 0x54, 0xFA, 0x85, 0x3D, 0xBA,
 0x2B, 0x79, 0x0A, 0x15, 0x9B, 0x9F, 0x5E, 0xCA, 0x4E, 0xD4, 0xAC, 0xE5, 0xF3, 0x73, 0xA7, 0x57,
 0xAF, 0x58, 0xA8, 0x50, 0xF4, 0xEA, 0xD6, 0x74, 0x4F, 0xAE, 0xE9, 0xD5, 0xE7, 0xE6, 0xAD, 0xE8,
 0x2C, 0xD7, 0x75, 0x7A, 0xEB, 0x16, 0x0B, 0xF5, 0x59, 0xCB, 0x5F, 0xB0, 0x9C, 0xA9, 0x51, 0xA0,
 0x7F, 0x0C, 0xF6, 0x6F, 0x17, 0xC4, 0x49, 0xEC, 0xD8, 0x43, 0x1F, 0x2D, 0xA4, 0x76, 0x7B, 0xB7,
 0xCC, 0xBB, 0x3E, 0x5A, 0xFB, 0x60, 0xB1, 0x86, 0x3B, 0x52, 0xA1, 0x6C, 0xAA, 0x55, 0x29, 0x9D,
 0x97, 0xB2, 0x87, 0x90, 0x61, 0xBE, 0xDC, 0xFC, 0xBC, 0x95, 0xCF, 0xCD, 0x37, 0x3F, 0x5B, 0xD1,
 0x53, 0x39, 0x84, 0x3C, 0x41, 0xA2, 0x6D, 0x47, 0x14, 0x2A, 0x9E, 0x5D, 0x56, 0xF2, 0xD3, 0xAB,
 0x44, 0x11, 0x92, 0xD9, 0x23, 0x20, 0x2E, 0x89, 0xB4, 0x7C, 0xB8, 0x26, 0x77, 0x99, 0xE3, 0xA5,
 0x67, 0x4A, 0xED, 0xDE, 0xC5, 0x31, 0xFE, 0x18, 0x0D, 0x63, 0x8C, 0x80, 0xC0, 0xF7, 0x70, 0x07};

local MIXTABLE = {
 0x02, 0x03, 0x01, 0x01,
 0x01, 0x02, 0x03, 0x01,
 0x01, 0x01, 0x02, 0x03,
 0x03, 0x01, 0x01, 0x02};

local IMIXTABLE = {
 0x0E, 0x0B, 0x0D, 0x09,
 0x09, 0x0E, 0x0B, 0x0D,
 0x0D, 0x09, 0x0E, 0x0B,
 0x0B, 0x0D, 0x09, 0x0E};

local RCON = {
[0] = 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d};


local GMUL = function(A,B)
	if(A == 0x01) then return B; end
	if(B == 0x01) then return A; end
	if(A == 0x00) then return 0; end
	if(B == 0x00) then return 0; end

	local LA = LTABLE[A];
	local LB = LTABLE[B];

	local sum = LA + LB;
	if (sum > 0xFF) then sum = sum - 0xFF; end

	return ETABLE[sum];
end

local byteSub = Array.substitute;

local shiftRow = Array.permute;

local mixCol = function(i,mix)
	local out = {};

	local a,b,c,d;

	a = GMUL(i[ 1],mix[ 1]);
	b = GMUL(i[ 2],mix[ 2]);
	c = GMUL(i[ 3],mix[ 3]);
	d = GMUL(i[ 4],mix[ 4]);
	out[ 1] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 5]);
	b = GMUL(i[ 2],mix[ 6]);
	c = GMUL(i[ 3],mix[ 7]);
	d = GMUL(i[ 4],mix[ 8]);
	out[ 2] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 9]);
	b = GMUL(i[ 2],mix[10]);
	c = GMUL(i[ 3],mix[11]);
	d = GMUL(i[ 4],mix[12]);
	out[ 3] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[13]);
	b = GMUL(i[ 2],mix[14]);
	c = GMUL(i[ 3],mix[15]);
	d = GMUL(i[ 4],mix[16]);
	out[ 4] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 5],mix[ 1]);
	b = GMUL(i[ 6],mix[ 2]);
	c = GMUL(i[ 7],mix[ 3]);
	d = GMUL(i[ 8],mix[ 4]);
	out[ 5] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 5]);
	b = GMUL(i[ 6],mix[ 6]);
	c = GMUL(i[ 7],mix[ 7]);
	d = GMUL(i[ 8],mix[ 8]);
	out[ 6] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 9]);
	b = GMUL(i[ 6],mix[10]);
	c = GMUL(i[ 7],mix[11]);
	d = GMUL(i[ 8],mix[12]);
	out[ 7] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[13]);
	b = GMUL(i[ 6],mix[14]);
	c = GMUL(i[ 7],mix[15]);
	d = GMUL(i[ 8],mix[16]);
	out[ 8] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 9],mix[ 1]);
	b = GMUL(i[10],mix[ 2]);
	c = GMUL(i[11],mix[ 3]);
	d = GMUL(i[12],mix[ 4]);
	out[ 9] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 5]);
	b = GMUL(i[10],mix[ 6]);
	c = GMUL(i[11],mix[ 7]);
	d = GMUL(i[12],mix[ 8]);
	out[10] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 9]);
	b = GMUL(i[10],mix[10]);
	c = GMUL(i[11],mix[11]);
	d = GMUL(i[12],mix[12]);
	out[11] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[13]);
	b = GMUL(i[10],mix[14]);
	c = GMUL(i[11],mix[15]);
	d = GMUL(i[12],mix[16]);
	out[12] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[13],mix[ 1]);
	b = GMUL(i[14],mix[ 2]);
	c = GMUL(i[15],mix[ 3]);
	d = GMUL(i[16],mix[ 4]);
	out[13] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 5]);
	b = GMUL(i[14],mix[ 6]);
	c = GMUL(i[15],mix[ 7]);
	d = GMUL(i[16],mix[ 8]);
	out[14] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 9]);
	b = GMUL(i[14],mix[10]);
	c = GMUL(i[15],mix[11]);
	d = GMUL(i[16],mix[12]);
	out[15] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[13]);
	b = GMUL(i[14],mix[14]);
	c = GMUL(i[15],mix[15]);
	d = GMUL(i[16],mix[16]);
	out[16] = XOR(XOR(a,b),XOR(c,d));

	return out;
end

local keyRound = function(key,round)
	local out = {};

	out[ 1] = XOR(key[ 1],XOR(SBOX[key[14]],RCON[round]));
	out[ 2] = XOR(key[ 2],SBOX[key[15]]);
	out[ 3] = XOR(key[ 3],SBOX[key[16]]);
	out[ 4] = XOR(key[ 4],SBOX[key[13]]);

	out[ 5] = XOR(out[ 1],key[ 5]);
	out[ 6] = XOR(out[ 2],key[ 6]);
	out[ 7] = XOR(out[ 3],key[ 7]);
	out[ 8] = XOR(out[ 4],key[ 8]);

	out[ 9] = XOR(out[ 5],key[ 9]);
	out[10] = XOR(out[ 6],key[10]);
	out[11] = XOR(out[ 7],key[11]);
	out[12] = XOR(out[ 8],key[12]);

	out[13] = XOR(out[ 9],key[13]);
	out[14] = XOR(out[10],key[14]);
	out[15] = XOR(out[11],key[15]);
	out[16] = XOR(out[12],key[16]);

	return out;
end

local keyExpand = function(key)
	local keys = {};

	local temp = key;

	keys[1] = temp;

	for i=1,10 do
		temp = keyRound(temp,i);
		keys[i+1] = temp;
	end

	return keys;

end

local addKey = Array.XOR;



local AES = {};

AES.blockSize = 16;

AES.encrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[1]);

	--round 1
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[2]);

	--round 2
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[3]);

	--round 3
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[4]);

	--round 4
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[5]);

	--round 5
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[6]);

	--round 6
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[7]);

	--round 7
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[8]);

	--round 8
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[9]);

	--round 9
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[10]);

	--round 10
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = addKey(block,key[11]);

	return block;

end

AES.decrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[11]);

	--round 1
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[10]);
	block = mixCol(block,IMIXTABLE);

	--round 2
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[9]);
	block = mixCol(block,IMIXTABLE);

	--round 3
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[8]);
	block = mixCol(block,IMIXTABLE);

	--round 4
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[7]);
	block = mixCol(block,IMIXTABLE);

	--round 5
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[6]);
	block = mixCol(block,IMIXTABLE);

	--round 6
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[5]);
	block = mixCol(block,IMIXTABLE);

	--round 7
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[4]);
	block = mixCol(block,IMIXTABLE);

	--round 8
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[3]);
	block = mixCol(block,IMIXTABLE);

	--round 9
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[2]);
	block = mixCol(block,IMIXTABLE);

	--round 10
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[1]);

	return block;
end

return AES;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/aes192.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");


local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

local SBOX = {
 [0]=0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
 0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
 0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
 0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
 0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
 0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
 0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
 0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
 0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
 0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
 0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
 0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
 0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
 0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
 0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
 0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16};

local ISBOX = {
 [0]=0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
 0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
 0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
 0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
 0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
 0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
 0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
 0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
 0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
 0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
 0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
 0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
 0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
 0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
 0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
 0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D};

local ROW_SHIFT =  {  1,  6, 11, 16,  5, 10, 15,  4,  9, 14,  3,  8, 13,  2,  7, 12,};
local IROW_SHIFT = {  1, 14, 11,  8,  5,  2, 15, 12,  9,  6,  3, 16, 13, 10,  7,  4,};

local ETABLE = {
 [0]=0x01, 0x03, 0x05, 0x0F, 0x11, 0x33, 0x55, 0xFF, 0x1A, 0x2E, 0x72, 0x96, 0xA1, 0xF8, 0x13, 0x35,
 0x5F, 0xE1, 0x38, 0x48, 0xD8, 0x73, 0x95, 0xA4, 0xF7, 0x02, 0x06, 0x0A, 0x1E, 0x22, 0x66, 0xAA,
 0xE5, 0x34, 0x5C, 0xE4, 0x37, 0x59, 0xEB, 0x26, 0x6A, 0xBE, 0xD9, 0x70, 0x90, 0xAB, 0xE6, 0x31,
 0x53, 0xF5, 0x04, 0x0C, 0x14, 0x3C, 0x44, 0xCC, 0x4F, 0xD1, 0x68, 0xB8, 0xD3, 0x6E, 0xB2, 0xCD,
 0x4C, 0xD4, 0x67, 0xA9, 0xE0, 0x3B, 0x4D, 0xD7, 0x62, 0xA6, 0xF1, 0x08, 0x18, 0x28, 0x78, 0x88,
 0x83, 0x9E, 0xB9, 0xD0, 0x6B, 0xBD, 0xDC, 0x7F, 0x81, 0x98, 0xB3, 0xCE, 0x49, 0xDB, 0x76, 0x9A,
 0xB5, 0xC4, 0x57, 0xF9, 0x10, 0x30, 0x50, 0xF0, 0x0B, 0x1D, 0x27, 0x69, 0xBB, 0xD6, 0x61, 0xA3,
 0xFE, 0x19, 0x2B, 0x7D, 0x87, 0x92, 0xAD, 0xEC, 0x2F, 0x71, 0x93, 0xAE, 0xE9, 0x20, 0x60, 0xA0,
 0xFB, 0x16, 0x3A, 0x4E, 0xD2, 0x6D, 0xB7, 0xC2, 0x5D, 0xE7, 0x32, 0x56, 0xFA, 0x15, 0x3F, 0x41,
 0xC3, 0x5E, 0xE2, 0x3D, 0x47, 0xC9, 0x40, 0xC0, 0x5B, 0xED, 0x2C, 0x74, 0x9C, 0xBF, 0xDA, 0x75,
 0x9F, 0xBA, 0xD5, 0x64, 0xAC, 0xEF, 0x2A, 0x7E, 0x82, 0x9D, 0xBC, 0xDF, 0x7A, 0x8E, 0x89, 0x80,
 0x9B, 0xB6, 0xC1, 0x58, 0xE8, 0x23, 0x65, 0xAF, 0xEA, 0x25, 0x6F, 0xB1, 0xC8, 0x43, 0xC5, 0x54,
 0xFC, 0x1F, 0x21, 0x63, 0xA5, 0xF4, 0x07, 0x09, 0x1B, 0x2D, 0x77, 0x99, 0xB0, 0xCB, 0x46, 0xCA,
 0x45, 0xCF, 0x4A, 0xDE, 0x79, 0x8B, 0x86, 0x91, 0xA8, 0xE3, 0x3E, 0x42, 0xC6, 0x51, 0xF3, 0x0E,
 0x12, 0x36, 0x5A, 0xEE, 0x29, 0x7B, 0x8D, 0x8C, 0x8F, 0x8A, 0x85, 0x94, 0xA7, 0xF2, 0x0D, 0x17,
 0x39, 0x4B, 0xDD, 0x7C, 0x84, 0x97, 0xA2, 0xFD, 0x1C, 0x24, 0x6C, 0xB4, 0xC7, 0x52, 0xF6, 0x01};

local LTABLE = {
 [0]=0x00, 0x00, 0x19, 0x01, 0x32, 0x02, 0x1A, 0xC6, 0x4B, 0xC7, 0x1B, 0x68, 0x33, 0xEE, 0xDF, 0x03,
 0x64, 0x04, 0xE0, 0x0E, 0x34, 0x8D, 0x81, 0xEF, 0x4C, 0x71, 0x08, 0xC8, 0xF8, 0x69, 0x1C, 0xC1,
 0x7D, 0xC2, 0x1D, 0xB5, 0xF9, 0xB9, 0x27, 0x6A, 0x4D, 0xE4, 0xA6, 0x72, 0x9A, 0xC9, 0x09, 0x78,
 0x65, 0x2F, 0x8A, 0x05, 0x21, 0x0F, 0xE1, 0x24, 0x12, 0xF0, 0x82, 0x45, 0x35, 0x93, 0xDA, 0x8E,
 0x96, 0x8F, 0xDB, 0xBD, 0x36, 0xD0, 0xCE, 0x94, 0x13, 0x5C, 0xD2, 0xF1, 0x40, 0x46, 0x83, 0x38,
 0x66, 0xDD, 0xFD, 0x30, 0xBF, 0x06, 0x8B, 0x62, 0xB3, 0x25, 0xE2, 0x98, 0x22, 0x88, 0x91, 0x10,
 0x7E, 0x6E, 0x48, 0xC3, 0xA3, 0xB6, 0x1E, 0x42, 0x3A, 0x6B, 0x28, 0x54, 0xFA, 0x85, 0x3D, 0xBA,
 0x2B, 0x79, 0x0A, 0x15, 0x9B, 0x9F, 0x5E, 0xCA, 0x4E, 0xD4, 0xAC, 0xE5, 0xF3, 0x73, 0xA7, 0x57,
 0xAF, 0x58, 0xA8, 0x50, 0xF4, 0xEA, 0xD6, 0x74, 0x4F, 0xAE, 0xE9, 0xD5, 0xE7, 0xE6, 0xAD, 0xE8,
 0x2C, 0xD7, 0x75, 0x7A, 0xEB, 0x16, 0x0B, 0xF5, 0x59, 0xCB, 0x5F, 0xB0, 0x9C, 0xA9, 0x51, 0xA0,
 0x7F, 0x0C, 0xF6, 0x6F, 0x17, 0xC4, 0x49, 0xEC, 0xD8, 0x43, 0x1F, 0x2D, 0xA4, 0x76, 0x7B, 0xB7,
 0xCC, 0xBB, 0x3E, 0x5A, 0xFB, 0x60, 0xB1, 0x86, 0x3B, 0x52, 0xA1, 0x6C, 0xAA, 0x55, 0x29, 0x9D,
 0x97, 0xB2, 0x87, 0x90, 0x61, 0xBE, 0xDC, 0xFC, 0xBC, 0x95, 0xCF, 0xCD, 0x37, 0x3F, 0x5B, 0xD1,
 0x53, 0x39, 0x84, 0x3C, 0x41, 0xA2, 0x6D, 0x47, 0x14, 0x2A, 0x9E, 0x5D, 0x56, 0xF2, 0xD3, 0xAB,
 0x44, 0x11, 0x92, 0xD9, 0x23, 0x20, 0x2E, 0x89, 0xB4, 0x7C, 0xB8, 0x26, 0x77, 0x99, 0xE3, 0xA5,
 0x67, 0x4A, 0xED, 0xDE, 0xC5, 0x31, 0xFE, 0x18, 0x0D, 0x63, 0x8C, 0x80, 0xC0, 0xF7, 0x70, 0x07};

local MIXTABLE = {
 0x02, 0x03, 0x01, 0x01,
 0x01, 0x02, 0x03, 0x01,
 0x01, 0x01, 0x02, 0x03,
 0x03, 0x01, 0x01, 0x02};

local IMIXTABLE = {
 0x0E, 0x0B, 0x0D, 0x09,
 0x09, 0x0E, 0x0B, 0x0D,
 0x0D, 0x09, 0x0E, 0x0B,
 0x0B, 0x0D, 0x09, 0x0E};

local RCON = {
[0] = 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d};


local GMUL = function(A,B)
	if(A == 0x01) then return B; end
	if(B == 0x01) then return A; end
	if(A == 0x00) then return 0; end
	if(B == 0x00) then return 0; end

	local LA = LTABLE[A];
	local LB = LTABLE[B];

	local sum = LA + LB;
	if (sum > 0xFF) then sum = sum - 0xFF; end

	return ETABLE[sum];
end

local byteSub = Array.substitute;

local shiftRow = Array.permute;

local mixCol = function(i,mix)
	local out = {};

	local a,b,c,d;

	a = GMUL(i[ 1],mix[ 1]);
	b = GMUL(i[ 2],mix[ 2]);
	c = GMUL(i[ 3],mix[ 3]);
	d = GMUL(i[ 4],mix[ 4]);
	out[ 1] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 5]);
	b = GMUL(i[ 2],mix[ 6]);
	c = GMUL(i[ 3],mix[ 7]);
	d = GMUL(i[ 4],mix[ 8]);
	out[ 2] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 9]);
	b = GMUL(i[ 2],mix[10]);
	c = GMUL(i[ 3],mix[11]);
	d = GMUL(i[ 4],mix[12]);
	out[ 3] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[13]);
	b = GMUL(i[ 2],mix[14]);
	c = GMUL(i[ 3],mix[15]);
	d = GMUL(i[ 4],mix[16]);
	out[ 4] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 5],mix[ 1]);
	b = GMUL(i[ 6],mix[ 2]);
	c = GMUL(i[ 7],mix[ 3]);
	d = GMUL(i[ 8],mix[ 4]);
	out[ 5] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 5]);
	b = GMUL(i[ 6],mix[ 6]);
	c = GMUL(i[ 7],mix[ 7]);
	d = GMUL(i[ 8],mix[ 8]);
	out[ 6] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 9]);
	b = GMUL(i[ 6],mix[10]);
	c = GMUL(i[ 7],mix[11]);
	d = GMUL(i[ 8],mix[12]);
	out[ 7] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[13]);
	b = GMUL(i[ 6],mix[14]);
	c = GMUL(i[ 7],mix[15]);
	d = GMUL(i[ 8],mix[16]);
	out[ 8] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 9],mix[ 1]);
	b = GMUL(i[10],mix[ 2]);
	c = GMUL(i[11],mix[ 3]);
	d = GMUL(i[12],mix[ 4]);
	out[ 9] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 5]);
	b = GMUL(i[10],mix[ 6]);
	c = GMUL(i[11],mix[ 7]);
	d = GMUL(i[12],mix[ 8]);
	out[10] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 9]);
	b = GMUL(i[10],mix[10]);
	c = GMUL(i[11],mix[11]);
	d = GMUL(i[12],mix[12]);
	out[11] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[13]);
	b = GMUL(i[10],mix[14]);
	c = GMUL(i[11],mix[15]);
	d = GMUL(i[12],mix[16]);
	out[12] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[13],mix[ 1]);
	b = GMUL(i[14],mix[ 2]);
	c = GMUL(i[15],mix[ 3]);
	d = GMUL(i[16],mix[ 4]);
	out[13] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 5]);
	b = GMUL(i[14],mix[ 6]);
	c = GMUL(i[15],mix[ 7]);
	d = GMUL(i[16],mix[ 8]);
	out[14] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 9]);
	b = GMUL(i[14],mix[10]);
	c = GMUL(i[15],mix[11]);
	d = GMUL(i[16],mix[12]);
	out[15] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[13]);
	b = GMUL(i[14],mix[14]);
	c = GMUL(i[15],mix[15]);
	d = GMUL(i[16],mix[16]);
	out[16] = XOR(XOR(a,b),XOR(c,d));

	return out;
end

local keyRound = function(key,round)
	local i=(round-1)*24;
	local out=key;

	out[25+i] = XOR(key[ 1+i],XOR(SBOX[key[22+i]],RCON[round]));
	out[26+i] = XOR(key[ 2+i],SBOX[key[23+i]]);
	out[27+i] = XOR(key[ 3+i],SBOX[key[24+i]]);
	out[28+i] = XOR(key[ 4+i],SBOX[key[21+i]]);

	out[29+i] = XOR(out[25+i],key[ 5+i]);
	out[30+i] = XOR(out[26+i],key[ 6+i]);
	out[31+i] = XOR(out[27+i],key[ 7+i]);
	out[32+i] = XOR(out[28+i],key[ 8+i]);

	out[33+i] = XOR(out[29+i],key[ 9+i]);
	out[34+i] = XOR(out[30+i],key[10+i]);
	out[35+i] = XOR(out[31+i],key[11+i]);
	out[36+i] = XOR(out[32+i],key[12+i]);

	out[37+i] = XOR(out[33+i],key[13+i]);
	out[38+i] = XOR(out[34+i],key[14+i]);
	out[39+i] = XOR(out[35+i],key[15+i]);
	out[40+i] = XOR(out[36+i],key[16+i]);

	out[41+i] = XOR(out[37+i],key[17+i]);
	out[42+i] = XOR(out[38+i],key[18+i]);
	out[43+i] = XOR(out[39+i],key[19+i]);
	out[44+i] = XOR(out[40+i],key[20+i]);

	out[45+i] = XOR(out[41+i],key[21+i]);
	out[46+i] = XOR(out[42+i],key[22+i]);
	out[47+i] = XOR(out[43+i],key[23+i]);
	out[48+i] = XOR(out[44+i],key[24+i]);

	return out;
end

local keyExpand = function(key)
	local bytes = Array.copy(key);

	for i=1,8 do
		keyRound(bytes,i);
	end

	local keys = {};

	keys[ 1] = Array.slice(bytes,1,16);
	keys[ 2] = Array.slice(bytes,17,32);
	keys[ 3] = Array.slice(bytes,33,48);
	keys[ 4] = Array.slice(bytes,49,64);
	keys[ 5] = Array.slice(bytes,65,80);
	keys[ 6] = Array.slice(bytes,81,96);
	keys[ 7] = Array.slice(bytes,97,112);
	keys[ 8] = Array.slice(bytes,113,128);
	keys[ 9] = Array.slice(bytes,129,144);
	keys[10] = Array.slice(bytes,145,160);
	keys[11] = Array.slice(bytes,161,176);
	keys[12] = Array.slice(bytes,177,192);
	keys[13] = Array.slice(bytes,193,208);

	return keys;

end

local addKey = Array.XOR;



local AES = {};

AES.blockSize = 16;

AES.encrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[1]);

	--round 1
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[2]);

	--round 2
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[3]);

	--round 3
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[4]);

	--round 4
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[5]);

	--round 5
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[6]);

	--round 6
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[7]);

	--round 7
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[8]);

	--round 8
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[9]);

	--round 9
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[10]);

	--round 10
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[11]);

	--round 11
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[12]);

	--round 12
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = addKey(block,key[13]);

	return block;

end

AES.decrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[13]);

	--round 1
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[12]);
	block = mixCol(block,IMIXTABLE);

	--round 2
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[11]);
	block = mixCol(block,IMIXTABLE);

	--round 3
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[10]);
	block = mixCol(block,IMIXTABLE);

	--round 4
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[9]);
	block = mixCol(block,IMIXTABLE);

	--round 5
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[8]);
	block = mixCol(block,IMIXTABLE);

	--round 6
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[7]);
	block = mixCol(block,IMIXTABLE);

	--round 7
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[6]);
	block = mixCol(block,IMIXTABLE);

	--round 8
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[5]);
	block = mixCol(block,IMIXTABLE);

	--round 9
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[4]);
	block = mixCol(block,IMIXTABLE);

	--round 10
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[3]);
	block = mixCol(block,IMIXTABLE);

	--round 11
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[2]);
	block = mixCol(block,IMIXTABLE);

	--round 12
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[1]);

	return block;
end

return AES;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/aes256.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");


local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

local SBOX = {
 [0]=0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
 0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
 0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
 0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
 0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
 0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
 0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
 0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
 0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
 0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
 0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
 0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
 0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
 0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
 0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
 0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16};

local ISBOX = {
 [0]=0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
 0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
 0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
 0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
 0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
 0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
 0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
 0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
 0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
 0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
 0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
 0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
 0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
 0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
 0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
 0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D};

local ROW_SHIFT =  {  1,  6, 11, 16,  5, 10, 15,  4,  9, 14,  3,  8, 13,  2,  7, 12,};
local IROW_SHIFT = {  1, 14, 11,  8,  5,  2, 15, 12,  9,  6,  3, 16, 13, 10,  7,  4,};

local ETABLE = {
 [0]=0x01, 0x03, 0x05, 0x0F, 0x11, 0x33, 0x55, 0xFF, 0x1A, 0x2E, 0x72, 0x96, 0xA1, 0xF8, 0x13, 0x35,
 0x5F, 0xE1, 0x38, 0x48, 0xD8, 0x73, 0x95, 0xA4, 0xF7, 0x02, 0x06, 0x0A, 0x1E, 0x22, 0x66, 0xAA,
 0xE5, 0x34, 0x5C, 0xE4, 0x37, 0x59, 0xEB, 0x26, 0x6A, 0xBE, 0xD9, 0x70, 0x90, 0xAB, 0xE6, 0x31,
 0x53, 0xF5, 0x04, 0x0C, 0x14, 0x3C, 0x44, 0xCC, 0x4F, 0xD1, 0x68, 0xB8, 0xD3, 0x6E, 0xB2, 0xCD,
 0x4C, 0xD4, 0x67, 0xA9, 0xE0, 0x3B, 0x4D, 0xD7, 0x62, 0xA6, 0xF1, 0x08, 0x18, 0x28, 0x78, 0x88,
 0x83, 0x9E, 0xB9, 0xD0, 0x6B, 0xBD, 0xDC, 0x7F, 0x81, 0x98, 0xB3, 0xCE, 0x49, 0xDB, 0x76, 0x9A,
 0xB5, 0xC4, 0x57, 0xF9, 0x10, 0x30, 0x50, 0xF0, 0x0B, 0x1D, 0x27, 0x69, 0xBB, 0xD6, 0x61, 0xA3,
 0xFE, 0x19, 0x2B, 0x7D, 0x87, 0x92, 0xAD, 0xEC, 0x2F, 0x71, 0x93, 0xAE, 0xE9, 0x20, 0x60, 0xA0,
 0xFB, 0x16, 0x3A, 0x4E, 0xD2, 0x6D, 0xB7, 0xC2, 0x5D, 0xE7, 0x32, 0x56, 0xFA, 0x15, 0x3F, 0x41,
 0xC3, 0x5E, 0xE2, 0x3D, 0x47, 0xC9, 0x40, 0xC0, 0x5B, 0xED, 0x2C, 0x74, 0x9C, 0xBF, 0xDA, 0x75,
 0x9F, 0xBA, 0xD5, 0x64, 0xAC, 0xEF, 0x2A, 0x7E, 0x82, 0x9D, 0xBC, 0xDF, 0x7A, 0x8E, 0x89, 0x80,
 0x9B, 0xB6, 0xC1, 0x58, 0xE8, 0x23, 0x65, 0xAF, 0xEA, 0x25, 0x6F, 0xB1, 0xC8, 0x43, 0xC5, 0x54,
 0xFC, 0x1F, 0x21, 0x63, 0xA5, 0xF4, 0x07, 0x09, 0x1B, 0x2D, 0x77, 0x99, 0xB0, 0xCB, 0x46, 0xCA,
 0x45, 0xCF, 0x4A, 0xDE, 0x79, 0x8B, 0x86, 0x91, 0xA8, 0xE3, 0x3E, 0x42, 0xC6, 0x51, 0xF3, 0x0E,
 0x12, 0x36, 0x5A, 0xEE, 0x29, 0x7B, 0x8D, 0x8C, 0x8F, 0x8A, 0x85, 0x94, 0xA7, 0xF2, 0x0D, 0x17,
 0x39, 0x4B, 0xDD, 0x7C, 0x84, 0x97, 0xA2, 0xFD, 0x1C, 0x24, 0x6C, 0xB4, 0xC7, 0x52, 0xF6, 0x01};

local LTABLE = {
 [0]=0x00, 0x00, 0x19, 0x01, 0x32, 0x02, 0x1A, 0xC6, 0x4B, 0xC7, 0x1B, 0x68, 0x33, 0xEE, 0xDF, 0x03,
 0x64, 0x04, 0xE0, 0x0E, 0x34, 0x8D, 0x81, 0xEF, 0x4C, 0x71, 0x08, 0xC8, 0xF8, 0x69, 0x1C, 0xC1,
 0x7D, 0xC2, 0x1D, 0xB5, 0xF9, 0xB9, 0x27, 0x6A, 0x4D, 0xE4, 0xA6, 0x72, 0x9A, 0xC9, 0x09, 0x78,
 0x65, 0x2F, 0x8A, 0x05, 0x21, 0x0F, 0xE1, 0x24, 0x12, 0xF0, 0x82, 0x45, 0x35, 0x93, 0xDA, 0x8E,
 0x96, 0x8F, 0xDB, 0xBD, 0x36, 0xD0, 0xCE, 0x94, 0x13, 0x5C, 0xD2, 0xF1, 0x40, 0x46, 0x83, 0x38,
 0x66, 0xDD, 0xFD, 0x30, 0xBF, 0x06, 0x8B, 0x62, 0xB3, 0x25, 0xE2, 0x98, 0x22, 0x88, 0x91, 0x10,
 0x7E, 0x6E, 0x48, 0xC3, 0xA3, 0xB6, 0x1E, 0x42, 0x3A, 0x6B, 0x28, 0x54, 0xFA, 0x85, 0x3D, 0xBA,
 0x2B, 0x79, 0x0A, 0x15, 0x9B, 0x9F, 0x5E, 0xCA, 0x4E, 0xD4, 0xAC, 0xE5, 0xF3, 0x73, 0xA7, 0x57,
 0xAF, 0x58, 0xA8, 0x50, 0xF4, 0xEA, 0xD6, 0x74, 0x4F, 0xAE, 0xE9, 0xD5, 0xE7, 0xE6, 0xAD, 0xE8,
 0x2C, 0xD7, 0x75, 0x7A, 0xEB, 0x16, 0x0B, 0xF5, 0x59, 0xCB, 0x5F, 0xB0, 0x9C, 0xA9, 0x51, 0xA0,
 0x7F, 0x0C, 0xF6, 0x6F, 0x17, 0xC4, 0x49, 0xEC, 0xD8, 0x43, 0x1F, 0x2D, 0xA4, 0x76, 0x7B, 0xB7,
 0xCC, 0xBB, 0x3E, 0x5A, 0xFB, 0x60, 0xB1, 0x86, 0x3B, 0x52, 0xA1, 0x6C, 0xAA, 0x55, 0x29, 0x9D,
 0x97, 0xB2, 0x87, 0x90, 0x61, 0xBE, 0xDC, 0xFC, 0xBC, 0x95, 0xCF, 0xCD, 0x37, 0x3F, 0x5B, 0xD1,
 0x53, 0x39, 0x84, 0x3C, 0x41, 0xA2, 0x6D, 0x47, 0x14, 0x2A, 0x9E, 0x5D, 0x56, 0xF2, 0xD3, 0xAB,
 0x44, 0x11, 0x92, 0xD9, 0x23, 0x20, 0x2E, 0x89, 0xB4, 0x7C, 0xB8, 0x26, 0x77, 0x99, 0xE3, 0xA5,
 0x67, 0x4A, 0xED, 0xDE, 0xC5, 0x31, 0xFE, 0x18, 0x0D, 0x63, 0x8C, 0x80, 0xC0, 0xF7, 0x70, 0x07};

local MIXTABLE = {
 0x02, 0x03, 0x01, 0x01,
 0x01, 0x02, 0x03, 0x01,
 0x01, 0x01, 0x02, 0x03,
 0x03, 0x01, 0x01, 0x02};

local IMIXTABLE = {
 0x0E, 0x0B, 0x0D, 0x09,
 0x09, 0x0E, 0x0B, 0x0D,
 0x0D, 0x09, 0x0E, 0x0B,
 0x0B, 0x0D, 0x09, 0x0E};

local RCON = {
[0] = 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d};


local GMUL = function(A,B)
	if(A == 0x01) then return B; end
	if(B == 0x01) then return A; end
	if(A == 0x00) then return 0; end
	if(B == 0x00) then return 0; end

	local LA = LTABLE[A];
	local LB = LTABLE[B];

	local sum = LA + LB;
	if (sum > 0xFF) then sum = sum - 0xFF; end

	return ETABLE[sum];
end

local byteSub = Array.substitute;

local shiftRow = Array.permute;

local mixCol = function(i,mix)
	local out = {};

	local a,b,c,d;

	a = GMUL(i[ 1],mix[ 1]);
	b = GMUL(i[ 2],mix[ 2]);
	c = GMUL(i[ 3],mix[ 3]);
	d = GMUL(i[ 4],mix[ 4]);
	out[ 1] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 5]);
	b = GMUL(i[ 2],mix[ 6]);
	c = GMUL(i[ 3],mix[ 7]);
	d = GMUL(i[ 4],mix[ 8]);
	out[ 2] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[ 9]);
	b = GMUL(i[ 2],mix[10]);
	c = GMUL(i[ 3],mix[11]);
	d = GMUL(i[ 4],mix[12]);
	out[ 3] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 1],mix[13]);
	b = GMUL(i[ 2],mix[14]);
	c = GMUL(i[ 3],mix[15]);
	d = GMUL(i[ 4],mix[16]);
	out[ 4] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 5],mix[ 1]);
	b = GMUL(i[ 6],mix[ 2]);
	c = GMUL(i[ 7],mix[ 3]);
	d = GMUL(i[ 8],mix[ 4]);
	out[ 5] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 5]);
	b = GMUL(i[ 6],mix[ 6]);
	c = GMUL(i[ 7],mix[ 7]);
	d = GMUL(i[ 8],mix[ 8]);
	out[ 6] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[ 9]);
	b = GMUL(i[ 6],mix[10]);
	c = GMUL(i[ 7],mix[11]);
	d = GMUL(i[ 8],mix[12]);
	out[ 7] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 5],mix[13]);
	b = GMUL(i[ 6],mix[14]);
	c = GMUL(i[ 7],mix[15]);
	d = GMUL(i[ 8],mix[16]);
	out[ 8] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[ 9],mix[ 1]);
	b = GMUL(i[10],mix[ 2]);
	c = GMUL(i[11],mix[ 3]);
	d = GMUL(i[12],mix[ 4]);
	out[ 9] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 5]);
	b = GMUL(i[10],mix[ 6]);
	c = GMUL(i[11],mix[ 7]);
	d = GMUL(i[12],mix[ 8]);
	out[10] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[ 9]);
	b = GMUL(i[10],mix[10]);
	c = GMUL(i[11],mix[11]);
	d = GMUL(i[12],mix[12]);
	out[11] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[ 9],mix[13]);
	b = GMUL(i[10],mix[14]);
	c = GMUL(i[11],mix[15]);
	d = GMUL(i[12],mix[16]);
	out[12] = XOR(XOR(a,b),XOR(c,d));


	a = GMUL(i[13],mix[ 1]);
	b = GMUL(i[14],mix[ 2]);
	c = GMUL(i[15],mix[ 3]);
	d = GMUL(i[16],mix[ 4]);
	out[13] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 5]);
	b = GMUL(i[14],mix[ 6]);
	c = GMUL(i[15],mix[ 7]);
	d = GMUL(i[16],mix[ 8]);
	out[14] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[ 9]);
	b = GMUL(i[14],mix[10]);
	c = GMUL(i[15],mix[11]);
	d = GMUL(i[16],mix[12]);
	out[15] = XOR(XOR(a,b),XOR(c,d));
	a = GMUL(i[13],mix[13]);
	b = GMUL(i[14],mix[14]);
	c = GMUL(i[15],mix[15]);
	d = GMUL(i[16],mix[16]);
	out[16] = XOR(XOR(a,b),XOR(c,d));

	return out;
end

local keyRound = function(key,round)
	local i=(round-1)*32;
	local out=key;

	out[33+i] = XOR(key[ 1+i],XOR(SBOX[key[30+i]],RCON[round]));
	out[34+i] = XOR(key[ 2+i],SBOX[key[31+i]]);
	out[35+i] = XOR(key[ 3+i],SBOX[key[32+i]]);
	out[36+i] = XOR(key[ 4+i],SBOX[key[29+i]]);

	out[37+i] = XOR(out[33+i],key[ 5+i]);
	out[38+i] = XOR(out[34+i],key[ 6+i]);
	out[39+i] = XOR(out[35+i],key[ 7+i]);
	out[40+i] = XOR(out[36+i],key[ 8+i]);

	out[41+i] = XOR(out[37+i],key[ 9+i]);
	out[42+i] = XOR(out[38+i],key[10+i]);
	out[43+i] = XOR(out[39+i],key[11+i]);
	out[44+i] = XOR(out[40+i],key[12+i]);

	out[45+i] = XOR(out[41+i],key[13+i]);
	out[46+i] = XOR(out[42+i],key[14+i]);
	out[47+i] = XOR(out[43+i],key[15+i]);
	out[48+i] = XOR(out[44+i],key[16+i]);


	out[49+i] = XOR(SBOX[out[45+i]],key[17+i]);
	out[50+i] = XOR(SBOX[out[46+i]],key[18+i]);
	out[51+i] = XOR(SBOX[out[47+i]],key[19+i]);
	out[52+i] = XOR(SBOX[out[48+i]],key[20+i]);

	out[53+i] = XOR(out[49+i],key[21+i]);
	out[54+i] = XOR(out[50+i],key[22+i]);
	out[55+i] = XOR(out[51+i],key[23+i]);
	out[56+i] = XOR(out[52+i],key[24+i]);

	out[57+i] = XOR(out[53+i],key[25+i]);
	out[58+i] = XOR(out[54+i],key[26+i]);
	out[59+i] = XOR(out[55+i],key[27+i]);
	out[60+i] = XOR(out[56+i],key[28+i]);

	out[61+i] = XOR(out[57+i],key[29+i]);
	out[62+i] = XOR(out[58+i],key[30+i]);
	out[63+i] = XOR(out[59+i],key[31+i]);
	out[64+i] = XOR(out[60+i],key[32+i]);

	return out;
end

local keyExpand = function(key)
	local bytes = Array.copy(key);

	for i=1,7 do
		keyRound(bytes,i);
	end

	local keys = {};

	keys[ 1] = Array.slice(bytes,1,16);
	keys[ 2] = Array.slice(bytes,17,32);
	keys[ 3] = Array.slice(bytes,33,48);
	keys[ 4] = Array.slice(bytes,49,64);
	keys[ 5] = Array.slice(bytes,65,80);
	keys[ 6] = Array.slice(bytes,81,96);
	keys[ 7] = Array.slice(bytes,97,112);
	keys[ 8] = Array.slice(bytes,113,128);
	keys[ 9] = Array.slice(bytes,129,144);
	keys[10] = Array.slice(bytes,145,160);
	keys[11] = Array.slice(bytes,161,176);
	keys[12] = Array.slice(bytes,177,192);
	keys[13] = Array.slice(bytes,193,208);
	keys[14] = Array.slice(bytes,209,224);
	keys[15] = Array.slice(bytes,225,240);

	return keys;

end

local addKey = Array.XOR;



local AES = {};

AES.blockSize = 16;

AES.encrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[1]);

	--round 1
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[2]);

	--round 2
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[3]);

	--round 3
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[4]);

	--round 4
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[5]);

	--round 5
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[6]);

	--round 6
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[7]);

	--round 7
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[8]);

	--round 8
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[9]);

	--round 9
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[10]);

	--round 10
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[11]);

	--round 11
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[12]);

	--round 12
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[13]);

	--round 13
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = mixCol(block,MIXTABLE);
	block = addKey(block,key[14]);

	--round 14
	block = byteSub(block,SBOX);
	block = shiftRow(block,ROW_SHIFT);
	block = addKey(block,key[15]);

	return block;

end

AES.decrypt = function(key,block)

	local key = keyExpand(key);

	--round 0
	block = addKey(block,key[15]);

	--round 1
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[14]);
	block = mixCol(block,IMIXTABLE);

	--round 2
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[13]);
	block = mixCol(block,IMIXTABLE);

	--round 3
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[12]);
	block = mixCol(block,IMIXTABLE);

	--round 4
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[11]);
	block = mixCol(block,IMIXTABLE);

	--round 5
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[10]);
	block = mixCol(block,IMIXTABLE);

	--round 6
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[9]);
	block = mixCol(block,IMIXTABLE);

	--round 7
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[8]);
	block = mixCol(block,IMIXTABLE);

	--round 8
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[7]);
	block = mixCol(block,IMIXTABLE);

	--round 9
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[6]);
	block = mixCol(block,IMIXTABLE);

	--round 10
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[5]);
	block = mixCol(block,IMIXTABLE);

	--round 11
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[4]);
	block = mixCol(block,IMIXTABLE);

	--round 12
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[3]);
	block = mixCol(block,IMIXTABLE);

	--round 13
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[2]);
	block = mixCol(block,IMIXTABLE);

	--round 14
	block = shiftRow(block,IROW_SHIFT);
	block = byteSub(block,ISBOX);
	block = addKey(block,key[1]);

	return block;
end

return AES;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/des.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");


local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

local IN_P = {	58, 50, 42, 34, 26, 18, 10,  2,
				60, 52, 44, 36, 28, 20, 12,  4,
				62, 54, 46, 38, 30, 22, 14,  6,
				64, 56, 48, 40, 32, 24, 16,  8,
				57, 49, 41, 33, 25, 17,  9,  1,
				59, 51, 43, 35, 27, 19, 11,  3,
				61, 53, 45, 37, 29, 21, 13,  5,
				63, 55, 47, 39, 31, 23, 15,  7};

local OUT_P = {	40,  8, 48, 16, 56, 24, 64, 32,
				39,  7, 47, 15, 55, 23, 63, 31,
				38,  6, 46, 14, 54, 22, 62, 30,
				37,  5, 45, 13, 53, 21, 61, 29,
				36,  4, 44, 12, 52, 20, 60, 28,
				35,  3, 43, 11, 51, 19, 59, 27,
				34,  2, 42, 10, 50, 18, 58, 26,
				33,  1, 41,  9, 49, 17, 57, 25};

-- add 32 to each because we do the expansion on the full LR table, not just R
local EBIT = {	32+32,  1+32,  2+32,  3+32,  4+32,  5+32,  4+32,  5+32,  6+32,  7+32,  8+32,  9+32,
				 8+32,  9+32, 10+32, 11+32, 12+32, 13+32, 12+32, 13+32, 14+32, 15+32, 16+32, 17+32,
				16+32, 17+32, 18+32, 19+32, 20+32, 21+32, 20+32, 21+32, 22+32, 23+32, 24+32, 25+32,
				24+32, 25+32, 26+32, 27+32, 28+32, 29+32, 28+32, 29+32, 30+32, 31+32, 32+32,  1+32, };

local LR_SWAP = {	33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,
					49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
					 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,
					17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};

local PC1 = {	57,49,41,33,25,17, 9, 1,58,50,42,34,26,18,
				10, 2,59,51,43,35,27,19,11, 3,60,52,44,36,
				63,55,47,39,31,23,15, 7,62,54,46,38,30,22,
				14, 6,61,53,45,37,29,21,13, 5,28,20,12, 4};

local PC2 = {	14,17,11,24, 1, 5, 3,28,15, 6,21,10,
                23,19,12, 4,26, 8,16, 7,27,20,13, 2,
                41,52,31,37,47,55,30,40,51,45,33,48,
                44,49,39,56,34,53,46,42,50,36,29,32};

local KS1 = {	 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28, 1,
				30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,29};
local KS2 = KS1;

local KS3 = {	 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28, 1, 2,
				31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,29,30};

local KS4  = KS3;
local KS5  = KS3;
local KS6  = KS3;
local KS7  = KS3;
local KS8  = KS3;
local KS9  = KS1;
local KS10 = KS3;
local KS11 = KS3;
local KS12 = KS3;
local KS13 = KS3;
local KS14 = KS3;
local KS15 = KS3;
local KS16 = KS1;


local SIND1 = {    2,   3,   4,   5,   1,   6 };
local SIND2 = {  2+6, 3+6, 4+6, 5+6, 1+6, 6+6 };
local SIND3 = { 2+12,3+12,4+12,5+12,1+12,6+12 };
local SIND4 = { 2+18,3+18,4+18,5+18,1+18,6+18 };
local SIND5 = { 2+24,3+24,4+24,5+24,1+24,6+24 };
local SIND6 = { 2+30,3+30,4+30,5+30,1+30,6+30 };
local SIND7 = { 2+36,3+36,4+36,5+36,1+36,6+36 };
local SIND8 = { 2+42,3+42,4+42,5+42,1+42,6+42 };

local SBOX1 = {	14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
		 		0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
		 		4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
		 		15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13};

local SBOX2 = {	15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
		 		3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
		 		0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
		 		13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9};

local SBOX3 = {	10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
				13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
				13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
				1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12};

local SBOX4 = {	7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
				13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
				10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
				3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14};

local SBOX5 = {	2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
				14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
				4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
				11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3};

local SBOX6 = {	12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
				10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
				9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
				4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13};

local SBOX7 = {	4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
				13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
				1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
				6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12};

local SBOX8 = {	13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
				1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
				7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
				2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11};

local ROUND_P = {	16, 7,20,21,29,12,28,17, 1,15,23,26, 5,18,31,10,
					 2, 8,24,14,32,27, 3, 9,19,13,30, 6,22,11, 4,25};

local permute = Array.permute;

local unpackBytes = function(bytes)
	local bits = {};

	for k,b in pairs(bytes) do
		table.insert(bits,RSHIFT(AND(b,0x80),7));
		table.insert(bits,RSHIFT(AND(b,0x40),6));
		table.insert(bits,RSHIFT(AND(b,0x20),5));
		table.insert(bits,RSHIFT(AND(b,0x10),4));
		table.insert(bits,RSHIFT(AND(b,0x08),3));
		table.insert(bits,RSHIFT(AND(b,0x04),2));
		table.insert(bits,RSHIFT(AND(b,0x02),1));
		table.insert(bits,      AND(b,0x01)   );
	end

	return bits;
end

local packBytes = function(bits)
	local bytes = {}

	for k,v in pairs(bits) do
		local index = Math.floor((k-1)/8) + 1;
		local shift = 7-Math.fmod((k-1),8);

		local bit = bits[k];
		local byte = bytes[index];

		if not byte then byte = 0x00; end
		byte = OR(byte,LSHIFT(bit,shift));
		bytes[index] = byte;
	end

	return bytes;
end

local mix = function(LR,key)

	local ER = permute(LR,EBIT);

	for k,v in pairs(ER) do
		ER[k] = XOR(ER[k],key[k]);
	end

	local FRK = {};

	local S = 0x00;
	S = OR(S,ER[1]); S = LSHIFT(S,1);
	S = OR(S,ER[6]); S = LSHIFT(S,1);
	S = OR(S,ER[2]); S = LSHIFT(S,1);
	S = OR(S,ER[3]); S = LSHIFT(S,1);
	S = OR(S,ER[4]); S = LSHIFT(S,1);
	S = OR(S,ER[5]); S = S+1;
	S = SBOX1[S];

	FRK[1] = RSHIFT(AND(S,0x08),3);
	FRK[2] = RSHIFT(AND(S,0x04),2);
	FRK[3] = RSHIFT(AND(S,0x02),1);
	FRK[4] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+6]); S = LSHIFT(S,1);
	S = OR(S,ER[6+6]); S = LSHIFT(S,1);
	S = OR(S,ER[2+6]); S = LSHIFT(S,1);
	S = OR(S,ER[3+6]); S = LSHIFT(S,1);
	S = OR(S,ER[4+6]); S = LSHIFT(S,1);
	S = OR(S,ER[5+6]); S = S+1;
	S = SBOX2[S];

	FRK[5] = RSHIFT(AND(S,0x08),3);
	FRK[6] = RSHIFT(AND(S,0x04),2);
	FRK[7] = RSHIFT(AND(S,0x02),1);
	FRK[8] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+12]); S = LSHIFT(S,1);
	S = OR(S,ER[6+12]); S = LSHIFT(S,1);
	S = OR(S,ER[2+12]); S = LSHIFT(S,1);
	S = OR(S,ER[3+12]); S = LSHIFT(S,1);
	S = OR(S,ER[4+12]); S = LSHIFT(S,1);
	S = OR(S,ER[5+12]); S = S+1;
	S = SBOX3[S];

	FRK[9] = RSHIFT(AND(S,0x08),3);
	FRK[10] = RSHIFT(AND(S,0x04),2);
	FRK[11] = RSHIFT(AND(S,0x02),1);
	FRK[12] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+18]); S = LSHIFT(S,1);
	S = OR(S,ER[6+18]); S = LSHIFT(S,1);
	S = OR(S,ER[2+18]); S = LSHIFT(S,1);
	S = OR(S,ER[3+18]); S = LSHIFT(S,1);
	S = OR(S,ER[4+18]); S = LSHIFT(S,1);
	S = OR(S,ER[5+18]); S = S+1;
	S = SBOX4[S];

	FRK[13] = RSHIFT(AND(S,0x08),3);
	FRK[14] = RSHIFT(AND(S,0x04),2);
	FRK[15] = RSHIFT(AND(S,0x02),1);
	FRK[16] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+24]); S = LSHIFT(S,1);
	S = OR(S,ER[6+24]); S = LSHIFT(S,1);
	S = OR(S,ER[2+24]); S = LSHIFT(S,1);
	S = OR(S,ER[3+24]); S = LSHIFT(S,1);
	S = OR(S,ER[4+24]); S = LSHIFT(S,1);
	S = OR(S,ER[5+24]); S = S+1;
	S = SBOX5[S];

	FRK[17] = RSHIFT(AND(S,0x08),3);
	FRK[18] = RSHIFT(AND(S,0x04),2);
	FRK[19] = RSHIFT(AND(S,0x02),1);
	FRK[20] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+30]); S = LSHIFT(S,1);
	S = OR(S,ER[6+30]); S = LSHIFT(S,1);
	S = OR(S,ER[2+30]); S = LSHIFT(S,1);
	S = OR(S,ER[3+30]); S = LSHIFT(S,1);
	S = OR(S,ER[4+30]); S = LSHIFT(S,1);
	S = OR(S,ER[5+30]); S = S+1;
	S = SBOX6[S];

	FRK[21] = RSHIFT(AND(S,0x08),3);
	FRK[22] = RSHIFT(AND(S,0x04),2);
	FRK[23] = RSHIFT(AND(S,0x02),1);
	FRK[24] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+36]); S = LSHIFT(S,1);
	S = OR(S,ER[6+36]); S = LSHIFT(S,1);
	S = OR(S,ER[2+36]); S = LSHIFT(S,1);
	S = OR(S,ER[3+36]); S = LSHIFT(S,1);
	S = OR(S,ER[4+36]); S = LSHIFT(S,1);
	S = OR(S,ER[5+36]); S = S+1;
	S = SBOX7[S];

	FRK[25] = RSHIFT(AND(S,0x08),3);
	FRK[26] = RSHIFT(AND(S,0x04),2);
	FRK[27] = RSHIFT(AND(S,0x02),1);
	FRK[28] = AND(S,0x01);


	S = 0x00;
	S = OR(S,ER[1+42]); S = LSHIFT(S,1);
	S = OR(S,ER[6+42]); S = LSHIFT(S,1);
	S = OR(S,ER[2+42]); S = LSHIFT(S,1);
	S = OR(S,ER[3+42]); S = LSHIFT(S,1);
	S = OR(S,ER[4+42]); S = LSHIFT(S,1);
	S = OR(S,ER[5+42]); S = S+1;
	S = SBOX8[S];

	FRK[29] = RSHIFT(AND(S,0x08),3);
	FRK[30] = RSHIFT(AND(S,0x04),2);
	FRK[31] = RSHIFT(AND(S,0x02),1);
	FRK[32] = AND(S,0x01);

	FRK = permute(FRK,ROUND_P);

	return FRK;
end

local DES = {};

DES.blockSize = 8;

DES.encrypt = function(keyBlock,inputBlock)

	local LR = unpackBytes(inputBlock);
	local keyBits = unpackBytes(keyBlock);


	local CD = permute(keyBits,PC1);

	--key schedule
	CD = permute(CD,KS1); local KEY1 = permute(CD,PC2);
	CD = permute(CD,KS2); local KEY2 = permute(CD,PC2);
	CD = permute(CD,KS3); local KEY3 = permute(CD,PC2);
	CD = permute(CD,KS4); local KEY4 = permute(CD,PC2);
	CD = permute(CD,KS5); local KEY5 = permute(CD,PC2);
	CD = permute(CD,KS6); local KEY6 = permute(CD,PC2);
	CD = permute(CD,KS7); local KEY7 = permute(CD,PC2);
	CD = permute(CD,KS8); local KEY8 = permute(CD,PC2);
	CD = permute(CD,KS9); local KEY9 = permute(CD,PC2);
	CD = permute(CD,KS10); local KEY10 = permute(CD,PC2);
	CD = permute(CD,KS11); local KEY11 = permute(CD,PC2);
	CD = permute(CD,KS12); local KEY12 = permute(CD,PC2);
	CD = permute(CD,KS13); local KEY13 = permute(CD,PC2);
	CD = permute(CD,KS14); local KEY14 = permute(CD,PC2);
	CD = permute(CD,KS15); local KEY15 = permute(CD,PC2);
	CD = permute(CD,KS16); local KEY16 = permute(CD,PC2);

	--input permutation
	LR = permute(LR,IN_P);

	--rounds
	local frk = mix(LR,KEY1);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY2);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY3);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY4);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY5);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY6);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY7);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY8);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY9);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY10);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY11);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY12);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY13);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY14);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY15);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY16);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	--LR = permute(LR,LR_SWAP);

	--output permutation
	LR = permute(LR,OUT_P);

	local outputBlock = packBytes(LR);
	return outputBlock;
end

DES.decrypt = function(keyBlock,inputBlock)


	local LR = unpackBytes(inputBlock);
	local keyBits = unpackBytes(keyBlock);


	local CD = permute(keyBits,PC1);

	--key schedule
	CD = permute(CD,KS1); local KEY1 = permute(CD,PC2);
	CD = permute(CD,KS2); local KEY2 = permute(CD,PC2);
	CD = permute(CD,KS3); local KEY3 = permute(CD,PC2);
	CD = permute(CD,KS4); local KEY4 = permute(CD,PC2);
	CD = permute(CD,KS5); local KEY5 = permute(CD,PC2);
	CD = permute(CD,KS6); local KEY6 = permute(CD,PC2);
	CD = permute(CD,KS7); local KEY7 = permute(CD,PC2);
	CD = permute(CD,KS8); local KEY8 = permute(CD,PC2);
	CD = permute(CD,KS9); local KEY9 = permute(CD,PC2);
	CD = permute(CD,KS10); local KEY10 = permute(CD,PC2);
	CD = permute(CD,KS11); local KEY11 = permute(CD,PC2);
	CD = permute(CD,KS12); local KEY12 = permute(CD,PC2);
	CD = permute(CD,KS13); local KEY13 = permute(CD,PC2);
	CD = permute(CD,KS14); local KEY14 = permute(CD,PC2);
	CD = permute(CD,KS15); local KEY15 = permute(CD,PC2);
	CD = permute(CD,KS16); local KEY16 = permute(CD,PC2);

	--input permutation
	LR = permute(LR,IN_P);

	--rounds
	local frk = mix(LR,KEY16);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY15);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY14);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY13);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY12);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY11);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY10);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY9);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY8);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY7);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY6);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY5);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY4);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY3);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY2);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	LR = permute(LR,LR_SWAP);

	frk = mix(LR,KEY1);
	for k,v in pairs(frk) do LR[k] = XOR(LR[k],frk[k]); end
	--LR = permute(LR,LR_SWAP);

	--output permutation
	LR = permute(LR,OUT_P);

	local outputBlock = packBytes(LR);
	return outputBlock;
end

return DES;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/des3.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local DES = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.cipher.des");

local DES3 = {};

local getKeys = function(keyBlock)
	local size = Array.size(keyBlock)

	local key1;
	local key2;
	local key3;

	if (size == 8) then
		key1 = keyBlock;
		key2 = keyBlock;
		key3 = keyBlock;
	elseif (size == 16) then
		key1 = Array.slice(keyBlock,1,8);
		key2 = Array.slice(keyBlock,9,16);
		key3 = key1;
	elseif (size == 24) then
		key1 = Array.slice(keyBlock,1,8);
		key2 = Array.slice(keyBlock,9,16);
		key3 = Array.slice(keyBlock,17,24);
	else
		assert(false,"Invalid key size for 3DES");
	end

	return key1,key2,key3;
end

DES3.blockSize = DES.blockSize;

DES3.encrypt = function(keyBlock,inputBlock)
	local key1;
	local key2;
	local key3;

	key1, key2, key3 = getKeys(keyBlock);

	local block = inputBlock;
	block = DES.encrypt(key1,block);
	block = DES.decrypt(key2,block);
	block = DES.encrypt(key3,block);

	return block;
end

DES3.decrypt = function(keyBlock,inputBlock)
	local key1;
	local key2;
	local key3;

	key1, key2, key3 = getKeys(keyBlock);

	local block = inputBlock;
	block = DES.decrypt(key3,block);
	block = DES.encrypt(key2,block);
	block = DES.decrypt(key1,block);

	return block;
end

return DES3;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/tea.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;


--NOTE: TEA is endian-dependent!
--The spec does not seem to specify which to use.
--It looks like most implementations use big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local TEA = {};

TEA.blockSize = 8;

TEA.encrypt = function(key,data)
	local y = bytes2word(data[1],data[2],data[3],data[4]);
	local z = bytes2word(data[5],data[6],data[7],data[8]);
	local delta = 0x9e3779b9;
	local sum = 0;

	local k0 = bytes2word(key[ 1],key[ 2],key[ 3],key[ 4]);
	local k1 = bytes2word(key[ 5],key[ 6],key[ 7],key[ 8]);
	local k2 = bytes2word(key[ 9],key[10],key[11],key[12]);
	local k3 = bytes2word(key[13],key[14],key[15],key[16]);

	for i = 1,32 do
		local temp;

		sum = AND(sum + delta, 0xFFFFFFFF);

		temp = z+sum;
		temp = XOR(temp,LSHIFT(z,4)+k0);
		temp = XOR(temp,RSHIFT(z,5)+k1);
		y = AND(y + temp, 0xFFFFFFFF);

		temp = y+sum;
		temp = XOR(temp,LSHIFT(y,4)+k2);
		temp = XOR(temp,RSHIFT(y,5)+k3);
		z = AND( z + temp, 0xFFFFFFFF);
	end

	local out = {};

	out[1],out[2],out[3],out[4] = word2bytes(y);
	out[5],out[6],out[7],out[8] = word2bytes(z);

	return out;
end

TEA.decrypt = function(key,data)
	local y = bytes2word(data[1],data[2],data[3],data[4]);
	local z = bytes2word(data[5],data[6],data[7],data[8]);

	local delta = 0x9e3779b9;
	local sum = 0xc6ef3720; --AND(delta*32,0xFFFFFFFF);

	local k0 = bytes2word(key[ 1],key[ 2],key[ 3],key[ 4]);
	local k1 = bytes2word(key[ 5],key[ 6],key[ 7],key[ 8]);
	local k2 = bytes2word(key[ 9],key[10],key[11],key[12]);
	local k3 = bytes2word(key[13],key[14],key[15],key[16]);

	for i = 1,32 do
		local temp;

		temp = y+sum;
		temp = XOR(temp,LSHIFT(y,4)+k2);
		temp = XOR(temp,RSHIFT(y,5)+k3);
		z = AND(z + 0x100000000 - temp,0xFFFFFFFF);

		temp = z+sum;
		temp = XOR(temp,LSHIFT(z,4)+k0);
		temp = XOR(temp,RSHIFT(z,5)+k1);
		y = AND(y + 0x100000000 - temp,0xFFFFFFFF);

		sum = AND(sum + 0x100000000 - delta,0xFFFFFFFF);
	end

	local out = {};

	out[1],out[2],out[3],out[4] = word2bytes(y);
	out[5],out[6],out[7],out[8] = word2bytes(z);

	return out;
end

return TEA;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/xtea.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local Math = require("math");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;


--NOTE: XTEA is endian-dependent!
--The spec does not seem to specify which to use.
--It looks like most implementations use big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local XTEA = {};

XTEA.blockSize = 8;

XTEA.encrypt = function(key,data)
	local y = bytes2word(data[1],data[2],data[3],data[4]);
	local z = bytes2word(data[5],data[6],data[7],data[8]);
	local delta = 0x9e3779b9;
	local sum = 0;

	local k0 = bytes2word(key[ 1],key[ 2],key[ 3],key[ 4]);
	local k1 = bytes2word(key[ 5],key[ 6],key[ 7],key[ 8]);
	local k2 = bytes2word(key[ 9],key[10],key[11],key[12]);
	local k3 = bytes2word(key[13],key[14],key[15],key[16]);
	local k = {[0] = k0, k1, k2, k3};

	for i = 1,32 do
		local temp;

		temp = XOR(LSHIFT(z,4),RSHIFT(z,5)) + z
		temp = XOR(temp,sum + k[ AND(sum,0x3) ])
		y = AND(y + temp, 0xFFFFFFFF);

		sum = AND(sum + delta, 0xFFFFFFFF);

		temp = XOR(LSHIFT(y,4),RSHIFT(y,5)) + y
		temp = XOR(temp,sum + k[ AND(RSHIFT(sum,11),0x3) ])
		z = AND( z + temp, 0xFFFFFFFF);
	end

	local out = {};

	out[1],out[2],out[3],out[4] = word2bytes(y);
	out[5],out[6],out[7],out[8] = word2bytes(z);

	return out;
end

XTEA.decrypt = function(key,data)
	local y = bytes2word(data[1],data[2],data[3],data[4]);
	local z = bytes2word(data[5],data[6],data[7],data[8]);

	local delta = 0x9e3779b9;
	local sum = 0xc6ef3720; --AND(delta*32,0xFFFFFFFF);

	local k0 = bytes2word(key[ 1],key[ 2],key[ 3],key[ 4]);
	local k1 = bytes2word(key[ 5],key[ 6],key[ 7],key[ 8]);
	local k2 = bytes2word(key[ 9],key[10],key[11],key[12]);
	local k3 = bytes2word(key[13],key[14],key[15],key[16]);
	local k = {[0] = k0, k1, k2, k3};

	for i = 1,32 do
		local temp;

		temp = XOR(LSHIFT(y,4),RSHIFT(y,5)) + y
		temp = XOR(temp,sum + k[ AND(RSHIFT(sum,11),0x3) ])
		z = AND(z + 0x100000000 - temp,0xFFFFFFFF);

		sum = AND(sum + 0x100000000 - delta,0xFFFFFFFF);

		temp = XOR(LSHIFT(z,4),RSHIFT(z,5)) + z
		temp = XOR(temp,sum + k[ AND(sum,0x3) ])
		y = AND(y + 0x100000000 - temp,0xFFFFFFFF);

	end

	local out = {};

	out[1],out[2],out[3],out[4] = word2bytes(y);
	out[5],out[6],out[7],out[8] = word2bytes(z);

	return out;
end

return XTEA;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/cbc.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local CBC = {};

CBC.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = Array.XOR(iv,block);
					out = blockCipher.encrypt(key,out);
					Array.writeToQueue(outputQueue,out);
					iv = out;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end


CBC.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = block;
					out = blockCipher.decrypt(key,out);
					out = Array.XOR(iv,out);
					Array.writeToQueue(outputQueue,out);
					iv = block;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
--		local data=Stream.toArray(outputQueue.pop)
--        local paddingByte=data[#data]

--        local realLength=#data-paddingByte--如果有padding,计算去除padding后的长度
--        local padded=true
--        for i=#data,realLength+1,-1 do
--            if(data[i]~=paddingByte) then
--                padded=false
--            end
--        end
--        print("realLength is "..realLength)
--        local paddedBytes=Array.slice(data,1,realLength)

--        if padded then
--            Array.writeToQueue(outputQueue,paddedBytes)
--        end

		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

return CBC;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/cfb.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local CFB = {};

CFB.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);
					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
					iv = out;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

CFB.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);
					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
					iv = block;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

return CFB;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/ctr.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--CTR counter is big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end


local CTR = {};

CTR.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	local updateIV = function()
		iv[16] = iv[16] + 1;
		if iv[16] <= 0xFF then return; end
		iv[16] = AND(iv[16],0xFF);

		iv[15] = iv[15] + 1;
		if iv[15] <= 0xFF then return; end
		iv[15] = AND(iv[15],0xFF);

		iv[14] = iv[14] + 1;
		if iv[14] <= 0xFF then return; end
		iv[14] = AND(iv[14],0xFF);

		iv[13] = iv[13] + 1;
		if iv[13] <= 0xFF then return; end
		iv[13] = AND(iv[13],0xFF);

		iv[12] = iv[12] + 1;
		if iv[12] <= 0xFF then return; end
		iv[12] = AND(iv[12],0xFF);

		iv[11] = iv[11] + 1;
		if iv[11] <= 0xFF then return; end
		iv[11] = AND(iv[11],0xFF);

		iv[10] = iv[10] + 1;
		if iv[10] <= 0xFF then return; end
		iv[10] = AND(iv[10],0xFF);

		iv[9] = iv[9] + 1;
		if iv[9] <= 0xFF then return; end
		iv[9] = AND(iv[9],0xFF);

		return;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);

			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);

					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
					updateIV();
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end


CTR.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	local updateIV = function()
		iv[16] = iv[16] + 1;
		if iv[16] <= 0xFF then return; end
		iv[16] = AND(iv[16],0xFF);

		iv[15] = iv[15] + 1;
		if iv[15] <= 0xFF then return; end
		iv[15] = AND(iv[15],0xFF);

		iv[14] = iv[14] + 1;
		if iv[14] <= 0xFF then return; end
		iv[14] = AND(iv[14],0xFF);

		iv[13] = iv[13] + 1;
		if iv[13] <= 0xFF then return; end
		iv[13] = AND(iv[13],0xFF);

		iv[12] = iv[12] + 1;
		if iv[12] <= 0xFF then return; end
		iv[12] = AND(iv[12],0xFF);

		iv[11] = iv[11] + 1;
		if iv[11] <= 0xFF then return; end
		iv[11] = AND(iv[11],0xFF);

		iv[10] = iv[10] + 1;
		if iv[10] <= 0xFF then return; end
		iv[10] = AND(iv[10],0xFF);

		iv[9] = iv[9] + 1;
		if iv[9] <= 0xFF then return; end
		iv[9] = AND(iv[9],0xFF);

		return;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);

			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);

					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
					updateIV();
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end




return CTR;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/ecb.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local ECB = {};

ECB.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				block = blockCipher.encrypt(key,block);

				Array.writeToQueue(outputQueue,block);
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

ECB.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				block = blockCipher.decrypt(key,block);

				Array.writeToQueue(outputQueue,block);
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
--		local data=Stream.toArray(outputQueue.pop)
--        local paddingByte=data[#data]

--        local realLength=#data-paddingByte--如果有padding,计算去除padding后的长度
--        local padded=true
--        for i=#data,realLength+1,-1 do
--            if(data[i]~=paddingByte) then
--                padded=false
--            end
--        end
--        print("realLength is "..realLength)
--        local paddedBytes=Array.slice(data,1,realLength)

--        if padded then
--            Array.writeToQueue(outputQueue,paddedBytes)
--        end

        paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end


return ECB;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/ige.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local IGE = {};

IGE.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local xPrev,yPrev;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		xPrev = nil;
		yPrev = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(yPrev == nil) then
					yPrev = block;
				elseif(xPrev == nil) then
					xPrev = block
				else
					local out = Array.XOR(yPrev,block);
					out = blockCipher.encrypt(key,out);
					out = Array.XOR(out,xPrev);
					Array.writeToQueue(outputQueue,out);
					xPrev = block;
					yPrev = out;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

IGE.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local xPrev,yPrev;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		xPrev = nil;
		yPrev = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(xPrev == nil) then
					xPrev = block;
				elseif(yPrev == nil) then
					yPrev = block
				else
					local out = Array.XOR(yPrev,block);
					out = blockCipher.decrypt(key,out);
					out = Array.XOR(out,xPrev);
					Array.writeToQueue(outputQueue,out);
					xPrev = block;
					yPrev = out;
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

return IGE;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/ofb.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local OFB = {};

OFB.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);
					iv = out;
					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

OFB.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = iv;
					out = blockCipher.encrypt(key,out);
					iv = out;
					out = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end


return OFB;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/cipher/mode/pcbc.lua"] = [=[local Array = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local PCBC = {};

PCBC.Cipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = block;
					out = Array.XOR(iv,out);
					out = blockCipher.encrypt(key,out);
					iv = Array.XOR(out,block);
					Array.writeToQueue(outputQueue,out);
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end

PCBC.Decipher = function()

	local public = {};

	local key;
	local blockCipher;
	local padding;
	local inputQueue;
	local outputQueue;
	local iv;

	public.setKey = function(keyBytes)
		key = keyBytes;
		return public;
	end

	public.setBlockCipher = function(cipher)
		blockCipher = cipher;
		return public;
	end

	public.setPadding = function(paddingMode)
		padding = paddingMode;
		return public;
	end

	public.init = function()
		inputQueue = Queue();
		outputQueue = Queue();
		iv = nil;
		return public;
	end

	public.update = function(messageStream)
		local byte = messageStream();
		while (byte ~= nil) do
			inputQueue.push(byte);
			if(inputQueue.size() >= blockCipher.blockSize) then
				local block = Array.readFromQueue(inputQueue,blockCipher.blockSize);

				if(iv == nil) then
					iv = block;
				else
					local out = block;
					out = blockCipher.decrypt(key,out);
					out = Array.XOR(iv,out);
					Array.writeToQueue(outputQueue,out);
					iv = Array.XOR(out,block);
				end
			end
			byte = messageStream();
		end
		return public;
	end

	public.finish = function()
		paddingStream = padding(blockCipher.blockSize,inputQueue.getHead());
		public.update(paddingStream);

		return public;
	end

	public.getOutputQueue = function()
		return outputQueue;
	end

	public.asHex = function()
		return Stream.toHex(outputQueue.pop);
	end

	public.asBytes = function()
		return Stream.toArray(outputQueue.pop);
	end

	return public;

end


return PCBC;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/md2.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local SUBST = {
  0x29, 0x2E, 0x43, 0xC9, 0xA2, 0xD8, 0x7C, 0x01, 0x3D, 0x36, 0x54, 0xA1, 0xEC, 0xF0, 0x06, 0x13,
  0x62, 0xA7, 0x05, 0xF3, 0xC0, 0xC7, 0x73, 0x8C, 0x98, 0x93, 0x2B, 0xD9, 0xBC, 0x4C, 0x82, 0xCA,
  0x1E, 0x9B, 0x57, 0x3C, 0xFD, 0xD4, 0xE0, 0x16, 0x67, 0x42, 0x6F, 0x18, 0x8A, 0x17, 0xE5, 0x12,
  0xBE, 0x4E, 0xC4, 0xD6, 0xDA, 0x9E, 0xDE, 0x49, 0xA0, 0xFB, 0xF5, 0x8E, 0xBB, 0x2F, 0xEE, 0x7A,
  0xA9, 0x68, 0x79, 0x91, 0x15, 0xB2, 0x07, 0x3F, 0x94, 0xC2, 0x10, 0x89, 0x0B, 0x22, 0x5F, 0x21,
  0x80, 0x7F, 0x5D, 0x9A, 0x5A, 0x90, 0x32, 0x27, 0x35, 0x3E, 0xCC, 0xE7, 0xBF, 0xF7, 0x97, 0x03,
  0xFF, 0x19, 0x30, 0xB3, 0x48, 0xA5, 0xB5, 0xD1, 0xD7, 0x5E, 0x92, 0x2A, 0xAC, 0x56, 0xAA, 0xC6,
  0x4F, 0xB8, 0x38, 0xD2, 0x96, 0xA4, 0x7D, 0xB6, 0x76, 0xFC, 0x6B, 0xE2, 0x9C, 0x74, 0x04, 0xF1,
  0x45, 0x9D, 0x70, 0x59, 0x64, 0x71, 0x87, 0x20, 0x86, 0x5B, 0xCF, 0x65, 0xE6, 0x2D, 0xA8, 0x02,
  0x1B, 0x60, 0x25, 0xAD, 0xAE, 0xB0, 0xB9, 0xF6, 0x1C, 0x46, 0x61, 0x69, 0x34, 0x40, 0x7E, 0x0F,
  0x55, 0x47, 0xA3, 0x23, 0xDD, 0x51, 0xAF, 0x3A, 0xC3, 0x5C, 0xF9, 0xCE, 0xBA, 0xC5, 0xEA, 0x26,
  0x2C, 0x53, 0x0D, 0x6E, 0x85, 0x28, 0x84, 0x09, 0xD3, 0xDF, 0xCD, 0xF4, 0x41, 0x81, 0x4D, 0x52,
  0x6A, 0xDC, 0x37, 0xC8, 0x6C, 0xC1, 0xAB, 0xFA, 0x24, 0xE1, 0x7B, 0x08, 0x0C, 0xBD, 0xB1, 0x4A,
  0x78, 0x88, 0x95, 0x8B, 0xE3, 0x63, 0xE8, 0x6D, 0xE9, 0xCB, 0xD5, 0xFE, 0x3B, 0x00, 0x1D, 0x39,
  0xF2, 0xEF, 0xB7, 0x0E, 0x66, 0x58, 0xD0, 0xE4, 0xA6, 0x77, 0x72, 0xF8, 0xEB, 0x75, 0x4B, 0x0A,
  0x31, 0x44, 0x50, 0xB4, 0x8F, 0xED, 0x1F, 0x1A, 0xDB, 0x99, 0x8D, 0x33, 0x9F, 0x11, 0x83, 0x14 };

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

local MD2 = function()

	local queue = Queue();

	local X = {};
	for i=0,47 do
		X[i] = 0x00;
	end

	local L = 0;
	local C = {};
	for i=0,15 do
		C[i] = 0x00;
	end

	local public = {};

	local processBlock = function()
		local block = {};

		for i=0,15 do
			block[i] = queue.pop();
		end

		for i=0,15 do
			X[i+16] = block[i];
			X[i+32] = XOR(X[i],block[i]); --mix
		end

		local t;

		--update block
		t=0;
		for i=0,17 do
			for j=0,47 do
				X[j] = XOR(X[j],SUBST[t+1]);
				t = X[j];
			end
			t = (t+i) % 256;
		end

		--update checksum
		t = C[15];
		for i=0,15 do
			C[i] = XOR(C[i],SUBST[XOR(block[i],t)+1]);
			t = C[i];
		end

	end

	public.init = function()
		queue.reset();

		X = {};
		for i=0,47 do
			X[i] = 0x00;
		end

		L = 0;
		C = {};
		for i=0,15 do
			C[i] = 0x00;
		end

		return public;
	end

	public.update = function(stream)
		for b in stream do
			queue.push(b);
			if(queue.size() >= 16) then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local i = 16-queue.size();

		while queue.size() < 16 do
			queue.push(i);
		end

		processBlock();

		queue.push(C[ 0]); queue.push(C[ 1]); queue.push(C[ 2]); queue.push(C[ 3]);
		queue.push(C[ 4]); queue.push(C[ 5]); queue.push(C[ 6]); queue.push(C[ 7]);
		queue.push(C[ 8]); queue.push(C[ 9]); queue.push(C[10]); queue.push(C[11]);
		queue.push(C[12]); queue.push(C[13]); queue.push(C[14]); queue.push(C[15]);

		processBlock();

		return public;
	end

	public.asBytes = function()
		return {X[ 0],X[ 1],X[ 2],X[ 3],X[ 4],X[ 5],X[ 6],X[ 7],
				X[ 8],X[ 9],X[10],X[11],X[12],X[13],X[14],X[15]};
	end

	public.asHex = function()
		return String.format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				X[ 0],X[ 1],X[ 2],X[ 3],X[ 4],X[ 5],X[ 6],X[ 7],
				X[ 8],X[ 9],X[10],X[11],X[12],X[13],X[14],X[15]);
	end

	return public;

end

return MD2;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/md4.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local SHIFT = {	3,  7, 11, 19,  3,  7, 11, 19,  3,  7, 11, 19,  3,  7, 11, 19,
				3,  5,  9, 13,  3,  5,  9, 13,  3,  5,  9, 13,  3,  5,  9, 13,
				3,  9, 11, 15,  3,  9, 11, 15,  3,  9, 11, 15,  3,  9, 11, 15 };

local WORD = {	0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
				0,  4,  8, 12,  1,  5,  9, 13,  2,  6, 10, 14,  3,  7, 11, 13,
				3,  8,  4, 12,  2, 10,  6, 14,  1,  9,  5, 13,  3,  1,  7, 15 };

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--MD4 is little-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b3; i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b0);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b0 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b3 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (j*0x100000000)+i;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(Math.floor(i/0x100000000));
	local b0,b1,b2,b3 = word2bytes(i);
	return b0,b1,b2,b3,b4,b5,b6,b7;
end

local F = function(x,y,z) return OR(AND(x,y),AND(NOT(x),z)); end
local G = function(x,y,z) return OR(AND(x,y), OR(AND(x,z), AND(y,z))); end
local H = function(x,y,z) return XOR(x,XOR(y,z)); end


local MD4 = function()

	local queue = Queue();

	local A = 0x67452301;
	local B = 0xefcdab89;
	local C = 0x98badcfe;
	local D = 0x10325476;
	local public = {};

	local processBlock = function()
		local a = A;
		local b = B;
		local c = C;
		local d = D;

		local X = {};

		for i=0,15 do
			X[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		a = LROT(a + F(b,c,d) + X[ 0], 3);
		d = LROT(d + F(a,b,c) + X[ 1], 7);
		c = LROT(c + F(d,a,b) + X[ 2],11);
		b = LROT(b + F(c,d,a) + X[ 3],19);

		a = LROT(a + F(b,c,d) + X[ 4], 3);
		d = LROT(d + F(a,b,c) + X[ 5], 7);
		c = LROT(c + F(d,a,b) + X[ 6],11);
		b = LROT(b + F(c,d,a) + X[ 7],19);

		a = LROT(a + F(b,c,d) + X[ 8], 3);
		d = LROT(d + F(a,b,c) + X[ 9], 7);
		c = LROT(c + F(d,a,b) + X[10],11);
		b = LROT(b + F(c,d,a) + X[11],19);

		a = LROT(a + F(b,c,d) + X[12], 3);
		d = LROT(d + F(a,b,c) + X[13], 7);
		c = LROT(c + F(d,a,b) + X[14],11);
		b = LROT(b + F(c,d,a) + X[15],19);


		a = LROT(a + G(b,c,d) + X[ 0] + 0x5A827999, 3);
		d = LROT(d + G(a,b,c) + X[ 4] + 0x5A827999, 5);
		c = LROT(c + G(d,a,b) + X[ 8] + 0x5A827999, 9);
		b = LROT(b + G(c,d,a) + X[12] + 0x5A827999,13);

		a = LROT(a + G(b,c,d) + X[ 1] + 0x5A827999, 3);
		d = LROT(d + G(a,b,c) + X[ 5] + 0x5A827999, 5);
		c = LROT(c + G(d,a,b) + X[ 9] + 0x5A827999, 9);
		b = LROT(b + G(c,d,a) + X[13] + 0x5A827999,13);

		a = LROT(a + G(b,c,d) + X[ 2] + 0x5A827999, 3);
		d = LROT(d + G(a,b,c) + X[ 6] + 0x5A827999, 5);
		c = LROT(c + G(d,a,b) + X[10] + 0x5A827999, 9);
		b = LROT(b + G(c,d,a) + X[14] + 0x5A827999,13);

		a = LROT(a + G(b,c,d) + X[ 3] + 0x5A827999, 3);
		d = LROT(d + G(a,b,c) + X[ 7] + 0x5A827999, 5);
		c = LROT(c + G(d,a,b) + X[11] + 0x5A827999, 9);
		b = LROT(b + G(c,d,a) + X[15] + 0x5A827999,13);


		a = LROT(a + H(b,c,d) + X[ 0] + 0x6ED9EBA1, 3);
		d = LROT(d + H(a,b,c) + X[ 8] + 0x6ED9EBA1, 9);
		c = LROT(c + H(d,a,b) + X[ 4] + 0x6ED9EBA1,11);
		b = LROT(b + H(c,d,a) + X[12] + 0x6ED9EBA1,15);

		a = LROT(a + H(b,c,d) + X[ 2] + 0x6ED9EBA1, 3);
		d = LROT(d + H(a,b,c) + X[10] + 0x6ED9EBA1, 9);
		c = LROT(c + H(d,a,b) + X[ 6] + 0x6ED9EBA1,11);
		b = LROT(b + H(c,d,a) + X[14] + 0x6ED9EBA1,15);

		a = LROT(a + H(b,c,d) + X[ 1] + 0x6ED9EBA1, 3);
		d = LROT(d + H(a,b,c) + X[ 9] + 0x6ED9EBA1, 9);
		c = LROT(c + H(d,a,b) + X[ 5] + 0x6ED9EBA1,11);
		b = LROT(b + H(c,d,a) + X[13] + 0x6ED9EBA1,15);

		a = LROT(a + H(b,c,d) + X[ 3] + 0x6ED9EBA1, 3);
		d = LROT(d + H(a,b,c) + X[11] + 0x6ED9EBA1, 9);
		c = LROT(c + H(d,a,b) + X[ 7] + 0x6ED9EBA1,11);
		b = LROT(b + H(c,d,a) + X[15] + 0x6ED9EBA1,15);


		A = AND(A + a, 0xFFFFFFFF);
		B = AND(B + b, 0xFFFFFFFF);
		C = AND(C + c, 0xFFFFFFFF);
		D = AND(D + d, 0xFFFFFFFF);
	end

	public.init = function()
		queue.reset();

		A = 0x67452301;
		B = 0xefcdab89;
		C = 0x98badcfe;
		D = 0x10325476;

		return public;
	end

	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if(queue.size() >= 64) then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);
		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		return {b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		return String.format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15);
	end

	return public;

end

return MD4;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/md5.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local SHIFT = {	7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
				5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
				4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
				6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 };

local CONSTANTS = {	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
					0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
					0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
					0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
					0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
					0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
					0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
					0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
					0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
					0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
					0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
					0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
					0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
					0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
					0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
					0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--MD5 is little-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b3; i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b0);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b0 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b3 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (j*0x100000000)+i;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(Math.floor(i/0x100000000));
	local b0,b1,b2,b3 = word2bytes(i);
	return b0,b1,b2,b3,b4,b5,b6,b7;
end

local F = function(x,y,z) return OR(AND(x,y),AND(NOT(x),z)); end
local G = function(x,y,z) return OR(AND(x,z),AND(y,NOT(z))); end
local H = function(x,y,z) return XOR(x,XOR(y,z)); end
local I = function(x,y,z) return XOR(y,OR(x,NOT(z))); end

local MD5 = function()

	local queue = Queue();

	local A = 0x67452301;
	local B = 0xefcdab89;
	local C = 0x98badcfe;
	local D = 0x10325476;
	local public = {};

	local processBlock = function()
		local a = A;
		local b = B;
		local c = C;
		local d = D;

		local X = {};

		for i=1,16 do
			X[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		for i=0,63 do
			local f,g,temp;

			if (0 <= i) and (i <= 15) then
				f = F(b,c,d);
				g = i;
			elseif (16 <= i) and (i <= 31) then
				f = G(b,c,d);
				g = (5*i + 1) % 16;
			elseif (32 <= i) and (i <= 47) then
				f = H(b,c,d);
				g = (3*i + 5) % 16;
			elseif (48 <= i) and (i <= 63) then
				f = I(b,c,d);
				g = (7*i) % 16;
			end
			temp = d;
			d = c;
			c = b;
			b = b + LROT((a + f + CONSTANTS[i+1] + X[g+1]), SHIFT[i+1]);
			a = temp;
		end

		A = AND(A + a, 0xFFFFFFFF);
		B = AND(B + b, 0xFFFFFFFF);
		C = AND(C + c, 0xFFFFFFFF);
		D = AND(D + d, 0xFFFFFFFF);
	end

	public.init = function()
		queue.reset();

		A = 0x67452301;
		B = 0xefcdab89;
		C = 0x98badcfe;
		D = 0x10325476;

		return public;
	end

	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if(queue.size() >= 64) then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);
		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		return {b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		return String.format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15);
	end

	return public;

end

return MD5;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/ripemd128.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--RIPEMD128 is little-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b3; i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b0);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b0 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b3 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (j*0x100000000)+i;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(Math.floor(i/0x100000000));
	local b0,b1,b2,b3 = word2bytes(i);
	return b0,b1,b2,b3,b4,b5,b6,b7;
end

local F = function(x,y,z) return XOR(x, XOR(y,z)); end
local G = function(x,y,z) return OR(AND(x,y), AND(NOT(x),z)); end
local H = function(x,y,z) return XOR(OR(x,NOT(y)),z); end
local I = function(x,y,z) return OR(AND(x,z),AND(y,NOT(z))); end

local FF = function(a,b,c,d,x,s)
	a = a + F(b,c,d) + x;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local GG = function(a,b,c,d,x,s)
	a = a + G(b,c,d) + x + 0x5a827999;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local HH = function(a,b,c,d,x,s)
	a = a + H(b,c,d) + x + 0x6ed9eba1;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local II = function(a,b,c,d,x,s)
	a = a + I(b,c,d) + x + 0x8f1bbcdc;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end


local FFF = function(a,b,c,d,x,s)
	a = a + F(b,c,d) + x;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local GGG = function(a,b,c,d,x,s)
	a = a + G(b,c,d) + x + 0x6d703ef3;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local HHH = function(a,b,c,d,x,s)
	a = a + H(b,c,d) + x + 0x5c4dd124;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local III = function(a,b,c,d,x,s)
	a = a + I(b,c,d) + x + 0x50a28be6;
	a = LROT(a,s);
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local RIPEMD128 = function()

	local queue = Queue();

	local A = 0x67452301;
	local B = 0xefcdab89;
	local C = 0x98badcfe;
	local D = 0x10325476;

	local public = {};

	local processBlock = function()
		local aa,bb,cc,dd = A,B,C,D;
		local aaa,bbb,ccc,ddd = A,B,C,D;

		local X = {};

		for i=0,15 do
			X[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		aa = FF(aa, bb, cc, dd, X[ 0], 11);
		dd = FF(dd, aa, bb, cc, X[ 1], 14);
		cc = FF(cc, dd, aa, bb, X[ 2], 15);
		bb = FF(bb, cc, dd, aa, X[ 3], 12);
		aa = FF(aa, bb, cc, dd, X[ 4],  5);
		dd = FF(dd, aa, bb, cc, X[ 5],  8);
		cc = FF(cc, dd, aa, bb, X[ 6],  7);
		bb = FF(bb, cc, dd, aa, X[ 7],  9);
		aa = FF(aa, bb, cc, dd, X[ 8], 11);
		dd = FF(dd, aa, bb, cc, X[ 9], 13);
		cc = FF(cc, dd, aa, bb, X[10], 14);
		bb = FF(bb, cc, dd, aa, X[11], 15);
		aa = FF(aa, bb, cc, dd, X[12],  6);
		dd = FF(dd, aa, bb, cc, X[13],  7);
		cc = FF(cc, dd, aa, bb, X[14],  9);
		bb = FF(bb, cc, dd, aa, X[15],  8);

		aa = GG(aa, bb, cc, dd, X[ 7],  7);
		dd = GG(dd, aa, bb, cc, X[ 4],  6);
		cc = GG(cc, dd, aa, bb, X[13],  8);
		bb = GG(bb, cc, dd, aa, X[ 1], 13);
		aa = GG(aa, bb, cc, dd, X[10], 11);
		dd = GG(dd, aa, bb, cc, X[ 6],  9);
		cc = GG(cc, dd, aa, bb, X[15],  7);
		bb = GG(bb, cc, dd, aa, X[ 3], 15);
		aa = GG(aa, bb, cc, dd, X[12],  7);
		dd = GG(dd, aa, bb, cc, X[ 0], 12);
		cc = GG(cc, dd, aa, bb, X[ 9], 15);
		bb = GG(bb, cc, dd, aa, X[ 5],  9);
		aa = GG(aa, bb, cc, dd, X[ 2], 11);
		dd = GG(dd, aa, bb, cc, X[14],  7);
		cc = GG(cc, dd, aa, bb, X[11], 13);
		bb = GG(bb, cc, dd, aa, X[ 8], 12);

		aa = HH(aa, bb, cc, dd, X[ 3], 11);
		dd = HH(dd, aa, bb, cc, X[10], 13);
		cc = HH(cc, dd, aa, bb, X[14],  6);
		bb = HH(bb, cc, dd, aa, X[ 4],  7);
		aa = HH(aa, bb, cc, dd, X[ 9], 14);
		dd = HH(dd, aa, bb, cc, X[15],  9);
		cc = HH(cc, dd, aa, bb, X[ 8], 13);
		bb = HH(bb, cc, dd, aa, X[ 1], 15);
		aa = HH(aa, bb, cc, dd, X[ 2], 14);
		dd = HH(dd, aa, bb, cc, X[ 7],  8);
		cc = HH(cc, dd, aa, bb, X[ 0], 13);
		bb = HH(bb, cc, dd, aa, X[ 6],  6);
		aa = HH(aa, bb, cc, dd, X[13],  5);
		dd = HH(dd, aa, bb, cc, X[11], 12);
		cc = HH(cc, dd, aa, bb, X[ 5],  7);
		bb = HH(bb, cc, dd, aa, X[12],  5);

		aa = II(aa, bb, cc, dd, X[ 1], 11);
		dd = II(dd, aa, bb, cc, X[ 9], 12);
		cc = II(cc, dd, aa, bb, X[11], 14);
		bb = II(bb, cc, dd, aa, X[10], 15);
		aa = II(aa, bb, cc, dd, X[ 0], 14);
		dd = II(dd, aa, bb, cc, X[ 8], 15);
		cc = II(cc, dd, aa, bb, X[12],  9);
		bb = II(bb, cc, dd, aa, X[ 4],  8);
		aa = II(aa, bb, cc, dd, X[13],  9);
		dd = II(dd, aa, bb, cc, X[ 3], 14);
		cc = II(cc, dd, aa, bb, X[ 7],  5);
		bb = II(bb, cc, dd, aa, X[15],  6);
		aa = II(aa, bb, cc, dd, X[14],  8);
		dd = II(dd, aa, bb, cc, X[ 5],  6);
		cc = II(cc, dd, aa, bb, X[ 6],  5);
		bb = II(bb, cc, dd, aa, X[ 2], 12);

		aaa = III(aaa, bbb, ccc, ddd, X[ 5],  8);
		ddd = III(ddd, aaa, bbb, ccc, X[14],  9);
		ccc = III(ccc, ddd, aaa, bbb, X[ 7],  9);
		bbb = III(bbb, ccc, ddd, aaa, X[ 0], 11);
		aaa = III(aaa, bbb, ccc, ddd, X[ 9], 13);
		ddd = III(ddd, aaa, bbb, ccc, X[ 2], 15);
		ccc = III(ccc, ddd, aaa, bbb, X[11], 15);
		bbb = III(bbb, ccc, ddd, aaa, X[ 4],  5);
		aaa = III(aaa, bbb, ccc, ddd, X[13],  7);
		ddd = III(ddd, aaa, bbb, ccc, X[ 6],  7);
		ccc = III(ccc, ddd, aaa, bbb, X[15],  8);
		bbb = III(bbb, ccc, ddd, aaa, X[ 8], 11);
		aaa = III(aaa, bbb, ccc, ddd, X[ 1], 14);
		ddd = III(ddd, aaa, bbb, ccc, X[10], 14);
		ccc = III(ccc, ddd, aaa, bbb, X[ 3], 12);
		bbb = III(bbb, ccc, ddd, aaa, X[12],  6);

		aaa = HHH(aaa, bbb, ccc, ddd, X[ 6],  9);
		ddd = HHH(ddd, aaa, bbb, ccc, X[11], 13);
		ccc = HHH(ccc, ddd, aaa, bbb, X[ 3], 15);
		bbb = HHH(bbb, ccc, ddd, aaa, X[ 7],  7);
		aaa = HHH(aaa, bbb, ccc, ddd, X[ 0], 12);
		ddd = HHH(ddd, aaa, bbb, ccc, X[13],  8);
		ccc = HHH(ccc, ddd, aaa, bbb, X[ 5],  9);
		bbb = HHH(bbb, ccc, ddd, aaa, X[10], 11);
		aaa = HHH(aaa, bbb, ccc, ddd, X[14],  7);
		ddd = HHH(ddd, aaa, bbb, ccc, X[15],  7);
		ccc = HHH(ccc, ddd, aaa, bbb, X[ 8], 12);
		bbb = HHH(bbb, ccc, ddd, aaa, X[12],  7);
		aaa = HHH(aaa, bbb, ccc, ddd, X[ 4],  6);
		ddd = HHH(ddd, aaa, bbb, ccc, X[ 9], 15);
		ccc = HHH(ccc, ddd, aaa, bbb, X[ 1], 13);
		bbb = HHH(bbb, ccc, ddd, aaa, X[ 2], 11);

		aaa = GGG(aaa, bbb, ccc, ddd, X[15],  9);
		ddd = GGG(ddd, aaa, bbb, ccc, X[ 5],  7);
		ccc = GGG(ccc, ddd, aaa, bbb, X[ 1], 15);
		bbb = GGG(bbb, ccc, ddd, aaa, X[ 3], 11);
		aaa = GGG(aaa, bbb, ccc, ddd, X[ 7],  8);
		ddd = GGG(ddd, aaa, bbb, ccc, X[14],  6);
		ccc = GGG(ccc, ddd, aaa, bbb, X[ 6],  6);
		bbb = GGG(bbb, ccc, ddd, aaa, X[ 9], 14);
		aaa = GGG(aaa, bbb, ccc, ddd, X[11], 12);
		ddd = GGG(ddd, aaa, bbb, ccc, X[ 8], 13);
		ccc = GGG(ccc, ddd, aaa, bbb, X[12],  5);
		bbb = GGG(bbb, ccc, ddd, aaa, X[ 2], 14);
		aaa = GGG(aaa, bbb, ccc, ddd, X[10], 13);
		ddd = GGG(ddd, aaa, bbb, ccc, X[ 0], 13);
		ccc = GGG(ccc, ddd, aaa, bbb, X[ 4],  7);
		bbb = GGG(bbb, ccc, ddd, aaa, X[13],  5);

		aaa = FFF(aaa, bbb, ccc, ddd, X[ 8], 15);
		ddd = FFF(ddd, aaa, bbb, ccc, X[ 6],  5);
		ccc = FFF(ccc, ddd, aaa, bbb, X[ 4],  8);
		bbb = FFF(bbb, ccc, ddd, aaa, X[ 1], 11);
		aaa = FFF(aaa, bbb, ccc, ddd, X[ 3], 14);
		ddd = FFF(ddd, aaa, bbb, ccc, X[11], 14);
		ccc = FFF(ccc, ddd, aaa, bbb, X[15],  6);
		bbb = FFF(bbb, ccc, ddd, aaa, X[ 0], 14);
		aaa = FFF(aaa, bbb, ccc, ddd, X[ 5],  6);
		ddd = FFF(ddd, aaa, bbb, ccc, X[12],  9);
		ccc = FFF(ccc, ddd, aaa, bbb, X[ 2], 12);
		bbb = FFF(bbb, ccc, ddd, aaa, X[13],  9);
		aaa = FFF(aaa, bbb, ccc, ddd, X[ 9], 12);
		ddd = FFF(ddd, aaa, bbb, ccc, X[ 7],  5);
		ccc = FFF(ccc, ddd, aaa, bbb, X[10], 15);
		bbb = FFF(bbb, ccc, ddd, aaa, X[14],  8);


		A, B, C, D = AND(B + cc + ddd, 0xFFFFFFFF),
					 AND(C + dd + aaa, 0xFFFFFFFF),
					 AND(D + aa + bbb, 0xFFFFFFFF),
					 AND(A + bb + ccc, 0xFFFFFFFF);

	end

	public.init = function()
		queue.reset();

		A = 0x67452301;
		B = 0xefcdab89;
		C = 0x98badcfe;
		D = 0x10325476;

		return public;
	end


	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if(queue.size() >= 64) then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);

		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		return { b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,
				b10,b11,b12,b13,b14,b15};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);

		local fmt = "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x";

		return String.format(fmt,
				 b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,
				b10,b11,b12,b13,b14,b15);
	end

	return public;

end

return RIPEMD128;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/ripemd160.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--RIPEMD160 is little-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b3; i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b0);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b0 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b3 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (j*0x100000000)+i;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(Math.floor(i/0x100000000));
	local b0,b1,b2,b3 = word2bytes(i);
	return b0,b1,b2,b3,b4,b5,b6,b7;
end

local F = function(x,y,z) return XOR(x, XOR(y,z)); end
local G = function(x,y,z) return OR(AND(x,y), AND(NOT(x),z)); end
local H = function(x,y,z) return XOR(OR(x,NOT(y)),z); end
local I = function(x,y,z) return OR(AND(x,z),AND(y,NOT(z))); end
local J = function(x,y,z) return XOR(x,OR(y,NOT(z))); end

local FF = function(a,b,c,d,e,x,s)
	a = a + F(b,c,d) + x;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local GG = function(a,b,c,d,e,x,s)
	a = a + G(b,c,d) + x + 0x5a827999;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local HH = function(a,b,c,d,e,x,s)
	a = a + H(b,c,d) + x + 0x6ed9eba1;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local II = function(a,b,c,d,e,x,s)
	a = a + I(b,c,d) + x + 0x8f1bbcdc;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local JJ = function(a,b,c,d,e,x,s)
	a = a + J(b,c,d) + x + 0xa953fd4e;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local FFF = function(a,b,c,d,e,x,s)
	a = a + F(b,c,d) + x;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local GGG = function(a,b,c,d,e,x,s)
	a = a + G(b,c,d) + x + 0x7a6d76e9;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local HHH = function(a,b,c,d,e,x,s)
	a = a + H(b,c,d) + x + 0x6d703ef3;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local III = function(a,b,c,d,e,x,s)
	a = a + I(b,c,d) + x + 0x5c4dd124;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local JJJ = function(a,b,c,d,e,x,s)
	a = a + J(b,c,d) + x + 0x50a28be6;
	a = LROT(a,s) + e;
	a = AND(a, 0xFFFFFFFF);
	return a;
end

local RIPEMD160 = function()

	local queue = Queue();

	local A = 0x67452301;
	local B = 0xefcdab89;
	local C = 0x98badcfe;
	local D = 0x10325476;
	local E = 0xc3d2e1f0;

	local public = {};

	local processBlock = function()
		local aa,bb,cc,dd,ee = A,B,C,D,E;
		local aaa,bbb,ccc,ddd,eee = A,B,C,D,E;

		local X = {};

		for i=0,15 do
			X[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		aa, cc = FF(aa, bb, cc, dd, ee, X[ 0], 11), LROT(cc,10);
		ee, bb = FF(ee, aa, bb, cc, dd, X[ 1], 14), LROT(bb,10);
		dd, aa = FF(dd, ee, aa, bb, cc, X[ 2], 15), LROT(aa,10);
		cc, ee = FF(cc, dd, ee, aa, bb, X[ 3], 12), LROT(ee,10);
		bb, dd = FF(bb, cc, dd, ee, aa, X[ 4],  5), LROT(dd,10);
		aa, cc = FF(aa, bb, cc, dd, ee, X[ 5],  8), LROT(cc,10);
		ee, bb = FF(ee, aa, bb, cc, dd, X[ 6],  7), LROT(bb,10);
		dd, aa = FF(dd, ee, aa, bb, cc, X[ 7],  9), LROT(aa,10);
		cc, ee = FF(cc, dd, ee, aa, bb, X[ 8], 11), LROT(ee,10);
		bb, dd = FF(bb, cc, dd, ee, aa, X[ 9], 13), LROT(dd,10);
		aa, cc = FF(aa, bb, cc, dd, ee, X[10], 14), LROT(cc,10);
		ee, bb = FF(ee, aa, bb, cc, dd, X[11], 15), LROT(bb,10);
		dd, aa = FF(dd, ee, aa, bb, cc, X[12],  6), LROT(aa,10);
		cc, ee = FF(cc, dd, ee, aa, bb, X[13],  7), LROT(ee,10);
		bb, dd = FF(bb, cc, dd, ee, aa, X[14],  9), LROT(dd,10);
		aa, cc = FF(aa, bb, cc, dd, ee, X[15],  8), LROT(cc,10);

		ee, bb = GG(ee, aa, bb, cc, dd, X[ 7],  7), LROT(bb,10);
		dd, aa = GG(dd, ee, aa, bb, cc, X[ 4],  6), LROT(aa,10);
		cc, ee = GG(cc, dd, ee, aa, bb, X[13],  8), LROT(ee,10);
		bb, dd = GG(bb, cc, dd, ee, aa, X[ 1], 13), LROT(dd,10);
		aa, cc = GG(aa, bb, cc, dd, ee, X[10], 11), LROT(cc,10);
		ee, bb = GG(ee, aa, bb, cc, dd, X[ 6],  9), LROT(bb,10);
		dd, aa = GG(dd, ee, aa, bb, cc, X[15],  7), LROT(aa,10);
		cc, ee = GG(cc, dd, ee, aa, bb, X[ 3], 15), LROT(ee,10);
		bb, dd = GG(bb, cc, dd, ee, aa, X[12],  7), LROT(dd,10);
		aa, cc = GG(aa, bb, cc, dd, ee, X[ 0], 12), LROT(cc,10);
		ee, bb = GG(ee, aa, bb, cc, dd, X[ 9], 15), LROT(bb,10);
		dd, aa = GG(dd, ee, aa, bb, cc, X[ 5],  9), LROT(aa,10);
		cc, ee = GG(cc, dd, ee, aa, bb, X[ 2], 11), LROT(ee,10);
		bb, dd = GG(bb, cc, dd, ee, aa, X[14],  7), LROT(dd,10);
		aa, cc = GG(aa, bb, cc, dd, ee, X[11], 13), LROT(cc,10);
		ee, bb = GG(ee, aa, bb, cc, dd, X[ 8], 12), LROT(bb,10);

		dd, aa = HH(dd, ee, aa, bb, cc, X[ 3], 11), LROT(aa,10);
		cc, ee = HH(cc, dd, ee, aa, bb, X[10], 13), LROT(ee,10);
		bb, dd = HH(bb, cc, dd, ee, aa, X[14],  6), LROT(dd,10);
		aa, cc = HH(aa, bb, cc, dd, ee, X[ 4],  7), LROT(cc,10);
		ee, bb = HH(ee, aa, bb, cc, dd, X[ 9], 14), LROT(bb,10);
		dd, aa = HH(dd, ee, aa, bb, cc, X[15],  9), LROT(aa,10);
		cc, ee = HH(cc, dd, ee, aa, bb, X[ 8], 13), LROT(ee,10);
		bb, dd = HH(bb, cc, dd, ee, aa, X[ 1], 15), LROT(dd,10);
		aa, cc = HH(aa, bb, cc, dd, ee, X[ 2], 14), LROT(cc,10);
		ee, bb = HH(ee, aa, bb, cc, dd, X[ 7],  8), LROT(bb,10);
		dd, aa = HH(dd, ee, aa, bb, cc, X[ 0], 13), LROT(aa,10);
		cc, ee = HH(cc, dd, ee, aa, bb, X[ 6],  6), LROT(ee,10);
		bb, dd = HH(bb, cc, dd, ee, aa, X[13],  5), LROT(dd,10);
		aa, cc = HH(aa, bb, cc, dd, ee, X[11], 12), LROT(cc,10);
		ee, bb = HH(ee, aa, bb, cc, dd, X[ 5],  7), LROT(bb,10);
		dd, aa = HH(dd, ee, aa, bb, cc, X[12],  5), LROT(aa,10);

		cc, ee = II(cc, dd, ee, aa, bb, X[ 1], 11), LROT(ee,10);
		bb, dd = II(bb, cc, dd, ee, aa, X[ 9], 12), LROT(dd,10);
		aa, cc = II(aa, bb, cc, dd, ee, X[11], 14), LROT(cc,10);
		ee, bb = II(ee, aa, bb, cc, dd, X[10], 15), LROT(bb,10);
		dd, aa = II(dd, ee, aa, bb, cc, X[ 0], 14), LROT(aa,10);
		cc, ee = II(cc, dd, ee, aa, bb, X[ 8], 15), LROT(ee,10);
		bb, dd = II(bb, cc, dd, ee, aa, X[12],  9), LROT(dd,10);
		aa, cc = II(aa, bb, cc, dd, ee, X[ 4],  8), LROT(cc,10);
		ee, bb = II(ee, aa, bb, cc, dd, X[13],  9), LROT(bb,10);
		dd, aa = II(dd, ee, aa, bb, cc, X[ 3], 14), LROT(aa,10);
		cc, ee = II(cc, dd, ee, aa, bb, X[ 7],  5), LROT(ee,10);
		bb, dd = II(bb, cc, dd, ee, aa, X[15],  6), LROT(dd,10);
		aa, cc = II(aa, bb, cc, dd, ee, X[14],  8), LROT(cc,10);
		ee, bb = II(ee, aa, bb, cc, dd, X[ 5],  6), LROT(bb,10);
		dd, aa = II(dd, ee, aa, bb, cc, X[ 6],  5), LROT(aa,10);
		cc, ee = II(cc, dd, ee, aa, bb, X[ 2], 12), LROT(ee,10);

		bb, dd = JJ(bb, cc, dd, ee, aa, X[ 4],  9), LROT(dd,10);
		aa, cc = JJ(aa, bb, cc, dd, ee, X[ 0], 15), LROT(cc,10);
		ee, bb = JJ(ee, aa, bb, cc, dd, X[ 5],  5), LROT(bb,10);
		dd, aa = JJ(dd, ee, aa, bb, cc, X[ 9], 11), LROT(aa,10);
		cc, ee = JJ(cc, dd, ee, aa, bb, X[ 7],  6), LROT(ee,10);
		bb, dd = JJ(bb, cc, dd, ee, aa, X[12],  8), LROT(dd,10);
		aa, cc = JJ(aa, bb, cc, dd, ee, X[ 2], 13), LROT(cc,10);
		ee, bb = JJ(ee, aa, bb, cc, dd, X[10], 12), LROT(bb,10);
		dd, aa = JJ(dd, ee, aa, bb, cc, X[14],  5), LROT(aa,10);
		cc, ee = JJ(cc, dd, ee, aa, bb, X[ 1], 12), LROT(ee,10);
		bb, dd = JJ(bb, cc, dd, ee, aa, X[ 3], 13), LROT(dd,10);
		aa, cc = JJ(aa, bb, cc, dd, ee, X[ 8], 14), LROT(cc,10);
		ee, bb = JJ(ee, aa, bb, cc, dd, X[11], 11), LROT(bb,10);
		dd, aa = JJ(dd, ee, aa, bb, cc, X[ 6],  8), LROT(aa,10);
		cc, ee = JJ(cc, dd, ee, aa, bb, X[15],  5), LROT(ee,10);
		bb, dd = JJ(bb, cc, dd, ee, aa, X[13],  6), LROT(dd,10);

		aaa, ccc = JJJ(aaa, bbb, ccc, ddd, eee, X[ 5],  8), LROT(ccc,10);
		eee, bbb = JJJ(eee, aaa, bbb, ccc, ddd, X[14],  9), LROT(bbb,10);
		ddd, aaa = JJJ(ddd, eee, aaa, bbb, ccc, X[ 7],  9), LROT(aaa,10);
		ccc, eee = JJJ(ccc, ddd, eee, aaa, bbb, X[ 0], 11), LROT(eee,10);
		bbb, ddd = JJJ(bbb, ccc, ddd, eee, aaa, X[ 9], 13), LROT(ddd,10);
		aaa, ccc = JJJ(aaa, bbb, ccc, ddd, eee, X[ 2], 15), LROT(ccc,10);
		eee, bbb = JJJ(eee, aaa, bbb, ccc, ddd, X[11], 15), LROT(bbb,10);
		ddd, aaa = JJJ(ddd, eee, aaa, bbb, ccc, X[ 4],  5), LROT(aaa,10);
		ccc, eee = JJJ(ccc, ddd, eee, aaa, bbb, X[13],  7), LROT(eee,10);
		bbb, ddd = JJJ(bbb, ccc, ddd, eee, aaa, X[ 6],  7), LROT(ddd,10);
		aaa, ccc = JJJ(aaa, bbb, ccc, ddd, eee, X[15],  8), LROT(ccc,10);
		eee, bbb = JJJ(eee, aaa, bbb, ccc, ddd, X[ 8], 11), LROT(bbb,10);
		ddd, aaa = JJJ(ddd, eee, aaa, bbb, ccc, X[ 1], 14), LROT(aaa,10);
		ccc, eee = JJJ(ccc, ddd, eee, aaa, bbb, X[10], 14), LROT(eee,10);
		bbb, ddd = JJJ(bbb, ccc, ddd, eee, aaa, X[ 3], 12), LROT(ddd,10);
		aaa, ccc = JJJ(aaa, bbb, ccc, ddd, eee, X[12],  6), LROT(ccc,10);

		eee, bbb = III(eee, aaa, bbb, ccc, ddd, X[ 6],  9), LROT(bbb,10);
		ddd, aaa = III(ddd, eee, aaa, bbb, ccc, X[11], 13), LROT(aaa,10);
		ccc, eee = III(ccc, ddd, eee, aaa, bbb, X[ 3], 15), LROT(eee,10);
		bbb, ddd = III(bbb, ccc, ddd, eee, aaa, X[ 7],  7), LROT(ddd,10);
		aaa, ccc = III(aaa, bbb, ccc, ddd, eee, X[ 0], 12), LROT(ccc,10);
		eee, bbb = III(eee, aaa, bbb, ccc, ddd, X[13],  8), LROT(bbb,10);
		ddd, aaa = III(ddd, eee, aaa, bbb, ccc, X[ 5],  9), LROT(aaa,10);
		ccc, eee = III(ccc, ddd, eee, aaa, bbb, X[10], 11), LROT(eee,10);
		bbb, ddd = III(bbb, ccc, ddd, eee, aaa, X[14],  7), LROT(ddd,10);
		aaa, ccc = III(aaa, bbb, ccc, ddd, eee, X[15],  7), LROT(ccc,10);
		eee, bbb = III(eee, aaa, bbb, ccc, ddd, X[ 8], 12), LROT(bbb,10);
		ddd, aaa = III(ddd, eee, aaa, bbb, ccc, X[12],  7), LROT(aaa,10);
		ccc, eee = III(ccc, ddd, eee, aaa, bbb, X[ 4],  6), LROT(eee,10);
		bbb, ddd = III(bbb, ccc, ddd, eee, aaa, X[ 9], 15), LROT(ddd,10);
		aaa, ccc = III(aaa, bbb, ccc, ddd, eee, X[ 1], 13), LROT(ccc,10);
		eee, bbb = III(eee, aaa, bbb, ccc, ddd, X[ 2], 11), LROT(bbb,10);

		ddd, aaa = HHH(ddd, eee, aaa, bbb, ccc, X[15],  9), LROT(aaa,10);
		ccc, eee = HHH(ccc, ddd, eee, aaa, bbb, X[ 5],  7), LROT(eee,10);
		bbb, ddd = HHH(bbb, ccc, ddd, eee, aaa, X[ 1], 15), LROT(ddd,10);
		aaa, ccc = HHH(aaa, bbb, ccc, ddd, eee, X[ 3], 11), LROT(ccc,10);
		eee, bbb = HHH(eee, aaa, bbb, ccc, ddd, X[ 7],  8), LROT(bbb,10);
		ddd, aaa = HHH(ddd, eee, aaa, bbb, ccc, X[14],  6), LROT(aaa,10);
		ccc, eee = HHH(ccc, ddd, eee, aaa, bbb, X[ 6],  6), LROT(eee,10);
		bbb, ddd = HHH(bbb, ccc, ddd, eee, aaa, X[ 9], 14), LROT(ddd,10);
		aaa, ccc = HHH(aaa, bbb, ccc, ddd, eee, X[11], 12), LROT(ccc,10);
		eee, bbb = HHH(eee, aaa, bbb, ccc, ddd, X[ 8], 13), LROT(bbb,10);
		ddd, aaa = HHH(ddd, eee, aaa, bbb, ccc, X[12],  5), LROT(aaa,10);
		ccc, eee = HHH(ccc, ddd, eee, aaa, bbb, X[ 2], 14), LROT(eee,10);
		bbb, ddd = HHH(bbb, ccc, ddd, eee, aaa, X[10], 13), LROT(ddd,10);
		aaa, ccc = HHH(aaa, bbb, ccc, ddd, eee, X[ 0], 13), LROT(ccc,10);
		eee, bbb = HHH(eee, aaa, bbb, ccc, ddd, X[ 4],  7), LROT(bbb,10);
		ddd, aaa = HHH(ddd, eee, aaa, bbb, ccc, X[13],  5), LROT(aaa,10);

		ccc, eee = GGG(ccc, ddd, eee, aaa, bbb, X[ 8], 15), LROT(eee,10);
		bbb, ddd = GGG(bbb, ccc, ddd, eee, aaa, X[ 6],  5), LROT(ddd,10);
		aaa, ccc = GGG(aaa, bbb, ccc, ddd, eee, X[ 4],  8), LROT(ccc,10);
		eee, bbb = GGG(eee, aaa, bbb, ccc, ddd, X[ 1], 11), LROT(bbb,10);
		ddd, aaa = GGG(ddd, eee, aaa, bbb, ccc, X[ 3], 14), LROT(aaa,10);
		ccc, eee = GGG(ccc, ddd, eee, aaa, bbb, X[11], 14), LROT(eee,10);
		bbb, ddd = GGG(bbb, ccc, ddd, eee, aaa, X[15],  6), LROT(ddd,10);
		aaa, ccc = GGG(aaa, bbb, ccc, ddd, eee, X[ 0], 14), LROT(ccc,10);
		eee, bbb = GGG(eee, aaa, bbb, ccc, ddd, X[ 5],  6), LROT(bbb,10);
		ddd, aaa = GGG(ddd, eee, aaa, bbb, ccc, X[12],  9), LROT(aaa,10);
		ccc, eee = GGG(ccc, ddd, eee, aaa, bbb, X[ 2], 12), LROT(eee,10);
		bbb, ddd = GGG(bbb, ccc, ddd, eee, aaa, X[13],  9), LROT(ddd,10);
		aaa, ccc = GGG(aaa, bbb, ccc, ddd, eee, X[ 9], 12), LROT(ccc,10);
		eee, bbb = GGG(eee, aaa, bbb, ccc, ddd, X[ 7],  5), LROT(bbb,10);
		ddd, aaa = GGG(ddd, eee, aaa, bbb, ccc, X[10], 15), LROT(aaa,10);
		ccc, eee = GGG(ccc, ddd, eee, aaa, bbb, X[14],  8), LROT(eee,10);

		bbb, ddd = FFF(bbb, ccc, ddd, eee, aaa, X[12] ,  8), LROT(ddd,10);
		aaa, ccc = FFF(aaa, bbb, ccc, ddd, eee, X[15] ,  5), LROT(ccc,10);
		eee, bbb = FFF(eee, aaa, bbb, ccc, ddd, X[10] , 12), LROT(bbb,10);
		ddd, aaa = FFF(ddd, eee, aaa, bbb, ccc, X[ 4] ,  9), LROT(aaa,10);
		ccc, eee = FFF(ccc, ddd, eee, aaa, bbb, X[ 1] , 12), LROT(eee,10);
		bbb, ddd = FFF(bbb, ccc, ddd, eee, aaa, X[ 5] ,  5), LROT(ddd,10);
		aaa, ccc = FFF(aaa, bbb, ccc, ddd, eee, X[ 8] , 14), LROT(ccc,10);
		eee, bbb = FFF(eee, aaa, bbb, ccc, ddd, X[ 7] ,  6), LROT(bbb,10);
		ddd, aaa = FFF(ddd, eee, aaa, bbb, ccc, X[ 6] ,  8), LROT(aaa,10);
		ccc, eee = FFF(ccc, ddd, eee, aaa, bbb, X[ 2] , 13), LROT(eee,10);
		bbb, ddd = FFF(bbb, ccc, ddd, eee, aaa, X[13] ,  6), LROT(ddd,10);
		aaa, ccc = FFF(aaa, bbb, ccc, ddd, eee, X[14] ,  5), LROT(ccc,10);
		eee, bbb = FFF(eee, aaa, bbb, ccc, ddd, X[ 0] , 15), LROT(bbb,10);
		ddd, aaa = FFF(ddd, eee, aaa, bbb, ccc, X[ 3] , 13), LROT(aaa,10);
		ccc, eee = FFF(ccc, ddd, eee, aaa, bbb, X[ 9] , 11), LROT(eee,10);
		bbb, ddd = FFF(bbb, ccc, ddd, eee, aaa, X[11] , 11), LROT(ddd,10);

		A, B, C, D, E = AND(B + cc + ddd, 0xFFFFFFFF),
						AND(C + dd + eee, 0xFFFFFFFF),
						AND(D + ee + aaa, 0xFFFFFFFF),
						AND(E + aa + bbb, 0xFFFFFFFF),
						AND(A + bb + ccc, 0xFFFFFFFF);

	end

	public.init = function()
		queue.reset();

		A = 0x67452301;
		B = 0xefcdab89;
		C = 0x98badcfe;
		D = 0x10325476;
		E = 0xc3d2e1f0;

		return public;
	end

	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if(queue.size() >= 64) then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);

		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);
		local b16,b17,b18,b19 = word2bytes(E);

		return { b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,
				b10,b11,b12,b13,b14,b15,b16,b17,b18,b19};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(A);
		local  b4, b5, b6, b7 = word2bytes(B);
		local  b8, b9,b10,b11 = word2bytes(C);
		local b12,b13,b14,b15 = word2bytes(D);
		local b16,b17,b18,b19 = word2bytes(E);

		local fmt = "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x";

		return String.format(fmt,
				 b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,
				b10,b11,b12,b13,b14,b15,b16,b17,b18,b19);
	end

	return public;

end

return RIPEMD160;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/sha1.lua"] = [=[require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox").insecure();

local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--SHA1 is big-endian
local bytes2word = function(b0, b1, b2, b3)
    local i = b0; i = LSHIFT(i, 8);
    i = OR(i, b1); i = LSHIFT(i, 8);
    i = OR(i, b2); i = LSHIFT(i, 8);
    i = OR(i, b3);
    return i;
end

local word2bytes = function(word)
    local b0, b1, b2, b3;
    b3 = AND(word, 0xFF); word = RSHIFT(word, 8);
    b2 = AND(word, 0xFF); word = RSHIFT(word, 8);
    b1 = AND(word, 0xFF); word = RSHIFT(word, 8);
    b0 = AND(word, 0xFF);
    return b0, b1, b2, b3;
end

local bytes2dword = function(b0, b1, b2, b3, b4, b5, b6, b7)
    local i = bytes2word(b0, b1, b2, b3);
    local j = bytes2word(b4, b5, b6, b7);
    return (i * 0x100000000) + j;
end

local dword2bytes = function(i)
    local b4, b5, b6, b7 = word2bytes(i);
    local b0, b1, b2, b3 = word2bytes(Math.floor(i / 0x100000000));
    return b0, b1, b2, b3, b4, b5, b6, b7;
end

local F = function(x, y, z) return XOR(z, AND(x, XOR(y, z))); end
local G = function(x, y, z) return XOR(x, XOR(y, z)); end
local H = function(x, y, z) return OR(AND(x, OR(y, z)), AND(y, z)); end
local I = function(x, y, z) return XOR(x, XOR(y, z)); end

local SHA1 = function()

    local queue = Queue();

    local h0 = 0x67452301;
    local h1 = 0xEFCDAB89;
    local h2 = 0x98BADCFE;
    local h3 = 0x10325476;
    local h4 = 0xC3D2E1F0;

    local public = {};

    local processBlock = function()
        local a = h0;
        local b = h1;
        local c = h2;
        local d = h3;
        local e = h4;
        local temp;
        local k;

        local w = {};
        for i = 0, 15 do
            w[i] = bytes2word(queue.pop(), queue.pop(), queue.pop(), queue.pop());
        end

        for i = 16, 79 do
            w[i] = LROT((XOR(XOR(w[i - 3], w[i - 8]), XOR(w[i - 14], w[i - 16]))), 1);
        end

        for i = 0, 79 do
            if (i <= 19) then
                temp = F(b, c, d);
                k = 0x5A827999;
            elseif (i <= 39) then
                temp = G(b, c, d);
                k = 0x6ED9EBA1;
            elseif (i <= 59) then
                temp = H(b, c, d);
                k = 0x8F1BBCDC;
            else
                temp = I(b, c, d);
                k = 0xCA62C1D6;
            end
            temp = LROT(a, 5) + temp + e + k + w[i];
            e = d;
            d = c;
            c = LROT(b, 30);
            b = a;
            a = temp;
        end

        h0 = AND(h0 + a, 0xFFFFFFFF);
        h1 = AND(h1 + b, 0xFFFFFFFF);
        h2 = AND(h2 + c, 0xFFFFFFFF);
        h3 = AND(h3 + d, 0xFFFFFFFF);
        h4 = AND(h4 + e, 0xFFFFFFFF);
    end

    public.init = function()
        queue.reset();
        h0 = 0x67452301;
        h1 = 0xEFCDAB89;
        h2 = 0x98BADCFE;
        h3 = 0x10325476;
        h4 = 0xC3D2E1F0;
        return public;
    end


    public.update = function(bytes)
        for b in bytes do
            queue.push(b);
            if queue.size() >= 64 then processBlock(); end
        end

        return public;
    end

    public.finish = function()
        local bits = queue.getHead() * 8;

        queue.push(0x80);
        while ((queue.size() + 7) % 64) < 63 do
            queue.push(0x00);
        end

        local b0, b1, b2, b3, b4, b5, b6, b7 = dword2bytes(bits);

        queue.push(b0);
        queue.push(b1);
        queue.push(b2);
        queue.push(b3);
        queue.push(b4);
        queue.push(b5);
        queue.push(b6);
        queue.push(b7);

        while queue.size() > 0 do
            processBlock();
        end

        return public;
    end

    public.asBytes = function()
        local  b0, b1, b2, b3 = word2bytes(h0);
        local  b4, b5, b6, b7 = word2bytes(h1);
        local  b8, b9, b10, b11 = word2bytes(h2);
        local b12, b13, b14, b15 = word2bytes(h3);
        local b16, b17, b18, b19 = word2bytes(h4);

        return {b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19};
    end

    public.asHex = function()
        local  b0, b1, b2, b3 = word2bytes(h0);
        local  b4, b5, b6, b7 = word2bytes(h1);
        local  b8, b9, b10, b11 = word2bytes(h2);
        local b12, b13, b14, b15 = word2bytes(h3);
        local b16, b17, b18, b19 = word2bytes(h4);

        return String.format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19);
    end

    return public;
end

return SHA1;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/sha2_224.lua"] = [=[local Bit = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local CONSTANTS = {
   0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
   0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
   0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
   0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
   0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
   0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
   0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
   0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2  };

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--SHA2 is big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (i*0x100000000)+j;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(i);
	local b0,b1,b2,b3 = word2bytes(Math.floor(i/0x100000000));
	return b0,b1,b2,b3,b4,b5,b6,b7;
end




local SHA2_224 = function()

	local queue = Queue();

	local h0 = 0xc1059ed8;
	local h1 = 0x367cd507;
	local h2 = 0x3070dd17;
	local h3 = 0xf70e5939;
	local h4 = 0xffc00b31;
	local h5 = 0x68581511;
	local h6 = 0x64f98fa7;
	local h7 = 0xbefa4fa4;

	local public = {};

	local processBlock = function()
		local a = h0;
		local b = h1;
		local c = h2;
		local d = h3;
		local e = h4;
		local f = h5;
		local g = h6;
		local h = h7;

		local w = {};

		for i=0,15 do
			w[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		for i=16,63 do
			local s0 = XOR(RROT(w[i-15],7), XOR(RROT(w[i-15],18), RSHIFT(w[i-15],3)));
			local s1 = XOR(RROT(w[i-2],17), XOR(RROT(w[i-2], 19), RSHIFT(w[i-2],10)));
			w[i] = AND(w[i-16] + s0 + w[i-7] + s1, 0xFFFFFFFF);
		end

		for i=0,63 do
			local s1 = XOR(RROT(e,6), XOR(RROT(e,11),RROT(e,25)));
			local ch = XOR(AND(e,f), AND(NOT(e),g));
			local temp1 = h + s1 + ch + CONSTANTS[i+1] + w[i];
			local s0 = XOR(RROT(a,2), XOR(RROT(a,13), RROT(a,22)));
			local maj = XOR(AND(a,b), XOR(AND(a,c), AND(b,c)));
			local temp2 = s0 + maj;

			h = g;
			g = f;
			f = e;
			e = d + temp1;
			d = c;
			c = b;
			b = a;
			a = temp1 + temp2;
		end

		h0 = AND(h0 + a, 0xFFFFFFFF);
		h1 = AND(h1 + b, 0xFFFFFFFF);
		h2 = AND(h2 + c, 0xFFFFFFFF);
		h3 = AND(h3 + d, 0xFFFFFFFF);
		h4 = AND(h4 + e, 0xFFFFFFFF);
		h5 = AND(h5 + f, 0xFFFFFFFF);
		h6 = AND(h6 + g, 0xFFFFFFFF);
		h7 = AND(h7 + h, 0xFFFFFFFF);
	end

	public.init = function()
		queue.reset();

		h0 = 0xc1059ed8;
		h1 = 0x367cd507;
		h2 = 0x3070dd17;
		h3 = 0xf70e5939;
		h4 = 0xffc00b31;
		h5 = 0x68581511;
		h6 = 0x64f98fa7;
		h7 = 0xbefa4fa4;

		return public;
	end

	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if queue.size() >= 64 then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);
		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(h0);
		local  b4, b5, b6, b7 = word2bytes(h1);
		local  b8, b9,b10,b11 = word2bytes(h2);
		local b12,b13,b14,b15 = word2bytes(h3);
		local b16,b17,b18,b19 = word2bytes(h4);
		local b20,b21,b22,b23 = word2bytes(h5);
		local b24,b25,b26,b27 = word2bytes(h6);

		return {  b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,b10,b11,b12,b13,b14,b15
				,b16,b17,b18,b19,b20,b21,b22,b23,b24,b25,b26,b27};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(h0);
		local  b4, b5, b6, b7 = word2bytes(h1);
		local  b8, b9,b10,b11 = word2bytes(h2);
		local b12,b13,b14,b15 = word2bytes(h3);
		local b16,b17,b18,b19 = word2bytes(h4);
		local b20,b21,b22,b23 = word2bytes(h5);
		local b24,b25,b26,b27 = word2bytes(h6);

		local fmt = "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"

		return String.format(fmt, b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,b10,b11,b12,b13,b14,b15
				,b16,b17,b18,b19,b20,b21,b22,b23,b24,b25,b26,b27);
	end

	return public;

end

return SHA2_224;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/digest/sha2_256.lua"] = [=[local Bit = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Math = require("math");
local Queue = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");

local CONSTANTS = {
   0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
   0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
   0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
   0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
   0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
   0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
   0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
   0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2  };

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--SHA2 is big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (i*0x100000000)+j;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(i);
	local b0,b1,b2,b3 = word2bytes(Math.floor(i/0x100000000));
	return b0,b1,b2,b3,b4,b5,b6,b7;
end




local SHA2_256 = function()

	local queue = Queue();

	local h0 = 0x6a09e667;
	local h1 = 0xbb67ae85;
	local h2 = 0x3c6ef372;
	local h3 = 0xa54ff53a;
	local h4 = 0x510e527f;
	local h5 = 0x9b05688c;
	local h6 = 0x1f83d9ab;
	local h7 = 0x5be0cd19;

	local public = {};

	local processBlock = function()
		local a = h0;
		local b = h1;
		local c = h2;
		local d = h3;
		local e = h4;
		local f = h5;
		local g = h6;
		local h = h7;

		local w = {};

		for i=0,15 do
			w[i] = bytes2word(queue.pop(),queue.pop(),queue.pop(),queue.pop());
		end

		for i=16,63 do
			local s0 = XOR(RROT(w[i-15],7), XOR(RROT(w[i-15],18), RSHIFT(w[i-15],3)));
			local s1 = XOR(RROT(w[i-2],17), XOR(RROT(w[i-2], 19), RSHIFT(w[i-2],10)));
			w[i] = AND(w[i-16] + s0 + w[i-7] + s1, 0xFFFFFFFF);
		end

		for i=0,63 do
			local s1 = XOR(RROT(e,6), XOR(RROT(e,11),RROT(e,25)));
			local ch = XOR(AND(e,f), AND(NOT(e),g));
			local temp1 = h + s1 + ch + CONSTANTS[i+1] + w[i];
			local s0 = XOR(RROT(a,2), XOR(RROT(a,13), RROT(a,22)));
			local maj = XOR(AND(a,b), XOR(AND(a,c), AND(b,c)));
			local temp2 = s0 + maj;

			h = g;
			g = f;
			f = e;
			e = d + temp1;
			d = c;
			c = b;
			b = a;
			a = temp1 + temp2;
		end

		h0 = AND(h0 + a, 0xFFFFFFFF);
		h1 = AND(h1 + b, 0xFFFFFFFF);
		h2 = AND(h2 + c, 0xFFFFFFFF);
		h3 = AND(h3 + d, 0xFFFFFFFF);
		h4 = AND(h4 + e, 0xFFFFFFFF);
		h5 = AND(h5 + f, 0xFFFFFFFF);
		h6 = AND(h6 + g, 0xFFFFFFFF);
		h7 = AND(h7 + h, 0xFFFFFFFF);
	end

	public.init = function()
		queue.reset();

		h0 = 0x6a09e667;
		h1 = 0xbb67ae85;
		h2 = 0x3c6ef372;
		h3 = 0xa54ff53a;
		h4 = 0x510e527f;
		h5 = 0x9b05688c;
		h6 = 0x1f83d9ab;
		h7 = 0x5be0cd19;

		return public;
	end

	public.update = function(bytes)
		for b in bytes do
			queue.push(b);
			if queue.size() >= 64 then processBlock(); end
		end

		return public;
	end

	public.finish = function()
		local bits = queue.getHead() * 8;

		queue.push(0x80);
		while ((queue.size()+7) % 64) < 63 do
			queue.push(0x00);
		end

		local b0,b1,b2,b3,b4,b5,b6,b7 = dword2bytes(bits);

		queue.push(b0);
		queue.push(b1);
		queue.push(b2);
		queue.push(b3);
		queue.push(b4);
		queue.push(b5);
		queue.push(b6);
		queue.push(b7);

		while queue.size() > 0 do
			processBlock();
		end

		return public;
	end

	public.asBytes = function()
		local  b0, b1, b2, b3 = word2bytes(h0);
		local  b4, b5, b6, b7 = word2bytes(h1);
		local  b8, b9,b10,b11 = word2bytes(h2);
		local b12,b13,b14,b15 = word2bytes(h3);
		local b16,b17,b18,b19 = word2bytes(h4);
		local b20,b21,b22,b23 = word2bytes(h5);
		local b24,b25,b26,b27 = word2bytes(h6);
		local b28,b29,b30,b31 = word2bytes(h7);


		return {  b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,b10,b11,b12,b13,b14,b15
				,b16,b17,b18,b19,b20,b21,b22,b23,b24,b25,b26,b27,b28,b29,b30,b31};
	end

	public.asHex = function()
		local  b0, b1, b2, b3 = word2bytes(h0);
		local  b4, b5, b6, b7 = word2bytes(h1);
		local  b8, b9,b10,b11 = word2bytes(h2);
		local b12,b13,b14,b15 = word2bytes(h3);
		local b16,b17,b18,b19 = word2bytes(h4);
		local b20,b21,b22,b23 = word2bytes(h5);
		local b24,b25,b26,b27 = word2bytes(h6);
		local b28,b29,b30,b31 = word2bytes(h7);

		local fmt = "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"

		return String.format(fmt, b0, b1, b2, b3, b4, b5, b6, b7, b8, b9,b10,b11,b12,b13,b14,b15
				,b16,b17,b18,b19,b20,b21,b22,b23,b24,b25,b26,b27,b28,b29,b30,b31);
	end

	return public;

end

return SHA2_256;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/kdf/hkdf.lua"] = [=[local HMAC = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.mac.hmac");

local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local HKDF = function()

    local public = {};

    local Digest = nil;
    local inputKeyMaterial;
    local salt = Array.fromHex("0000000000000000000000000000000000000000");
    local info;
    local outputLen;

    local hashLen;
    local secret;

    local extract = function()
        local res = HMAC()
                        .setDigest(Digest)
                        .setKey(salt)
                        .init()
                        .update(Stream.fromArray(inputKeyMaterial))
                        .finish()
                        .asBytes();
        hashLen = #res;
        return res;
    end

    local expand = function(prk)
        local iterations = math.ceil(outputLen / hashLen);
        local mixin = {};
        local results = {};
        local remainingBytes = outputLen;

        for i = 1, iterations do
            local mac = HMAC()
                            .setDigest(Digest)
                            .setKey(prk)
                            .init();

            mac.update(Stream.fromArray(mixin));
            if info then
                mac.update(Stream.fromArray(info));
            end
            mac.update(Stream.fromArray({ i }));

            local stepResult = mac.finish().asBytes();
            local stepSize = math.min(remainingBytes, #stepResult);

            for j = 1, stepSize do
                results[#results + 1] = stepResult[j];
            end

            mixin = stepResult;
            remainingBytes = remainingBytes - stepSize;
        end

        return results;
    end

    public.setDigest = function(digestModule)
        Digest = digestModule;
        return public;
    end

    public.setInputKeyMaterial = function(ikm)
        inputKeyMaterial = ikm;
        return public;
    end

    public.setSalt = function(s)
        salt = s or salt;
        return public;
    end

    public.setInfo = function(i)
        info = i;
        return public;
    end

    public.setOutputLen = function(len)
        outputLen = len;
        return public;
    end

    public.finish = function()
        local prk = extract(salt, inputKeyMaterial);
        secret = expand(prk, info, outputLength);
        return public;
    end

    public.asBytes = function()
        return secret;
    end

    public.asHex = function()
        return Array.toHex(secret);
    end

    return public;
end

return HKDF;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/kdf/pbkdf2.lua"] = [=[local Bit = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Math = require("math");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;

--PBKDF2 is big-endian
local bytes2word = function(b0,b1,b2,b3)
	local i = b0; i = LSHIFT(i,8);
	i = OR(i,b1); i = LSHIFT(i,8);
	i = OR(i,b2); i = LSHIFT(i,8);
	i = OR(i,b3);
	return i;
end

local word2bytes = function(word)
	local b0,b1,b2,b3;
	b3 = AND(word,0xFF); word = RSHIFT(word,8);
	b2 = AND(word,0xFF); word = RSHIFT(word,8);
	b1 = AND(word,0xFF); word = RSHIFT(word,8);
	b0 = AND(word,0xFF);
	return b0,b1,b2,b3;
end

local bytes2dword = function(b0,b1,b2,b3,b4,b5,b6,b7)
	local i = bytes2word(b0,b1,b2,b3);
	local j = bytes2word(b4,b5,b6,b7);
	return (i*0x100000000)+j;
end

local dword2bytes = function(i)
	local b4,b5,b6,b7 = word2bytes(i);
	local b0,b1,b2,b3 = word2bytes(i/0x100000000);
	return b0,b1,b2,b3,b4,b5,b6,b7;
end



local PBKDF2 = function()

	local public = {};

	local blockLen = 16;
	local dKeyLen = 256;
	local iterations = 4096;

	local salt;
	local password;


	local PRF;

	local dKey;


	public.setBlockLen = function(len)
		blockLen = len;
		return public;
	end

	public.setDKeyLen = function(len)
		dKeyLen = len
		return public;
	end

	public.setIterations = function(iter)
		iterations = iter;
		return public;
	end

	public.setSalt = function(saltBytes)
		salt = saltBytes;
		return public;
	end

	public.setPassword = function(passwordBytes)
		password = passwordBytes;
		return public;
	end

	public.setPRF = function(prf)
		PRF = prf;
		return public;
	end

	local buildBlock = function(i)
		local b0,b1,b2,b3 = word2bytes(i);
		local ii = {b0,b1,b2,b3};
		local s = Array.concat(salt,ii);

		local out = {};

		PRF.setKey(password);
		for c = 1,iterations do
			PRF.init()
				.update(Stream.fromArray(s));

			s = PRF.finish().asBytes();
			if(c > 1) then
				out = Array.XOR(out,s);
			else
				out = s;
			end
		end

		return out;
	end

	public.finish = function()
		local blocks = Math.ceil(dKeyLen / blockLen);

		dKey = {};

		for b = 1, blocks do
			local block = buildBlock(b);
			dKey = Array.concat(dKey,block);
		end

		if(Array.size(dKey) > dKeyLen) then dKey = Array.truncate(dKey,dKeyLen); end

		return public;
	end

	public.asBytes = function()
		return dKey;
	end

	public.asHex = function()
		return Array.toHex(dKey);
	end

	return public;
end

return PBKDF2;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/mac/hmac.lua"] = [=[local Bit = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");
local String = require("string");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");
local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");

local XOR = Bit.bxor;

local HMAC = function()

	local public = {};
	local blockSize = 64;
	local Digest = nil;
	local outerPadding = {};
	local innerPadding = {}
	local digest;

	public.setBlockSize = function(bytes)
		blockSize = bytes;
		return public;
	end

	public.setDigest = function(digestModule)
		Digest = digestModule;
		digest = Digest();
		return public;
	end

	public.setKey = function(key)
		local keyStream;

		if(Array.size(key) > blockSize) then
			keyStream = Stream.fromArray(Digest()
						.update(Stream.fromArray(key))
						.finish()
						.asBytes());
		else
			keyStream = Stream.fromArray(key);
		end

		outerPadding = {};
		innerPadding = {};

		for i=1,blockSize do
			local byte = keyStream();
			if byte == nil then byte = 0x00; end
			outerPadding[i] = XOR(0x5C,byte);
			innerPadding[i] = XOR(0x36,byte);
		end

		return public;
	end

	public.init = function()
		digest	.init()
				.update(Stream.fromArray(innerPadding));
		return public;
	end

	public.update = function(messageStream)
		digest.update(messageStream);
		return public;
	end

	public.finish = function()
		local inner = digest.finish().asBytes();
		digest	.init()
				.update(Stream.fromArray(outerPadding))
				.update(Stream.fromArray(inner))
				.finish();

		return public;
	end

	public.asBytes = function()
		return digest.asBytes();
	end

	public.asHex = function()
		return digest.asHex();
	end

	return public;

end

return HMAC;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/padding/ansix923.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");

local ANSIX923Padding = function(blockSize,byteCount)

	local paddingCount = blockSize - (byteCount % blockSize);
	local bytesLeft = paddingCount;

	local stream = function()
		if bytesLeft > 1 then
			bytesLeft = bytesLeft - 1;
			return 0x00;
		elseif bytesLeft > 0 then
			bytesLeft = bytesLeft - 1;
			return paddingCount;
		else
			return nil;
		end
	end

	return stream;

end

return ANSIX923Padding;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/padding/isoiec7816.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");


local ISOIEC7816Padding = function(blockSize,byteCount)

	local paddingCount = blockSize - (byteCount % blockSize);
	local bytesLeft = paddingCount;

	local stream = function()
		if bytesLeft == paddingCount then
			bytesLeft = bytesLeft - 1;
			return 0x80;
		elseif bytesLeft > 0 then
			bytesLeft = bytesLeft - 1;
			return 0x00;
		else
			return nil;
		end
	end

	return stream;

end

return ISOIEC7816Padding;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/padding/pkcs7.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");

local PKCS7Padding = function(blockSize,byteCount)

	local paddingCount = blockSize - ((byteCount -1) % blockSize) + 1;
--    local paddingCount = blockSize - byteCount % blockSize;
	local bytesLeft = paddingCount;

	local stream = function()
		if bytesLeft > 0 then
			bytesLeft = bytesLeft - 1;
			return paddingCount;
		else
			return nil;
		end
	end

	return stream;
end

return PKCS7Padding;
]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/padding/zero.lua"] = [=[local Stream = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");

local ZeroPadding = function(blockSize,byteCount)

	local paddingCount = blockSize - ((byteCount - 1) % blockSize) + 1;
--    local paddingCount = blockSize - byteCount % blockSize;
	local bytesLeft = paddingCount;

	local stream = function()
		if bytesLeft > 0 then
            bytesLeft = bytesLeft - 1;
			return 0x00;
		else
			return nil;
		end
	end

	return stream;

end

return ZeroPadding;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/util/array.lua"] = [=[local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local XOR = Bit.bxor;

local Array = {};

Array.size = function(array)
	return #array;
end

Array.fromString = function(string)
	local bytes = {};

	local i=1;
	local byte = String.byte(string,i);
	while byte ~= nil do
		bytes[i] = byte;
		i = i + 1;
		byte = String.byte(string,i);
	end

	return bytes;

end

Array.toString = function(bytes)
	local chars = {};
	local i=1;

	local byte = bytes[i];
	while byte ~= nil do
		chars[i] = String.char(byte);
		i = i+1;
		byte = bytes[i];
	end

	return table.concat(chars,"");
end

Array.fromStream = function(stream)
	local array = {};
	local i=1;

	local byte = stream();
	while byte ~= nil do
		array[i] = byte;
		i = i+1;
		byte = stream();
	end

	return array;
end

Array.readFromQueue = function(queue,size)
	local array = {};

	for i=1,size do
		array[i] = queue.pop();
	end

	return array;
end

Array.writeToQueue = function(queue,array)
	local size = Array.size(array);

	for i=1,size do
		queue.push(array[i]);
	end
end

Array.toStream = function(array)
	local queue = Queue();
	local i=1;

	local byte = array[i];
	while byte ~= nil do
		queue.push(byte);
		i=i+1;
		byte = array[i];
	end

	return queue.pop;
end


local fromHexTable = {};
for i=0,255 do
	fromHexTable[String.format("%02X",i)]=i;
	fromHexTable[String.format("%02x",i)]=i;
end

Array.fromHex = function(hex)
	local array = {};

	for i=1,String.len(hex)/2 do
		local h = String.sub(hex,i*2-1,i*2);
		array[i] = fromHexTable[h];
	end

	return array;
end


local toHexTable = {};
for i=0,255 do
	toHexTable[i]=String.format("%02X",i);
end

Array.toHex = function(array)
	local hex = {};
	local i = 1;

	local byte = array[i];
	while byte ~= nil do
		hex[i] = toHexTable[byte];
		i=i+1;
		byte = array[i];
	end

	return table.concat(hex,"");

end

Array.concat = function(a,b)
	local concat = {};
	local out=1;

	local i=1;
	local byte = a[i];
	while byte ~= nil do
		concat[out] = byte;
		i = i + 1;
		out = out + 1;
		byte = a[i];
	end

	local i=1;
	local byte = b[i];
	while byte ~= nil do
		concat[out] = byte;
		i = i + 1;
		out = out + 1;
		byte = b[i];
	end

	return concat;
end

Array.truncate = function(a,newSize)
	local x = {};

	for i=1,newSize do
		x[i]=a[i];
	end

	return x;
end

Array.XOR = function(a,b)
	local x = {};

	for k,v in pairs(a) do
		x[k] = XOR(v,b[k]);
	end

	return x;
end

Array.substitute = function(input,sbox)
	local out = {};

	for k,v in pairs(input) do
		out[k] = sbox[v];
	end

	return out;
end

Array.permute = function(input,pbox)
	local out = {};

	for k,v in pairs(pbox) do
		out[k] = input[v];
	end

	return out;
end

Array.copy = function(input)
	local out = {};

	for k,v in pairs(input) do
		out[k] = v;
	end
	return out;
end

Array.slice = function(input,start,stop)
	local out = {};

	for i=start,stop do
		out[i-start+1] = input[i];
	end
	return out;
end

return Array;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/util/base64.lua"] = [=[local String = require("string");
local Bit = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.bit");

local Array = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.array");
local Stream = require("]=] .. _zmm._PLUGIN_DIR_ .. [=[.lockbox.util.stream");

local AND = Bit.band;
local OR  = Bit.bor;
local NOT = Bit.bnot;
local XOR = Bit.bxor;
local LROT = Bit.lrotate;
local RROT = Bit.rrotate;
local LSHIFT = Bit.lshift;
local RSHIFT = Bit.rshift;


local SYMBOLS = {
[0]="A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
    "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
    "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "/"};

local LOOKUP = {};

for k, v in pairs(SYMBOLS) do
    LOOKUP[k] = v;
    LOOKUP[v] = k;
end


local Base64 = {};

Base64.fromStream = function(stream)
    local bits = 0x00;
    local bitCount = 0;
    local base64 = {};

    local byte = stream();
    while byte ~= nil do
        bits = OR(LSHIFT(bits, 8), byte);
        bitCount = bitCount + 8;
        while bitCount >= 6 do
            bitCount = bitCount - 6;
            local temp = RSHIFT(bits, bitCount);
            table.insert(base64, LOOKUP[temp]);
            bits = AND(bits, NOT(LSHIFT(0xFFFFFFFF, bitCount)));
        end
        byte = stream();
    end

    if (bitCount == 4) then
        bits = LSHIFT(bits, 2);
        table.insert(base64, LOOKUP[bits]);
        table.insert(base64, "=");
    elseif (bitCount == 2) then
        bits = LSHIFT(bits, 4);
        table.insert(base64, LOOKUP[bits]);
        table.insert(base64, "==");
    end

    return table.concat(base64, "");
end

Base64.fromArray = function(array)
    local ind = 0;

    local streamArray = function()
        ind = ind + 1;
        return array[ind];
    end

    return Base64.fromStream(streamArray);
end

Base64.fromString = function(string)
    return Base64.fromStream(Stream.fromString(string));
end



Base64.toStream = function(base64)
    local stream = coroutine.create(function()
      local bits = 0x00;
      local bitCount = 0;

      local yield = coroutine.yield;

      for c in String.gmatch(base64, ".") do
          if (c == "=") then
              bits = RSHIFT(bits, 2); bitCount = bitCount - 2;
          else
              bits = LSHIFT(bits, 6); bitCount = bitCount + 6;
              bits = OR(bits, LOOKUP[c]);
          end

          while(bitCount >= 8) do
              bitCount = bitCount - 8;
              local byte = RSHIFT(bits, bitCount);
              bits = AND(bits, NOT(LSHIFT(0xFFFFFFFF, bitCount)));
              yield(byte);
          end
      end
    end)

    local resume = coroutine.resume;
    local status = coroutine.status;

    return function()
        if status(stream) == 'dead' then return nil; end

        local _, byte = coroutine.resume(stream);
        return byte;
    end
end

Base64.toArray = function(base64)
    return Stream.toArray(Base64.toStream(base64));
end

Base64.toString = function(base64)
    return Stream.toString(Base64.toStream(base64));
end

return Base64;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/util/bit.lua"] = [=[local ok, e
if not ok then
	ok, e = pcall(require, "bit") -- the LuaJIT one ?
end
if not ok then
	ok, e = pcall(require, "bit32") -- Lua 5.2
end
if not ok then
	ok, e = pcall(require, "bit.numberlua") -- for Lua 5.1, https://github.com/tst2005/lua-bit-numberlua/
end
if not ok then
	error("no bitwise support found", 2)
end
assert(type(e)=="table", "invalid bit module")

-- Workaround to support Lua 5.2 bit32 API with the LuaJIT bit one
if e.rol and not e.lrotate then
	e.lrotate = e.rol
end
if e.ror and not e.rrotate then
	e.rrotate = e.ror
end

return e]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/util/queue.lua"] = [=[local Queue = function()
	local queue = {};
	local tail = 0;
	local head = 0;

	local public = {};

	public.push = function(obj)
		queue[head] = obj;
		head = head + 1;
		return;
	end

	public.pop = function()
		if tail < head
		then
			local obj = queue[tail];
			queue[tail] = nil;
			tail = tail + 1;
			return obj;
		else
			return nil;
		end
	end

	public.size = function()
		return head - tail;
	end

	public.getHead = function()
		return head;
	end

	public.getTail = function()
		return tail;
	end

	public.reset = function()
		queue = {};
		head = 0;
		tail = 0;
	end

	return public;
end

return Queue;]=]
	luatext[_zmm._GLOBAL_PATH_ .. "lockbox/util/stream.lua"] = [=[local Queue = require("]=] ..
	_zmm._PLUGIN_DIR_ .. [=[.lockbox.util.queue");
local String = require("string");

local Stream = {};


Stream.fromString = function(string)
    local i = 0;
    return function()
        i = i + 1;
        return String.byte(string, i);
    end
end


Stream.toString = function(stream)
    local array = {};
    local i = 1;

    local byte = stream();
    while byte ~= nil do
        array[i] = String.char(byte);
        i = i + 1;
        byte = stream();
    end

    return table.concat(array);
end


Stream.fromArray = function(array)
    local queue = Queue();
    local i = 1;

    local byte = array[i];
    while byte ~= nil do
        queue.push(byte);
        i = i + 1;
        byte = array[i];
    end

    return queue.pop;
end


Stream.toArray = function(stream)
    local array = {};
    local i = 1;

    local byte = stream();
    while byte ~= nil do
        array[i] = byte;
        i = i + 1;
        byte = stream();
    end

    return array;
end


local fromHexTable = {};
for i = 0, 255 do
    fromHexTable[String.format("%02X", i)] = i;
    fromHexTable[String.format("%02x", i)] = i;
end

Stream.fromHex = function(hex)
    local queue = Queue();

    for i = 1, String.len(hex) / 2 do
        local h = String.sub(hex, i * 2 - 1, i * 2);
        queue.push(fromHexTable[h]);
    end

    return queue.pop;
end



local toHexTable = {};
for i = 0, 255 do
    toHexTable[i] = String.format("%02X", i);
end

Stream.toHex = function(stream)
    local hex = {};
    local i = 1;

    local byte = stream();
    while byte ~= nil do
        hex[i] = toHexTable[byte];
        i = i + 1;
        byte = stream();
    end

    return table.concat(hex);
end

return Stream;]=]
	return luatext
end

_zmm.setLockBoxCode = function(path, version)
	local cjson = require("cjson")
	_zmm.filewrite(path, cjson.encode({ version = version }))
	local luatext = _zmm.getLockBoxCode()
	--    os.execute("mkdir lockbox")
	--    os.execute("mkdir lockbox/cipher")
	--    os.execute("mkdir lockbox/cipher/mode")
	--    os.execute("mkdir lockbox/digest")
	--    os.execute("mkdir lockbox/kdf")
	--    os.execute("mkdir lockbox/mac")
	--    os.execute("mkdir lockbox/padding")
	--    os.execute("mkdir lockbox/util")

	for k, v in pairs(luatext) do
		local dir = k:match("^.*%/")
		if LuaAuxLib.DIR_Exist(dir) ~= 2 then
			LuaAuxLib.DIR_Create(dir)
		end
		_zmm.filewrite(k, v)
	end
end


function _zmm.InitLockBox()
	_zmm.CreatePluginDir()
	local json_path = _zmm._GLOBAL_PATH_ .. "zm_InitLockBox.json"
	local isok, errors = pcall(
		function()
			local version = "1.3"
			local text = _zmm.fileread(json_path)
			if text then
				local cjson = require("cjson")
				local json = cjson.decode(text)
				if json["version"] ~= version then
					_zmm.setLockBoxCode(json_path, version)
				end
			else
				_zmm.setLockBoxCode(json_path, version)
			end

			require(_zmm._PLUGIN_DIR_ .. ".lockbox")
		end
	)
	if isok then
		return errors
	else
		os.remove(json_path)
		traceprint("初始化InitLockBox()失败, 可尝试重启手机解决, 对云识别和加解密相关函数有影响, 错误信息：",
			errors)
	end
end

--[=[
@fname:EncodeHMAC
@cname:HMAC签名
@format:消息, 密钥[, 算法][, 返回格式]
@tname:对消息 $1 以密钥 $2 按算法 $3 进行HMAC签名
@note:计算消息的HMAC签名结果, 支持md2,md4,md5,sha1,sha224,sha256,ripemd128,ripemd160算法
@ret:签名结果, string或table, 返回HMAC签名结果, 数据类型由参数返回格式决定
@arg:消息, string, 待计算的消息内容
@arg:密钥, string, 用于计算的密钥
@arg:算法, string, 可选, 省略默认为sha1算法, 可填写md2,md4,md5,sha1,sha224,sha256,ripemd128,ripemd160
@arg:返回格式, number, 可选, 表示返回值内容, 省略默认为16$n 0为原生二进制数据字符串$n 2为字节数组$n 16为十六进制字符串$n 64为原生内容base64编码
@exp:Dim hmac = zm.EncodeHMAC("紫猫学院", "_ZM_.vip") //对紫猫学院以密钥zimao.vip进行HMAC-SHA1签名返回十六进制字符串
--]=]
function zmm.EncodeHMAC(message, key, digest, out)
	return try {
		function()
			_zmm.InitLockBox()
			--            local Bit = require("lockbox.util.bit");
			--            local String = require("string");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");
			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");

			--            local Queue = require("lockbox.util.queue");

			local HMAC = require(_zmm._PLUGIN_DIR_ .. ".lockbox.mac.hmac");

			if type(digest) == "number" then
				digest, out = out, digest
			end

			digest = digest or "sha1"
			if digest == "sha224" then
				digest = "sha2_224"
			elseif digest == "sha256" then
				digest = "sha2_256"
			end

			digest = digest:lower()
			assert(
			digest == "md2" or digest == "md4" or digest == "md5" or digest == "sha1" or digest == "sha2_224" or
			digest == "sha2_256" or digest == "ripemd128" or digest == "ripemd160", "参数digest不符合要求")
			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.digest." .. digest:lower())

			local v = {
				digest = mode,
				blockSize = 64,
				key = Array.fromString(key),
				message = Stream.fromString(message)
			}

			local hash = HMAC();

			out = out or 16
			assert(out == 0 or out == 2 or out == 16 or out == 64, "参数out只能填写数值0, 2, 16, 64中的一个")
			local res = hash
				.setBlockSize(v.blockSize)
				.setDigest(v.digest)
				.setKey(v.key)
				.init()
				.update(v.message)
				.finish();

			if out == 0 then
				res = Array.toString(res.asBytes())
			elseif out == 2 then
				res = res.asBytes()
			elseif out == 16 then
				res = res.asHex()
			elseif out == 64 then
				res = zmm.EncodeBase64(Array.toString(res.asBytes()))
			end

			return res
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeHMAC()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeAES
@cname:AES加密
@format:明文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行AES加密
@note:对数据进行非标准AES加密, 支持AES128,AES196和AES256
@ret:加密结果, string, 返回AES加密结果
@arg:明文, string, 待加密的内容
@arg:密钥, string, 用于加密的密码, 最大长度为数据块/8
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | 128,192,256 | 128 | 数据块$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim aes = zm.EncodeAES("紫猫学院", "_ZM_.vip") //对紫猫学院以密钥zimao.vip进行AES加密
--]=]
_zmm.AESAttr = {}
_zmm.AESAttr.default = { mode = "ecb", padding = "zero", block = 128, iv = "", cipherout = 16 }
_zmm.AESAttr.mt = { __index = _zmm.AESAttr.default }
function zmm.EncodeAES(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.AESAttr.mt)

			if #key < (t.block / 8) then
				key = key .. string.rep("\0", t.block / 8 - #key)
			elseif #key > (t.block / 8) then
				error("在AES" .. t.block .. "中, 密钥长度不得超过" .. t.block / 8 .. "位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");

			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.aes" .. t.block);

			local v = {}
			v.cipher = mode.Cipher
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.plaintext = Array.fromString(data)
			v.padding = padding

			local cipher = v.cipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local cipherOutput = cipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.plaintext))
				.finish();

			if t.cipherout == 0 then
				cipherOutput = Array.toString(cipherOutput.asBytes())
			elseif t.cipherout == 2 then
				cipherOutput = cipherOutput.asBytes()
			elseif t.cipherout == 16 then
				cipherOutput = cipherOutput.asHex()
			elseif t.cipherout == 64 then
				cipherOutput = zmm.EncodeBase64(Array.toString(cipherOutput.asBytes()))
			end
			return cipherOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeAES()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DecodeAES
@cname:AES解密
@format:密文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行AES解密
@note:对数据进行非标准AES解密, 支持AES128,AES196和AES256
@ret:解密结果, string, 返回AES解密结果
@arg:密文, string, 待解密的密文内容
@arg:密钥, string, 用于解密的密码, 最大长度为数据块/8
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | 128,192,256 | 128 | 数据块$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim aes = zm.DecodeAES("552735436AE4A96E81AE9997914E0DF3", "_ZM_.vip") //解密结果是紫猫学院
--]=]
function zmm.DecodeAES(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.AESAttr.mt)

			--            local String = require("string");

			if #key < (t.block / 8) then
				key = key .. string.rep("\0", t.block / 8 - #key)
			elseif #key > (t.block / 8) then
				error("在AES" .. t.block .. "中, 密钥长度不得超过" .. t.block / 8 .. "位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");


			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.aes" .. t.block);

			local v = {}
			v.decipher = mode.Decipher
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.padding = padding

			if t.cipherout == 0 then
				v.ciphertext = Array.fromString(data)
			elseif t.cipherout == 2 then
				v.ciphertext = data
			elseif t.cipherout == 16 then
				v.ciphertext = Array.fromHex(data)
			elseif t.cipherout == 64 then
				v.ciphertext = Array.fromString(zmm.DecodeBase64(data))
			end

			local decipher = v.decipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local plainOutput = decipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.ciphertext))
				.finish()
				.asBytes();

			plainOutput = Array.toString(plainOutput):gsub("\0", "")
			return plainOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeAES()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeDES
@cname:DES加密
@format:明文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行DES加密
@note:对数据进行非标准DES加密, 支持DES和DES3
@ret:加密结果, string, 返回DES加密结果, 失败返回null
@arg:明文, string, 待加密的内容
@arg:密钥, string, 用于加密的密码, 最大长度为8
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | des,des3 | des | 加密算法$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim des = zm.EncodeDES("紫猫学院", "_ZM_.vip") //对紫猫学院以密钥zimao.vip进行DES加密
--]=]
_zmm.DESAttr = {}
_zmm.DESAttr.default = { mode = "ecb", padding = "zero", iv = "", block = "des", cipherout = 16 }
_zmm.DESAttr.mt = { __index = _zmm.DESAttr.default }
function zmm.EncodeDES(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.DESAttr.mt)

			--            local String = require("string");

			if #key < 8 then
				key = key .. string.rep("\0", 8 - #key)
			elseif #key > 8 then
				error("在" .. t.block .. "中, 密钥长度不得超过8位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");

			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher." .. t.block);

			local v = {}
			v.mode = mode
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.plaintext = Array.fromString(data)
			v.padding = padding

			local cipher = v.mode.Cipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local cipherOutput = cipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.plaintext))
				.finish();

			if t.cipherout == 0 then
				cipherOutput = Array.toString(cipherOutput.asBytes())
			elseif t.cipherout == 2 then
				cipherOutput = cipherOutput.asBytes()
			elseif t.cipherout == 16 then
				cipherOutput = cipherOutput.asHex()
			elseif t.cipherout == 64 then
				cipherOutput = zmm.EncodeBase64(Array.toString(cipherOutput.asBytes()))
			end
			return cipherOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeDES()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DecodeDES
@cname:DES解密
@format:密文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行DES解密
@note:对数据进行非标准DES解密, 支持DES和DES3
@ret:解密结果, string, 返回DES解密结果, 失败返回null
@arg:密文, string, 待解密的内容
@arg:密钥, string, 用于解密的密码, 最大长度为8
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | des,des3 | des | 加密算法$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim des = zm.DecodeDES("F6D8E9EBB96451844A2B29DCD192F7C5", "_ZM_.vip") //解密结果是紫猫学院
--]=]
function zmm.DecodeDES(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.DESAttr.mt)

			--            local String = require("string");

			if #key < 8 then
				key = key .. string.rep("\0", 8 - #key)
			elseif #key > 8 then
				error("在" .. t.block .. "中, 密钥长度不得超过8位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");


			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher." .. t.block);

			local v = {}
			v.mode = mode
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.padding = padding

			if t.cipherout == 0 then
				v.ciphertext = Array.fromString(data)
			elseif t.cipherout == 2 then
				v.ciphertext = data
			elseif t.cipherout == 16 then
				v.ciphertext = Array.fromHex(data)
			elseif t.cipherout == 64 then
				v.ciphertext = Array.fromString(zmm.DecodeBase64(data))
			end

			local decipher = v.mode.Decipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local plainOutput = decipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.ciphertext))
				.finish()
				.asBytes();

			plainOutput = Array.toString(plainOutput):gsub("\0", "")
			return plainOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeDES()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeTEA
@cname:TEA加密
@format:明文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行TEA加密
@note:对数据进行TEA加密, 支持TEA和XTEA
@ret:加密结果, string, 返回TEA加密结果, 失败返回null
@arg:明文, string, 待加密的内容
@arg:密钥, string, 用于加密的密码, 最大长度为16
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | tea,xtea | tea | 加密算法$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim tea = zm.EncodeTEA("紫猫学院", "_ZM_.vip") //对紫猫学院以密钥zimao.vip进行TEA加密
--]=]
_zmm.TEAAttr = {}
_zmm.TEAAttr.default = { mode = "ecb", padding = "zero", iv = "", block = "tea", cipherout = 16 }
_zmm.TEAAttr.mt = { __index = _zmm.TEAAttr.default }
function zmm.EncodeTEA(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.TEAAttr.mt)

			--            local String = require("string");

			if #key < 16 then
				key = key .. string.rep("\0", 16 - #key)
			elseif #key > 16 then
				error("在" .. t.block .. "中, 密钥长度不得超过16位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");

			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher." .. t.block);

			local v = {}
			v.mode = mode
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.plaintext = Array.fromString(data)
			v.padding = padding

			local cipher = v.mode.Cipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local cipherOutput = cipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.plaintext))
				.finish();

			if t.cipherout == 0 then
				cipherOutput = Array.toString(cipherOutput.asBytes())
			elseif t.cipherout == 2 then
				cipherOutput = cipherOutput.asBytes()
			elseif t.cipherout == 16 then
				cipherOutput = cipherOutput.asHex()
			elseif t.cipherout == 64 then
				cipherOutput = zmm.EncodeBase64(Array.toString(cipherOutput.asBytes()))
			end
			return cipherOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeTEA()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DecodeTEA
@cname:TEA解密
@format:密文, 密钥[, 高级属性]
@tname:对 $1 以密钥 $2 进行TEA解密
@note:对数据进行TEA解密, 支持TEA和XTEA
@ret:解密结果, string, 返回TEA解密结果, 失败返回null
@arg:密文, string, 待解密的内容
@arg:密钥, string, 用于解密的密码, 最大长度为16
@arg:高级属性, table, 可选, 用于设置加解密模式等选项，格式为{“属性名”:”属性值”,…}, 详见下方内容$n 属性名 | 可选值列表 | 默认值 | 作用$n mode | ecb,cbc,ctr,ofb,cfb,ige,pcbc | ecb | 加解密模式$n padding | zero,pkcs7,isoiec7816,ansix923 | zero | 填充方式$n block | tea,xtea | tea | 加密算法$n iv | 任意字符串 | 0 | iv偏移量, ecb模式不用填写, 其他模式必填且要有16个字节长度$n cipherout | 0,2,16,64 | 16 | 密文类型, 0为原二进制字符串, 2为字节数组, 16为十六进制字符串, 64为原二进制字符串Base64编码
@exp:Dim tea = zm.DecodeTEA("F89AF6A6A87D976E9D2D670C6E100AA9", "_ZM_.vip") //解密结果是紫猫学院
--]=]
function zmm.DecodeTEA(data, key, t)
	return try {
		function()
			_zmm.InitLockBox()
			t = t or {}
			setmetatable(t, _zmm.TEAAttr.mt)

			--            local String = require("string");

			if #key < 16 then
				key = key .. string.rep("\0", 16 - #key)
			elseif #key > 16 then
				error("在" .. t.block .. "中, 密钥长度不得超过16位长度")
			end

			local Array = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.array");
			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");


			local mode = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher.mode." .. t.mode)
			local padding = require(_zmm._PLUGIN_DIR_ .. ".lockbox.padding." .. t.padding)
			local block = require(_zmm._PLUGIN_DIR_ .. ".lockbox.cipher." .. t.block);

			local v = {}
			v.mode = mode
			v.iv = Array.fromString(t.iv)
			v.key = Array.fromString(key)
			v.padding = padding

			if t.cipherout == 0 then
				v.ciphertext = Array.fromString(data)
			elseif t.cipherout == 2 then
				v.ciphertext = data
			elseif t.cipherout == 16 then
				v.ciphertext = Array.fromHex(data)
			elseif t.cipherout == 64 then
				v.ciphertext = Array.fromString(zmm.DecodeBase64(data))
			end

			local decipher = v.mode.Decipher()
				.setKey(v.key)
				.setBlockCipher(block)
				.setPadding(v.padding);

			local plainOutput = decipher
				.init()
				.update(Stream.fromArray(v.iv))
				.update(Stream.fromArray(v.ciphertext))
				.finish()
				.asBytes();

			plainOutput = Array.toString(plainOutput):gsub("\0", "")
			return plainOutput
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DecodeTEA()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeSHA
@cname:SHA计算
@format:数据[, 算法]
@tname:计算 $1 的SHA结果
@note:对数据进行SHA签名计算, 支持SHA1,SHA224和SHA256
@ret:SHA结果, string, 返回SHA计算结果
@arg:数据, string, 待计算的内容
@arg:算法, string, 可选, 省略默认为sha1算法, 可填写sha1, sha224, sha256
@exp:TracePrint "sha1:", zm.EncodeSHA("紫猫")
@exp:TracePrint "sha224:", zm.EncodeSHA("紫猫", "sha224")
@exp:TracePrint "sha256:", zm.EncodeSHA("紫猫", "sha256")
--]=]
function zmm.EncodeSHA(data, mode)
	return try {
		function()
			_zmm.InitLockBox()
			mode = mode or "sha1"
			mode = mode:lower()
			if mode == "sha1" then
				mode = "sha1"
			elseif mode == "sha224" then
				mode = "sha2_224"
			elseif mode == "sha256" then
				mode = "sha2_256"
			else
				error("不支持此" .. mode .. "类型")
			end

			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");
			local Digest = require(_zmm._PLUGIN_DIR_ .. ".lockbox.digest." .. mode);
			--            local String = require("string");

			local actual = Digest()
				.update(Stream.fromString(data))
				.finish()
				.asHex();
			return actual
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeSHA()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeMD5
@cname:MD5计算
@format:数据[, 算法]
@tname:计算 $1 的MD5结果
@note:对数据进行MD5计算, 支持MD2,MD4和MD5
@ret:MD5结果, string, 返回MD5计算结果, 也支持MD4和MD2
@arg:数据, string, 待计算的内容
@arg:算法, string, 可选, 省略默认为md5, 可填写md2, md4, md5
@exp:TracePrint "md5:", zm.EncodeMD5("紫猫")
@exp:TracePrint "md4:", zm.EncodeMD5("紫猫", "md4")
@exp:TracePrint "md2:", zm.EncodeMD5("紫猫", "md2")
--]=]
function zmm.EncodeMD5(data, mode)
	return try {
		function()
			_zmm.InitLockBox()
			mode = mode or "md5"
			mode = mode:lower()

			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");
			local Digest = require(_zmm._PLUGIN_DIR_ .. ".lockbox.digest." .. mode);
			--            local String = require("string");

			local actual = Digest()
				.update(Stream.fromString(data))
				.finish()
				.asHex();
			return actual
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeMD5()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeRIPEMD
@cname:RIPEMD计算
@format:数据[, 算法]
@tname:计算 $1 的RIPEMD结果
@note:对数据进行RIPEMD计算, 支持RIPEMD128和RIPEMD160
@ret:RIPEMD结果, string, 返回RIPEMD128计算结果, 也支持RIPEMD160
@arg:数据, string, 待计算的内容
@arg:算法, string, 可选, 省略默认为ripemd128, 可填写ripemd128, ripemd160
@exp:TracePrint "ripemd128:", zm.EncodeRIPEMD("紫猫")
@exp:TracePrint "ripemd160:", zm.EncodeRIPEMD("紫猫", "ripemd160")
--]=]
function zmm.EncodeRIPEMD(data, mode)
	return try {
		function()
			_zmm.InitLockBox()
			mode = mode or "ripemd128"
			mode = mode:lower()

			local Stream = require(_zmm._PLUGIN_DIR_ .. ".lockbox.util.stream");
			local Digest = require(_zmm._PLUGIN_DIR_ .. ".lockbox.digest." .. mode);
			--            local String = require("string");

			local actual = Digest()
				.update(Stream.fromString(data))
				.finish()
				.asHex();
			return actual
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeRIPEMD()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeRsaSign
@cname:RSA签名
@format:数据, 私钥
@tname:计算 $1 的RSA签名结果
@note:对数据进行RSA签名
@ret:签名结果, string, 返回RSA签名结果
@arg:数据, string, 待签名的数据内容
@arg:私钥, string, 使用的RSA私钥
@exp:TracePrint zm.EncodeRSASign("紫猫学院", "请修改成你的RSA私钥")
--]=]
function zmm.EncodeRsaSign(data, private_key)
	return try {
		function()
			if private_key:find("\n") == nil then
				local k = _zmm.fileread(private_key)
				if k then
					private_key = k
				end
			end
			traceprint(private_key)
			return LuaAuxLib.RsaSign(data, private_key)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeRsaSign()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:EncodeRsaVerifySign
@cname:RSA签名验证
@format:源数据, 签名数据, 公钥
@tname:使用公钥 $3 对 $2 解密并与原数据 $1 比较判断是否一致
@note:验证RSA签名是否一致
@ret:对比结果, boolean, 一致返回true, 不一致返回false
@arg:源数据, string, 原数据内容
@arg:签名数据, string, 私钥签名后的密文内容
@arg:公钥, string, 使用的rsa公钥
@exp:TracePrint zm.EncodeRsaVerifySign("紫猫学院", "请修改成私钥签名的密文", "请修改成你的RSA公钥")
--]=]
function zmm.EncodeRsaVerifySign(data, encode_data, public_key)
	return try {
		function()
			if public_key:find("\n") == nil then
				local k = _zmm.fileread(public_key)
				if k then
					public_key = k
				end
			end
			return LuaAuxLib.RsaVerifySign(data, encode_data, public_key)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.EncodeRsaVerifySign()，错误信息：", errors)
			end
		}
	}
end

function _zmm.QiniuUploadToken(accessKey, secretKey, t)
	local cjson = require("cjson")
	local putPolicy = cjson.encode(t)
	--    local putPolicy = [[{"scope":"my-bucket:sunflower.jpg","deadline":1451491200,"returnBody":"{\"name\":$(fname),\"size\":$(fsize),\"w\":$(imageInfo.width),\"h\":$(imageInfo.height),\"hash\":$(etag)}"}]]
	local encodedPutPolicy = _zmm.EncodeBase64(putPolicy):gsub("%+", "-"):gsub("%/", "_")
	local sign = zmm.EncodeHMAC(encodedPutPolicy, secretKey, "sha1", 0)
	local encodedSign = zmm.EncodeBase64(sign):gsub("%+", "-"):gsub("%/", "_")
	local uploadToken = accessKey .. ":" .. encodedSign .. ":" .. encodedPutPolicy
	return uploadToken
end

--[[**************************************************************************]]
--[[****************************** 文件操作 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:FileTemp
@cname:生成临时文件
@format:[文件名]
@tname:生成文件名为 $1 的临时文件路径
@note:随机生成一个不重复的临时文件路径名, 当你执行写文件等保存文件操作后, 才会真正生成文件
@ret:文件路径, string, 返回一个临时文件路径, 失败返回null
@arg:文件名, string, 可选, 指定文件名, 省略默认为随机文件名
@exp:TracePrint "临时文件路径:", zm.FileTemp()
--]=]
function zmm.FileTemp(filename)
	return try {
		function()
			local FileTempPath
			if filename then
				FileTempPath = _zmm._GLOBAL_PATH_ .. filename
			else
				FileTempPath = _zmm._GLOBAL_PATH_ .. os.tmpname():match("[^/]+$")
			end
			return FileTempPath
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileTemp()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FileInit
@cname:设置File默认参数
@format:参数表
@tname:设置File默认参数为 $1
@note:初始化设置File系列命令的默认参数内容, 对绝大多数File系列命令有效
@ret:属性表, table, 返回File默认参数表, 失败返回null
@arg:参数表, table, 设置File系列命令的默认编码与是否自动判断编码, 默认为{"encode":"utf-8", "replacepath":{}}$n 属性名 | 数据类型 | 可选值 | 默认值 | 解释 $n encode | 字符串 | "utf-8", "电脑", "auto" | "utf-8" | 设置默认文件编码, 可填写任意正常有效编码或特殊编码"电脑"和"auto", "电脑"表示从电脑上传过来的文件, "auto"表示自动判断编码, 但会降低读写速度$n replacepath | 表 | 任意有效table | {} | 可选, 遍历替换传入路径, 可传入类似{"^/storage/emulated/0/":"/sdcard/"}等解决读取不到文件问题
@exp:zm.FileInit {"autocode":true} //设置默认自动识别文件编码
--]=]
_zmm.gFileAttr = {}
_zmm.gFileAttr.default = { encode = "utf-8", replacepath = {} }
_zmm.gFileAttr.mt = { __index = _zmm.gFileAttr.default }
function zmm.FileInit(t)
	return try {
		function()
			if t then
				for k, v in pairs(t) do
					if _zmm.gFileAttr.default[k:lower()] ~= nil then
						_zmm.gFileAttr.default[k:lower()] = v
					end
				end
			end
			return _zmm.gFileAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileInit()，错误信息：", errors)
			end
		}
	}
end

function _zmm.filereplacepath(args)
	if type(args) == "table" then
		for k, v in pairs(args.replacepath) do
			args.path = args.path:gsub(k, v)
		end
		return args.path
	else
		for k, v in pairs(_zmm.gFileAttr.default.replacepath) do
			args = args:gsub(k, v)
		end
		return args
	end
end

--[=[
@fname:FileEncode
@cname:获取文件编码
@format:文件路径[, 是否设置编码[, 检测长度]]
@tname:获取 $1 文件编码
@note:获取一个文件的编码, 目前仅支持UTF-8和中文编码(兼容gb2312等)
@ret:文件编码, string, 成功返回utf-8或gb18030, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@arg:是否设置编码, boolean, 可选, 是否将获取编码设置为File系列命令的默认编码, 省略默认false
@arg:检测行数, number, 可选, 检测多少行内容, 省略默认为所有行内容, 行数越小, 检测耗时越短
@exp:Dim code = zm.FileEncode("/sdcard/紫猫.txt", True)$n //得到并设置文件编码, 可以给zm.FileInt()使用, 也可以给读写命令使用
--]=]
function zmm.FileEncode(path, isset, length)
	local f
	return try {
		function()
			local encode = "utf-8"
			local isFirst = true
			local ls = 0
			local args = setmetatable({ path = path, isset = isset }, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)
			f = io.open(args.path, "r")
			if f ~= nil then
				for text in f:lines() do
					if length and ls > length then
						ls = ls + 1
						break
					end
					if isFirst and text:match("^" .. string.char(0xEF, 0xBB, 0xBF)) then
						encode = "utf-8"
						break
					else
						for i = 1, #text do
							local t = {}
							t[1] = zmm.ConvBase(text:byte(i, i), 10, 2)
							t[2] = zmm.ConvBase(text:byte(i + 1, i + 1), 10, 2)
							t[3] = zmm.ConvBase(text:byte(i + 2, i + 2), 10, 2)
							if t[1]:match("^1110%d%d%d%d") then
								if not (t[2]:match("^10%d%d%d%d%d%d") and t[3]:match("^10%d%d%d%d%d%d")) then
									encode = "gb18030"
									break
								end
							elseif t[1]:match("^110%d%d%d%d%d") then
								if t[2]:match("^10%d%d%d%d%d%d") then
									encode = "gb18030"
									break
								end
							end
						end
					end
					if encode == "gb18030" then
						break
					end
					isFirst = false
				end

				f:close()
			else
				encode = "utf-8"
			end
			if args.isset then
				zmm.FileInit({ encode = encode })
			end
			return encode
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zmm.FileEncode()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileRead
@cname:读取文本文件
@format:文件路径[, 文件编码]
@tname:以 $2 编码读取 $1 文件
@note:读取文件所有内容, 支持设置文件编码
@ret:文本内容, string, 返回该文件的所有文本内容, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@arg:文件编码, string, 可选, 填写"auto"可实现自动判断编码, 解决乱码问题, 省略默认为初始化编码
@exp:TracePrint zm.FileRead("/sdcard/电脑文件.txt", "电脑") //可以直接读取电脑传过来的文件
--]=]
function zmm.FileRead(path, encode)
	local f
	return try {
		function()
			local args = setmetatable({ path = path, encode = encode }, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)
			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end


			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end
			local ret = f:read("*all")
			f:close()

			ret = _zmm.clearbom(ret)
			if args.encode ~= "utf-8" then
				ret = zmm.ConvCoding(ret, args.encode, "utf-8")
			end

			--             if args.encode == "utf-8" then ret = _ZM_.clearbom(ret) end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileRead()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileReadLine
@cname:读取指定行文本
@format:文件路径, 指定行[, 文件编码]
@tname:以 $3 编码读取 $1 文件的第 $2 行内容
@note:读取指定行文本内容, 支持倒数行数, 超出行数返回null, 支持设置文件编码
@ret:文本内容, string, 返回指定行的文本内容, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@arg:指定行, number, 表示读取第几行内容, 负数为倒数第几行内容, 0表示随机读取一行
@arg:文件编码, string, 可选, 填写"auto"可实现自动判断编码, 解决乱码问题, 省略默认为初始化编码
@exp:TracePrint zm.FileReadLine("/sdcard/紫猫学院.txt", 2) //读取第2行内容
@exp:TracePrint zm.FileReadLine("/sdcard/紫猫学院.txt", -5) //读取倒数第5行内容
--]=]
function zmm.FileReadLine(path, line, encode)
	local args, f = { path = path, line = line, encode = encode }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)
			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end

			if args.line == 0 then
				local l = 0
				for _ in f:lines() do
					l = l + 1
				end
				args.line = zmm.RndNum(1, l)
			end


			if args.line < 0 then
				local l = 0
				for _ in f:lines() do
					l = l + 1
				end
				f:seek("set")
				args.line = l + args.line + 1
			end

			local ll, text = 0
			for c in f:lines() do
				ll = ll + 1
				if ll == 1 then c = _zmm.clearbom(c) end
				if ll == args.line then
					if args.encode ~= "utf-8" then
						c = zmm.ConvCoding(c, args.encode, "utf-8")
					end
					text = c:gsub("[\r\n]", "")
					break
				end
			end

			f:close()
			return text
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReadLine()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileReadLines
@cname:读取文本数组
@format:文件路径[, 文件编码]
@tname:以 $2 编码读取 $1 文件内容保存到数组
@note:读取文件所有内容保存到一维数组中, 支持设置文件编码
@ret:文本数组, table, 返回文件每一行内容保存到一维数组中
@arg:文件路径, string, 要操作的文件路径
@arg:文件编码, string, 可选, 填写"auto"可实现自动判断编码, 解决乱码问题, 省略默认为初始化编码
@exp:Dim txtArr = zm.FileReadLine("/sdcard/紫猫学院.txt") //读取所有内容保存到数组txtArr中
@exp:Dim txtArr = zm.FileReadLine("/sdcard/紫猫学院.txt", "电脑") //以电脑文件编码读取所有内容保存到数组txtArr中
--]=]
function zmm.FileReadLines(path, encode)
	local args, f = { path = path, encode = encode }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)
			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end
			local t = {}
			for c in f:lines() do
				if #t == 0 then c = _zmm.clearbom(c) end
				if args.encode ~= "utf-8" then
					c = zmm.ConvCoding(c, args.encode, "utf-8")
				end
				c = c:gsub("[\r\n]", "")
				--                if args.encode == "utf-8" then c = _ZM_.clearbom(c) end
				t[#t + 1] = c
			end
			f:close()
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReadLines()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileReadForm
@cname:读取文本表格
@format:文件路径, 分隔符[, 文件编码]
@tname:以 $3 编码读取 $1 文件内容按 $2 分隔保存到二维数组
@note:按指定分隔符读取文件所有内容保存到二维数组中, 支持设置文件编码
@ret:文本数组, table, 返回每一行被分隔后的二维数组文本
@arg:文件路径, string, 要操作的文件路径
@arg:分隔符, string或number, 字符串表示每行文本中的分隔符, 数值型表示按长度分隔
@arg:文件编码, string, 可选, 填写"auto"可实现自动判断编码, 解决乱码问题, 省略默认为初始化编码
@exp:Dim path = "/sdcard/紫猫学院.txt"$n zm.FileWrite path, {"帐号1====密码1====大区1", "帐号2====密码2====大区2", "帐号3====密码3====大区3"}$n Dim txts = zm.FileReadForm(path, "====")$n TracePrint zm.VarInfo(txts)
--]=]
function zmm.FileReadForm(path, sep, encode)
	local args, f = { path = path, sep = sep, encode = encode }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end
			local t = {}
			for c in f:lines() do
				if #t == 0 then c = _zmm.clearbom(c) end
				if args.encode ~= "utf-8" then
					c = zmm.ConvCoding(c, args.encode, "utf-8")
				end
				--                if args.encode == "utf-8" then c = _ZM_.clearbom(c) end
				c = c:gsub("[\r\n]", "")
				local tt = zmm.Split(c, args.sep)
				t[#t + 1] = tt
			end
			f:close()
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReadForm()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

function _zmm.ArrayFromString(str)
	local bytes = {};

	local i = 1;
	local byte = string.byte(str, i);
	while #str >= i do
		bytes[i] = byte;
		i = i + 1;
		byte = string.byte(str, i);
	end

	return bytes;
end

--[=[
@fname:FileReadBinary
@cname:读取二进制文件
@format:文件路径[, 读取长度][, 返回十六进制数组]
@tname:读取 $1 文件二进制内容
@note:以二进制读取文件内容
@ret:二进制, string, 返回读取到的二进制内容
@arg:文件路径, string, 要操作的文件路径
@arg:读取长度, number, 可选, 要读取的字节长度, 省略默认为全部读取
@arg:返回十六进制数组, table, 可选, 数组变量, 用于返回十六进制数组结果
@exp:Dim bin = zm.FileReadBinary("/sdcard/截图1.png") //读取二进制数据, 一般无法直接输出, 可以用Len查看长度
--]=]
function zmm.FileReadBinary(path, length, hex)
	local args, f = { path = path, length = length, hex = hex }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			f = io.open(args.path, "rb")
			if f == nil then
				return nil
			end

			if type(args.length) == "table" then
				args.length, args.hex = args.hex, args.length
			end

			if args.length == nil then
				args.length = "*a"
			end

			local ret = f:read(args.length)
			f:close()

			if type(args.hex) == "table" then
				zmm.TableClear(args.hex)
				for k, v in pairs(_zmm.ArrayFromString(ret)) do
					args.hex[k] = v
				end
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReadBinary()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileReadBinaryBase64
@cname:以Base64读取二进制文件
@format:文件路径
@tname:读取 $1 文件二进制内容
@note:读取文件二进制内容, 并转为Base64编码, 受zm.InitBase64()影响, 失败返回null
@ret:Base64内容, string, 返回读取的Base64结果, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@exp:Dim b64 = zm.FileReadBinaryBase64("/sdcard/截图1.png") //内容可能比较长, 无法显示
--]=]
function zmm.FileReadBinaryBase64(path)
	local f
	return try {
		function()
			path = _zmm.filereplacepath(path)
			f = io.open(path, "rb")
			if f == nil then
				return nil
			end
			local bytes = f:read("*all")
			f:close()
			return zmm.EncodeBase64(bytes)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReadBinaryBase64()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileWrite
@cname:覆盖写入文本
@format:文件路径, 内容[, ...]
@tname:覆盖写入 $2 到文件 $1 中
@note:覆盖写入文本内容到文件中, 支持zm.FileInit()默认参数
@ret:文本数组, table, 返回写入的文本数组, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@arg:内容, string或table, 支持一个数组参数或者多个字符串参数$n 如果内容是一个数组参数, 则表示换行写入每个元素$n 如果内容是多个字符串参数, 则表示不换行依次写入每个字符串
@exp:zm.FileWrite("/sdcard/紫猫学院.txt", "欢迎加入紫猫学院,", "紫猫老师QQ: 345911220 ", "免费交流群: 7333555") //不换行覆盖写入
@exp:zm.FileWrite("/sdcard/紫猫学院.txt", {"欢迎加入紫猫学院,", "紫猫老师QQ: 345911220 ", "免费交流群: 7333555"}) //换行覆盖写入
--]=]
function zmm.FileWrite(path, ...)
	local args, f = { path = path, texts = { ... } }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "w")
			if f == nil then
				return nil
			end

			if type(args.texts[1]) == "table" then
				if args.encode ~= "utf-8" then
					args.sep = "\r\n"
				else
					args.sep = "\n"
				end
				args.texts = args.texts[1]
			end

			args.text = table.concat(args.texts, args.sep)
			if args.encode ~= "utf-8" then
				args.text = zmm.ConvCoding(args.text, "utf-8", args.encode)
			end

			f:write(args.text)
			f:close()
			return args.texts
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileWrite()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileWriteLine
@cname:插入指定行文本
@format:文件路径, 指定行, 内容[, ...]
@tname:追加 $3 到文件 $1 的第 $2 行中
@note:追加写入到指定行, 支持zm.FileInit()默认参数
@ret:文本数组, table, 返回写入后的文本数组, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@arg:指定行, number, 表示插入第几行内容, 负数为倒数第几行, 0为末尾新增一行, 不能超出最大行
@arg:内容, string或table, 支持一个数组参数或者多个字符串参数$n 如果内容是一个数组参数, 则表示换行写入每个元素$n 如果内容是多个字符串参数, 则表示不换行依次写入每个字符串
@exp:zm.FileWrite("/sdcard/紫猫学院.txt", 2, "好好学习, 天天向上") //在文件第2行插入文本
--]=]
function zmm.FileWriteLine(path, line, ...)
	local args, f = { path = path, line = line, texts = { ... } }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r+")
			if f == nil then
				error("文件不存在!")
			end

			-- 统计倒数行数
			local l = 0
			if args.line <= 0 then
				for _ in f:lines() do
					l = l + 1
				end
				f:seek("set")
				args.line = l + args.line + 1
			end

			local ll, t, ischange = 0, {}
			--写入函数
			local w = function()
				ischange = true
				local sep
				if type(args.texts[1]) == "table" then
					args.texts = args.texts[1]
					if args.encode ~= "utf-8" then
						sep = "\r\n"
					else
						sep = "\n"
					end
				end

				args.text = table.concat(args.texts, sep)
				t[#t + 1] = args.text
			end

			for c in f:lines() do
				ll = ll + 1
				if ll == 1 then c = _zmm.clearbom(c) end
				if ll == args.line then
					w()
				end
				if args.encode ~= "utf-8" then
					c = zmm.ConvCoding(c, args.encode, "utf-8")
				end
				c = c:gsub("[\r\n]", "")
				t[#t + 1] = c
			end

			if args.line == ll + 1 then
				w()
			end

			if not ischange then
				f:close()
				return t
			end

			if args.encode ~= "utf-8" then
				args.sep = "\r\n"
			else
				args.sep = "\n"
			end

			args.text = table.concat(t, args.sep)
			if args.encode ~= "utf-8" then
				args.text = zmm.ConvCoding(args.text, "utf-8", args.encode)
			end

			f:seek("set")
			f:write(args.text)
			f:close()
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileWriteLine()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileWriteBinary
@cname:覆盖写入二进制
@format:文件路径, 二进制文本
@tname:覆盖写入二进制 $2 到文件 $1 中
@note:覆盖写入二进制到文件中
@ret:是否成功, boolean, 成功返回true, 失败或出错返回null
@arg:文件路径, string, 要操作的文件路径
@arg:二进制文本, string, 待写入的二进制内容
@exp:Dim bin = zm.FileReadBinary("/sdcard/截图1.png")$n zm.FileWriteBinary "/sdcard/截图2.png", bin
--]=]
function zmm.FileWriteBinary(path, bin)
	local args, f = { path = path, text = bin }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			f = io.open(args.path, "wb")
			if f == nil then
				return nil
			end

			f:write(args.text)
			f:close()
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileWriteBinary()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileWriteBinaryBase64
@cname:覆盖写入Base64解密二进制
@format:文件路径, Base64文本
@tname:覆盖写入Base64 $2 到文件 $1 中
@note:将内容进行Base64解密, 并以二进制写入文件中, 受zm.InitBase64()影响
@ret:是否成功, boolean, 成功返回true, 失败或出错返回null
@arg:文件路径, string, 要操作的文件路径
@arg:Base64文本, string, 待解密的Base64文本内容
@exp:Dim b64 = zm.FileReadBinaryBase64("/sdcard/截图1.png")$n zm.FileWriteBinaryBase64 "/sdcard/截图2.png", b64
--]=]
function zmm.FileWriteBinaryBase64(path, bin)
	local args, f = { path = path, text = bin }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			f = io.open(args.path, "wb")
			if f == nil then
				return nil
			end

			args.text = zmm.DecodeBase64(args.text)

			f:write(args.text)
			f:close()
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileWriteBinaryBase64()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileWriteAppend
@cname:追加写入文本
@format:文件路径, 内容[, ...]
@tname:追加 $2 到文件 $1 行中
@note:追加写入文本内容到文件中, 写完自动换行, 支持zm.FileInit()默认参数
@ret:是否成功, boolean, 成功返回true, 失败或出错返回null
@arg:文件路径, string, 要操作的文件路径
@arg:内容, string或table, 支持一个数组参数或者多个字符串参数$n 如果内容是一个数组参数, 则表示换行写入每个元素$n 如果内容是多个字符串参数, 则表示不换行依次写入每个字符串
@exp:zm.FileWriteAppend("/sdcard/紫猫学院.txt", "好好学习, 天天向上") //在末尾追加内容
--]=]
function zmm.FileWriteAppend(path, ...)
	local args, f = { path = path, texts = { ... } }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "a")
			if f == nil then
				return nil
			end

			if type(args.texts[1]) == "table" then
				if args.encode ~= "utf-8" then
					args.sep = "\r\n"
				else
					args.sep = "\n"
				end
				args.texts = args.texts[1]
			end

			args.text = table.concat(args.texts, args.sep)

			if args.encode ~= "utf-8" then
				args.text = zmm.ConvCoding(args.text, "utf-8", args.encode)
			end

			f:write(args.text, args.encode ~= "utf-8" and "\r\n" or "\n")
			f:close()
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileWriteAppend()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileDeleteLine
@cname:删除指定行
@format:文件路径, 指定行
@tname:删除文件 $1 的第 $2 行内容
@note:删除指定行文本内容, 支持zm.FileInit()默认参数
@ret:文本数组, table, 成功返回删除后的文本数组, 失败或出错返回null
@arg:文件路径, string, 要操作的文件路径
@arg:指定行, number, 表示删除第几行内容, 负数为倒数第几行内容
@exp:zm.FileDeleteLine("/sdcard/紫猫学院.txt", 1) //删除第一行内容
--]=]
function zmm.FileDeleteLine(path, line)
	local args, f = { path = path, line = line }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end

			if args.line < 0 then
				local l = 0
				for _ in f:lines() do
					l = l + 1
				end
				f:seek("set")
				args.line = l + args.line + 1
			end

			local ll, t, ischange = 0, {}
			for c in f:lines() do
				ll = ll + 1
				if ll == 1 then c = _zmm.clearbom(c) end
				if ll ~= args.line then
					if args.encode ~= "utf-8" then
						c = zmm.ConvCoding(c, args.encode, "utf-8")
					end
					c = c:gsub("[\r\n]", "")
					t[#t + 1] = c
				else
					ischange = true
				end
			end
			f:close()
			if not ischange then
				return t
			end

			if args.encode ~= "utf-8" then
				args.sep = "\r\n"
			else
				args.sep = "\n"
			end

			args.text = table.concat(t, args.sep)

			if args.encode ~= "utf-8" then
				args.text = zmm.ConvCoding(args.text, "utf-8", args.encode)
			end

			f = io.open(args.path, "w")
			if f == nil then
				return nil
			end
			f:write(args.text)
			f:close()
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileDeleteLine()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileReplaceLine
@cname:替换指定行文本
@format:文件路径, 指定行, 内容[, ...]
@tname:替换文件 $1 的第 $2 行内容为 $3
@note:替换指定行文本, 支持zm.FileInit()默认参数
@ret:文本数组, table, 成功返回替换后的文本数组, 失败或出错返回null
@arg:文件路径, string, 要操作的文件路径
@arg:指定行, number, 表示替换第几行内容, 负数为倒数第几行内容
@arg:内容, string或table, 替换后的内容, 支持一个数组参数或者多个字符串参数$n 如果内容是一个数组参数, 则表示换行写入每个元素$n 如果内容是多个字符串参数, 则表示不换行依次写入每个字符串
@exp:zm.FileReplaceLine("/sdcard/紫猫学院.txt", 2, "免费交流群7333555") //把第2行替换成指定内容
--]=]
function zmm.FileReplaceLine(path, line, ...)
	local args, f = { path = path, line = line, texts = { ... } }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			if args.encode == "auto" then
				args.encode = zmm.FileEncode(args.path)
			end

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end

			if args.line < 0 then
				local l = 0
				for _ in f:lines() do
					l = l + 1
				end
				f:seek("set")
				args.line = l + args.line + 1
			end

			local ll, t, ischange = 0, {}
			for c in f:lines() do
				ll = ll + 1
				if ll == 1 then c = _zmm.clearbom(c) end
				if ll ~= args.line then
					if args.encode ~= "utf-8" then
						c = zmm.ConvCoding(c, args.encode, "utf-8")
					end
					c = c:gsub("[\r\n]", "")
					t[#t + 1] = c
				else
					ischange = true
					local sep
					if type(args.texts[1]) == "table" then
						args.texts = args.texts[1]
						if args.encode ~= "utf-8" then
							sep = "\r\n"
						else
							sep = "\n"
						end
					end

					args.text = table.concat(args.texts, sep)
					t[#t + 1] = args.text
				end
			end
			f:close()
			if not ischange then
				return t
			end

			if args.encode ~= "utf-8" then
				args.sep = "\r\n"
			else
				args.sep = "\n"
			end

			args.text = table.concat(t, args.sep)

			if args.encode ~= "utf-8" then
				args.text = zmm.ConvCoding(args.text, "utf-8", args.encode)
			end

			f = io.open(args.path, "w")
			if f == nil then
				return nil
			end
			f:write(args.text)
			f:close()
			return t
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileReplaceLine()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileLinesNumber
@cname:获取文件总行数
@format:文件路径
@tname:获取文件 $1 的总行数
@note:获取文本总行数
@ret:总行数, number, 成功返回文件总行数, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@exp:Dim line = zm.FileLinesNumber("/sdcard/紫猫学院.txt") //获取文件总行数
--]=]
function zmm.FileLinesNumber(path)
	local args, f = { path = path }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end
			local line = 0
			for _ in f:lines() do
				line = line + 1
			end
			f:close()
			return line
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileLinesNumber()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileBytes
@cname:获取文件字节大小
@format:文件路径
@tname:获取文件 $1 的总字节大小
@note:获取以字节为单位的文件大小
@ret:文件大小, number, 成功返回文件字节大小, 失败返回null
@arg:文件路径, string, 要操作的文件路径
@exp:Dim b = zm.FileBytes("/sdcard/紫猫学院.txt") //获取文件字节大小
--]=]
function zmm.FileBytes(path)
	local args, f = { path = path }
	return try {
		function()
			setmetatable(args, _zmm.gFileAttr.mt)
			_zmm.filereplacepath(args)

			f = io.open(args.path, "r")
			if f == nil then
				return nil
			end
			local bytes = f:seek("end")
			f:close()
			return bytes
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileBytes()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}
	}
end

--[=[
@fname:FileCreate
@cname:创建文件
@format:文件路径
@tname:创建文件 $1
@note:在指定目录下创建一个空白文件, 目录必须存在, 基于Linux的touch命令行开发
@ret:无
@arg:文件路径, string, 要创建的文件路径
@exp:zm.FileCreate "/sdcard/紫猫学院.txt"
--]=]
function zmm.FileCreate(path)
	return try {
		function()
			zmm.Execute("touch " .. path)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileCreate()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FileLoadINI
@cname:加载并解析INI
@format:INI路径[, 文件编码][, 自动转换类型]
@tname:加载并解析 $1 文件
@note:对INI文件进行加载并解析成table数据, 支持zm.FileInit()默认参数
@ret:INI对象, table, 返回解析成功的数据, 格式类似于{小节名1:{键名1:键值1, 键名2:键值2}, 小节名A:{键名A:键值A}, ...}, 失败返回null
@arg:文件路径, string, INI文件的加载路径
@arg:文件编码, string, 可选, 设置文件编码解决乱码问题, 省略默认为初始化编码
@arg:自动转换类型, boolean, 可选, 将小节名, 键名, 键值进行智能转换成数值型或布尔型, 省略默认false, 表示不转换
@exp:Dim data()$ndata["紫猫学院"] = {"官网":"zimaoxy.com", "报名QQ":345911220}$ndata["小节名"] = {"键名":"键值"}$nDim path="/sdcard/Pictures/aini.ini"$n$nzm.FileSavaINI path, data$n$nDim iniData = zm.FileLoadINI(path)$nTracePrint iniData["紫猫学院"]["官网"]$nTracePrint iniData["紫猫学院"]["报名QQ"]$nTracePrint iniData["小节名"]["键名"]
--]=]
function zmm.FileLoadINI(path, encode, changetype)
	return try {
		function()
			assert(type(path) == 'string', '文件路径参数必须是字符串');
			local data = {};
			local section;
			if encode then
				if type(encode) == 'boolean' then
					encode, changetype = changetype, encode
				end
			end


			local text = zmm.FileReadLines(path, encode)
			for _, line in ipairs(text) do
				local tempSection = line:match('^%b[]$');
				if tempSection and #tempSection > 2 then
					section = tempSection:sub(2, -2)
					if changetype then
						section = tonumber(section) and tonumber(section) or section;
					end
					data[section] = data[section] or {};
				end
				local param, value = line:match('^([^=]+)%s-=%s-(.*)$');
				if param and value ~= nil then
					if changetype and tonumber(value) then
						value = tonumber(value);
					elseif changetype and value == 'true' then
						value = true;
					elseif changetype and value == 'false' then
						value = false;
					else
						value = value:gsub("([^\\])\\r", "%1\r"):gsub("([^\\])\\n", "%1\n"):gsub("\\\\([rn])", "\\%1")
					end
					if changetype and tonumber(param) then
						param = tonumber(param);
					end
					data[section][param] = value;
				end
			end

			return data;
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileLoadINI()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FileSavaINI
@cname:保存INI文件
@format:INI路径, INI数据[, 文件编码]
@tname:保存 $2 INI数据到 $1 文件
@note:将INI结构的数据保存到文件中, 支持zm.FileInit()默认参数
@ret:无
@arg:文件路径, string, INI文件的保存路径
@arg:INI数据, table, INI的数据内容, 格式类似于{小节名1:{键名1:键值1, 键名2:键值2}, 小节名A:{键名A:键值A}, ...}
@arg:文件编码, string, 可选, 设置文件编码解决乱码问题, 省略默认为初始化编码
@exp:Dim data()$ndata["紫猫学院"] = {"官网":"zimaoxy.com", "报名QQ":345911220}$ndata["小节名"] = {"键名":"键值"}$nDim path="/sdcard/Pictures/aini.ini"$n$nzm.FileSavaINI path, data$n$nDim iniData = zm.FileLoadINI(path)$nTracePrint iniData["紫猫学院"]["官网"]$nTracePrint iniData["紫猫学院"]["报名QQ"]$nTracePrint iniData["小节名"]["键名"]
--]=]
function zmm.FileSavaINI(path, data, encode)
	return try {
		function()
			assert(type(path) == 'string', '文件路径参数必须是字符串');
			assert(type(data) == 'table', 'ini内容的数据类型必须是table型');
			local contents = '';
			for section, param in pairs(data) do
				contents = contents .. ('[%s]\n'):format(tostring(section));
				for key, value in pairs(param) do
					value = tostring(value):gsub("\\([rn])", "\\\\%1"):gsub("\r", "\\r"):gsub("\n", "\\n")
					contents = contents .. ('%s=%s\n'):format(key, value);
				end
				contents = contents .. '\n';
			end
			local args
			if encode then
				args = zmm.FileInit()
				zmm.FileInit({ encode = encode })
			end
			zmm.FileWrite(path, contents)
			if encode then
				zmm.FileInit({ encode = args.encode })
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FileSavaINI()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 系统操作 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:GetGateway
@cname:获取默认网关地址
@format:无
@tname:获取默认网关地址
@note:获取默认网关地址
@ret:网关地址, string, 成功返回默认网关地址, 失败返回null
@arg:无
@exp:TracePrint zm.GetGateway() //显示当前网关地址
--]=]
function zmm.GetGateway()
	return try {
		function()
			local txt = zmm.Execute("ip route")
			if txt and txt ~= "" then
				local gw = txt:match("default via (%d+%.%d+%.%d+%.%d+)")
				if gw ~= "" then
					return gw
				end
			end
			return nil
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetGateway()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DirCopy
@cname:复制文件或目录
@format:源路径, 目标路径[, 模式]
@tname:复制 $1 到 $2
@note:复制文件或目录, 支持多文件复制, 基于linux的cp命令行开发.
@ret:无
@arg:源路径, string或table, 等待复制的源文件或文件夹路径, 支持通配符*和?, 如果是table类型, 则表示多个文件
@arg:目标路径, string, 复制到新的文件或文件夹, 若源路径是多文件, 则该参数必须是文件夹
@arg:模式, string, 可选, 模式选项, "-a"表示普通复制, "-af"表示覆盖复制, 更多模式请自行搜索cp命令行, 省略默认为"-af"
@exp:zm.DirCopy "/sdcard/紫猫学院.txt", "/sdcard/zimaoxy.com.txt"
--]=]
function zmm.DirCopy(oldpath, newpath, mode)
	return try {
		function()
			mode = mode or "-af"
			if type(oldpath) == "table" then
				oldpath = table.concat(oldpath, " ")
			end
			zmm.Execute("cp " .. mode .. " " .. oldpath .. " " .. newpath)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DirCopy()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DirMove
@cname:移动文件或目录
@format:源路径, 目标路径[, 模式]
@tname:移动 $1 到 $2
@note:移动文件或目录, 支持多文件移动, 基于linux的mv命令行开发.
@ret:无
@arg:源路径, string或table, 等待移动的源文件或文件夹路径, 支持通配符*和?, 如果是table类型, 则表示多个文件
@arg:目标路径, string, 移动到新的文件或文件夹, 若源路径是多文件, 则该参数必须是文件夹
@arg:模式, string, 可选, 模式选项, ""表示普通移动不覆盖, "-f"表示覆盖移动, 更多模式请自行搜索mv命令行, 省略默认为"-f"
@exp:zm.DirMove "/sdcard/紫猫学院.txt", "/sdcard/Pictures/"
--]=]
function zmm.DirMove(oldpath, newpath, mode)
	return try {
		function()
			mode = mode or "-f"
			if type(oldpath) == "table" then
				oldpath = table.concat(oldpath, " ")
			end
			zmm.Execute("mv " .. mode .. " " .. oldpath .. " " .. newpath)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DirMove()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DirDelete
@cname:删除文件或目录
@format:路径[, 模式]
@tname:删除 $1
@note:删除文件或目录, 支持删除多文件, 基于linux的rm命令行开发.
@ret:无
@arg:路径, string或table, 要删除的源文件或目录路径, 支持通配符*, 如果是table类型, 则表示多个文件或目录
@arg:模式, string, 可选, 模式选项, ""表示普通删除, "-rf"表示强制删除, 更多模式请自行搜索rm命令行, 省略默认为"-rf"
@exp:zm.DirDelete "/sdcard/紫猫学院.txt"
--]=]
function zmm.DirDelete(path, mode)
	return try {
		function()
			mode = mode or "-rf"
			if type(path) == "table" then
				path = table.concat(path, " ")
			end
			zmm.Execute("rm " .. mode .. " " .. path)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DirDelete()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DirScan
@cname:遍历文件或目录
@format:目录路径[, 文件名或目录名[, 遍历类型[, 遍历深度[, 是否区分大小写]]]]
@tname:在 $1 中查找 $2 文件目录
@note:在指定目录路径中进行遍历
@ret:找到路径, table, 成功返回找到的路径数组, 未找到或出错返回null
@arg:目录路径, string, 在该目录路径下进行遍历文件或目录
@arg:文件名或目录名, string, 可选, 要遍历的文件名或目录名, 支持通配符?*, 省略默认为"*"
@arg:遍历类型, number, 可选, 1代表只遍历文件, 2代表只遍历目录, 3代表文件与目录都要遍历, 省略默认为3
@arg:遍历深度, number, 可选, 遍历目录深度, -1表示无限制, 省略默认为-1
@arg:是否区分大小写, boolean, 可选, 省略默认为false, 表示不区分大小写
@exp:zm.DirFind "/sdcard/", "紫猫学院.txt"
--]=]
function zmm.DirScan(path, name, param1, param2, param3)
	return try {
		function()
			name = name or "*"
			local params = {}
			if param3 then
				table.insert(params, "-name '" .. name .. "'")
			else
				table.insert(params, "-iname '" .. name .. "'")
			end
			param1 = param1 or 3
			if param1 == 1 then
				table.insert(params, "-type f")
			elseif param1 == 2 then
				table.insert(params, "-type d")
			end

			param2 = param2 or -1
			if param2 > 0 then
				table.insert(params, "-maxdepth " .. param2)
			end

			local text = zmm.Execute("find " .. path .. " -mindepth 1 " .. table.concat(params, " "))
			if text == "" then
				return nil
			else
				return zmm.Split(text, "\n")
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DirScan()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:DirCreate
@cname:创建目录
@format:目录路径[, 强制创建]
@tname:创建目录 $1
@note:创建指定路径的目录, 父目录不存在时, 可强制创建, 基于Linux的mkdir命令行开发
@ret:无
@arg:目录路径, string, 要创建的目录路径
@arg:强制创建, boolean, 可选, 省略默认为true, 表示父目录不存在时将自动创建父目录
@exp:zm.DirCreate "/sdcard/紫猫学院/教程目录/"
--]=]
function zmm.DirCreate(path, mode)
	return try {
		function()
			local param
			if mode == nil or mode == true then
				param = "-p"
			end

			zmm.Execute("mkdir " .. param .. " '" .. path .. "'")
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.DirCreate()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 超级命令 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:TracePrint
@cname:超级调试输出
@format:[行号, ]内容[, ...]
@tname:超级调试输出 $1
@note:输出变量的长度, 数据类型与值, 可以直接获取表的值
@ret:输出内容, string, 成功返回变量的长度, 数据类型与值, 可以直接获取表的值, 失败返回null
@arg:行号, 字符串, 可选, 调试显示第几行代码, 格式为"_行号", 例如"_10"表示第10行代码
@arg:内容, 任意类型, 要输出的内容, 支持多个参数
@exp:zm.TracePrint "_5", {"紫猫", "学院"} //在第5行输出数组内容与长度
--]=]
function zmm.TracePrint(...)
	local paramCount = select("#", ...)
	local varType, printStr, t, line = "", "", {}, ":"
	for i = 1, paramCount do
		local v = select(i, ...)
		try {
			function()
				local jq = function(v)
					if #v > 255 then
						v = v:sub(1, 255) .. "...(紫猫插件:由于长度超过255,后面内容不予显示!)"
					end
					return v
				end
				if i == 1 and type(v) == "string" and string.find(v, "^_%d+$", 1, false) then
					line = v .. ":"
				else
					varType = type(v)
					if varType == "table" then
						try {
							function()
								printStr = "【" ..
								varType .. " " .. tostring(#v) .. "】" .. jq(LuaAuxLib.Encode_GetJsonLib():encode(v))
							end,
							catch {
								try {
									function()
										printStr = "【" .. varType ..
										" " .. tostring(#v) .. "】" .. jq(_zmm.tabledump(v))
									end
								}
							}
						}
					elseif varType == "number" or varType == "string" then
						printStr = "【" .. varType .. " " .. tostring(#tostring(v)) .. "】" .. jq(tostring(v))
					elseif varType == "boolean" or varType == "null" then
						printStr = "【" .. varType .. "】" .. tostring(v)
					else
						printStr = "【" .. varType .. "】 unknown"
					end
					table.insert(t, #t + 1, printStr)
				end
			end,
			catch {
				function(errors)
					traceprint("发生运行时错误！错误代码：zm.TracePrint()，错误信息：", errors)
				end
			}
		}
	end
	printStr = table.concat(t, ", ")
	LuaAuxLib.TracePrint(line, printStr)
	return printStr
end

_zmm.gShowLog = {}
_zmm.gShowLog["显示"], _zmm.gShowLog["show"] = 0, 0
_zmm.gShowLog["显示找到"], _zmm.gShowLog["showfind"] = 1, 1
_zmm.gShowLog["显示没找到"], _zmm.gShowLog["shownofind"] = -1, -1
_zmm.gShowLog["隐藏"], _zmm.gShowLog["hide"] = false, false

--[=[
@fname:SetShowLog
@cname:设置日志输出显示
@format:显示隐藏
@tname:设置日志为 $1
@note:设置内置日志输出默认值
@ret:设置结果, string, 成功返回设置结果, 失败返回null
@arg:显示隐藏, boolean或string, 可取以下[true, false, "显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]几个值
@exp:zm.SetShowLog "显示" //设置超级命令系列日志为显示
--]=]
function zmm.SetShowLog(s)
	return try {
		function()
			if type(s) == "boolean" then
				s = s and "显示" or "隐藏"
			end

			s = s:lower()
			if _zmm.gShowLog[s] ~= nil then
				_zmm.FindPicAttr.default["showlog"] = s
				_zmm.FindMultiColorAttr.default["showlog"] = s
				_zmm.FindColorAttr.default["showlog"] = s
				_zmm.FindStrAttr.default["showlog"] = s
				_zmm.TapAttr.default["showlog"] = s
				_zmm.CmpColorExAttr.default["showlog"] = s
				if s == "显示" or s == "show" then
					traceprint("设置日志输出默认为显示")
				elseif s == "显示找到" or s == "showfind" then
					traceprint("设置日志输出默认为显示找到")
				elseif s == "显示没找到" or s == "shownofind" then
					traceprint("设置日志输出默认为显示没找到")
				elseif s == "隐藏" or s == "hide" then
					traceprint("设置日志输出默认为隐藏")
				end
			else
				traceprint("无法设置日志输出默认为[", s,
					"], 目前仅支持[true, false, \"显示\", \"显示找到\", \"显示没找到\", \"隐藏\", \"show\", \"showfind\", \"shownofind\", \"hide\"], 请仔细检查是否有错别字或者多余的空格")
			end

			return s
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetShowLog()，错误信息：", errors)
			end
		}
	}
end

function _zmm.showlog(showlvl, isfind, mode, log, line)
	local show = function(mode, log, line)
		if mode == 0 then
			traceprint(line, log)
		elseif mode == 1 then
			LuaAuxLib.ShowMessage(log, 3000, LuaAuxLib.GetScreenInfo(0) / 3, LuaAuxLib.GetScreenInfo(1) - 150)
		end
	end

	if _zmm.gShowLog[showlvl] == 0 then
		show(mode, log, line)
	elseif _zmm.gShowLog[showlvl] == 1 and isfind == true then
		show(mode, log, line)
	elseif _zmm.gShowLog[showlvl] == -1 and isfind == false then
		show(mode, log, line)
	end
end

--[[**************************************************************************]]
--[[****************************** 网络通讯 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:OpenURL
@cname:打开网页
@format:网址
@tname:打开 $1 网址
@note:使用默认浏览器打开网页, 支持包含&的网址, 支持IOS
@ret:无
@arg:网址, string, 要打开的网址
@exp:zm.OpenURL "www.baidu.com"
--]=]
function zmm.OpenURL(url)
	return try {
		function()
			if url:match("://") == nil then url = "http://" .. url end
			-- 安卓打开网页
			url = url:gsub("%%", "%%%%")
			os.execute(string.format("am start -a android.intent.action.VIEW -d '" .. url .. "'"))
			-- ios打开网页
			os.execute(string.format("uiopen '" .. url .. "'"))
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OpenURL()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:GetIPNet
@cname:获取外网IP
@format:[返回格式[, 接口[, 缓存]]]
@tname:获取外网IP地址
@note:获取外网IP地址, 根据参数决定返回内容
@ret:IP地址, string或table, 返回IP地址或详细信息表, 由参数返回格式决定返回内容, 失败返回null
@arg:返回格式, number, 可选, 填写0表示只返回ip地址字符串, 填写大于0的数字表示返回详细信息表, 包括省份等内容, 不同数字表示接口不同, 返回格式也不同, 省略默认为0
@arg:接口, number, 可选, 只返回ip地址的接口, 0时表示随机接口, 大于0时表示指定接口, 不同数值代表不同接口, 省略默认为0
@arg:缓存, boolean, 可选, 是否强制获取最新数据, true是刷新缓存获取最新数据, false是以接口自身设定为准, 省略默认为false
@exp:Dim ip = zm.GetIPNet() //获取外网IP地址
--]=]
function zmm.GetIPNet(mode, id, isrefresh)
	return try {
		function()
			mode = mode or 0

			local ipurl = {
				-- 限制10qbs
				{ "http://ip.taobao.com/outGetIpInfo", "ip=myip&accessKey=alibaba-inc" },
				"https://pv.sohu.com/cityjson?ie=utf-8",
				-- 限制每天1000次
				"http://ip-api.com/json?lang=zh-CN",
				{ "https://ipapi.co/ip/",              "" },
				"http://whois.pconline.com.cn/ipJson.jsp?json=true",
				"https://api.ip.sb/ip",
				--官方声明无限制
				"https://api.ipify.org/",
				"http://myip.com.tw/",
				"http://ip111.cn/",
				"https://www.taobao.com/help/getip.php",
				"http://www.hao7188.com/",
				"https://bgp.he.net/",
				"http://ip.cip.cc/",
				"http://myip.ipip.net/",
				"https://ip.cn/",
				"http://www.net.cn/static/customercare/yourip.asp",
				"http://ip.dnsexit.com/",
				"http://icanhazip.com/",
				"http://www.trackip.net/ip",
				"http://ident.me/",
				"http://whatismyip.akamai.com/",
				"http://inet-ip.info/ip",
				-- 第三方
				"https://api.bilibili.com/client_info?type=jsonp"
			}

			local matchIP = function(url)
				if type(url) == "string" then
					if isrefresh then
						if string.find(url, "%?") then
							url = url .. "&" .. zmm.RndNum(10000, 99999)
						else
							url = url .. "?" .. zmm.RndNum(10000, 99999)
						end
					end

					local jsonData = LuaAuxLib.URL_OperationGet(url, 1)
					if jsonData then
						return jsonData:match("%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?")
					end
				else
					if string.find(url[1], "%?") then
						url[1] = url[1] .. "&" .. zmm.RndNum(10000, 99999)
					else
						url[1] = url[1] .. "?" .. zmm.RndNum(10000, 99999)
					end
					local jsonData = LuaAuxLib.URL_OperationPost(url[1], url[2], 1)
					if jsonData then
						return jsonData:match("%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?")
					end
				end
			end

			local getip = function()
				if id == 0 or id == nil then
					id = zmm.RndNum(1, #ipurl)
				end

				local ip = matchIP(ipurl[id])
				return ip
			end

			local geoip = {}
			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationPost(
				"http://ip.taobao.com/service/getIpInfo2.php" .. (isrefresh and "?" .. zmm.RndNum(10000, 99999) or ""),
					"ip=myip&accessKey=alibaba-inc", 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.code == 0 then
							local t = {}
							t.ip = tableData.data.ip           -- ip
							t.country = tableData.data.country -- 国家
							t.region = tableData.data.region   -- 省份
							t.city = tableData.data.city       -- 城市
							t.isp = tableData.data.isp         -- 运营商
							return t
						end
					end
				end
			end
			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationGet(
				"http://ip-api.com/json?lang=zh-CN" .. (isrefresh and "&" .. zmm.RndNum(10000, 99999) or ""), 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.status == "success" then
							local t = {}
							t.ip = tableData.query          -- ip
							t.country = tableData.country   -- 国家
							t.region = tableData.regionName -- 省份
							t.city = tableData.city         -- 城市
							t.isp = tableData.org           -- 运营商
							return t
						end
					end
				end
			end
			geoip[#geoip + 1] = function()
				local ip = getip()
				if ip then
					local jsonData = LuaAuxLib.URL_OperationGet(
					"http://ip.ws.126.net/ipquery?ip=" .. ip .. (isrefresh and "&" .. zmm.RndNum(10000, 99999) or ""), 1)
					if jsonData then
						local t = {}
						t.ip = ip
						t.region = jsonData:match("province:\"([^\"]*)")
						t.city = jsonData:match("city:\"([^\"]*)")
						return t
					end
				end
			end

			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationGet(
				"http://whois.pconline.com.cn/ipJson.jsp?json=true" ..
				(isrefresh and "&" .. zmm.RndNum(10000, 99999) or ""), 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.ip then
							local t = {}
							t.ip = tableData.ip                         -- ip
							t.region = tableData.pro                    -- 省份
							t.city = tableData.city                     -- 城市
							t.isp = tableData.addr:match("%s+([^\"]*)") -- 运营商
							return t
						end
					end
				end
			end

			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationGet(
				"http://myip.ipip.net/" .. (isrefresh and "?" .. zmm.RndNum(10000, 99999) or ""), 1)
				if jsonData then
					local ip = jsonData:match("%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?")
					if ip then
						local t = {}
						t.ip = ip -- ip
						t.country, t.region, t.city, t.isp = jsonData:match(
						"来自于：([^%s]*)%s([^%s]*)%s([^%s]*)%s+([^%s]*)")
						return t
					end
				end
			end

			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationPost(
				"https://ipapi.co/json/" .. (isrefresh and "?" .. zmm.RndNum(10000, 99999) or ""), "", 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.ip then
							local t = {}
							t.ip = tableData.ip                -- ip
							t.country = tableData.country_name -- 省份
							t.region = tableData.region        -- 省份
							t.city = tableData.city            -- 城市
							return t
						end
					end
				end
			end

			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationGet(
				"https://api.ip.sb/geoip" .. (isrefresh and "?" .. zmm.RndNum(10000, 99999) or ""), 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.ip then
							local t = {}
							t.ip = tableData.ip           -- ip
							t.country = tableData.country -- 省份
							t.region = tableData.region   -- 省份
							t.city = tableData.city       -- 城市
							return t
						end
					end
				end
			end
			geoip[#geoip + 1] = function()
				local jsonData = LuaAuxLib.URL_OperationPost(
				"http://ip.taobao.com/outGetIpInfo" .. (isrefresh and "?" .. zmm.RndNum(10000, 99999) or ""),
					"ip=myip&accessKey=alibaba-inc", 1)
				if jsonData then
					jsonData = jsonData:match("{[%s%S]*}")
					if jsonData then
						local cjson = require("cjson")
						local tableData = cjson.decode(jsonData)
						if tableData.code == 0 then
							local t = {}
							t.ip = tableData.data.ip           -- ip
							t.country = tableData.data.country -- 国家
							t.region = tableData.data.region   -- 省份
							t.city = tableData.data.city       -- 城市
							t.isp = tableData.data.isp         -- 运营商
							return t
						end
					end
				end
			end

			local ip
			if mode == 0 then
				ip = getip()
			elseif mode <= #geoip and mode > 0 then
				ip = geoip[mode]()
			end

			return ip
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetIPNet()，错误信息：", errors)
			end
		}
	}
end

-- 处理http的参数
function _zmm.httpargs(args)
	if args.url:match("://") == nil then
		args.url = "'http://" .. args.url .. "'"
	else
		args.url = "'" .. args.url .. "'"
	end
	if args.path == nil or args.path == "" then
		args.path = "-O"
	else
		args.path = "-o '" .. args.path .. "'"
	end
	if type(args.header) == "string" and args.header ~= "" then
		args.header = "-H '" .. args.header "'"
	elseif type(args.header) == "table" then
		if #args.header > 0 then
			args.header = "-H '" .. table.concat(args.header, "' -H '") .. "'"
		else
			local h
			for k, v in pairs(args.header) do
				h = h .. "-H '" .. k .. ":" .. v .. "' "
			end
			args.header = h
		end
	else
		args.header = ""
	end
	if args.cookie ~= nil and args.cookie ~= "" then
		args.cookie = "-b '" .. args.cookie .. "'"
	else
		args.cookie = ""
	end
	if args.setcookie ~= nil and args.setcookie ~= "" then
		args.setcookie = "-c '" .. args.setcookie .. "'"
	else
		args.setcookie = ""
	end
	if args.data ~= nil and args.data ~= "" then
		args.data = "-d '" .. args.data .. "'"
	else
		args.data = ""
	end

	if args.customize == nil then
		args.customize = ""
	end

	if args.code == "" then
		args.code = nil
	end
end

--[=[
@fname:HttpDownload
@cname:下载文件
@format:[对象][网址[, 保存路径[, 提交cookie[, 请求头信息[, 自定义]]]]]
@tname:从 $1 文件中下载文件保存到 $2
@note:以HTTP协议的Get方法下载文件, 支持提交cookie与头信息, 需要curl支持
@ret:无
@arg:对象, table, 可选, 若填写对象将忽略其他参数$n 格式为{“url”:”网址”, “path”:”保存路径”, “cookie”:”cookie字符串或文件”, “header”:{“头信息1”, “头信息2”}, “customize”:”自定义curl参数”}
@arg:网址, string, 可选, 与对象参数二选一填写, 文件的下载地址
@arg:保存路径, string, 可选, 文件的保存路径, 省略为当前路径下的默认文件名
@arg:提交cookie, string, 可选, 要提交的cookie字符串或者cookie文件路径, 省略为””
@arg:请求头信息, string或table, 可选, 要提交的头信息, 多个头信息可用一维数组传入, 省略为””
@arg:自定义, string, 可选, curl的高级参数, 一般不需要填写, 省略默认为””
@exp:zm.HttpDownload "http://m.anjian.com/download/MobileAnjian3.2.9.apk", "/sdcard/anjian.apk" //下载按键精灵手机版APK文件到SD卡目录下
--]=]
function zmm.HttpDownload(...)
	local args = { ... }
	return try {
		function()
			if type(args[1]) == "table" then
				args = args[1]
			else
				args.url = args[1]
				args.path = args[2]
				args.cookie = args[3]
				args.header = args[4]
				args.customize = args[5]
			end
			if args.url == nil then
				traceprint("错误: zm.HttpDownload() 的网址url参数为空")
				return nil
			end

			_zmm.httpargs(args)

			args.cmd = string.format("curl %s %s %s %s %s", args.path, args.header, args.cookie, args.customize, args
			.url)
			return os.execute(args.cmd)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.HttpDownload()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:HttpGet
@cname:获取网页源码
@format:[对象][网址[, 网页编码[, 保存cookie[, 提交cookie[, 请求头信息[, 自定义]]]]]]
@tname:获取 $1 的网页源码
@note:以Http协议的Get方法获取网页源码, 支持提交cookie与头信息, 可保存当前cookie文件, 需要curl支持
@ret:网页源码, string, 返回网页的源码内容, 失败返回null
@arg:对象, table, 可选, 若填写对象将忽略其他参数$n 格式为{“url”:”网址”,”code”:”网页编码”, “setcookie”:”保存cookie文件路径”, “cookie”:”发送cookie字符串或文件”, “header”:{“头信息名1”:“头信息值1”, “头信息名2”:“头信息值2”}, “customize”:”自定义curl参数”}
@arg:网址, string, 可选, 与对象参数二选一填写, 要获取网页源码的网址
@arg:网页编码, string, 可选, 省略默认为”UTF-8”, 可通过右击查看网页源码中的charset获知正确编码
@arg:保存cookie, string, 可选, 省略默认为””, 表示保存cookie的路径, 可用 zm.FileTemp() 生成路径保存
@arg:提交cookie, string, 可选, 要提交的cookie字符串或者cookie文件路径, 省略为””
@arg:请求头信息, string或table, 可选, 要提交的头信息, 多个头信息可用一维数组传入, 省略为””
@arg:自定义, string, 可选, curl的高级参数, 一般不需要填写, 省略默认为””
@exp:TracePrint zm.HttpGet("www.baidu.com") //显示百度源码
--]=]
function zmm.HttpGet(...)
	local args = { ... }
	return try {
		function()
			if type(args[1]) == "table" then
				args = args[1]
			else
				args.url = args[1]
				args.code = args[2]
				args.setcookie = args[3]
				args.cookie = args[4]
				args.header = args[5]
				args.customize = args[6]
			end
			if args.url == nil then
				traceprint("错误: zm.HttpGet() 的网址url参数为空")
				return nil
			end

			_zmm.httpargs(args)

			args.tmp = zmm.FileTemp()
			args.cmd = string.format("curl -o '" .. args.tmp .. "' %s %s %s %s --connect-timeout 10 -m 10 %s",
				args.setcookie, args.header, args.cookie, args.customize, args.url)
			os.execute(args.cmd)
			args.html = zmm.Trim(_zmm.fileread(args.tmp, true), "\r\n ")
			if args.code then
				args.html = zmm.ConvCoding(args.html, args.code, "utf-8")
			end

			return args.html
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.HttpGet()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:HttpPost
@cname:Post提交
@format:[对象][网址[, 提交数据[, 网页编码[, 保存cookie[, 提交cookie[, 请求头信息[, 自定义]]]]]]]
@tname:向 $1 网页提交数据 $2
@note:以Http协议的Post方法提交数据获取网页源码, 支持提交cookie与头信息, 可用返回参数保存cookie, 需要curl支持
@ret:网页源码, string, 返回网页的源码内容, 失败返回null
@arg:对象, table, 可选, 若填写对象将忽略其他参数$n 格式为{“url”:”网址”,”code”:”网页编码”, “setcookie”:”保存cookie文件路径”, “cookie”:”发送cookie字符串或文件”, “header”:{“头信息名1”:“头信息值1”, “头信息名2”:“头信息值2”}, “customize”:”自定义curl参数”}
@arg:网址, string, 可选, 与对象参数二选一填写, 要获取网页源码的网址
@arg:提交数据, string, 可选, 省略默认为””, 包含中文或空格之类时, 建议先使用 zm.EncodeURL() 进行编码转换后传入
@arg:网页编码, string, 可选, 省略默认为”UTF-8”, 可通过右击查看网页源码中的charset获知正确编码
@arg:保存cookie, string, 可选, 省略默认为””, 表示保存cookie的路径, 可用 zm.FileTemp() 生成路径保存
@arg:提交cookie, string, 可选, 要提交的cookie字符串或者cookie文件路径, 省略为””
@arg:请求头信息, string或table, 可选, 要提交的头信息, 多个头信息可用一维数组传入, 省略为””
@arg:自定义, string, 可选, curl的高级参数, 一般不需要填写, 省略默认为””
@exp:TracePrint zm.HttpPost("www.baidu.com") //显示百度源码
--]=]
function zmm.HttpPost(...)
	local args = { ... }
	return try {
		function()
			if type(args[1]) == "table" then
				args = args[1]
			else
				args.url = args[1]
				args.data = args[2]
				args.code = args[3]
				args.setcookie = args[4]
				args.cookie = args[5]
				args.header = args[6]
				args.customize = args[7]
			end
			if args.url == nil then
				traceprint("错误: zm.HttpPost() 的网址url参数为空")
				return nil
			end

			_zmm.httpargs(args)

			args.tmp = zmm.FileTemp()
			args.cmd = string.format("curl -o '" .. args.tmp .. "' -X POST %s %s %s %s %s --connect-timeout 10 -m 10 %s",
				args.data, args.setcookie, args.header, args.cookie, args.customize, args.url)
			os.execute(args.cmd)
			args.html = zmm.Trim(_zmm.fileread(args.tmp, true), "\r\n ")
			if args.code then
				args.html = zmm.ConvCoding(args.html, args.code, "utf-8")
			end

			return args.html
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.HttpPost()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataInit
@cname:初始化网络数据
@format:[网址][, 数据表名[, 通信密钥]][, 是否清空]
@tname:初始化 $1 的 $2 网络数据共享
@note:初始化共享网络数据, 创建指定数据表, 需要网站程序配合, 详见命令帮助例子
@ret:无
@arg:网址, string, 可选, 网站程序sql.php的网址
@arg:数据表名, string, 可选, 用于存放变量数据的表
@arg:通信密钥, string, 可选, 用于通信的密钥, 由install.php安装时自定义填写提供
@arg:是否清空, boolean, 可选, 清空指定数据表下所有变量数据, 不会删除数据表, 省略默认为false, 表示不清空
@arg:超时时间, number, 可选, 单位毫秒, 执行NetData系列命令允许最大时间, 超时返回null, 省略默认为3000
@exp:zm.NetDataInit "http://192.168.1.100/sql.php", "测试脚本"
--]=]
_zmm.gNetDataAttr = {}
_zmm.gNetDataAttr.default = { url = "http://192.168.1.100/sql.php", tbl = "紫猫学院", timeout = 3,
	token = "QQ345911220" }
_zmm.gNetDataAttr.mt = { __index = _zmm.gNetDataAttr.default }
function zmm.NetDataInit(...)
	local params = { ... }
	return try {
		function()
			local i = 0
			local isdel = false
			for k, v in pairs(params) do
				if type(k) == "number" then
					if type(v) == "string" then
						if v:match("https?://") then
							_zmm.gNetDataAttr.default.url = v
						else
							if i == 0 then
								_zmm.gNetDataAttr.default.tbl = v
								i = i + 1
							else
								_zmm.gNetDataAttr.default.token = v
							end
						end
					elseif type(v) == "boolean" then
						isdel = v
					elseif type(v) == "number" then
						_zmm.gNetDataAttr.default.timeout = v / 1000
					end
				end
			end
			local buf = "action=init" ..
			"&token=" ..
			_zmm.gNetDataAttr.default.token ..
			"&isdel=" .. tostring(isdel) .. "&table=" .. zmm.EncodeURL(_zmm.gNetDataAttr.default.tbl)
			local ret = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf, _zmm.gNetDataAttr.default
			.timeout)
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataInit()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataSet
@cname:设置网络数据
@format:键名, 键值[, 数据表名]
@tname:在 $3 中设置 $1 的值为 $2
@note:往数据表中添加或修改共享网络数据, 需要网站程序配合, 详见命令帮助例子
@ret:无
@arg:键名, string, 类似于变量名, 区分大小写
@arg:键值, 任意类型, 存放的数据, 类似于变量值
@arg:数据表名, string, 可选, 往指定数据表中设置共享数据, 数据表必须存在, 省略默认为初始化时的数据表名
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataSet(key, value, tbl)
	return try {
		function()
			tbl = tbl or _zmm.gNetDataAttr.default.tbl
			local value_type = type(value)
			if value_type == "table" then
				value = LuaAuxLib.Encode_GetJsonLib():encode(value)
			end
			local buf = "action=set" ..
			"&token=" ..
			_zmm.gNetDataAttr.default.token ..
			"&key=" ..
			zmm.EncodeURL(tostring(key)) ..
			"&value=" .. zmm.EncodeURL(tostring(value)) .. "&type=" .. value_type .. "&table=" .. zmm.EncodeURL(tbl)
			local ret = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf, _zmm.gNetDataAttr.default
			.timeout)
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataSet()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataGet
@cname:获取网络数据
@format:键名[, 数据表名][, 是否删除]
@tname:获取 $2 中的键值 $1
@note:从数据表中获取共享网络数据, 可选参数顺序可以打乱, 需要网站程序配合, 详见命令帮助例子
@ret:键值, 任意类型, 返回存入的数据, 该数据类型与写入时相同
@arg:键名, string, 类似于变量名, 区分大小写
@arg:数据表名, string, 可选, 从指定数据表中获取共享数据, 数据表必须存在, 省略默认为初始化时的数据表名
@arg:是否删除, boolean, 可选, 表示获取后是否直接删除该键名, 省略默认为false
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataGet(key, arg1, arg2)
	return try {
		function()
			local isdel = false
			local tbl = _zmm.gNetDataAttr.default.tbl
			local args = { arg1, arg2 }
			for _, v in ipairs(args) do
				if type(v) == "string" then
					tbl = v
				elseif type(v) == "boolean" then
					isdel = v
				end
			end
			local buf = "action=get" ..
			"&token=" ..
			_zmm.gNetDataAttr.default.token ..
			"&key=" .. zmm.EncodeURL(tostring(key)) .. "&table=" .. zmm.EncodeURL(tbl) .. "&isdel=" .. tostring(isdel)
			local jsonData = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf,
				_zmm.gNetDataAttr.default.timeout)
			if jsonData == "" then
				error("无法获取网页源码，请检查网址是否正确！")
				return nil
			end
			if jsonData ~= "false" and jsonData ~= "null" then
				local ret = LuaAuxLib.Encode_GetJsonLib():decode(_zmm.clearbom(jsonData))
				if ret.type == "number" then
					ret.value = tonumber(ret.value)
				elseif ret.type == "boolean" then
					if ret.value == "true" then
						ret.value = true
					else
						ret.value = false
					end
				elseif ret.type == "table" then
					ret.value = LuaAuxLib.Encode_GetJsonLib():decode(ret.value)
				elseif ret.type == "null" then
					ret.value = nil
				end
				return ret.value
			else
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataGet()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataGetRows
@cname:获取多行网络数据
@format:行数[, 起始行][, 数据表名][, 是否删除]
@tname:获取 $2 到 $1 行数据
@note:从数据表中获取指定行数的共享网络数据, 可选参数顺序可以打乱, 需要网站程序配合, 详见命令帮助例子
@ret:二维表, table, 返回二维表, 格式{{"id":id,"key":key,"value":value}, ...}, 失败返回null
@arg:行数, number, 从指定数据表中获取多少行数据
@arg:起始行, number, 可选, 表示从第几行开始获取, 省略默认为1
@arg:数据表名, string, 可选, 从指定数据表中获取共享数据, 数据表必须存在, 省略默认为初始化时的数据表名
@arg:是否删除, boolean, 可选, 表示获取后是否直接删除这些数据, 省略默认为false
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataGetRows(arg1, arg2, arg3, arg4)
	return try {
		function()
			local isdel = false
			local startrow, rows
			local tbl = _zmm.gNetDataAttr.default.tbl
			local args = { arg1, arg2, arg3, arg4 }
			for _, v in ipairs(args) do
				if type(v) == "string" then
					tbl = v
				elseif type(v) == "boolean" then
					isdel = v
				elseif type(v) == "number" then
					if rows == nil then
						rows = v
						startrow = 0
					else
						startrow = v - 1
					end
				end
			end
			local buf = "action=getrows" ..
			"&token=" ..
			_zmm.gNetDataAttr.default.token ..
			"&startrow=" ..
			tostring(startrow) ..
			"&rows=" .. tostring(rows) .. "&table=" .. zmm.EncodeURL(tbl) .. "&isdel=" .. tostring(isdel)
			local jsonData = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf,
				_zmm.gNetDataAttr.default.timeout)
			if jsonData == "" then
				error("无法获取网页源码，请检查网址是否正确！")
				return nil
			end
			local ret
			if jsonData ~= "[]" then
				ret = LuaAuxLib.Encode_GetJsonLib():decode(_zmm.clearbom(jsonData))
				for k, v in pairs(ret) do
					if v.type == "number" then
						ret[k].value = tonumber(ret[k].value)
					elseif v.type == "boolean" then
						if ret[k].value == "true" then
							ret[k].value = true
						else
							ret[k].value = false
						end
					elseif v.type == "table" then
						ret[k].value = LuaAuxLib.Encode_GetJsonLib():decode(ret[k].value)
					elseif v.type == "null" then
						ret[k].value = nil
					end
				end
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataGetRows()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataDel
@cname:删除网络数据
@format:键名[, 数据表名]
@tname:删除 $2 中的 $1 数据
@note:从数据表中删除指定共享网络数据, 仅删除一个变量数据, 不会清空所有数据, 需要网站程序配合, 详见命令帮助例子
@ret:无
@arg:键名, string, 类似于变量名, 区分大小写
@arg:数据表名, string, 可选, 从指定数据表中获取共享数据, 数据表必须存在, 省略默认为初始化时的数据表名
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataDel(key, tbl)
	return try {
		function()
			tbl = tbl or _zmm.gNetDataAttr.default.tbl
			local buf = "action=del" ..
			"&token=" .. _zmm.gNetDataAttr.default.token .. "&key=" .. zmm.EncodeURL(tostring(key)) .. "&table=" .. tbl
			local ret = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf, _zmm.gNetDataAttr.default
			.timeout)
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataDel()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataCount
@cname:网络数据行数
@format:[数据表名]
@tname:获取 $1 网络数据总行数
@note:数据表中获取数据总数量, 需要网站程序配合, 详见命令帮助例子
@ret:数据总数, number, 返回数据总行数, 失败返回null
@arg:数据表名, string, 可选, 从指定数据表中获取总数量, 数据表必须存在, 省略默认为初始化时的数据表名
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataCount(tbl)
	return try {
		function()
			tbl = tbl or _zmm.gNetDataAttr.default.tbl
			local buf = "action=count" .. "&token=" .. _zmm.gNetDataAttr.default.token .. "&table=" .. tbl
			local ret = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf, _zmm.gNetDataAttr.default
			.timeout)
			return tonumber(_zmm.clearbom(ret))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataCount()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:NetDataQuery
@cname:执行SQL语句
@format:SQL语句
@tname:执行SQL语句 $1
@note:执行SQL语句, 需要一定SQL语法知识, 需要网站程序配合, 详见命令帮助例子
@ret:执行结果, string, 返回执行SQL后的返回结果, 失败返回null
@arg:SQL语句, string, 要执行的SQL语句
@exp://代码过长, 详见命令帮助例子
--]=]
function zmm.NetDataQuery(query)
	return try {
		function()
			local buf = "action=query" ..
			"&token=" .. _zmm.gNetDataAttr.default.token .. "&query=" .. zmm.EncodeURL(tostring(query))
			local ret = LuaAuxLib.URL_OperationPost(_zmm.gNetDataAttr.default.url, buf, _zmm.gNetDataAttr.default
			.timeout)
			return _zmm.clearbom(ret)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.NetDataQuery()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TCPConnect
@cname:连接TCP服务端
@format:目标IP, 目标端口[, 超时时间]
@tname:限时 $3 秒内连接 $1:$2 的TCP服务器
@note:连接TCP服务端
@ret:连接结果, number, 成功返回数值1, 失败返回null
@arg:目标IP, string, 要连接的TCP服务端IP地址
@arg:目标端口, number, 要连接的TCP服务端端口
@arg:超时时间, number, 可选, 单位秒, 默认为10秒
@exp://连接192.168.1.100:12345的TCP服务端$n Dim isConn = zm.TCPConnect("192.168.1.100", 12345)$n If isConn Then$n     //发送数据到服务端$n     TracePrint zm.TCPSend("QQ345911220", 15)$n     //接收数据$n     TracePrint zm.TCPReceive(60)$n     //关闭TCP连接$n     TracePrint zm.TCPClose()$n Else $n     TracePrint "TCP连接失败"$n End If
--]=]
local tcp_client = {}
function zmm.TCPConnect(host, port, timeout)
	return try {
		function()
			timeout = timeout or 10
			local socket = require("socket")
			local err
			tcp_client[1], err = socket.connect(host, port)
			--            tcp_client[1], err = socket.tcp()
			--            if err then
			--                traceprint("TCPConnect错误:", err)
			--                return nil, err;
			--            end
			--            tcp_client[1]:settimeout(timeout)

			--            ret, err = tcp_client[1]:connect(host, port)
			if err then
				traceprint("TCPConnect错误:", err)
				return nil, err;
			end
			return 1
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TCPConnect()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TCPSend
@cname:发送TCP数据
@format:数据[, 超时时间]
@tname:在 $2 秒内发送TCP数据 $1
@note:发送TCP数据
@ret:发送结果, number, 成功返回大于0的数据长度, 失败返回null
@arg:数据, string, 要发送的数据内容, 注意, 末尾会默认加上\n字符
@arg:超时时间, number, 可选, 单位秒, 默认为10秒
@arg:追加换行, boolean, 可选, 是否在发送数据后面追加\n字符, 默认为true
@exp://连接192.168.1.100:12345的TCP服务端$n Dim isConn = zm.TCPConnect("192.168.1.100", 12345)$n If isConn Then$n     //发送数据到服务端$n     TracePrint zm.TCPSend("QQ345911220", 15)$n     //接收数据$n     TracePrint zm.TCPReceive(60)$n     //关闭TCP连接$n     TracePrint zm.TCPClose()$n Else $n     TracePrint "TCP连接失败"$n End If
--]=]
function zmm.TCPSend(data, timeout, newline)
	return try {
		function()
			timeout = timeout or 10
			if newline == nil then
				newline = true
			end
			local n = newline and "\n" or ""

			if tcp_client[1] then
				tcp_client[1]:settimeout(timeout);
				local ret, err = tcp_client[1]:send(data .. n)
				if not err then
					print("发送成功!")
				elseif err == "closed" then
					error("连接已关闭!")
				elseif err == "timeout" then
					error("发送超时!")
				end
				return ret
			else
				error("不存在连接!")
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TCPSend()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TCPReceive
@cname:接收TCP数据
@format:[超时时间[, 接收模式]]
@tname:在 $1 秒内接收TCP数据
@note:接收TCP数据，此命令为阻塞命令！
@ret:接收数据, string, 成功返回字符串数据内容, 失败返回null
@arg:超时时间, number, 可选, 单位秒, 默认为10秒
@arg:接收模式, string, 可选, 支持*a和*l两种模式, 省略默认为*l
@exp://连接192.168.1.100:12345的TCP服务端$n Dim isConn = zm.TCPConnect("192.168.1.100", 12345)$n If isConn Then$n     //发送数据到服务端$n     TracePrint zm.TCPSend("QQ345911220", 15)$n     //接收数据$n     TracePrint zm.TCPReceive(60)$n     //关闭TCP连接$n     TracePrint zm.TCPClose()$n Else $n     TracePrint "TCP连接失败"$n End If
--]=]
function zmm.TCPReceive(timeout, mode)
	return try {
		function()
			timeout = timeout or 10
			mode = mode or "*l"
			if tcp_client[1] then
				tcp_client[1]:settimeout(timeout)
				local data, status, partial = tcp_client[1]:receive(mode)
				if status == "timeout" then
					if partial ~= "" then
						return partial
					else
						return ""
					end
				elseif status == "closed" then
					error("连接已关闭")
					return nil
				else
					return data
				end
			else
				error("不存在连接!")
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TCPReceive()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:TCPClose
@cname:关闭TCP连接
@format:无
@tname:关闭TCP连接
@note:关闭TCP连接
@ret:关闭结果, number, 返回1表示关闭成功, 返回2表示当前无连接, 失败返回null
@arg:无
@exp://连接192.168.1.100:12345的TCP服务端$n Dim isConn = zm.TCPConnect("192.168.1.100", 12345)$n If isConn Then$n     //发送数据到服务端$n     TracePrint zm.TCPSend("QQ345911220", 15)$n     //接收数据$n     TracePrint zm.TCPReceive(60)$n     //关闭TCP连接$n     TracePrint zm.TCPClose()$n Else $n     TracePrint "TCP连接失败"$n End If
--]=]
function zmm.TCPClose()
	return try {
		function()
			if tcp_client[1] then
				return tcp_client[1]:close()
			else
				return 2
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.TCPClose()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:UDPBind
@cname:绑定UDP
@format:IP地址, 端口[, 超时时间]
@tname:绑定 $1:$2 的UDP
@note:绑定UDP
@ret:绑定结果, number, 成功返回大于0的数值, 失败返回null
@arg:IP地址, string, 要绑定的IP地址
@arg:端口, number, 要绑定的端口
@arg:超时时间, number, 可选, 单位秒, 默认为10秒
@exp://往192.168.1.100:12345发送数据$n TracePrint zm.UDPSend("QQ345911220", "192.168.1.100", 12345)$n //绑定192.168.1.101:12345$n Dim isBind = zm.UDPBind("192.168.1.101", 12345, 15)$n If isBind Then$n     //接收数据$n     TracePrint zm.UDPReceive(60)$n     //关闭UDP$n     TracePrint zm.UDPClose()$n Else $n     TracePrint "UDP绑定失败"$n End If
--]=]
local udp = {}
function zmm.UDPBind(ip, port, timeout)
	return try {
		function()
			timeout = timeout or 10
			local err
			local socket = require("socket")
			udp[1], err = socket.udp()
			if err then
				traceprint("UDPBind错误:", err)
				return nil, err
			end
			udp[1]:settimeout(timeout)
			local ret
			ret, err = udp[1]:setsockname(ip, port)
			if err then
				traceprint("UDPBind错误:", err)
				return nil, err
			end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.UDPBind()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:UDPReceive
@cname:接收UDP数据
@format:[超时时间]
@tname:在 $1 秒内接收UDP数据
@note:接收UDP数据, 注意, 此命令为阻塞命令
@ret:数据, table, 成功返回{数据内容, 来源IP, 来源端口}, 失败返回null
@arg:超时时间, number, 可选, 单位秒, 默认为10秒
@exp://往192.168.1.100:12345发送数据$n TracePrint zm.UDPSend("QQ345911220", "192.168.1.100", 12345)$n //绑定192.168.1.101:12345$n Dim isBind = zm.UDPBind("192.168.1.101", 12345, 15)$n If isBind Then$n     //接收数据$n     TracePrint zm.UDPReceive(60)$n     //关闭UDP$n     TracePrint zm.UDPClose()$n Else $n     TracePrint "UDP绑定失败"$n End If
--]=]
function zmm.UDPReceive(timeout)
	return try {
		function()
			timeout = timeout or 10
			if udp[1] then
				local data, msg_or_ip, port_or_nil = udp[1]:receivefrom()
				if data then
					return { data, msg_or_ip, port_or_nil }
				else
					traceprint("UDPReceive错误:", tostring(msg_or_ip))
					return data, msg_or_ip
				end
			else
				traceprint("UDPReceive错误:", "未绑定监听地址与端口")
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.UDPReceive()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:UDPSend
@cname:发送UDP数据
@format:数据内容, 目标IP, 目标端口
@tname:向 $2:$3 发送UDP数据 $1
@note:发送UDP数据
@ret:发送结果, number, 成功返回大于0的数值, 失败返回null
@arg:数据内容, string, 要发送的数据内容
@arg:目标IP, string, 目标IP地址
@arg:目标端口, number, 目标端口
@exp://往192.168.1.100:12345发送数据$n TracePrint zm.UDPSend("QQ345911220", "192.168.1.100", 12345)$n //绑定192.168.1.101:12345$n Dim isBind = zm.UDPBind("192.168.1.101", 12345, 15)$n If isBind Then$n     //接收数据$n     TracePrint zm.UDPReceive(60)$n     //关闭UDP$n     TracePrint zm.UDPClose()$n Else $n     TracePrint "UDP绑定失败"$n End If
--]=]
function zmm.UDPSend(data, ip, port)
	return try {
		function()
			local udp, err, ret
			local socket = require("socket")
			udp, err = socket.udp()
			if err then
				traceprint("UDPSend错误:", err)
				return nil, err
			end

			udp:setpeername(ip, port)
			ret, err = udp:send(data)
			if err then
				traceprint("UDPSend错误:", err)
				return nil, err
			end
			udp:close()
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.UDPSend()，错误信息：", errors)
			end
		}
	}
	--     广播代码无效
	--    udp:setoption('broadcast', true)
	--    udp:settimeout(3)
	--    return(udp:sendto("Z-SEARCH * \r\n", '192.168.1.255', 8080))
end

--[=[
@fname:UDPClose
@cname:关闭UDP
@format:无
@tname:关闭UDP
@note:关闭UDP
@ret:关闭结果, number, 成功返回大于0的数值, 失败返回null
@arg:无
@exp://往192.168.1.100:12345发送数据$n TracePrint zm.UDPSend("QQ345911220", "192.168.1.100", 12345)$n //绑定192.168.1.101:12345$n Dim isBind = zm.UDPBind("192.168.1.101", 12345, 15)$n If isBind Then$n     //接收数据$n     TracePrint zm.UDPReceive(60)$n     //关闭UDP$n     TracePrint zm.UDPClose()$n Else $n     TracePrint "UDP绑定失败"$n End If
--]=]
function zmm.UDPClose()
	return try {
		function()
			if udp[1] then
				return udp[1]:close()
			else
				return 2
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.UDPClose()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[******************************* OCR函数 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:OcrNumbers
@cname:免字库识别数字或坐标
@format:x1, y1, x2, y2, 颜色, 识别模式
@tname:识别 $1, $2, $3, $4 范围内 $5 颜色的数字或坐标
@note:免字库识别指定范围内符合颜色的数值或坐标, 支持小数, 基于按键自带的SmartOcr()开发
@ret:数值或坐标表, number或table, 根据识别模式参数返回识别的数值或坐标数值数组, 坐标table格式为{x, y, "x":x, "y":y} 失败返回null
@arg:x1, number, 识别范围的左上角x坐标, 区域范围全为0时表示全屏范围
@arg:y1, number, 识别范围的左上角y坐标, 区域范围全为0时表示全屏范围
@arg:x2, number, 识别范围的右上角x坐标, 区域范围全为0时表示全屏范围
@arg:y2, number, 识别范围的右上角y坐标, 区域范围全为0时表示全屏范围
@arg:颜色, string, 识别数字的16进制颜色, 格式为“BBGGRR”, 多个颜色用“|”隔开, 偏色使用“-”隔开, 比如”FFFFFF-101010|123456”
@arg:识别模式, number, 可选, 0表示只识别数字, 返回数值型; 1表示识别数字和小数点, 返回数值型; 2表示识别坐标, 返回数值型数组; 省略默认为0, 非0模式下, 识别范围越小, 准确性越高
@exp:Dim 数值 = zm.OcrNumbers(100,100,120,110,"000000", 1)
@exp:Dim 坐标 = zm.OcrNumbers(100,100,120,110,"000000", 2)$n If 坐标 then$n TracePrint 坐标["x"], 坐标["y"]$n End If
--]=]
function zmm.OcrNumbers(x1, y1, x2, y2, colors, mode)
	return try {
		function()
			mode = mode or 0
			local rotation = LuaAuxLib.GetScreenInfo(3)
			local tcolor = zmm.Split(colors, "|")
			local color = {}
			for i = 1, #tcolor do
				color[i] = {}
				local t = zmm.Split(tcolor[i], "-")
				color[i][1] = zmm.ColorToRGB(t[1])
				if #t == 1 then
					color[i][2] = { 0, 0, 0 }
				else
					color[i][2] = zmm.ColorToRGB(t[2])
				end
			end

			local checkColor = function(getcolor, setcolor)
				if getcolor then
					for i = 1, #setcolor do
						if math.abs(getcolor[1] - setcolor[i][1][1]) <= setcolor[i][2][1] and math.abs(getcolor[2] - setcolor[i][1][2]) <= setcolor[i][2][2] and math.abs(getcolor[3] - setcolor[i][1][3]) <= setcolor[i][2][3] then
							return true
						end
					end
					return false
				end
			end

			local t, mm = {}, {}
			local function search(data, color, i, j, k)
				if t[i][j] == nil then
					t[i][j] = checkColor(data[i][j], color)
					if t[i][j] then
						if k == 1 then
							mm[#mm + 1] = { mini = i, maxi = i, minj = j, maxj = j }
						else
							mm[#mm].maxi = i > mm[#mm].maxi and i or mm[#mm].maxi
							mm[#mm].maxj = j > mm[#mm].maxj and j or mm[#mm].maxj
							mm[#mm].mini = i < mm[#mm].mini and i or mm[#mm].mini
							mm[#mm].minj = i < mm[#mm].minj and j or mm[#mm].minj
						end
						if i > 1 and j > 1 then
							search(data, color, i - 1, j - 1)
						end
						if j > 1 then
							search(data, color, i, j - 1)
						end
						if i < #data and j > 1 then
							search(data, color, i + 1, j - 1)
						end
						if i > 1 then
							search(data, color, i - 1, j)
						end
						if i < #data then
							search(data, color, i + 1, j)
						end
						if i > 1 and j < #data[1] then
							search(data, color, i - 1, j + 1)
						end
						if j < #data[1] then
							search(data, color, i, j + 1)
						end
						if i < #data and j < #data[1] then
							search(data, color, i + 1, j + 1)
						end
					end
				end
			end

			local searchAll = function(data, color)
				for i = 1, #data do
					t[i] = {}
				end

				for i = 1, #data do
					for j = 1, #data[1] do
						if t[i][j] == nil then
							search(data, color, i, j, 1)
						end
					end
				end
			end

			local numbers = ""
			local position, leftnum, rightnum
			if mode == 0 then
				return LuaAuxLib.SmartOcr(x1, y1, x2, y2, colors)
			elseif mode == 1 or mode == 2 then
				local allcolor = LuaAuxLib.GetScreenData(x1, y1, x2, y2)
				searchAll(allcolor, color)
				if #mm > 0 then
					--                    table.sort(mm, function(a, b) return (a.maxj-a.minj)*(a.maxi-a.mini) < (b.maxj-b.minj)*(b.maxi-b.mini) end)
					if rotation == 0 then
						table.sort(mm, function(a, b) return a.maxj - a.minj < b.maxj - b.minj end)
						if (mm[1].maxj - mm[1].minj) * 2 < mm[#mm].maxj - mm[#mm].minj then
							leftnum = LuaAuxLib.SmartOcr(x1, y1, x1 + mm[1].mini - 1, y2, colors)
							rightnum = LuaAuxLib.SmartOcr(x1 + mm[1].maxi + 1, y1, x2, y2, colors)
						end
					elseif rotation == 1 then
						table.sort(mm, function(a, b) return a.maxi - a.mini < b.maxi - b.mini end)
						if (mm[1].maxi - mm[1].mini) * 2 < mm[#mm].maxi - mm[#mm].mini then
							leftnum = LuaAuxLib.SmartOcr(x1, y1, x2, y1 + mm[1].minj - 1, colors)
							rightnum = LuaAuxLib.SmartOcr(x1, y1 + mm[1].maxj + 1, x2, y2, colors)
						end
					elseif rotation == 2 then
						table.sort(mm, function(a, b) return a.maxj - a.minj < b.maxj - b.minj end)
						if (mm[1].maxj - mm[1].minj) * 2 < mm[#mm].maxj - mm[#mm].minj then
							leftnum = LuaAuxLib.SmartOcr(x1 + mm[1].maxi + 1, y1, x2, y2, colors)
							rightnum = LuaAuxLib.SmartOcr(x1, y1, x1 + mm[1].mini - 1, y2, colors)
						end
					elseif rotation == 3 then
						table.sort(mm, function(a, b) return a.maxi - a.mini < b.maxi - b.mini end)
						if (mm[1].maxi - mm[1].mini) * 2 < mm[#mm].maxi - mm[#mm].mini then
							leftnum = LuaAuxLib.SmartOcr(x1, y1 + mm[1].maxj + 1, x2, y2, colors)
							rightnum = LuaAuxLib.SmartOcr(x1, y1, x2, y1 + mm[1].minj - 1, colors)
						end
					end

					if leftnum and leftnum ~= "" then
						if mode == 1 then
							numbers = leftnum .. "." .. rightnum
						elseif mode == 2 then
							if rightnum and rightnum ~= "" then
								position = { tonumber(leftnum), tonumber(rightnum), x = tonumber(leftnum),
									y = tonumber(rightnum) }
							end
						end
					else
						numbers = LuaAuxLib.SmartOcr(x1, y1, x2, y2, colors)
					end

					if mode == 1 then
						return tonumber(numbers)
					elseif mode == 2 then
						return position
					end
				end
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrNumbers()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrYouDaoInit
@cname:初始化有道OCR
@format:[应用ID, 应用密钥][高级属性]
@tname:初始化有道OCR
@note:初始化设置有道智云OCR的账户数据, 只要调用一次, 账户注册详见命令帮助
@ret:参数列表, table, 成功返回设置后的参数列表, 失败返回null
@arg:应用ID, string, 可选, 与高级属性参数二选一, 有道智云后台创建的应用ID, 详见命令帮助
@arg:应用密钥, string, 可选, 与高级属性参数二选一, 有道智云后台创建的应用密钥, 详见命令帮助
@arg:高级属性, table, 可选, 与应用ID密钥参数二选一, 建议不写, 以table类型详细设置每个参数, 若填写本参数, 则将忽略其他参数, 详见命令帮助
@exp:zm.OcrYouDaoInit "请修改成你的应用ID", "请修改成你的应用密钥"
--]=]
_zmm.OcrYouDaoAttr = {}
_zmm.OcrYouDaoAttr.default = { appKey = "", secKey = "", langType = "zh-en", detectType = "10011", imageType = "1",
	docType = "json", postURL = "http://openapi.youdao.com/ocrapi" }
_zmm.OcrYouDaoAttr.mt = { __index = _zmm.OcrYouDaoAttr.default }
function zmm.OcrYouDaoInit(t, secKey)
	return try {
		function()
			if type(t) == "table" then
				for k, v in pairs(t) do
					_zmm.OcrYouDaoAttr.default[k] = v
				end
			else
				_zmm.OcrYouDaoAttr.default.appKey = t
				_zmm.OcrYouDaoAttr.default.secKey = secKey
			end
			return _zmm.OcrYouDaoAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouDaoInit()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrYouDao
@cname:有道智云OCR
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:设置有道智云OCR
@note:在线云识别图片内容, 支持范围识别或指定图片识别, 请先调用zm.OcrYouDaoInit()设置后才使用本命令
@ret:识别结果, string, 成功返回识别结果, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouDaoInit "请修改成你的应用ID", "请修改成你的应用密钥"$n Dim ret = zm.OcrYouDao(100,100,400,400)
--]=]
function zmm.OcrYouDao(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end
			zmm.TableClear(tRet)

			_zmm.OcrYouDaoAttr.default.imgBase64 = zmm.FileReadBinaryBase64(imgPath)
			_zmm.OcrYouDaoAttr.default.salt = zmm.RndNum(1, 65535)

			local img = "img=" .. zmm.EncodeURL(_zmm.OcrYouDaoAttr.default.imgBase64)
			local langType = "langType=" .. _zmm.OcrYouDaoAttr.default.langType
			local detectType = "detectType=" .. _zmm.OcrYouDaoAttr.default.detectType
			local imageType = "imageType=" .. _zmm.OcrYouDaoAttr.default.imageType
			local appKey = "appKey=" .. _zmm.OcrYouDaoAttr.default.appKey
			local salt = "salt=" .. _zmm.OcrYouDaoAttr.default.salt
			local sign = "sign=" ..
			LuaAuxLib.Encode_Md5(_zmm.OcrYouDaoAttr.default.appKey ..
			_zmm.OcrYouDaoAttr.default.imgBase64 .. _zmm.OcrYouDaoAttr.default.salt .. _zmm.OcrYouDaoAttr.default.secKey)
			:upper()
			local docType = "docType=" .. _zmm.OcrYouDaoAttr.default.salt

			local buf = img ..
			"&" ..
			langType ..
			"&" .. detectType .. "&" .. imageType .. "&" .. appKey .. "&" .. salt .. "&" .. sign .. "&" .. docType
			local jsonData = LuaAuxLib.URL_OperationPost(_zmm.OcrYouDaoAttr.default.postURL, buf, 3)
			local s
			if jsonData and jsonData:match("{.*}") then
				local t = {}
				local ret = LuaAuxLib.Encode_GetJsonLib():decode(jsonData)
				if ret then
					for k, v in pairs(ret) do
						tRet[k] = v
					end
					if ret.errorCode == "0" then
						for _, v in ipairs(ret.Result.regions) do
							for _, vv in ipairs(v.lines) do
								for _, vvv in ipairs(vv.words) do
									table.insert(t, vvv.text)
								end
								table.insert(t, "\n")
							end
						end
					else
						return nil
					end
				end
				s = table.concat(t, "")
			end
			return s
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouDao()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrBaiDuInit
@cname:初始化百度OCR
@format:[高级属性][[apikey, seckey][, posturl]]
@tname:初始化百度OCR
@note:初始化设置百度云文字识别key和请求参数, 一般只要调用一次, 除非需要用不同的识别模式与参数, 账户注册详见命令帮助
@ret:默认属性列表, table, 成功返回设置后的参数列表, 失败返回null
@arg:高级属性, table, 可选, 一般不需要填写, 以table类型详细设置每个参数, 若填写本参数, 则将忽略其他参数, 详见 http://ai.baidu.com/docs#/OCR-API/top 中的请求参数, 其中请求URL的键名为posturl
@arg:apikey, string, 可选, 百度云文字识别控制台创建的API Key, 详见命令帮助
@arg:seckey, string, 可选, 百度云文字识别控制台创建的Secret Key, 详见命令帮助
@arg:posturl, string, 可选, 百度云文字识别的请求URL, 用于不同模式的识别, 省略默认为https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic
@exp:Dim r = zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")
--]=]
_zmm.OcrBaiDuAttr = {}
_zmm.OcrBaiDuAttr.default = { apikey = "", seckey = "", posturl =
"https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic", detect_direction = "true" }
_zmm.OcrBaiDuAttr.mt = { __index = _zmm.OcrBaiDuAttr.default }
--_ZM_.FindStrBaiDuAttr = {}
--_ZM_.FindStrBaiDuAttr.default = {apikey="", seckey="", findstrmode=1, x1=0,y1=0,x2=0,y2=0,timeout=1,delaytime=0,tap=false,tapxy="",showlog="隐藏",line="",note="",clear=-1,cleartime=-2000,ret={}}
--_ZM_.FindStrBaiDuAttr.mt = {__index = _ZM_.FindStrBaiDuAttr.default}
function zmm.OcrBaiDuInit(t, secKey, direction, posturl)
	return try {
		function()
			if type(t) == "table" then
				for k, v in pairs(t) do
					_zmm.OcrBaiDuAttr.default[k] = zmm.Trim(v)
					--                    if type(k) == "string" and _ZM_.FindStrBaiDuAttr.default[k:lower()] ~= nil then
					--                        _ZM_.FindStrBaiDuAttr.default[k:lower()] = v
					--                    end
				end
			else
				if type(t) == "string" and t:match("^https?://") then
					_zmm.OcrBaiDuAttr.default.posturl = t
				else
					_zmm.OcrBaiDuAttr.default.apikey = zmm.Trim(t)
					_zmm.OcrBaiDuAttr.default.seckey = zmm.Trim(secKey)
					--                    _ZM_.FindStrBaiDuAttr.default.apikey = _ZM_.OcrBaiDuAttr.default.apikey
					--                    _ZM_.FindStrBaiDuAttr.default.seckey = _ZM_.OcrBaiDuAttr.default.seckey
					if type(direction) == "string" then
						posturl, direction = direction, posturl
					end
					_zmm.OcrBaiDuAttr.default.posturl = posturl or _zmm.OcrBaiDuAttr.default.posturl
				end
			end

			--            if _ZM_.FindStrBaiDuAttr.default.findstrmode == 1 then
			--                _ZM_.FindStrBaiDuAttr.default.posturl = "https://aip.baidubce.com/rest/2.0/ocr/v1/general"
			--            elseif _ZM_.FindStrBaiDuAttr.default.findstrmode == 2 then
			--                _ZM_.FindStrBaiDuAttr.default.posturl = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate"
			--            end

			if type(t) == "string" or t.apikey ~= nil then
				local access_token = LuaAuxLib.URL_OperationPost("https://aip.baidubce.com/oauth/2.0/token",
					"grant_type=client_credentials&client_id=" ..
					_zmm.OcrBaiDuAttr.default.apikey .. "&client_secret=" .. _zmm.OcrBaiDuAttr.default.seckey, 3)
				if access_token and access_token:match("{.*}") then
					local token = LuaAuxLib.Encode_GetJsonLib():decode(access_token)
					_zmm.OcrBaiDuAttr.default.error = nil
					if token.error then
						-- 生成token失败
						_zmm.OcrBaiDuAttr.default.error = access_token
						error("生成百度云token失败" .. access_token)
						return nil
					end
					_zmm.OcrBaiDuAttr.default.access_token = token.access_token
					--                    _ZM_.FindStrBaiDuAttr.default.access_token = token.access_token
					return _zmm.OcrBaiDuAttr.default
				else
					error("访问获取access_token网络失败")
					return nil
				end
			else
				return _zmm.OcrBaiDuAttr.default
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuInit()，错误信息：", errors)
			end
		}
	}
end

-- 整合post参数
function _zmm.getPostParms(t, sort)
	local parmstr
	if sort then
		local tkeys = {}
		for k, v in pairs(t) do
			if v ~= nil and type(v) ~= "table" then
				tkeys[#tkeys + 1] = k
			end
		end
		table.sort(tkeys, function(a, b) return a < b end)
		for _, v in pairs(tkeys) do
			if type(v) ~= "table" then
				if parmstr then
					parmstr = parmstr .. "&" .. v .. "=" .. t[v]
				else
					parmstr = v .. "=" .. t[v]
				end
			end
		end
	else
		local parms = {}
		for k, v in pairs(t) do
			if type(v) ~= "table" then
				table.insert(parms, k .. "=" .. tostring(v))
			end
		end
		parmstr = table.concat(parms, "&")
	end

	return parmstr
end

--[=[
@fname:OcrBaiDu
@cname:百度云通用文字识别
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:百度云文字识别
@note:在线云识别图片内容, 支持范围识别或指定图片识别, 请先调用zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, string, 成功返回识别结果, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 暂不支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDu(0, 0, 0, 0) //全屏识别文字
--]=]
function zmm.OcrBaiDu(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			zmm.TableClear(tRet)

			if imgPath:match("^http") then
				_zmm.OcrBaiDuAttr.default.image = nil
				_zmm.OcrBaiDuAttr.default.url = zmm.EncodeURL(imgPath)
			else
				_zmm.OcrBaiDuAttr.default.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
				_zmm.OcrBaiDuAttr.default.url = nil
			end
			_zmm.OcrBaiDuAttr.default.id_card_side = nil

			local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
			local postURL = _zmm.OcrBaiDuAttr.default.posturl ..
			"?access_token=" .. _zmm.OcrBaiDuAttr.default.access_token
			local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
			local words = {}
			local word
			if ret and ret:match("{.*}") then
				local t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
				for k, v in pairs(t) do
					tRet[k] = v
				end
				if t.words_result then
					for _, v in pairs(t.words_result) do
						table.insert(words, v.words)
					end
					word = table.concat(words, "\n")
					return word
				else
					return nil
				end
			else
				tRet.error = "访问获取识别网络失败"
				error("访问获取识别网络失败")
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

function _zmm.FindStrBaiDu(x1, y1, x2, y2, str)
	local imgPath
	return try {
		function()
			imgPath = zmm.FileTemp() .. ".png"
			LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
			local strT = zmm.Split(str, "|")

			local retocr = zmm.OcrBaiDu(imgPath)
			if retocr then
				for i = 1, #strT do
					if retocr:match(strT[i]) then
						break
					elseif i == #strT then
						return nil
					end
				end
			end

			local parms, ret = {}, {}

			parms.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
			parms.recognize_granularity = "small"
			parms.detect_direction = "true"

			local baiduURL = "https://aip.baidubce.com/rest/2.0/ocr/v1/general"
			if _zmm.OcrBaiDuAttr.default.posturl == "https://aip.baidubce.com/rest/2.0/ocr/v1/general" or _zmm.OcrBaiDuAttr.default.posturl == "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate" then
				baiduURL = _zmm.OcrBaiDuAttr.default.posturl
			end

			local postURL = baiduURL .. "?access_token=" .. _zmm.OcrBaiDuAttr.default.access_token
			local html = LuaAuxLib.URL_OperationPost(postURL, _zmm.getPostParms(parms), 30)
			if html and html:match("{.*}") then
				local cjson = require("cjson")
				local t = cjson.decode(html)
				ret.baidu = t
				if t.words_result then
					for kk, vv in pairs(strT) do
						for _, v in pairs(t.words_result) do
							local i = LuaAuxLib.UTF8_InStr(v.words, vv, 1)
							if i > 0 then
								local x = v.chars[i].location.left + x1
								local y = v.chars[i].location.top + y1
								ret[#ret + 1] = { kk - 1, x, y, vv }
							end
						end
					end
				end
			end
			return #ret > 0 and ret or nil
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：FindStrBaiDu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if imgPath then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:FindStrBaiDu
@cname:百度云找字
@format:[对象][x1, y1, x2, y2, ]文字[, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:百度云找字
@note:使用百度云识别来实现找字功能, 不需要字库, 字体颜色之类的参数, 必须先使用zm.OcrBaiDuInit()进行初始化后使用, 默认参数使用zm.SetFindStr()修改
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name,"miss":miss,"baidu":原生返回值}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStrBaiDu(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%$.*$") then
								args.str = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif args.str == nil then
								args.str = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindStrAttr.mt)

			if args.str == nil then
				traceprint(args.line, "zm.FindStrBaiDu( )出错了, 原因:未填写需要查找的文字参数")
				return nil
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)


			args.ret.id, args.ret.x, args.ret.y, args.ret.name = -1, -1, -1, ""
			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			repeat
				args.times = args.times + 1
				local ret = _zmm.FindStrBaiDu(args.x1, args.y1, args.x2, args.y2, args.str)
				if ret then
					args.ret.id, args.ret.x, args.ret.y, args.ret.name = ret[1][1], ret[1][2], ret[1][3], ret[1][4]
					args.ret.baidu = ret.baidu
					break
				end
				LuaAuxLib.Sleep(args.delaytime)
			until (LuaAuxLib.GetTickCount() - time) > args.timeout
			time = LuaAuxLib.GetTickCount() - time

			args.ret[1], args.ret[2], args.ret[3], args.ret[4] = args.ret.id, args.ret.x, args.ret.y, args.ret.name

			local ret
			if args.ret.x > -1 then
				args.tapx, args.tapy = _zmm.setScaleXY(args.tapx, args.tapy, 1)
				args.ret.x, args.ret.y = _zmm.setScaleXY(args.ret.x, args.ret.y, 1)
				args.ret[2], args.ret[3] = args.ret.x, args.ret.y
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, x = args.ret.x, y = args.ret.y,
					id = args.ret.id, name = args.ret.name, time = time, counts = args.times, baidu = args.ret.baidu }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 百度云找字=" ..
				args.str ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 .. "," .. args.y2 .. " 耗时=" .. time .. " 次数=" ..
				args.times .. " 是否点击=" .. tostring(args.tap)
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					if not _zmm.FindStrBaiDu(args.x1, args.y1, args.x2, args.y2, args.str) then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "文字已消失: " .. args.pic
					else
						log = "文字还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrBaiDu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" then
					LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
				end
			end
		}
	}
end

--[=[
@fname:FindStrTapBaiDu
@cname:百度云找字并点击
@format:[对象][x1, y1, x2, y2, ]文字[, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:百度云找字
@note:使用百度云识别来实现找字功能, 不需要字库, 字体颜色之类的参数, 找到后自动点击
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name,"miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStrTapBaiDu(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindStrBaiDu(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTapBaiDu()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindStrTableBaiDu
@cname:百度云找字遍历
@format:对象table
@tname:百度云找字遍历
@note:与zm.FindStrBaiDu()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindStrBaiDu()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"史莱姆","000000",true}, "怪物2":{10,20,30,40,"黑龙","123456"}}$n Dim ret = zm.FindStrTableBaiDu(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindStrTableBaiDu(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindStrBaiDu(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTableBaiDu()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuAll
@cname:百度云识别通用函数
@format:[x1, y1, x2, y2][图片路径]
@tname:百度云识别通用函数
@note:百度云在线识别通用函数, 支持范围识别或指定图片识别$n 必须通过zm.OcrBaiDuInit()设置请求posturl等参数后才使用本命令
@ret:识别结果, table, 返回格式与百度云文档中返回值相同, 详见百度云文字识别API文档的返回说明内容
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右下角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右下角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuAll(0, 0, 0, 0) //全屏识别文字
--]=]
function zmm.OcrBaiDuAll(x1, y1, x2, y2)
	local imgPath
	return try {
		function()
			--            local cjson = require("cjson")
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
			else
				imgPath = x1
			end

			if imgPath:match("^http") then
				_zmm.OcrBaiDuAttr.default.image = nil
				_zmm.OcrBaiDuAttr.default.url = zmm.EncodeURL(imgPath)
			else
				_zmm.OcrBaiDuAttr.default.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
				_zmm.OcrBaiDuAttr.default.url = nil
			end

			local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
			local postURL = _zmm.OcrBaiDuAttr.default.posturl ..
			"?access_token=" .. _zmm.OcrBaiDuAttr.default.access_token
			local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
			if ret and ret:match("{.*}") then
				local t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
				return t
			else
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuAll()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

-- 解析部分百度识别返回值
function _zmm.getBaiduWordsResult(ret, tRet)
	--local cjson = require("cjson")
	return try {
		function()
			local words = {}
			zmm.TableClear(tRet)
			local t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
			for k, v in pairs(t) do
				tRet[k] = v
			end
			if t.words_result then
				for k, v in pairs(t.words_result) do
					words[k] = v.words
				end
				return words
			elseif t.codes_result then
				for k, v in pairs(t.codes_result) do
					words[k] = table.concat(v.text, ",")
				end
				return words
			elseif t.result then
				for k, v in pairs(t.result) do
					words[k] = v
				end
				return words
			else
				error(ret)
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：百度云识别内部函数.getBaiduWordsResult()，错误信息：",
					errors)
			end
		}
	}
end

-- 处理部分百度通用识别函数
function _zmm.ocrbaidu(url, x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			_zmm.OcrBaiDuAttr.default.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
			_zmm.OcrBaiDuAttr.default.url = nil
			local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
			local postURL = url .. "?access_token=" .. _zmm.OcrBaiDuAttr.default.access_token
			local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
			if ret and ret:match("{.*}") then
				return _zmm.getBaiduWordsResult(ret, tRet)
			else
				tRet.error = "访问获取识别网络失败"
				error(tRet.error)
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：百度云识别内部函数.ocrbaidu()，错误信息：",
					errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrBaiDuNumbers
@cname:百度云数字识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云数字识别
@note:识别屏幕上的数字, 返回字符串数据类型, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, string, 返回识别的数字内容, 注意数据类型是字符串格式
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuNumbers(0, 0, 0, 0)$n TracePrint "识别结果:", ret
--]=]
function zmm.OcrBaiDuNumbers(x1, y1, x2, y2, tRet)
	return try {
		function()
			local t = _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/numbers", x1, y1, x2, y2, tRet)
			return table.concat(t)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuNumbers()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuHandWriting
@cname:百度云手写文字识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云手写文字识别
@note:识别屏幕上的手写文字, 返回字符串数据类型, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, string, 返回识别的文字内容
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuHandWriting(0, 0, 0, 0)$n TracePrint "识别结果:", ret
--]=]
function zmm.OcrBaiDuHandWriting(x1, y1, x2, y2, tRet)
	return try {
		function()
			local t = _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/handwriting", x1, y1, x2, y2, tRet)
			return table.concat(t)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiHandWriting()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuIDCard
@cname:百度云身份证识别
@format:正背面, [x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云身份证识别
@note:识别身份证正背面, 内置固定的请求URL, 除图片和正背面id_card_side参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“公民身份号码”:”345911220”,”姓名”:”紫猫”, …}, 详细请自行遍历查看
@arg:正背面, string, 必选, 身份证照片的正背面, 正面照片填写"正面"或"front", 背面照片填写"背面"或"back"
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuIDCard(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuIDCard(id_card_side, x1, y1, x2, y2, tRet)
	return try {
		function()
			if id_card_side == "正面" then
				id_card_side = "front"
			elseif id_card_side == "背面" then
				id_card_side = "back"
			end
			_zmm.OcrBaiDuAttr.default.id_card_side = id_card_side
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/idcard", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuIDCard()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuBankCard
@cname:百度云银行卡识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云银行卡识别
@note:识别银行卡并返回卡号和发卡行, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式为{“bank_card_number”:”622500000000000”,”bank_name”:”招商银行”,”bank_card_type”:1}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuBankCard(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuBankCard(x1, y1, x2, y2, tRet)
	return try {
		function()
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/bankcard", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuBankCard()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuDriving
@cname:百度云驾驶证识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云驾驶证识别
@note:对机动车驾驶证所有关键字段进行识别, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“证号”:”345911220”,”姓名”:”紫猫”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuDriving(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuDriving(x1, y1, x2, y2, tRet)
	return try {
		function()
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/driving_license", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuDriving()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuVehicle
@cname:百度云行驶证识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云行驶证识别
@note:对机动车行驶证正本所有关键字段进行识别, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“品牌型号”:”保时捷GT37182RUCRE”,”所有人”:”紫猫”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuVehicle(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuVehicle(x1, y1, x2, y2, tRet)
	return try {
		function()
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/vehicle_license", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuVehicle()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuPlate
@cname:百度云车牌识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云车牌识别
@note:识别机动车车牌并返回签发地和号牌, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“鄂A345911220”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuPlate(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuPlate(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			zmm.TableClear(tRet)

			_zmm.OcrBaiDuAttr.default.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
			_zmm.OcrBaiDuAttr.default.url = nil

			local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
			local postURL = "https://aip.baidubce.com/rest/2.0/ocr/v1/license_plate?access_token=" ..
			_zmm.OcrBaiDuAttr.default.access_token
			local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
			local words = {}
			if ret and ret:match("{.*}") then
				local t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
				for k, v in pairs(t) do
					tRet[k] = v
				end
				if t.words_result then
					for k, v in pairs(t.words_result) do
						if type(v) == "table" then
							table.insert(words, v.number)
						elseif k == "number" then
							table.insert(words, v)
						end
					end
					return words
				else
					return nil
				end
			else
				tRet.error = "访问获取识别网络失败"
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuPlate()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrBaiDuBusiness
@cname:百度云营业执照识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云营业执照识别
@note:识别营业执照, 并返回关键字段的值, 包括单位名称/法人/地址/有效期/证件编号/社会信用代码等, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“单位名称”:”紫猫编程学院”,”法人”:”紫猫”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuBusiness(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuBusiness(x1, y1, x2, y2, tRet)
	return try {
		function()
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/business_license", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuBusiness()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuForm
@cname:百度云表格文字识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云表格文字识别
@note:已整合为同步请求, 自动识别表格线及表格内容, 结构化输出表头, 表尾及每个单元格的文字内容, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, string, 返回表格下载地址或json文本
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuForm(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuForm(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			--            local cjson = require("cjson")
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			zmm.TableClear(tRet)

			_zmm.OcrBaiDuAttr.default.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
			_zmm.OcrBaiDuAttr.default.url = nil

			local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
			local postURL = "https://aip.baidubce.com/rest/2.0/solution/v1/form_ocr/request?access_token=" ..
			_zmm.OcrBaiDuAttr.default.access_token
			local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
			--        local words = {}
			if ret and ret:match("{.*}") then
				local t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
				for k, v in pairs(t) do
					tRet[k] = v
				end
				if t.result then
					_zmm.OcrBaiDuAttr.default.request_id = t.result[1].request_id
					local t
					repeat
						local postURL =
						"https://aip.baidubce.com/rest/2.0/solution/v1/form_ocr/get_request_result?access_token=" ..
						_zmm.OcrBaiDuAttr.default.access_token
						local parms = _zmm.getPostParms(_zmm.OcrBaiDuAttr.default)
						local ret = LuaAuxLib.URL_OperationPost(postURL, parms, 30)
						if ret then
							t = LuaAuxLib.Encode_GetJsonLib():decode(ret)
							for k, v in pairs(t) do
								tRet[k] = v
							end
						else
							tRet.error = "访问获取识别网络失败"
							return nil
						end
						LuaAuxLib.Sleep(1000)
					until t.result.ret_msg == "已完成"
					return t.result.result_data
				else
					return nil
				end
			else
				tRet.error = "访问获取识别网络失败"
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuForm()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrBaiDuReceipt
@cname:百度云通用票据识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云通用票据识别
@note:识别医疗票据/发票/的士票/保险保单等票据类图片中的所有文字, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“345911220”,”紫猫”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuReceipt(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuReceipt(x1, y1, x2, y2, tRet)
	return try {
		function()
			return _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/receipt", x1, y1, x2, y2, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuReceipt()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrBaiDuQRCode
@cname:百度云二维码识别
@format:[x1, y1, x2, y2][图片路径][, 返回json]
@tname:百度云二维码识别
@note:对图片中的二维码, 条形码进行检测和识别, 返回存储的文字信息, 内置固定的请求URL, 除图片参数外, 需要zm.OcrBaiDuInit()设置后才使用本命令
@ret:识别结果, table, 返回优化后的识别结果, 格式类似于{“345911220”,”紫猫”, …}, 详细请自行遍历查看
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径, string, 可选, 要识别内容的图片本地路径, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不建议填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrBaiDuInit("请修改成你的API Key", "请修改成你的Secret Key")$n Dim ret = zm.OcrBaiDuQRCode(0, 0, 0, 0)
--]=]
function zmm.OcrBaiDuQRCode(x1, y1, x2, y2, tRet)
	return try {
		function()
			local ret = _zmm.ocrbaidu("https://aip.baidubce.com/rest/2.0/ocr/v1/qrcode", x1, y1, x2, y2, tRet)
			if ret and #ret == 1 then
				return ret[1]
			end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrBaiDuQRCode()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrYouTuInit
@cname:初始化优图OCR
@format:AppID, AppKey
@tname:初始化优图OCR
@note:初始化设置腾讯优图OCR云识别(https://ai.qq.com/)应用数据, 只要调用一次, 账户注册详见命令帮助
@ret:应用数据, table, 返回AppID和AppKey的table表
@arg:AppID, string, 应用的AppID
@arg:AppKey, string, 应用的AppKey
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTu(0, 0, 400, 400) //识别(0,0,400,400)范围内的文字
--]=]
_zmm.OcrYouTuAttr = {}
_zmm.OcrYouTuAttr.default = { AppID = "", AppKey = "" }
_zmm.OcrYouTuAttr.mt = { __index = _zmm.OcrYouTuAttr.default }
function zmm.OcrYouTuInit(AppID, AppKey)
	return try {
		function()
			if type(AppID) == "table" then
				for k, v in pairs(AppID) do
					_zmm.OcrYouTuAttr.default[k] = v
				end
			else
				_zmm.OcrYouTuAttr.default.AppID = AppID
				_zmm.OcrYouTuAttr.default.AppKey = AppKey
			end
			return _zmm.OcrYouTuAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuInit()，错误信息：", errors)
			end
		}
	}
end

_zmm.getYouTuSign = function(t, AppKey)
	local parms = _zmm.getPostParms(t, true) .. "&app_key=" .. AppKey
	return LuaAuxLib.Encode_Md5(parms):upper()
end

_zmm.ocryoutu = function(postURL, imgPath, tRet, parms)
	zmm.TableClear(tRet)

	parms = parms or {}
	parms.app_id = _zmm.OcrYouTuAttr.default.AppID
	parms.time_stamp = os.time()
	parms.nonce_str = zmm.RndStr(zmm.RndNum(10, 32))
	if imgPath:match("^http") then
		parms.image_url = zmm.EncodeURL(imgPath)
	else
		parms.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
	end

	parms.sign = _zmm.getYouTuSign(parms, _zmm.OcrYouTuAttr.default.AppKey)
	local parmstr = _zmm.getPostParms(parms)

	local json = LuaAuxLib.URL_OperationPost(postURL, parmstr, 30)

	local word, istable
	local words = {}
	if json and json:match("{.*}") then
		local cjson = require("cjson")
		local t = cjson.decode(json)
		for k, v in pairs(t) do
			tRet[k] = v
		end

		if t.msg == "ok" then
			if t.data.item_list then
				for _, v in pairs(t.data.item_list) do
					if v.item ~= "" then
						words[v.item] = v.itemstring
						istable = true
					else
						words[#words + 1] = v.itemstring
					end
				end
			else
				for k, v in pairs(t.data) do
					if type(k) ~= "" then
						words[k] = v
						istable = true
					else
						words[#words + 1] = v
					end
				end
			end
			word = table.concat(words, "\n")
		else
			error("错误编号: " .. t.ret .. ", 错误信息: " .. t.msg)
		end
	else
		tRet.msg = "访问获取识别网络失败"
		error("访问优图失败, 请咨询优图客服解决")
	end

	if istable then
		return words
	else
		return word
	end
end

--[=[
@fname:OcrYouTu
@cname:优图通用OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图通用OCR
@note:在线云识别图片内容, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:识别结果, string, 成功返回识别结果, 多行内容以"\n"分割, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 包含位置坐标, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTu(0, 0, 0, 0) //全屏识别文字
--]=]
function zmm.OcrYouTu(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end
			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_generalocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

function _zmm.FindStrYouTu(x1, y1, x2, y2, str)
	local imgPath
	return try {
		function()
			imgPath = zmm.FileTemp() .. ".png"
			LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
			local parms, ret = {}, {}
			local strT = zmm.Split(str, "|")

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_generalocr"

			parms.app_id = _zmm.OcrYouTuAttr.default.AppID
			parms.time_stamp = os.time()
			parms.nonce_str = zmm.RndStr(zmm.RndNum(10, 32))
			parms.image = zmm.EncodeURL(zmm.FileReadBinaryBase64(imgPath))
			parms.sign = _zmm.getYouTuSign(parms, _zmm.OcrYouTuAttr.default.AppKey)

			local parmstr = _zmm.getPostParms(parms)

			local json = LuaAuxLib.URL_OperationPost(postURL, parmstr, 30)
			if json and json:match("{.*}") then
				local cjson = require("cjson")
				local t = cjson.decode(json)

				if t.msg == "ok" then
					if t.data.item_list then
						for kk, vv in pairs(strT) do
							for _, v in pairs(t.data.item_list) do
								local i = LuaAuxLib.UTF8_InStr(v.itemstring, vv, 1)
								if i > 0 then
									local x, y
									if v.itemcoord[1].height > v.itemcoord[1].width then
										x = v.itemcoord[1].x + x1
										y = v.itemcoord[1].y +
										(math.floor((v.itemcoord[1].height / LuaAuxLib.UTF8_Length(v.itemstring)))) *
										(i - 1) + y1
									else
										x = v.itemcoord[1].x +
										(math.floor((v.itemcoord[1].width / LuaAuxLib.UTF8_Length(v.itemstring)))) *
										(i - 1) + x1
										y = v.itemcoord[1].y + y1
									end
									ret[#ret + 1] = { kk - 1, x, y, vv }
								end
							end
						end
					end
				else
					error("错误编号: " .. t.ret .. ", 错误信息: " .. t.msg)
				end
			else
				error("访问优图失败, 请咨询优图客服解决")
			end

			return #ret > 0 and ret or nil
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：FindStrYouTu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if imgPath then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:FindStrYouTu
@cname:优图云找字
@format:[对象][x1, y1, x2, y2, ]文字[, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:优图云找字
@note:使用优图云识别来实现找字功能, 不需要字库, 字体颜色之类的参数, 必须先使用zm.OcrYouTuInit()进行初始化后使用, 可以用zm.SetFindStr()修改默认参数
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name,"miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStrYouTu(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%$.*$") then
								args.str = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif args.str == nil then
								args.str = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindStrAttr.mt)

			if args.str == nil then
				traceprint(args.line, "zm.FindStrYouTu( )出错了, 原因:未填写需要查找的文字参数")
				return nil
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)


			args.ret.id, args.ret.x, args.ret.y, args.ret.name = -1, -1, -1, ""
			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			repeat
				args.times = args.times + 1
				local ret = _zmm.FindStrYouTu(args.x1, args.y1, args.x2, args.y2, args.str)
				if ret then
					args.ret.id, args.ret.x, args.ret.y, args.ret.name = ret[1][1], ret[1][2], ret[1][3], ret[1][4]
					break
				end
				LuaAuxLib.Sleep(args.delaytime)
			until (LuaAuxLib.GetTickCount() - time) > args.timeout
			time = LuaAuxLib.GetTickCount() - time

			args.ret[1], args.ret[2], args.ret[3], args.ret[4] = args.ret.id, args.ret.x, args.ret.y, args.ret.name

			local ret
			if args.ret.x > -1 then
				args.tapx, args.tapy = _zmm.setScaleXY(args.tapx, args.tapy, 1)
				args.ret.x, args.ret.y = _zmm.setScaleXY(args.ret.x, args.ret.y, 1)
				args.ret[2], args.ret[3] = args.ret.x, args.ret.y
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, x = args.ret.x, y = args.ret.y,
					id = args.ret.id, name = args.ret.name, time = time, counts = args.times }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 优图找字=" ..
				args.str ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 .. "," .. args.y2 .. " 耗时=" .. time .. " 次数=" ..
				args.times .. " 是否点击=" .. tostring(args.tap)
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					if not _zmm.FindStrYouTu(args.x1, args.y1, args.x2, args.y2, args.str) then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "文字已消失: " .. args.pic
					else
						log = "文字还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrYouTu()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" then
					LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
				end
			end
		}
	}
end

--[=[
@fname:FindStrTapYouTu
@cname:优图云找字并点击
@format:[对象][x1, y1, x2, y2, ]文字[, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:优图云找字
@note:使用优图云识别来实现找字功能, 不需要字库, 字体颜色之类的参数, 找到后自动点击
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name,"miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStrTapYouTu(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindStrYouTu(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTapYouTu()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindStrTableYouTu
@cname:优图云找字遍历
@format:对象table
@tname:优图云找字遍历
@note:与zm.FindStrYouTu()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindStrBaiDu()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"史莱姆","000000",true}, "怪物2":{10,20,30,40,"黑龙","123456"}}$n Dim ret = zm.FindStrTableYouTu(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindStrTableYouTu(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindStrYouTu(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTableYouTu()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:OcrYouTuHandWriting
@cname:优图手写OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图手写OCR
@note:在线云识别图片内容, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:识别结果, string, 成功返回识别结果, 多行内容以"\n"分割, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 包含位置坐标, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuHandWriting(0, 0, 0, 0) //全屏识别文字
--]=]
function zmm.OcrYouTuHandWriting(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end
			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_handwritingocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuHandWriting()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuIDCard
@cname:优图身份证OCR
@format:正反面, [x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图身份证OCR
@note:在线云识别身份证信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:身份证信息, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:正反面, string或number, 必选, 身份证照片的正反面, 正面照片填写"正面"或0, 反面照片填写"反面"或1
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuIDCard(0, "/sdcard/身份证正面.png")
--]=]
function zmm.OcrYouTuIDCard(card_type, x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if card_type == "正面" then
				card_type = 0
			elseif card_type == "反面" then
				card_type = 1
			end
			assert(card_type == 0 or card_type == 1, "身份证图片类型错误")

			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_idcardocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet, { card_type = card_type })
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuIDCard()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuBC
@cname:优图名片OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图名片OCR
@note:在线云识别名片信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:名片信息, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuBC("/sdcard/名片.png")
--]=]
function zmm.OcrYouTuBC(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_bcocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuBC()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuDriverLicense
@cname:优图行驶证驾驶证OCR
@format:类型, [x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图行驶证驾驶证OCR
@note:在线云识别行驶证驾驶证信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:名片信息, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:类型, string或number, 必选, 行驶证照片填写"行驶证"或0, 驾驶证照片填写"驾驶证"或1
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuDriverLicense(0, "/sdcard/行驶证.png")
--]=]
function zmm.OcrYouTuDriverLicense(card_type, x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if card_type == "行驶证" then
				card_type = 0
			elseif card_type == "驾驶证" then
				card_type = 1
			end

			assert(card_type == 0 or card_type == 1, "行驶证或驾驶证参数错误")

			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_driverlicenseocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet, { type = card_type })
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuDriverLicense()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuPlate
@cname:优图车牌OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图车牌OCR
@note:在线云识别车牌信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:车牌信息, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuPlate("/sdcard/车牌.png")
--]=]
function zmm.OcrYouTuPlate(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_plateocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuPlate()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuBusiness
@cname:优图营业执照OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图营业执照OCR
@note:在线云识别营业执照信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:营业执照, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuBusiness("/sdcard/营业执照.png")
--]=]
function zmm.OcrYouTuBusiness(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_bizlicenseocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuBusiness()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:OcrYouTuBankCard
@cname:优图银行卡OCR
@format:[x1, y1, x2, y2][图片路径或URL][, 返回json]
@tname:优图银行卡OCR
@note:在线云识别银行卡信息, 支持范围识别或指定图片识别, 图片大小上限1MB, 请先调用zm.OcrYouTuInit()初始化后才使用本命令
@ret:银行卡, table, 成功返回键值对table结果, 请遍历查看, 失败返回null
@arg:x1, number, 可选, 识别范围的左上角x坐标, 不可与图片路径参数同时存在
@arg:y1, number, 可选, 识别范围的左上角y坐标, 不可与图片路径参数同时存在
@arg:x2, number, 可选, 识别范围的右上角x坐标, 不可与图片路径参数同时存在
@arg:y2, number, 可选, 识别范围的右上角y坐标, 不可与图片路径参数同时存在
@arg:图片路径或URL, string, 可选, 要识别内容的图片本地路径或者URL图片链接, 支持https链接, 不可与范围参数同时存在
@arg:返回json, table, 可选, 一般不需要填写, 以表的形式返回原生结果, 详情请自行遍历查看
@exp:zm.OcrYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n Dim ret = zm.OcrYouTuBankCard("/sdcard/银行卡.png")
--]=]
function zmm.OcrYouTuBankCard(x1, y1, x2, y2, tRet)
	local imgPath
	return try {
		function()
			if type(x1) == "number" then
				imgPath = zmm.FileTemp() .. ".png"
				x1, y1 = _zmm.setScaleXY(x1, y1)
				x2, y2 = _zmm.setScaleXY(x2, y2)
				LuaAuxLib.SnapShot(imgPath, x1, y1, x2, y2)
				tRet = tRet or {}
			else
				imgPath = x1
				tRet = y1 or {}
			end

			local postURL = "https://api.ai.qq.com/fcgi-bin/ocr/ocr_creditcardocr"

			return _zmm.ocryoutu(postURL, imgPath, tRet)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.OcrYouTuBankCard()，错误信息：", errors)
			end
		},
		finally {
			function()
				if type(x1) == "number" then
					os.remove(imgPath)
				end
			end
		}
	}
end

--[=[
@fname:SpeechSsYouTuInit
@cname:初始化优图语音合成
@format:[高级属性][AppID, AppKey]
@tname:初始化优图语音合成
@note:初始化优图语音合成的账户数据, 一般只要调用一次, 账户注册详见命令帮助
@ret:参数列表, table, 成功返回设置后的参数列表, 失败返回null
@arg:高级属性, table, 可选, 一般不需要填写, 以table类型详细设置每个参数, 若填写本参数, 则将忽略其他参数, 详见 https://ai.qq.com/doc/aaitts.shtml 中的请求参数
@arg:app_id, string, 可选, 语音合成应用的AppID, 详见命令帮助
@arg:app_key, string, 可选, 语音合成应用的AppKey, 详见命令帮助
@exp:zm.SpeechSsYouTuInit("请修改成你的AppID", "请修改成你的AppKey")$n zm.SpeechSsYouTu("紫猫老师真帅!", True)$n Delay(5000) //防止脚本停止导致播放停止
--]=]
_zmm.SpeechSsYouTuAttr = {}
_zmm.SpeechSsYouTuAttr.default = { app_id = "", app_key = "", interface = "aai_tts", speaker = 1, format = 2, volume = 0,
	speed_tts = 100, speed_tta = 0, aht = 0, apc = 58, model_type = 0, isplay = false, isdel = false }
_zmm.SpeechSsYouTuAttr.mt = { __index = _zmm.SpeechSsYouTuAttr.default }
function zmm.SpeechSsYouTuInit(AppID, AppKey)
	return try {
		function()
			if type(AppID) == "table" then
				for k, v in pairs(AppID) do
					_zmm.SpeechSsYouTuAttr.default[k] = v
				end
			else
				_zmm.SpeechSsYouTuAttr.default.app_id = AppID
				_zmm.SpeechSsYouTuAttr.default.app_key = AppKey
			end
			return _zmm.SpeechSsYouTuAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SpeechSsYouTuInit()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SpeechSsYouTu
@cname:优图语音合成播放
@format:文本内容[, 是否播放[, 是否删除]]
@tname:合成 $1 语音
@note:使用优图提供的语音合成功能实现文字转声音并播放, 注意脚本停止会导致播放停止.$n 请先调用zm.SpeechSsYouTuInit()初始化后才使用本命令, 设置发音人等内容都在初始化中调整
@ret:文件路径, string, 成功返回合成的音频文件路径, 失败返回null
@arg:文本内容, string, 准备合成语音的文本内容
@arg:是否播放, boolean, 可选, 是否自动播放音频, 注意脚本停止会导致播放停止
@arg:是否删除, boolean, 可选, 是否删除合成的文件, 注意前面必须要有是否播放参数
@exp:zm.SpeechSsYouTuInit("请修改成你的AppID", "请修改成你的AppKey") //初始化账户数据$n zm.SpeechSsYouTuInit({"speaker":6}) //更换发音人$n zm.SpeechSsYouTu("紫猫老师真帅!", True)$n Delay(5000) //防止脚本停止导致播放停止
--]=]
function zmm.SpeechSsYouTu(text, isplay, isdel)
	return try {
		function()
			local parms = {}
			local path
			local file_att = { "pcm", "wav", "mp3" }
			if isplay == nil then isplay = _zmm.SpeechSsYouTuAttr.default.isplay end
			if isdel == nil then isdel = _zmm.SpeechSsYouTuAttr.default.isdel end
			local interface = _zmm.SpeechSsYouTuAttr.default.interface
			parms.app_id = _zmm.SpeechSsYouTuAttr.default.app_id
			parms.time_stamp = os.time()
			parms.nonce_str = zmm.RndStr(zmm.RndNum(10, 32))
			parms.text = zmm.EncodeURL(text)

			local postURL = "https://api.ai.qq.com/fcgi-bin/aai/" .. interface

			if interface == "aai_tts" then
				parms.speaker = _zmm.SpeechSsYouTuAttr.default.speaker
				parms.format = _zmm.SpeechSsYouTuAttr.default.format
				parms.volume = _zmm.SpeechSsYouTuAttr.default.volume
				parms.speed = _zmm.SpeechSsYouTuAttr.default.speed_tts
				parms.aht = _zmm.SpeechSsYouTuAttr.default.aht
				parms.apc = _zmm.SpeechSsYouTuAttr.default.apc
			elseif interface == "aai_tta" then
				parms.model_type = _zmm.SpeechSsYouTuAttr.default.model_type
				parms.speed = _zmm.SpeechSsYouTuAttr.default.speed_tta
			end

			parms.sign = _zmm.getYouTuSign(parms, _zmm.SpeechSsYouTuAttr.default.app_key)
			local parmstr = _zmm.getPostParms(parms)

			local json = LuaAuxLib.URL_OperationPost(postURL, parmstr, 30)

			if json and json ~= "" then
				local cjson = require("cjson")
				local t = cjson.decode(json)

				if t.msg == "ok" then
					if interface == "aai_tts" then
						path = zmm.FileTemp() .. "." .. file_att[parms.format]
						zmm.FileWriteBinaryBase64(path, t.data.speech)
					elseif interface == "aai_tta" then
						path = zmm.FileTemp() .. ".wav"
						zmm.FileWriteBinaryBase64(path, t.data.voice)
					end

					if isplay then
						LuaAuxLib.PlaySound(path)
						LuaAuxLib.Sleep(100)
					end

					if isdel then
						os.remove(path)
					end
				else
					error("错误编号: " .. t.ret .. ", 错误信息: " .. t.msg)
				end
			else
				error("访问网络失败")
			end

			return path
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SpeechSsYouTu()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 颜色函数 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:GetPicSize
@cname:获取图片宽高
@format:图片路径
@tname:获取图片 $1 的宽度和高度
@note:获取一张图片的宽度和高度, 支持PNG, BMP, GIF这3种图片格式, 支持附件路径
@ret:图片宽高, table, 成功返回格式{宽度, 高度, "w":宽度, "h":高度, "type":图片类型}
@arg:图片路径, string, 图片的所在路径, 支持附件
@exp:Dim 图片宽高 = zm.GetPicSize("Attachment:紫猫.png")$n TracePrint "第1种写法: 宽度=", 图片宽高(0), ", 高度=", 图片宽高(1)$n TracePrint "第2种写法: 宽度=", 图片宽高["w"], ", 高度=", 图片宽高["h"]
--]=]
function zmm.GetPicSize(path)
	local f
	return try {
		function()
			local tmp = path:match("^[aA][tT][tT][aA][cC][hH][mM][eE][nN][tT]:(.*)")
			if tmp then
				LuaAuxLib.ExtractAttachment(_zmm._GLOBAL_PATH_, tmp)
				path = _zmm._GLOBAL_PATH_ .. tmp
			end
			local hex = {}
			zmm.FileReadBinary(path, 25, hex)
			if #hex > 0 then
				local pictype, w, h

				local pictypetable = { png = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, jpg = { 0xFF, 0xD8 },
					bmp = { 0x42, 0x4D }, gif = { 0x47, 0x49, 0x46, 0x38, { 0x37, 0x39 }, 0x61 } }
				for k, v in pairs(pictypetable) do
					local istype = true
					for i = 1, #v do
						if type(v[i]) == "number" and hex[i] ~= v[i] then
							istype = false
							break
						elseif type(v[i]) == "type" then
							local istypeex = false
							for vv in pairs(v[i]) do
								if v[i] == vv then
									istypeex = true
									break
								end
							end
							istype = istypeex
							if istype == false then
								break
							end
						end
					end
					if istype then
						pictype = k
						break
					end
				end

				if not (pictype) then
					error("未知图片格式")
				end

				if pictype == "png" then
					w = tonumber(string.format("%02X%02X%02X%02X", hex[17], hex[18], hex[19], hex[20]), 16)
					h = tonumber(string.format("%02X%02X%02X%02X", hex[21], hex[22], hex[23], hex[24]), 16)
				elseif pictype == "bmp" then
					w = tonumber(string.format("%02X%02X%02X%02X", hex[18], hex[17], hex[20], hex[19]), 16)
					h = tonumber(string.format("%02X%02X%02X%02X", hex[22], hex[21], hex[24], hex[23]), 16)
				elseif pictype == "gif" then
					w = tonumber(string.format("%02X%02X", hex[8], hex[7]), 16)
					h = tonumber(string.format("%02X%02X", hex[10], hex[9]), 16)
				elseif pictype == "jpg" then
					error("暂不支持jpg格式图片")
				end

				return { w, h, w = w, h = h, type = pictype }
			else
				error("图片文件不存在!")
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetPicSize()，错误信息：", errors)
			end
		},
		finally {
			function()
				if io.type(f) == "file" then f:close() end
			end
		}

	}
end

--[=[
@fname:BGRToRGB
@cname:BGR与RGB互转
@format:BGR颜色或RGB颜色
@tname:将BGR颜色 $1 转为RGB
@note:RGB与BGR颜色相互转换
@ret:颜色, string, 返回转换后的RGB格式颜色
@arg:BGR颜色或RGB颜色, number或string, BGR格式的颜色, 数值型时表示十进制, 字符串时表示十六进制。
@exp:TracePrint zm.BGRToRGB("FF00AA")
--]=]
function zmm.BGRToRGB(bgr)
	return try {
		function()
			if type(bgr) == "string" then
				bgr = string.format("%06s", bgr)
			else
				bgr = string.format("%06x", bgr)
			end
			local rgb = string.sub(bgr, 5, 6) .. string.sub(bgr, 3, 4) .. string.sub(bgr, 1, 2)
			return rgb
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.BGRToRGB()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RGBToBGR
@cname:RGB与BGR互转
@format:RGB颜色或BGR颜色
@tname:将BGR颜色 $1 转为RGB
@note:RGB与BGR颜色相互转换
@ret:颜色, string, 返回转换后的BGR格式颜色
@arg:RGB颜色或BGR颜色, number或string, RGB格式的颜色, 数值型时表示十进制, 字符串时表示十六进制。
@exp:TracePrint zm.BGRToRGB("FF00AA")
--]=]
zmm.RGBToBGR = zmm.BGRToRGB

--[=[
@fname:RGBToColor
@cname:合成RGB颜色
@format:R颜色分量, G颜色分量, B颜色分量
@tname:将 $1, $2, $3 合成RGB颜色
@note:合成三个RGB颜色分量到十六进制RGB字符串中
@ret:RGB颜色, string, 返回合成后的RGB格式颜色
@arg:R颜色分量, number或string, 红色分量, 数值型时表示十进制, 字符串时表示十六进制
@arg:G颜色分量, number或string, 绿色分量, 数值型时表示十进制, 字符串时表示十六进制
@arg:B颜色分量, number或string, 蓝色分量, 数值型时表示十进制, 字符串时表示十六进制
@exp:TracePrint zm.RGBToColor(10,20,30)
--]=]
function zmm.RGBToColor(r, g, b)
	return try {
		function()
			if type(r) == "string" then
				r = string.format("%02s", r)
			else
				r = string.format("%02x", math.floor(r))
			end
			if type(g) == "string" then
				g = string.format("%02s", g)
			else
				g = string.format("%02x", math.floor(g))
			end
			if type(b) == "string" then
				b = string.format("%02s", b)
			else
				b = string.format("%02x", math.floor(b))
			end
			return r .. g .. b
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RGBToColor()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:BGRToColor
@cname:合成BGR颜色
@format:B颜色分量, G颜色分量, R颜色分量
@tname:将 $1, $2, $3 合成BGR颜色
@note:合成三个BGR颜色分量到十六进制BGR字符串中
@ret:BGR颜色, string, 返回合成后的BGR格式颜色
@arg:B颜色分量, number或string, 蓝色分量, 数值型时表示十进制, 字符串时表示十六进制
@arg:R颜色分量, number或string, 红色分量, 数值型时表示十进制, 字符串时表示十六进制
@arg:G颜色分量, number或string, 绿色分量, 数值型时表示十进制, 字符串时表示十六进制
@exp:TracePrint zm.BGRToColor(10,20,30)
--]=]
zmm.BGRToColor = zmm.RGBToColor

--[=[
@fname:ColorToRGB
@cname:分解RGB颜色
@format:RGB颜色或BGR颜色[, 返回颜色表]
@tname:分解 $1 到RGB三分量中
@note:分解RGB颜色至R、G、B三个分量中
@ret:颜色表, table, 返回分解后的结果, 格式为{r, g, b, "r":r, "g":g, "b":b}
@arg:RGB颜色或BGR颜色, number或string, RGB颜色, 字符串时为十六进制, 数值时为十进制
@arg:返回颜色表, table, 可选, 返回分解保存的表, 格式与返回值相同
@exp:TracePrint zmVarInfo(zm.ColorToRGB("11BBDD"))
--]=]
function zmm.ColorToRGB(rgb, t)
	return try {
		function()
			t = t or {}
			if type(rgb) == "string" then
				rgb = string.format("%06s", rgb)
			else
				rgb = string.format("%06x", rgb)
			end

			t.r = tonumber("0x" .. string.sub(rgb, 1, 2))
			t.g = tonumber("0x" .. string.sub(rgb, 3, 4))
			t.b = tonumber("0x" .. string.sub(rgb, 5, 6))

			t[1] = tonumber("0x" .. string.sub(rgb, 1, 2))
			t[2] = tonumber("0x" .. string.sub(rgb, 3, 4))
			t[3] = tonumber("0x" .. string.sub(rgb, 5, 6))
			return t, t.r, t.g, t.b
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ColorToRGB()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ColorToHSV
@cname:分解HSV颜色
@format:hsv颜色[, 返回颜色表]
@tname:分解 $1 到hsv三分量中
@note:分解HSV颜色至H、S、V三个分量中
@ret:颜色表, table, 返回分解后的结果, 格式为{h, s, v, "h":h, "s":s, "v":v}
@arg:hsv颜色, string, HSV颜色, 格式为“H.S.V”
@arg:返回颜色表, table, 可选, 返回分解保存的表, 格式与返回值相同
@exp:TracePrint zmVarInfo(zm.ColorToHSV("100.20.30"))
--]=]
function zmm.ColorToHSV(hsv, t)
	return try {
		function()
			t = t or {}
			local i = 1
			for w in string.gmatch(hsv, "%d+") do
				t[i] = tonumber(w)
				i = i + 1
			end
			t.h = t[1]
			t.s = t[2]
			t.v = t[3]
			return t, t[1], t[2], t[3]
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ColorToHSV()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RGBToHSV
@cname:RGB转HSV
@format:rgb颜色[, 是否bgr颜色]
@tname:转换RGB颜色 $1 到HSV颜色
@note:将RGB颜色转换成HSV颜色
@ret:hsv颜色, string, 返回“H.S.V”格式的颜色
@arg:rgb颜色, number或string, RGB颜色, 字符串时为十六进制, 数值时为十进制
@arg:是否bgr颜色, boolean, 可选, true表示参数1格式为BGR颜色, false表示参数1为RGB颜色, 省略默认为false
@exp:TracePrint zm.RGBToHSV("102030")
--]=]
function zmm.RGBToHSV(rgb, mode)
	return try {
		function()
			local h, s, v = 0, 0, 0
			local _, r, g, b, max, min
			if mode then
				rgb = zmm.BGRToRGB(rgb)
			end
			_, r, g, b = zmm.ColorToRGB(rgb)
			max = math.max(r, g, b)
			min = math.min(r, g, b)
			if max == min then
				h = 0
			elseif max == r and g >= b then
				h = 60 * (g - b) / (max - min) + 0
			elseif max == r and g < b then
				h = 60 * (g - b) / (max - min) + 360
			elseif max == g then
				h = 60 * (b - r) / (max - min) + 120
			elseif max == b then
				h = 60 * (r - g) / (max - min) + 240
			end
			h = math.floor(h)
			if max == 0 then
				s = 0
			else
				s = math.floor((1 - min / max) * 100 + 0.5)
			end
			v = math.floor((max / 255) * 100 + 0.5)
			return h .. "." .. s .. "." .. v, { h, s, v, h = h, s = s, v = v }
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RGBToHSV()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:HSVToRGB
@cname:HSV转RGB
@format:hsv颜色[, 是否返回bgr颜色]
@tname:转换HSV颜色 $1 到RGB颜色
@note:将HSV颜色转换成RGB颜色
@ret:rgb颜色, string, 返回十六进制的颜色, 格式由参数2决定
@arg:hsv颜色, string, HSV颜色, 格式为“H.S.V”
@arg:是否返回bgr颜色, boolean, 可选, true表示返回BGR颜色, false表示返回RGB颜色, 省略默认为false
@exp:TracePrint zm.HSVToRGB("100.20.30")
--]=]
function zmm.HSVToRGB(hsv, mode)
	return try {
		function()
			local rgb = ""
			local _, h, s, v, hi, f, p, q, t, r, g, b
			_, h, s, v = zmm.ColorToHSV(hsv)
			h, s, v = h, s / 100, v / 100
			hi = math.floor(h / 60) % 6
			f = h / 60 - hi
			p = v * (1 - s)
			q = v * (1 - f * s)
			t = v * (1 - (1 - f) * s)
			if hi == 0 then
				r, g, b = v, t, p
			elseif hi == 1 then
				r, g, b = q, v, p
			elseif hi == 2 then
				r, g, b = p, v, t
			elseif hi == 3 then
				r, g, b = p, q, v
			elseif hi == 4 then
				r, g, b = t, p, v
			elseif hi == 5 then
				r, g, b = v, p, q
			end
			rgb = zmm.RGBToColor(r * 255, g * 255, b * 255)
			if mode then
				rgb = zmm.BGRToRGB(rgb)
			end
			return rgb
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.HSVToRGB()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:GetPixelHSV
@cname:得到指定点HSV颜色
@format:横坐标x, 纵坐标y[, hsv数组]
@tname:获取$1, $2坐标点的HSV颜色
@note:得到指定点HSV颜色
@ret:hsv颜色, string, 返回指定点的HSV颜色, 格式为"H.S.V"
@arg:横坐标x, number, 要获取点的X坐标
@arg:横坐标y, number, 要获取点的Y坐标
@arg:hsv数组, table, 可选, 返回table格式的hsv三分量结果, 格式为{h, s, v, "h":h, "s":s, "v":v}
@exp:TracePrint zm.GetPixelHSV(100, 200)
--]=]
function zmm.GetPixelHSV(x, y, tHSV)
	return try {
		function()
			tHSV = tHSV or {}
			local bgr = LuaAuxLib.GetPixelColor(x, y)
			local hsv, _hsv = zmm.RGBToHSV(bgr, true)
			for k, v in pairs(_hsv) do
				tHSV[k] = v
			end
			return hsv
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetPixelHSV()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ColorSim
@cname:计算颜色相似度
@format:颜色1, 颜色2
@tname:获取 $1 和 $2 的颜色相似度
@note:获取两个颜色之间的相似度, 注意两个参数颜色格式必须一致, 可能与按键自带的相似度算法不同
@ret:相似度, number, 返回两个颜色的相似度
@arg:颜色1, string或number或table, BGR或RGB颜色, 字符串时表示十六进制, 数值时表示十进制, table时格式为{r, g, b}
@arg:颜色2, string或number或table, BGR或RGB颜色, 字符串时表示十六进制, 数值时表示十进制, table时格式为{r, g, b}
@exp:TracePrint zm.ColorSim("FFFFFF", "000000")$n TracePrint zm.ColorSim("FFFFFF", "FFFFFF")$n TracePrint zm.ColorSim("FFFFFF", "EEEEEE")
--]=]
function zmm.ColorSim(color1, color2)
	return try {
		function()
			if type(color1) ~= "table" then
				color1 = zmm.ColorToRGB(color1)
			end
			if type(color2) ~= "table" then
				color2 = zmm.ColorToRGB(color2)
			end
			local r, g, b = (color1[1] - color2[1]), (color1[2] - color2[2]), (color1[3] - color2[3])

			local sim = 1 - math.sqrt((r ^ 2 + g ^ 2 + b ^ 2) / (255 * 255 + 255 * 255 + 256 * 255))

			return sim
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ColorSim()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:IsDisplayDead
@cname:是否卡屏
@format:x1, y1, x2, y2, 时长[, 相似度][, 判断间隔][, 判断模式]
@tname:判断范围 $1 $2 $3 $4 是否卡屏
@note:获取指定范围时间内是否出现卡屏, 画面不变化的情况, 如果画面变化则立即返回
@ret:是否卡屏, boolean, 卡屏返回true, 不卡屏返回false
@arg:x1, number, 卡屏判断范围左上角x坐标
@arg:y1, number, 卡屏判断范围左上角y坐标
@arg:x2, number, 卡屏判断范围右下角x坐标
@arg:y2, number, 卡屏判断范围右下角y坐标
@arg:时长, number, 限时判断时长, 单位毫秒
@arg:相似度, number, 可选, 画面变化相似度, 省略默认为0.8
@arg:判断间隔, number, 可选, 每次对比判断的间隔毫秒, 必须大于1, 省略默认为10
@arg:判断模式, number, 可选, -1为快速模式, -2为精准模式, 省略默认为-1
@exp:TracePrint zm.IsDisplayDead(100,100,200,200,5000)//在5秒内判断屏幕100,100,200,200范围是否卡屏
--]=]
function zmm.IsDisplayDead(x1, y1, x2, y2, t, ...)
	local path
	local args = { ... }
	return try {
		function()
			local isdead = true
			if x1 == 0 and y1 == 0 and x2 == 0 and y2 == 0 then
				x2, y2 = LuaAuxLib.GetScreenInfo(0), LuaAuxLib.GetScreenInfo(1)
			end
			local sim, dt, mode
			for _, v in ipairs(args) do
				if v >= 0 and v <= 1 then
					sim = v
				elseif v > 1 then
					dt = v
				else
					mode = v
				end
			end
			sim = sim or 0.8
			dt = dt or 10
			mode = mode or -1

			x1, y1 = _zmm.setScaleXY(x1, y1)
			x2, y2 = _zmm.setScaleXY(x2, y2)
			path = zmm.FileTemp() .. ".png"

			if mode == -2 then
				LuaAuxLib.SnapShot(path, x1, y1, x2, y2)
				local t1 = LuaAuxLib.GetTickCount()
				while LuaAuxLib.GetTickCount() - t1 < t do
					if LuaAuxLib.FindPicture(x1 - 10, y1 - 10, x2 + 10, y2 + 10, path, "101010", 0, sim) == -1 then
						return false
					end
					LuaAuxLib.Sleep(dt)
				end
			elseif mode == -1 then
				local data = LuaAuxLib.GetScreenData(x1, y1, x2, y2)
				local pixel = (x2 - x1 + 1) * (y2 - y1 + 1)
				local success = math.modf(pixel * sim)
				local fail = pixel - success
				local t1 = LuaAuxLib.GetTickCount()
				while LuaAuxLib.GetTickCount() - t1 < t do
					local fcount, scount = 0, 0
					local data_new = LuaAuxLib.GetScreenData(x1, y1, x2, y2)
					for i = 1, #data do
						for j = 1, #data[i] do
							if zmm.ColorSim(data[i][j], data_new[i][j]) < sim then
								fcount = fcount + 1
								if fcount > fail then
									return false
								end
							else
								scount = scount + 1
								if scount > success then
									goto jumpfor
								end
							end
						end
					end
					::jumpfor::
					LuaAuxLib.Sleep(dt)
				end
			end

			return isdead
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.IsDisplayChange()，错误信息：", errors)
			end
		},
		finally {
			function()
				local f = io.open(path)
				if f then
					f:close()
					os.remove(path)
				end
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 设备函数 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:SetScreenScale
@cname:超级缩放
@format:开发宽度, 开发高度[, 输出缩放]
@tname:设置超级缩放的开发宽度 $1 , 开发高度 $2
@note:与按键自带的SetScreenScale()相比, 多了对zm.FindPic()和zm.FindStr()的支持, 另外还可以使用zm.GetScreenScale()获取缩放状态$n 缩放找图和缩放找字仅针对传入与传出坐标进行缩放, 对应的图片与字库请自行设置多套数据供命令调用
@ret:无
@arg:开发宽度, number, 开发脚本时使用的设备横向分辨率
@arg:开发高度, number, 开发脚本时使用的设备纵向分辨率
@arg:输出缩放, number, 可选参数, 默认为1$n 0表示只对传入函数的坐标（如范围坐标等）进行缩放, 从函数传出的坐标（如返回坐标等）不缩放, 即为当前设备的真实坐标$n 1表示对传入的坐标（如范围坐标等）进行缩放, 对函数传出的坐标（如返回坐标）进行反向缩放
@exp:zm.SetScreenScale 720, 1280
--]=]
_zmm.gDefaultScreenScale = { state = 1, x = 0, y = 0, mode = 0 }
function zmm.SetScreenScale(x, y, mode)
	return try {
		function()
			mode = mode or 1
			LuaAuxLib.SetScreenScale(0, x, y, mode)
			_zmm.gDefaultScreenScale.state = 0
			_zmm.gDefaultScreenScale.x = x
			_zmm.gDefaultScreenScale.y = y
			_zmm.gDefaultScreenScale.mode = mode
			return mode
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetScreenScale()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ResetScreenScale
@cname:取消超级缩放
@format:无
@tname:取消超级缩放
@note:取消超级缩放功能
@ret:无
@arg:无
@exp:zm.ResetScreenScale
--]=]
function zmm.ResetScreenScale()
	return try {
		function()
			LuaAuxLib.SetScreenScale(1, 0, 0, 0)
			_zmm.gDefaultScreenScale.state = 1
			_zmm.gDefaultScreenScale.x = 0
			_zmm.gDefaultScreenScale.y = 0
			_zmm.gDefaultScreenScale.mode = 0
			return 0
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ResetScreenScale()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:GetScreenScale
@cname:获取超级缩放状态
@format:无
@tname:获取超级缩放状态
@note:获取超级缩放状态, 只有通过zm.SetScreenScale()和zm.ResetScreenScale()设置的超级缩放有效, 对官方自带的缩放无效
@ret:缩放状态, number, 返回数字, 具体如下:$n -1表示没有缩放$n 0表示传入缩放, 传出不缩放$n 1表示传入缩放, 传出反向缩放
@arg:无
@exp:TracePrint zm.GetScreenScale()
--]=]
function zmm.GetScreenScale()
	return try {
		function()
			if _zmm.gDefaultScreenScale.state == 1 then
				return -1
			else
				return _zmm.gDefaultScreenScale.mode
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetScreenScale()，错误信息：", errors)
			end
		}
	}
end

function _zmm.setScaleXY(x, y, mode)
	return try {
		function()
			if _zmm.gDefaultScreenScale.state == 0 then
				if mode then
					if _zmm.gDefaultScreenScale.mode == 1 then
						x = x * _zmm.gDefaultScreenScale.x / LuaAuxLib.GetScreenInfo(0)
						y = y * _zmm.gDefaultScreenScale.y / LuaAuxLib.GetScreenInfo(1)
					end
				else
					x = x * LuaAuxLib.GetScreenInfo(0) / _zmm.gDefaultScreenScale.x
					y = y * LuaAuxLib.GetScreenInfo(1) / _zmm.gDefaultScreenScale.y
				end
			end
			return x, y
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：setScaleXY()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 触摸函数 **********************************]]
--[[**************************************************************************]]
--[=[
@fname:CatchTouchPoint
@cname:获取点击坐标
@format:[点击次数]
@tname:获取 $1 次点击坐标
@note:获取屏幕上手指点击的坐标(命令为阻塞状态), 解决自带命令获取结果不准确的问题
@ret:坐标二维数组, table, 格式为[点击数][坐标]
@arg:点击次数, number, 可选参数, 表示点击次数, 省略默认为1
@exp:Dim 返回坐标 = zm.CatchTouchPoint(2)  //获取用户点击坐标(命令为阻塞状态)$n TracePrint 返回坐标[1][1]   //得到第1个点击坐标$n TracePrint 返回坐标[1][2]$n TracePrint 返回坐标[2][1]   //得到第2个点击坐标$n TracePrint 返回坐标[2][2]
--]=]
function zmm.CatchTouchPoint(counts)
	return try {
		function()
			counts = counts or 1
			local points = LuaAuxLib.CatchTouchPoint(counts)
			local val = zmm.Execute("getevent -p | grep -e '0035' -e '0036'")
			if val == "" then
				return points
			end

			local minX, maxX, minY, maxY = val:gmatch("0035.*min (%d+).*max (%d+).*0036.*min (%d+).*max (%d+)")()
			if maxY == nil then
				return points
			end

			local screenX = LuaAuxLib.GetScreenInfo(0)
			local screenY = LuaAuxLib.GetScreenInfo(1)

			for i = 1, #points do
				points[i][1] = math.floor((points[i][1] - minX) * screenX / (maxX - minX + 1))
				points[i][2] = math.floor((points[i][2] - minY) * screenY / (maxY - minY + 1))
			end
			return points
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.CatchTouchPoint()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Swipe
@cname:超级划动
@format:x1, y1, x2, y2[, 划动耗时[, 轨迹次数]][, 按住时间][, 模式][, 禁用轨迹]
@tname:从 $1, $2 划动到 $3, $4 耗时 $5
@note:快速精准无惯性的模拟划动操作, 可切换模式支持网页划动
@ret:无
@arg:x1, number, 划动的起点坐标
@arg:y1, number, 划动的起点坐标
@arg:x2, number, 划动的终点坐标
@arg:y2, number, 划动的终点坐标
@arg:划动耗时, number, 可选, 划动花费的时间, 单位毫秒, 省略默认为100毫秒
@arg:轨迹次数, number, 可选, 划动轨迹多少次, 省略默认为10, 若填写轨迹次数参数, 则前面必须写耗时参数
@arg:按住时间, number, 可选, 按住不放一段时间后再移动, 格式为小于-50的负数, 例如-500表示按住500毫秒, 省略默认为-50
@arg:模式, number, 可选, -1为普通模拟，-2为硬件模拟，-3为翻页模拟, -4为空格翻页, -5为导航移动, 当普通模式无法划动时, 可以试下切换至其他模拟, 省略默认为-1
@arg:禁用轨迹, boolean, 可选, 是否模拟真实带轨迹移动, 对翻页模式无效, 省略默认为false表示禁用
@exp:zm.Swipe 100, 500, 100, 200, 500, -2
--]=]
function zmm.Swipe(x1, y1, x2, y2, ...)
	local args = { ... }
	return try {
		function()
			local times, mode, isReal, count, downtime = 100, -1, false, 10, 50
			local istime = false
			for _, v in pairs(args) do
				if type(v) == "boolean" then
					isReal = v
				elseif type(v) == "number" then
					if v > 0 then
						if not istime then
							times = v
							istime = true
						else
							count = v
						end
					elseif v == 0 then
						error("耗时或轨迹次数不能为0")
						return false
					elseif v > -50 then
						mode = v
					else
						downtime = math.abs(v)
					end
				end
			end

			local mv = function(swipe)
				swipe(0, x1, y1, 2262, 0)
				LuaAuxLib.Sleep(downtime)
				if isReal then
					if x1 == x2 then
						if y1 ~= y2 then
							for y = y1, y2, (y2 - y1) / count do
								local x = x1 + zmm.RndNum(-10, 10)
								local yy = y + zmm.RndNum(-10, 10)
								swipe(1, x, yy, 2262, times / count)
							end
						end
					else
						local k = (y1 - y2) / (x1 - x2)
						local b = (y2 * x1 - y1 * x2) / (x1 - x2)
						if math.abs(x1 - x2) >= math.abs(y1 - y2) then
							for x = x1, x2, (x2 - x1) / count do
								local y = k * x + b + zmm.RndNum(-10, 10)
								local xx = x + zmm.RndNum(-10, 10)
								swipe(1, xx, y, 2262, times / count)
							end
						else
							for y = y1, y2, (y2 - y1) / count do
								local x = (y - b) / k + zmm.RndNum(-10, 10)
								local yy = y + zmm.RndNum(-10, 10)
								swipe(1, x, yy, 2262, times / count)
							end
						end
					end
					swipe(1, x2, y2, 2262, 120)
					LuaAuxLib.Sleep(100)
				else
					swipe(1, x2, y2, 2262, times)
					LuaAuxLib.Sleep(100)
				end
				swipe(1, x2, y2, 2262, 100)
				LuaAuxLib.Sleep(100)
				swipe(2, 0, 0, 2262, 0)
			end

			if mode == -1 then
				mv(LuaAuxLib.TouchOperation)
			elseif mode == -2 then
				mv(LuaAuxLib.WritePointerInput)
			elseif mode == -3 then
				if y1 > y2 then
					LuaAuxLib.KeyPress("PageDown", 0)
				else
					LuaAuxLib.KeyPress("PageUp", 0)
				end
			elseif mode == -4 then
				if y1 > y2 then
					LuaAuxLib.KeyPress(62, 0) -- 空格
				else
					LuaAuxLib.KeyPress(59, 1) -- 按下左shtif
					LuaAuxLib.KeyPress(62, 0) -- 按下空格
					LuaAuxLib.KeyPress(59, 2) -- 按下左shtif
				end
			elseif mode == -5 then
				if y1 > y2 then
					LuaAuxLib.KeyPress(20, 0) -- 导航键 向下
				else
					LuaAuxLib.KeyPress(19, 0) -- 导航键 向上
				end
			else
				return false
			end
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Swipe()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SwipeEx
@cname:多次划动
@format:[xy, ...][, 偏移量][, 划动耗时[, 轨迹次数]][, 按住时间][, 模式][, 禁用轨迹]
@tname:从 $1 开始依次划动多个点
@note:与zm.Swipe()类似, 区别在于本命令支持指定多个坐标划动
@ret:无
@arg:xy, table 或 string, 要划动经过的坐标点, 支持多个坐标参数, 格式为{x, y} 或 "x,y|x,y|...", 详见例子
@ary:偏移量, string, 可选, 坐标的偏移量, 格式为"±x,±y", 默认为"+0,+0", 例如"+0,-20", 表示实际点击传入坐标x+0,y-20的位置, 常用于答题上的初始坐标系
@arg:划动耗时, number, 可选, 每个划动花费的时间, 单位毫秒, 省略默认为100毫秒
@arg:轨迹次数, number, 可选, 每个划动轨迹多少次, 省略默认为10, 若填写轨迹次数参数, 则前面必须写耗时参数
@arg:按住时间, number, 可选, 第一点按住不放一段时间后再移动, 格式为小于-50的负数, 例如-500表示按住500毫秒, 省略默认为-50
@arg:模式, number, 可选, -1为普通模拟，-2为硬件模拟, 当普通模式无法划动时, 可以试下切换至硬件模拟, 省略默认为-1
@arg:禁用轨迹, boolean, 可选, 是否模拟真实带轨迹移动, 省略默认为false表示禁用
@exp://以真实轨迹从10,600划动到10,700到200,500到300,800$n zm.SwipeEx {10, 600}, {10, 700}, {200, 500}, {300, 800}, True
@exp://以真实轨迹从10,600划动到10,700到200,500到300,800$n zm.SwipeEx "10,600", "10,700", "200,500", "300,800", True
@exp://以真实轨迹从20,630划动到20,730到220,530到320,830$n zm.SwipeEx "10,600|10,700|200,500|300,800", "+10,+30", True
--]=]
function zmm.SwipeEx(...)
	local args = { ... }
	return try {
		function()
			local times, mode, isReal, count, downtime, xp, yp = 100, -1, false, 10, 50, 0, 0
			local istime = false
			local xy = {}
			for _, v in pairs(args) do
				if type(v) == "boolean" then
					isReal = v
				elseif type(v) == "number" then
					if v > 0 then
						if not istime then
							times = v
							istime = true
						else
							count = v
						end
					elseif v == 0 then
						error("耗时或轨迹次数不能为0")
						return false
					elseif v > -50 then
						mode = v
					else
						downtime = math.abs(v)
					end
				elseif type(v) == "table" then
					xy[#xy + 1] = v
				elseif type(v) == "string" then
					if v:match("^([%+%-]%d+), *([%+%-]%d+)$") then
						xp, yp = v:match("([%+%-]%d+), *([%+%-]%d+)")
						xp = tonumber(xp)
						yp = tonumber(yp)
					else
						for x, y in string.gmatch(v, "(%d+)[^%d]+(%d+)") do
							xy[#xy + 1] = { tonumber(x), tonumber(y) }
						end
					end
				end
			end

			assert(#xy > 1, "坐标数组参数的数量不对")

			if xp ~= 0 or yp ~= 0 then
				for i = 1, #xy do
					xy[i][1] = xy[i][1] + xp
					xy[i][2] = xy[i][2] + yp
				end
			end

			local mv = function(swipe, xy)
				swipe(0, xy[1][1], xy[1][2], 2262, 0)
				LuaAuxLib.Sleep(downtime)
				if isReal then
					for i = 2, #xy do
						local x1, y1, x2, y2 = xy[i - 1][1], xy[i - 1][2], xy[i][1], xy[i][2]
						if x1 == x2 then
							if y1 ~= y2 then
								for y = y1, y2, (y2 - y1) / count do
									local x = x1 + zmm.RndNum(-10, 10)
									local yy = y + zmm.RndNum(-10, 10)
									swipe(1, x, yy, 2262, times / count)
								end
							end
						else
							local k = (y1 - y2) / (x1 - x2)
							local b = (y2 * x1 - y1 * x2) / (x1 - x2)
							if math.abs(x1 - x2) >= math.abs(y1 - y2) then
								for x = x1, x2, (x2 - x1) / count do
									local y = k * x + b + zmm.RndNum(-10, 10)
									local xx = x + zmm.RndNum(-10, 10)
									swipe(1, xx, y, 2262, times / count)
								end
							else
								for y = y1, y2, (y2 - y1) / count do
									local x = (y - b) / k + zmm.RndNum(-10, 10)
									local yy = y + zmm.RndNum(-10, 10)
									swipe(1, x, yy, 2262, times / count)
								end
							end
						end
					end
					swipe(1, xy[#xy][1], xy[#xy][2], 2262, 120)
					LuaAuxLib.Sleep(100)
				else
					for i = 2, #xy do
						swipe(1, xy[i][1], xy[i][2], 2262, times)
						LuaAuxLib.Sleep(100)
					end
				end
				swipe(1, xy[#xy][1], xy[#xy][2], 2262, 100)
				LuaAuxLib.Sleep(100)
				swipe(2, 0, 0, 2262, 0)
			end

			if mode == -1 then
				mv(LuaAuxLib.TouchOperation, xy)
			elseif mode == -2 then
				mv(LuaAuxLib.WritePointerInput, xy)
			else
				return false
			end
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SwipeEx()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SetTap
@cname:设置Tap默认参数
@format:属性表
@tname:设置zm.Tap()默认参数 $1
@note:修改 zm.Tap() 的默认参数值, 并影响超级图色命令中的点击参数效果
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值$n 例如{“t”:2000, “showlog”:”显示”}表示修改点击后默认延时时间为2000毫秒, 日志输出默认为显示$n 详细属性如下:$n minx: 数值型, 表示点击x坐标的浮动下限, 省略默认为0$n maxx: 数值型, 表示点击x坐标的浮动上限, 省略默认为0$n miny: 数值型, 表示点击y坐标的浮动下限, 省略默认为0$n maxy: 数值型, 表示点击y坐标的浮动上限, 省略默认为0$n t: 数值型, 点击后默认延时时间, 单位毫秒, 省略默认为0$n mintouch: 数值型, 点击时按下与弹起之间的最小间隔, 单位毫秒, 省略默认为0$n maxtouch: 数值型, 点击时按下与弹起之间的最大间隔, 单位毫秒, 省略默认为50$n norandom: 布尔型, 是否禁用随机, 填写true后所有点击都失去随机效果, 省略默认为false$n showlog: 字符串, 显示输出日志, 可选[“显示”,”隐藏”,”show”,”hide”]这几个值, 省略默认"隐藏"
@exp://设置固定坐标x浮动范围[-10,15], 固定坐标y浮动范围[-5,20], 点击后默认延时2000毫秒$n zm.SetTap {"minx":-10, "maxx":15, "miny":-5, "maxy":20, "t":2000}
--]=]
_zmm.TapAttr = {}
_zmm.TapAttr.default = { minx = 0, maxx = 0, miny = 0, maxy = 0, t = 0, mintouch = 0, maxtouch = 50, norandom = false,
	showlog = "隐藏" }
_zmm.TapAttr.mt = { __index = _zmm.TapAttr.default }
function zmm.SetTap(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.TapAttr.default[k:lower()] ~= nil then
					_zmm.TapAttr.default[k:lower()] = v
				end
			end
			return _zmm.TapAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:Tap
@cname:超级点击
@format:[对象][坐标字符串][x, y][, rx, ry][, 延时毫秒][, 禁用随机][, 显示日志][, 输出行号][, 备注]
@tname:超级点击坐标 $1 $2
@note:短暂点击屏幕并延时, 可加入随机防检测
@ret:点击坐标, table, 返回实际点击的坐标与延时表, 格式为{x,y,t,”x”:x,”y”:y,”t”:t}, 失败返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:坐标字符串, string, 可选, 与下面的x,y参数二选一填写, 坐标格式为"x,y", 省略默认为null
@arg:x, number, 可选, 要点击的x坐标, 填写对象参数后可省略
@arg:y, number, 可选, 要点击的y坐标, 填写对象参数后可省略
@arg:rx, number, 可选, 从x到rx范围中随机获取一个数作为横坐标x, 省略默认与x相同
@arg:ry, number, 可选, 从y到ry范围中随机获取一个数作为纵坐标y, 省略默认与y相同
@arg:延时毫秒, number, 可选, 点击后延时时间, 单位毫秒, 省略默认为0
@arg:禁用随机, boolean, 可选, 填写true时禁用任何随机功能, 填写false表示不禁用, 省略默认为false
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示","隐藏","show","hide"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@exp://点击固定坐标100,200后延时3000毫秒$n zm.Tap 100, 200, 3000$n //点击字符串坐标100,200后, 延时300毫秒$n zm.Tap "100,200", 300$n //在范围100,200,150,250内随机点击某个点, 并显示输出$n zm.Tap 100, 200, 150, 250, "show"$n //设置固定坐标x浮动范围[-10,15], 固定坐标y浮动范围[-5,20], 点击后默认延时2000毫秒$n zm.SetTap {"minx":-10, "maxx":15, "miny":-5, "maxy":20, "t":2000}$n //x从100-10到100+15中随机取值, y从200-5到200+20中随机取值, 点击后默认延时2000毫秒$n Dim t = zm.Tap(100, 200)$n //查看实际点击坐标$n TracePrint zm.VarInfo(t)
--]=]
function zmm.Tap(...)
	local args = { ... }
	return try {
		function()
			local f = function(t)
				local nt = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "boolean" then
							args.norandom = v
						elseif type(v) == "string" then
							if _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v:lower()
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif zmm.Trim(v):match("^%d+$") then
								nt[#nt + 1] = tonumber(v)
							else
								local x, y = zmm.Trim(v):match("^(%d+),%s*(%d+)$")
								if x and y then
									nt[#nt + 1] = tonumber(x)
									nt[#nt + 1] = tonumber(y)
								end
							end
						elseif type(v) == "number" then
							nt[#nt + 1] = tonumber(v)
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				if #nt == 1 then
					args.t = nt[1]
				elseif #nt >= 2 then
					args.x = nt[1]
					args.y = nt[2]
					if #nt == 3 then
						args.t = nt[3]
					else
						args.xx = nt[3]
						args.yy = nt[4]
						if #nt == 5 then
							args.t = nt[5]
						end
					end
				end
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.TapAttr.mt)

			local touchtime = zmm.RndNum(args.mintouch, args.maxtouch)

			if args.xx and args.yy and args.norandom == false then
				args.x = zmm.RndNum(args.x, args.xx)
				args.y = zmm.RndNum(args.y, args.yy)
			elseif args.x and args.norandom == false then
				args.x = zmm.RndNum(args.x + args.minx, args.x + args.maxx)
				args.y = zmm.RndNum(args.y + args.miny, args.y + args.maxy)
			end

			if _zmm.gShowLog[args.showlog] then
				local log
				if args.x then
					log = "点击坐标=" .. args.x .. "," .. args.y .. " 延时=" .. args.t
				else
					log = "由于坐标无效, 故不点击"
				end
				if args.note then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						LuaAuxLib.ShowMessage(log, 3000, LuaAuxLib.GetScreenInfo(0) / 3, LuaAuxLib.GetScreenInfo(1) - 150)
					end
				end
				traceprint(args.line or "", log)
			end

			if args.x then
				LuaAuxLib.Tap(args.x, args.y, touchtime)
				if args.t > 0 then
					LuaAuxLib.Sleep(args.t)
				end
			end

			return { args.x, args.y, args.t, x = args.x, y = args.y, t = args.t }
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.Tap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SetKeyPress
@cname:设置KeyPress默认参数
@format:属性表
@tname:设置zm.KeyPress()默认参数 $1
@note:修改 zm.KeyPress() 的默认参数值
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值$n 例如{“t”:2000, “showlog”:”显示”}表示修改按键按下与弹起之间的默认时间间隔为2000毫秒, 日志输出默认为显示$n 详细属性如下:$n t: 数值型, 表示按键按下与弹起之间的间隔时间, -1时与自带KeyPress相同, 省略默认为-1$n rndt: 数值型, 与属性t组成随机范围, -1表示不随机, 省略默认为-1$n showlog: 字符串, 显示输出日志, 可选[“显示”,”隐藏”,”show”,”hide”]这几个值, 省略默认"隐藏"
@exp://设置按键按下与弹起之间的间隔时间为随机1000毫秒到2000毫秒$n zm.SetKeyPress {"t":1000, "rndt":2000}
--]=]
_zmm.KeyPressAttr = {}
_zmm.KeyPressAttr.default = { t = -1, rndt = -1, showlog = "隐藏" }
_zmm.KeyPressAttr.mt = { __index = _zmm.KeyPressAttr.default }
function zmm.SetKeyPress(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.KeyPressAttr.default[k:lower()] ~= nil then
					_zmm.KeyPressAttr.default[k:lower()] = v
				end
			end
			return _zmm.KeyPressAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetKeyPress()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPress
@cname:超级按键
@format:[对象][按键[, 间隔毫秒[, 随机毫秒]]][, 显示日志][, 输出行号][, 备注]
@tname:超级按键 $1
@note:按下并弹起一个按键, 可加入随机延时防检测
@ret:按键与间隔, table, 返回实际按键与间隔时间表, 格式为{key,t,”key”:key,”t”:t}, 失败返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:按键, string或number, 可选, 字符串时表示某个键的名字, 数值时表示某个键的编码, 填写对象参数后可省略
@arg:间隔毫秒, number, 可选, 按下与弹起之间的间隔时间, -1表示与自带KeyPress相同, 单位毫秒, 省略默认为-1
@arg:随机毫秒, number, 可选, 与间隔毫秒组成随机数范围, -1表示不随机, 单位毫秒, 省略默认为-1
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示","隐藏","show","hide"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@exp://按住电源键3秒后松开$n zm.KeyPress "power", 3000$n //按住音量+随机3到5秒$n zm.KeyPress "VolUp", 3000, 5000
--]=]
function zmm.KeyPress(...)
	local args = { ... }
	return try {
		function()
			local f = function(t)
				local nt = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v:lower()
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							else
								args.key = v
							end
						elseif type(v) == "number" then
							nt[#nt + 1] = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				if args.key then
					if nt[1] then
						args.t = nt[1]
					end
					if nt[2] then
						args.rndt = nt[2]
					end
				else
					if nt[1] then
						args.key = nt[1]
					end
					if nt[2] then
						args.t = nt[2]
					end
					if nt[3] then
						args.rndt = nt[3]
					end
				end
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			assert(args.key, "没有传入按键名称或虚拟键码")

			setmetatable(args, _zmm.KeyPressAttr.mt)

			if args.t > -1 and args.rndt > -1 then
				args.t = zmm.RndNum(args.t, args.rndt)
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "按键=" .. args.key .. " 延时=" .. args.t
				if args.note then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						LuaAuxLib.ShowMessage(log, 3000, LuaAuxLib.GetScreenInfo(0) / 3, LuaAuxLib.GetScreenInfo(1) - 150)
					end
				end
				traceprint(args.line or "", log)
			end

			if args.t == -1 then
				LuaAuxLib.KeyPress(args.key, 0)
			else
				LuaAuxLib.KeyPress(args.key, 1)
				LuaAuxLib.Sleep(args.t)
				LuaAuxLib.KeyPress(args.key, 2)
			end

			return { args.key, args.t, key = args.key, t = args.t }
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPress()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressStr
@cname:模拟按键输入
@format:按键字符串[, 间隔毫秒]
@tname:模拟按键输入 $1
@note:模拟KeyPress输入字符串内容, 在InputText无效情况下尝试使用, 仅支持小写字母, 数字, 英文标点符号, 不支持大写字母, 中文!
@ret:是否成功, boolean, 成功返回true, 失败返回null
@arg:按键字符串, string, 待输入的字符串内容, 仅支持小写字母, 数字, 英文标点符号, 不支持大写字母, 中文
@arg:间隔毫秒, number, 可选, 两个字符串之间按键间隔时间, 单位毫秒, 省略默认为200
@exp://输入abc,def$n zm.KeyPressStr "abc,def"
--]=]
function zmm.KeyPressStr(keylist, t)
	return try {
		function()
			t = t or 200
			for i = 1, #keylist do
				local key = keylist:sub(i, i)
				LuaAuxLib.KeyPress(key, 0)
				LuaAuxLib.Sleep(t)
			end
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressStr()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressSwitch
@cname:打开运行程序列表
@format:无
@tname:打开运行程序列表
@note:打开当前后台程序列表
@ret:无
@arg:无
@exp:zm.KeyPressSwitch()
--]=]
function zmm.KeyPressSwitch()
	return try {
		function()
			LuaAuxLib.KeyPress(187, 0)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressSwitch()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressScreenShot
@cname:按下截图键
@format:[截图文件夹][, 保存路径]
@tname:按下截图键
@note:按下截图键自动保存, 个别设备需要设置截图文件夹
@ret:原路径, 字符串, 截图文件夹里的图片文件路径
@arg:截图文件夹, 字符串, 可选, 指定设备默认截图文件夹路径, 大部分设备默认路径为"/sdcard/Pictures/Screenshots/", 截图路径不对时, 请手动截图一次查看截图文件夹路径, 本参数必须以"/"结尾
@arg:保存路径, 字符串, 可选, 复制截取的图片到指定文件路径, 省略默认不复制
@exp:zm.KeyPressScreenShot()
--]=]
function zmm.KeyPressScreenShot(...)
	local args = { ... }
	return try {
		function()
			LuaAuxLib.KeyPress(120, 0)
			LuaAuxLib.Sleep(1000)
			local dir, path
			for _, v in ipairs(args) do
				if v:match("/$") then
					dir = v
				else
					path = v
				end
			end

			dir = dir or "/sdcard/Pictures/Screenshots/"

			local files = zmm.Execute("ls -t " .. dir)
			local filepath = dir .. files:match("[^\n]*")
			if path then
				LuaAuxLib.DIR_Copy(filepath, path)
			end
			return filepath
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressScreenShot()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressCut
@cname:剪贴文本
@format:[是否关闭菜单]
@tname:剪贴文本
@note:按下快捷键剪贴文本, 部分设备或APP可能不兼容
@ret:无
@arg:是否关闭菜单, boolean, 可选, 部分app按下组合键后会出现菜单, 省略默认false
@exp:zm.KeyPressCut()
--]=]
function zmm.KeyPressCut(isclosemenu)
	return try {
		function()
			LuaAuxLib.KeyPress(82, 1) -- 按住menu
			LuaAuxLib.KeyPress(52, 0) -- 按下x
			LuaAuxLib.KeyPress(82, 2) -- 松开menu
			if isclosemenu then
				LuaAuxLib.Sleep(300)
				LuaAuxLib.KeyPress(82, 0) -- 按下menu
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressCut()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressCopy
@cname:复制文本
@format:[是否关闭菜单]
@tname:复制文本
@note:按下快捷键复制文本, 部分设备或APP可能不兼容
@ret:无
@arg:是否关闭菜单, boolean, 可选, 部分app按下组合键后会出现菜单, 省略默认false
@exp:zm.KeyPressCopy()
--]=]
function zmm.KeyPressCopy(isclosemenu)
	return try {
		function()
			LuaAuxLib.KeyPress(82, 1) -- 按住menu
			LuaAuxLib.KeyPress(31, 0) -- 按下c
			LuaAuxLib.KeyPress(82, 2) -- 松开menu
			if isclosemenu then
				LuaAuxLib.Sleep(300)
				LuaAuxLib.KeyPress(82, 0) -- 按下menu
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressCopy()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressPaste
@cname:粘贴文本
@format:[是否关闭菜单]
@tname:粘贴文本
@note:按下快捷键粘贴文本, 部分设备或APP可能不兼容
@ret:无
@arg:是否关闭菜单, boolean, 可选, 部分app按下组合键后会出现菜单, 省略默认false
@exp:zm.KeyPressPaste()
--]=]
function zmm.KeyPressPaste(isclosemenu)
	return try {
		function()
			LuaAuxLib.KeyPress(82, 1) -- 按住menu
			LuaAuxLib.KeyPress(50, 0) -- 按下v
			LuaAuxLib.KeyPress(82, 2) -- 松开menu
			if isclosemenu then
				LuaAuxLib.Sleep(300)
				LuaAuxLib.KeyPress(82, 0) -- 按下menu
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressPaste()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressSelectAll
@cname:全选文本
@format:[是否关闭菜单]
@tname:全选文本
@note:按下快捷键全选文本, 部分设备或APP可能不兼容
@ret:无
@arg:是否关闭菜单, boolean, 可选, 部分app按下组合键后会出现菜单, 省略默认false
@exp:zm.KeyPressPaste()
--]=]
function zmm.KeyPressSelectAll(isclosemenu)
	return try {
		function()
			LuaAuxLib.KeyPress(82, 1) -- 按住menu
			LuaAuxLib.KeyPress(29, 0) -- 按下A
			LuaAuxLib.KeyPress(82, 2) -- 松开menu
			if isclosemenu then
				LuaAuxLib.Sleep(300)
				LuaAuxLib.KeyPress(82, 0) -- 按下menu
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressSelectAll()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:KeyPressDel
@cname:按下删除键
@format:[方向]
@tname:按下删除键
@note:删除字符, 部分设备或APP可能不兼容
@ret:无
@arg:方向, 数值型, 可选, 0表示删除光标左边的字符, 1表示删除光标右边的字符, 2表示删除当前一行内容, 省略默认为0
@exp:zm.KeyPressDel()
--]=]
function zmm.KeyPressDel(dir)
	return try {
		function()
			dir = dir or 0
			if dir == 0 then
				LuaAuxLib.KeyPress(67, 0)  -- 按下del
			elseif dir == 1 then
				LuaAuxLib.KeyPress(59, 1)  -- 按下左shtif
				LuaAuxLib.KeyPress(67, 0)  -- 按下del
				LuaAuxLib.KeyPress(59, 2)  -- 按下左shtif
			elseif dir == 2 then
				LuaAuxLib.KeyPress(122, 0) -- 光标移动到开始
				LuaAuxLib.KeyPress(59, 1)  -- 按下左shtif
				LuaAuxLib.KeyPress(123, 0) -- 光标移动到结束
				LuaAuxLib.KeyPress(59, 2)  -- 按下左shtif
				LuaAuxLib.KeyPress(67, 0)  -- 按下del
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.KeyPressDel()，错误信息：", errors)
			end
		}
	}
end

--[[**************************************************************************]]
--[[****************************** 超级图色 **********************************]]
--[[**************************************************************************]]
-- 整理超级图色中整数参数分配
function _zmm.getrangesimtime(numargs, args)
	local fsimtime = function(numargs, args)
		for i = 1, #numargs do
			if numargs[i] >= 0 and numargs[i] <= 1 then
				if numargs.sim == nil then
					args.sim = numargs[i]
					numargs.sim = true
				elseif numargs.autosim == nil then
					args.autosim = args.sim .. "-" .. numargs[i]
					numargs.autosim = true
				else
					args.autosim = args.autosim .. "-" .. numargs[i]
				end
			else
				args.timeout = numargs[i]
			end
		end
	end

	local frange = function(numargs, args)
		args.x1, args.y1, args.x2, args.y2 = numargs[1], numargs[2], numargs[3], numargs[4]
	end

	if #numargs < 4 then
		fsimtime(numargs, args)
	elseif #numargs == 4 then
		local isDbl = false
		for i = 1, #numargs do
			if numargs[i] > 0 and numargs[i] < 1 then
				isDbl = true
				break
			end
		end

		if isDbl then
			fsimtime(numargs, args)
		else
			frange(numargs, args)
		end
	else
		for i = 1, #numargs do
			if numargs[i] > 0 and numargs[i] < 1 then
				if numargs.sim == nil then
					args.sim = numargs[i]
					numargs.sim = true
				elseif numargs.autosim == nil then
					args.autosim = args.sim .. "-" .. numargs[i]
					numargs.autosim = true
				else
					args.autosim = args.autosim .. "-" .. numargs[i]
				end
			else
				if numargs.x1 == nil then
					if numargs[i] <= numargs[i + 2] and numargs[i + 1] <= numargs[i + 3] then
						args.x1 = numargs[i]
						numargs.x1 = true
					else
						if numargs[i] == 0 or numargs[i] == 1 then
							if numargs.sim == nil then
								args.sim = numargs[i]
								numargs.sim = true
							elseif numargs.autosim == nil then
								args.autosim = args.sim .. "-" .. numargs[i]
								numargs.autosim = true
							else
								args.autosim = args.autosim .. "-" .. numargs[i]
							end
						else
							args.timeout = numargs[i]
						end
					end
				elseif numargs.y1 == nil then
					args.y1 = numargs[i]
					numargs.y1 = true
				elseif numargs.x2 == nil then
					args.x2 = numargs[i]
					numargs.x2 = true
				elseif numargs.y2 == nil then
					args.y2 = numargs[i]
					numargs.y2 = true
				else
					if numargs[i] == 0 or numargs[i] == 1 then
						if numargs.sim == nil then
							args.sim = numargs[i]
							numargs.sim = true
						elseif numargs.autosim == nil then
							args.autosim = args.sim .. "-" .. numargs[i]
							numargs.autosim = true
						else
							args.autosim = args.autosim .. "-" .. numargs[i]
						end
					else
						args.timeout = numargs[i]
					end
				end
			end
		end
	end
end

-- 整理超级图色中点击坐标与延时分配
function _zmm.gettapxyt(args)
	if args.tap then
		if args.tapxy ~= "" then
			args.tapx, args.tapy, args.tapt = args.tapxy:match("^([%+%-]?%d+)%, *([%+%-]?%d+),? *(%d*)$")
			args.tapx = args.tapx:match("^[%+%-]") and (args.ret.x + tonumber(args.tapx)) or tonumber(args.tapx)
			args.tapy = args.tapy:match("^[%+%-]") and (args.ret.y + tonumber(args.tapy)) or tonumber(args.tapy)
			args.tapt = args.tapt == "" and _zmm.TapAttr.default.t or tonumber(args.tapt)
		else
			args.tapx, args.tapy, args.tapt = args.ret.x, args.ret.y, _zmm.TapAttr.default.t
		end
	end
end

-- 整理超级图色中智能相似度取值
function _zmm.getsimmaxminstep(args)
	args.simmax, args.simmin, args.simstep = args.autosim:match("^(%d%.?%d*)%-(%d%.?%d*)%-?(%d?%.?%d*)$")
	args.simmax, args.simmin = tonumber(args.simmax), tonumber(args.simmin)
	if args.simmax < args.simmin then args.simmax, args.simmin = args.simmin, args.simmax end
	args.simstep = (args.simstep == "" or tonumber(args.simstep) == 0) and 0.05 or tonumber(args.simstep)
	args.simmax = args.simmax > 1 and 1 or args.simmax
	args.simmin = args.simmin < 0 and 0 or args.simmin
end

--[=[
@fname:SetFindPic
@cname:设置FindPic默认参数
@format:属性表
@tname:设置zm.FindPic()默认参数 $1
@note:设置zm.FindPic()的参数默认值, 参数为键值对表, 改动后影响后面所有zm.FindPic()
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值, $n 例如{“sim”:0.9, “showlog”:”显示”}表示修改相似度默认为0.9, 日志输出默认为显示$n 详细属性如下:$n x1: 数值型, 查找范围左上角x坐标, 省略默认为0$n y1: 数值型, 查找范围左上角y坐标, 省略默认为0$n x2: 数值型, 查找范围右下角x坐标, 省略默认为0$n y2: 数值型, 查找范围右下角y坐标, 省略默认为0$n pic: 字符串, 默认图片路径, 当图片参数只有文件名时, 自动追加此路径, 省略默认为"Attachment:"$n color: 字符串, 图片偏色, 省略默认为"000000"$n dir: 字符串, 查找方向, 可选[“左上”,”中心”,”右上”,”左下”,”右下”]这几个值, 省略默认为"左上"$n sim: 数值型, 查找相似度, 省略默认为0.8$n autosim: 字符串, 智能相似度, 省略默认""$n timeout: 数值型, 查找超时时间, 省略默认为1毫秒$n delaytime: 数值型, 循环查找时间间隔, 省略默认为10毫秒$n tap: 布尔型, 找到后是否点击, 省略默认为false$n showlog: 字符串, 日志输出显示, 可选["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为"隐藏"$n ret: table, 参数返回值, 只能写数组变量名, 设置后, 若命令不带参数返回值, 则默认返回到此变量中, 省略默认为null
@exp:Dim IXYs() //预定义默认参数返回数组$n //设置默认参数返回值IXYs, 若找图命令不填写参数返回值, 则默认保存在该变量中$n //设置默认日志输出为显示状态, 后面所有 zm.FindColor() 都默认输出调试信息$n zm.SetFindPic {"ret":IXYs, "showlog":"显示"}
--]=]
_zmm.FindPicAttr = {}
_zmm.FindPicAttr.default = { x1 = 0, y1 = 0, x2 = 0, y2 = 0, pic = "Attachment:", color = "000000", dir = "左上",
	sim = 0.8, autosim = "", timeout = 1, delaytime = 10, delaytap = 0, tap = false, tapxy = "", showlog = "隐藏",
	line = "", note = "", clear = -1, cleartime = -2000, ret = {} }
_zmm.FindPicAttr.mt = { __index = _zmm.FindPicAttr.default }
function zmm.SetFindPic(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.FindPicAttr.default[k:lower()] ~= nil then
					_zmm.FindPicAttr.default[k:lower()] = v
				end
			end
			return _zmm.FindPicAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetFindPic()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindPic
@cname:超级找图
@format:[对象][x1, y1, x2, y2, ][图片][, 偏色][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找图
@note:超级找图查找屏幕上的图片, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图片, string, 可选, 查找的图片路径, 多个图片路径用“|”隔开, 支持透明图, 支持PNG BMP JPG等格式
@arg:偏色, string, 可选, 图片的偏色, 格式为“BBGGRR”, 省略默认为”000000”
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.8, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示","显示找到","显示没找到","隐藏","show","showfind","shownofind","hide"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindPic(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local dir = { [0] = "左上", "中心", "右下", "左下", "右上", "上左", "上右", "下左", "下右" }
			dir["左上"], dir["中心"], dir["中间"], dir["右下"], dir["左下"], dir["右上"], dir["上左"], dir["上右"], dir["下左"], dir["下右"] =
			0, 1, 1, 2, 3, 4, 5, 6, 7, 8
			dir["lefttop"], dir["center"], dir["rightbottom"], dir["leftbottom"], dir["righttop"], dir["topleft"], dir["topright"], dir["bottomleft"], dir["bottomright"] =
			0, 1, 2, 3, 4, 5, 6, 7, 8

			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^.*%.png$") or v:match("^.*%.bmp$") or v:match("^.*%.jpg$") then
								args.pic = v
							elseif v:match("^%x%x%x%x%x%x$") then
								args.color = v
							elseif dir[v:lower()] ~= nil then
								args.dir = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								numargs[#numargs + 1] = v
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindPicAttr.mt)


			local picNames = {}
			if args.pic then
				local pics = {}
				for picName in (args.pic .. "|"):gmatch("(.-)|") do
					table.insert(picNames, picName:match(".-([^%:%/]*)%."))
					if picName:match("[%/%:]") then
						table.insert(pics, picName)
					else
						table.insert(pics, _zmm.FindPicAttr.default.pic .. picName)
					end
				end
				args.pic = table.concat(pics, "|")
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)


			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2,
						args.pic, args.color, dir[args.dir], args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
				until args.ret.x > -1


				if args.ret.x == -1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2,
							args.pic, args.color, dir[args.dir], args.sim)
						args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
						if args.ret.x > -1 then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2,
						args.pic, args.color, dir[args.dir], args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
					if args.ret.x > -1 then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
			end
			time = LuaAuxLib.GetTickCount() - time

			local ret
			args.ret[4], args.ret.name = picNames[args.ret.id + 1] or "", picNames[args.ret.id + 1] or ""

			args.ret.fun = "findpic"
			if args.ret.x > -1 then
				args.ret.x, args.ret.y = _zmm.setScaleXY(args.ret.x, args.ret.y, 1)
				args.ret[2], args.ret[3] = args.ret.x, args.ret.y
				args.tapx, args.tapy = _zmm.setScaleXY(args.tapx, args.tapy, 1)
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, id = args.ret.id, x = args.ret.x,
					y = args.ret.y, name = args.ret.name, sim = args.sim, time = time, counts = args.times,
					fun = args.ret.fun }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 查找图片=" ..
				args.pic ..
				" 是否点击=" ..
				tostring(args.tap) ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 相似度=" ..
				args.sim ..
				" 偏色=" .. args.color .. " 方向=" .. dir[dir[args.dir]] .. " 耗时=" ..
				time .. " 次数=" .. args.times
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				LuaAuxLib.Sleep(args.delaytap)
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					local x, y, id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2, args.pic, args.color,
						dir[args.dir], args.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "图片已消失: " .. args.pic
					else
						log = "图片还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindPic()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" then LuaAuxLib.KeepReleaseScreenSnapshot(iskeep) end
			end
		}
	}
end

--[=[
@fname:FindPicTap
@cname:超级找图点击
@format:[对象][x1, y1, x2, y2, ][图片][, 偏色][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找图点击
@note:与zm.FindPic()类似, 区别在于本命令内置找到后点击. 由于查找与点击是两个动作组成, 为方便代码阅读理解, 故新增本命令
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图片, string, 可选, 查找的图片路径, 多个图片路径用“|”隔开, 支持透明图, 支持PNG BMP JPG等格式
@arg:偏色, string, 可选, 图片的偏色, 格式为“BBGGRR”, 省略默认为”000000”
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.8, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindPicTap(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindPic(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindPicTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindPicTable
@cname:超级找图遍历
@format:对象table
@tname:超级找图遍历
@note:与zm.FindPic()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindPic()的对象参数放入table数组中, 实现遍历查找图片
@exp:Dim t = {"怪物1":{"图片1.bmp", true}, "怪物2":{10,20,30,40,"图片2.bmp"}}$n Dim ret = zm.FindPicTable(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindPicTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindPic(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindPicTable()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindPicEx
@cname:超级找图返回所有坐标
@format:[对象][x1, y1, x2, y2, ][图片][, 偏色][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找图返回所有坐标
@note:超级找图查找屏幕上的图片返回所有的坐标, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{ {id, x, y}, ..., "xy":{id:{ {x,y}, ...}, ...}, "id":{id, ...}, "counts":数量}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图片, string, 可选, 查找的图片路径, 多个图片路径用“|”隔开, 支持透明图, 支持PNG BMP 格式, 暂不支持JPG图片
@arg:偏色, string, 可选, 图片的偏色, 格式为“BBGGRR”, 省略默认为”000000”
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.8, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindPicEx(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^.*%.png$") or v:match("^.*%.bmp$") then
								args.pic = v
							elseif v:match("^%x%x%x%x%x%x$") then
								args.color = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								numargs[#numargs + 1] = v
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindPicAttr.mt)

			local picNames, picSizes = {}, {}
			if args.pic then
				local pics = {}
				for picName in (args.pic .. "|"):gmatch("(.-)|") do
					table.insert(picNames, picName:match(".-([^%:%/]*)%."))
					if not (picName:match("[%/%:]")) then
						picName = _zmm.FindPicAttr.default.pic .. picName
					end
					table.insert(pics, picName)
					picSizes[#picSizes + 1] = zmm.GetPicSize(picName)
				end
				args.pic = table.concat(pics, "|")
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)

			if args.x1 == 0 and args.y1 == 0 and args.x2 == 0 and args.y2 == 0 then
				args.x2, args.y2 = LuaAuxLib.GetScreenInfo(0), LuaAuxLib.GetScreenInfo(1)
			end

			local xyid = {}
			local fp = function()
				local x1, y1, x2, y2 = args.x1, args.y1, args.x2, args.y2
				local isfind = false
				repeat
					local x, y, id = LuaAuxLib.FindPicture(x1, y1, x2, y2, args.pic, args.color, 0, args.sim)
					if x > -1 then
						isfind = true
						xyid[#xyid + 1] = { id, x, y }
						local xx, yy = x, y + picSizes[id + 1].h
						repeat
							local x, y, id = LuaAuxLib.FindPicture(xx + picSizes[id + 1].w, y1, x2, yy, args.pic,
								args.color, 0, args.sim)
							if x > -1 then
								xyid[#xyid + 1] = { id, x, y }
								xx = x
							end
						until x == -1 or x + picSizes[id + 1].w > x2
						y1 = y + 1
					end
				until x == -1 or y1 > y2
				return isfind
			end

			local isfind = false

			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					isfind = fp()
				until isfind

				if not (isfind) then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						isfind = fp()
						if isfind then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				repeat
					args.times = args.times + 1
					isfind = fp()
					if isfind then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			end
			time = LuaAuxLib.GetTickCount() - time

			local excludepicpos = function(pos, t)
				t.xy, t.id = {}, {}
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						local isok = true
						for _, vv in ipairs(t) do
							if v[1] == vv[1] and math.abs(v[2] - vv[2]) < picSizes[v[1] + 1].w and math.abs(v[3] - vv[3]) < picSizes[v[1] + 1].h then
								isok = false
								break
							end
						end
						if isok then
							t[#t + 1] = v
							if t.xy[v[1]] == nil then
								t.xy[v[1]] = { { v[2], v[3] } }
								t.id[#t.id + 1] = v[1]
							else
								t.xy[v[1]][#t.xy[v[1]] + 1] = { v[2], v[3] }
							end
						end
					end
				end
			end

			local ret
			zmm.TableClear(args.ret)
			if isfind then
				excludepicpos(xyid, args.ret)
				args.ret.counts = #args.ret
				args.ret.fun = "findpicex"
				ret = zmm.Clone(args.ret)
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "找到数量=" ..
				#args.ret ..
				" 查找图片=" ..
				args.pic ..
				" 是否点击=" ..
				tostring(args.tap) ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," .. args.y2 .. " 相似度=" ..
				args.sim .. " 偏色=" .. args.color .. " 耗时=" .. time .. " 次数=" .. args.times
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n数量=" .. #args.ret
						_zmm.showlog(args.showlog, isfind, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, isfind, 0, log, args.line)
			end

			if args.tap and isfind then
				LuaAuxLib.Sleep(args.delaytap)
				for _, v in ipairs(ret) do
					if args.tapxy ~= "" then
						args.tapx, args.tapy, args.tapt = args.tapxy:match("^([%+%-]?%d+)%, *([%+%-]?%d+),? *(%d*)$")
						args.tapx = args.tapx:match("^[%+%-]") and (v[2] + tonumber(args.tapx)) or tonumber(args.tapx)
						args.tapy = args.tapy:match("^[%+%-]") and (v[3] + tonumber(args.tapy)) or tonumber(args.tapy)
						args.tapt = args.tapt == "" and _zmm.TapAttr.default.t or tonumber(args.tapt)
					else
						args.tapx, args.tapy, args.tapt = v[2], v[3], _zmm.TapAttr.default.t
					end
					zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
				end
			end

			if args.clear == -2 and isfind then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					local x, y, id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2, args.pic, args.color, 0,
						args.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "图片已消失: " .. args.pic
					else
						log = "图片还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, isfind, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindPicEx()，错误信息：", errors)
			end
		},
		finally {
			function()
				LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
			end
		}
	}
end

--[=[
@fname:FindPicExA
@cname:超级找图返回所有坐标(Image版)
@format:[对象][x1, y1, x2, y2, ][图片][, 偏色][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找图返回所有坐标
@note:使用按键的Image.FindPic()查找屏幕上的图片返回所有的坐标, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{ {id, x, y}, ..., "xy":{id:{ {x,y}, ...}, ...}, "id":{id, ...}, "counts":数量}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图片, string, 可选, 查找的图片路径, 多个图片路径用“|”隔开, 支持透明图, 支持PNG BMP 格式, 暂不支持JPG图片
@arg:偏色, string, 可选, 图片的偏色, 格式为“BBGGRR”, 省略默认为”000000”
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.8, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindPicExA(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local dir = { [0] = "左上", "中心", "右下", "左下", "右上", "上左", "上右", "下左", "下右" }
			dir["左上"], dir["中心"], dir["中间"], dir["右下"], dir["左下"], dir["右上"], dir["上左"], dir["上右"], dir["下左"], dir["下右"] =
			0, 1, 1, 2, 3, 4, 5, 6, 7, 8
			dir["lefttop"], dir["center"], dir["rightbottom"], dir["leftbottom"], dir["righttop"], dir["topleft"], dir["topright"], dir["bottomleft"], dir["bottomright"] =
			0, 1, 2, 3, 4, 5, 6, 7, 8

			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^.*%.png$") or v:match("^.*%.bmp$") or v:match("^.*%.jpg$") then
								args.pic = v
							elseif v:match("^%x%x%x%x%x%x$") then
								args.color = v
							elseif dir[v:lower()] ~= nil then
								args.dir = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								numargs[#numargs + 1] = v
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindPicAttr.mt)

			local picNames = {}
			if args.pic then
				local pics = {}
				for picName in (args.pic .. "|"):gmatch("(.-)|") do
					table.insert(picNames, picName:match(".-([^%:%/]*)%."))
					if not (picName:match("[%/%:]")) then
						picName = _zmm.FindPicAttr.default.pic .. picName
					end
					table.insert(pics, picName)
				end
				args.pic = table.concat(pics, "|")
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)

			if args.x1 == 0 and args.y1 == 0 and args.x2 == 0 and args.y2 == 0 then
				args.x2, args.y2 = LuaAuxLib.GetScreenInfo(0), LuaAuxLib.GetScreenInfo(1)
			end

			local xy = {}
			local fp = function()
				local x1, y1, x2, y2 = args.x1, args.y1, args.x2, args.y2
				local isfind = false
				xy = LuaAuxLib.FindPic(x1, y1, x2, y2, args.pic, args.color, dir[args.dir], args.sim)
				if #xy > 0 then
					isfind = true
				end
				return isfind
			end

			local isfind = false

			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					isfind = fp()
				until isfind

				if not (isfind) then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						isfind = fp()
						if isfind then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				repeat
					args.times = args.times + 1
					isfind = fp()
					if isfind then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			end
			time = LuaAuxLib.GetTickCount() - time

			local ret
			zmm.TableClear(args.ret)
			if isfind then
				for k, v in ipairs(xy) do
					args.ret[k] = v
				end
				args.ret.counts = #args.ret
				args.ret.fun = "findpicexa"
				ret = zmm.Clone(args.ret)
			else
				args.ret.counts = 0
				args.ret.fun = "findpicexa"
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "找到数量=" ..
				#args.ret ..
				" 查找图片=" ..
				args.pic ..
				" 是否点击=" ..
				tostring(args.tap) ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," .. args.y2 .. " 相似度=" ..
				args.sim .. " 偏色=" .. args.color .. " 耗时=" .. time .. " 次数=" .. args.times
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n数量=" .. #args.ret
						_zmm.showlog(args.showlog, isfind, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, isfind, 0, log, args.line)
			end

			if args.tap and isfind then
				LuaAuxLib.Sleep(args.delaytap)
				for _, v in ipairs(ret) do
					if args.tapxy ~= "" then
						args.tapx, args.tapy, args.tapt = args.tapxy:match("^([%+%-]?%d+)%, *([%+%-]?%d+),? *(%d*)$")
						args.tapx = args.tapx:match("^[%+%-]") and (v[1] + tonumber(args.tapx)) or tonumber(args.tapx)
						args.tapy = args.tapy:match("^[%+%-]") and (v[2] + tonumber(args.tapy)) or tonumber(args.tapy)
						args.tapt = args.tapt == "" and _zmm.TapAttr.default.t or tonumber(args.tapt)
					else
						args.tapx, args.tapy, args.tapt = v[1], v[2], _zmm.TapAttr.default.t
					end
					zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
				end
			end

			if args.clear == -2 and isfind then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					local x, y, id = LuaAuxLib.FindPicture(args.x1, args.y1, args.x2, args.y2, args.pic, args.color,
						dir[args.dir], args.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "图片已消失: " .. args.pic
					else
						log = "图片还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, isfind, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindPicExA()，错误信息：", errors)
			end
		},
		finally {
			function()
				LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
			end
		}
	}
end

--[=[
@fname:SetFindMultiColor
@cname:设置FindMultiColor默认参数
@format:属性表
@tname:设置zm.FindMultiColor()默认参数 $1
@note:设置zm.FindMultiColor()的参数默认值, 参数为键值对表, 改动后影响后面所有zm.FindMultiColor()
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值, $n 例如{“sim”:0.9, “showlog”:”显示”}表示修改相似度默认为0.9, 日志输出默认为显示$n 详细属性如下:$n x1: 数值型, 查找范围左上角x坐标, 省略默认为0$n y1: 数值型, 查找范围左上角y坐标, 省略默认为0$n x2: 数值型, 查找范围右下角x坐标, 省略默认为0$n y2: 数值型, 查找范围右下角y坐标, 省略默认为0$n dir: 字符串, 查找方向, 可选[“左上”,”中心”,”右上”,”左下”,”右下”]这几个值, 省略默认为"左上"$n sim: 数值型, 查找相似度, 省略默认为0.9$n autosim: 字符串, 智能相似度, 省略默认""$n timeout: 数值型, 查找超时时间, 省略默认为1毫秒$n delaytime: 数值型, 循环查找时间间隔, 省略默认为10毫秒$n tap: 布尔型, 找到后是否点击, 省略默认为false$n showlog: 字符串, 日志输出显示, 可选["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为"隐藏"$n ret: table, 参数返回值, 只能写数组变量名, 设置后, 若命令不带参数返回值, 则默认返回到此变量中, 省略默认为null
@exp:Dim IXYs() //预定义默认参数返回数组$n //设置默认参数返回值IXYs, 若多点找色命令不填写参数返回值, 则默认保存在该变量中$n //设置默认日志输出为显示状态, 后面所有 zm.FindMultiColor() 都默认输出调试信息$n zm.SetFindMultiColor {"ret":IXYs, "showlog":"显示"}
--]=]
_zmm.FindMultiColorAttr = {}
_zmm.FindMultiColorAttr.default = { x1 = 0, y1 = 0, x2 = 0, y2 = 0, first_color = {}, offset_color = {}, offsetxy = 0,
	dir = "左上", sim = 0.9, autosim = "", timeout = 1, delaytime = 10, delaytap = 0, tap = false, tapxy = "",
	showlog = "隐藏", line = "", note = "", clear = -1, cleartime = -2000, ret = {} }
_zmm.FindMultiColorAttr.mt = { __index = _zmm.FindMultiColorAttr.default }
function zmm.SetFindMultiColor(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.FindMultiColorAttr.default[k:lower()] ~= nil then
					_zmm.FindMultiColorAttr.default[k] = v
				end
			end
			return _zmm.FindMultiColorAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetFindMultiColor()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindMultiColor
@cname:超级多点找色
@format:[对象][x1, y1, x2, y2, ][第一点颜色, 偏移颜色组, ...][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级多点找色
@note:超级多点找色查找, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:第一点颜色, string, 可选, 要查找的16进制颜色, 格式为“BBGGRR”, 多个颜色用“|”隔开, 偏色使用“-”隔开, 比如”FFFFFF-101010|123456”, 后面必须跟偏移颜色组, 支持多参数, 详见示例
@arg:偏移颜色组, string, 可选, 相对与第一个颜色坐标点的偏移颜色组, 推荐用抓抓工具快速生成, 前面必须是第一点颜色，支持多参数, 详见示例
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindMultiColor(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local dir = { [0] = "左上", "中心", "右下", "左下", "右上", "上左", "上右", "下左", "下右" }
			dir["左上"], dir["中心"], dir["中间"], dir["右下"], dir["左下"], dir["右上"], dir["上左"], dir["上右"], dir["下左"], dir["下右"] =
			0, 1, 1, 2, 3, 4, 5, 6, 7, 8
			dir["lefttop"], dir["center"], dir["rightbottom"], dir["leftbottom"], dir["righttop"], dir["topleft"], dir["topright"], dir["bottomleft"], dir["bottomright"] =
			0, 1, 2, 3, 4, 5, 6, 7, 8

			args.first_color, args.offset_color = {}, {}
			local f = function(t)
				local numargs = {}
				local firstreg = "^%x%x%x%x%x%x[%-%|%x]*$"
				local offsetreg = "^%-?%d%d?%d?%d?%.?%d*%|%-?%d%d?%d?%d?.?%d*%|%x%x%x%x%x%x[%-%,%.%|%x]*$"
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match(firstreg) then
								table.insert(args.first_color, v)
							elseif v:match(offsetreg) then
								table.insert(args.offset_color, v)
							elseif dir[v:lower()] ~= nil then
								args.dir = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							local iscolor = 0
							for _, vv in pairs(v) do
								if type(vv) == "table" then
									if #vv == 2 and type(vv[1]) == "string" and type(vv[2]) == "string" and vv[1]:match(firstreg) and vv[2]:match(offsetreg) then
										iscolor = 1
									else
										iscolor = 0
										break
									end
								else
									if #v == 2 and type(v[1]) == "string" and type(v[2]) == "string" and v[1]:match(firstreg) and v[2]:match(offsetreg) then
										iscolor = 2
									else
										iscolor = 0
									end
									break
								end
							end

							if iscolor == 1 then
								for _, vv in pairs(v) do
									table.insert(args.first_color, vv[1])
									table.insert(args.offset_color, vv[2])
								end
							elseif iscolor == 2 then
								table.insert(args.first_color, v[1])
								table.insert(args.offset_color, v[2])
							elseif iscolor == 0 then
								args.ret = v
							end
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindMultiColorAttr.mt)

			if args.offsetxy ~= 0 then
				local t = zmm.Clone(args.offset_color)
				for i = 1, #t do
					local setoffcolor = function(f)
						local colors = t[i]:gsub("(%-?%d+%.?%d*)%|(%-?%d+%.?%d*)%|([^,]*)", f)
						table.insert(args.first_color, args.first_color[i])
						table.insert(args.offset_color, colors)
					end

					setoffcolor(function(x, y, c)
						return tostring(x + args.offsetxy) .. "|" .. tostring(y) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x - args.offsetxy) .. "|" .. tostring(y) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x) .. "|" .. tostring(y + args.offsetxy) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x) .. "|" .. tostring(y - args.offsetxy) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x + args.offsetxy) .. "|" .. tostring(y + args.offsetxy) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x + args.offsetxy) .. "|" .. tostring(y - args.offsetxy) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x - args.offsetxy) .. "|" .. tostring(y - args.offsetxy) .. "|" .. c
					end)
					setoffcolor(function(x, y, c)
						return tostring(x - args.offsetxy) .. "|" .. tostring(y + args.offsetxy) .. "|" .. c
					end)
				end
			end

			args.times = 0
			args.ret[1], args.ret.id = -1, -1
			local time = LuaAuxLib.GetTickCount()
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					for j = 1, #args.first_color do
						args.ret.x, args.ret.y = LuaAuxLib.FindMultiColor(args.x1, args.y1, args.x2, args.y2,
							args.first_color[j], args.offset_color[j], dir[args.dir], args.sim)
						args.ret[2], args.ret[3] = args.ret.x, args.ret.y
						if args.ret.x > -1 then
							args.ret.id, args.ret[1] = j - 1, j - 1
							break
						end
					end
				until args.ret.x > -1

				if args.ret.x == -1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						for j = 1, #args.first_color do
							args.ret.x, args.ret.y = LuaAuxLib.FindMultiColor(args.x1, args.y1, args.x2, args.y2,
								args.first_color[j], args.offset_color[j], dir[args.dir], args.sim)
							args.ret[2], args.ret[3] = args.ret.x, args.ret.y
							if args.ret.x > -1 then
								args.ret.id, args.ret[1] = j - 1, j - 1
								break
							end
						end
						if args.ret.x > -1 then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					if #args.first_color > 1 then
						LuaAuxLib.KeepReleaseScreenSnapshot(false)
						LuaAuxLib.KeepReleaseScreenSnapshot(true)
					end
					for j = 1, #args.first_color do
						args.ret.x, args.ret.y = LuaAuxLib.FindMultiColor(args.x1, args.y1, args.x2, args.y2,
							args.first_color[j], args.offset_color[j], dir[args.dir], args.sim)
						args.ret[2], args.ret[3] = args.ret.x, args.ret.y
						if args.ret.x > -1 then
							args.ret.id, args.ret[1] = j - 1, j - 1
							break
						end
					end
					if args.ret.x > -1 then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
				if #args.first_color > 1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
				end
			end
			time = LuaAuxLib.GetTickCount() - time

			args.ret.name = args.first_color[args.ret.id + 1] and args.first_color[args.ret.id + 1] or ""
			args.ret[4] = args.ret.name
			args.ret.fun = "findmulticolor"
			local ret
			if args.ret.x > -1 then
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, id = args.ret.id, x = args.ret.x,
					y = args.ret.y, name = args.ret.name, sim = args.sim, time = time, counts = args.times,
					fun = args.ret.fun }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 多点找色(只显示1个)=" ..
				args.first_color[1] ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 耗时=" ..
				time ..
				" 次数=" ..
				args.times ..
				" 相似度=" ..
				args.sim ..
				" 方向=" ..
				args.dir ..
				" 是否点击=" ..
				tostring(args.tap) .. " 偏移颜色(只显示1个)=" ..
				args.offset_color[1]:sub(1, 9) .. " 多点数量=" .. #args.offset_color
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				LuaAuxLib.Sleep(args.delaytap)
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					if #args.first_color > 1 then
						LuaAuxLib.KeepReleaseScreenSnapshot(false)
						LuaAuxLib.KeepReleaseScreenSnapshot(true)
					end
					for j = 1, #args.first_color do
						local x, y = LuaAuxLib.FindMultiColor(args.x1, args.y1, args.x2, args.y2, args.first_color[j],
							args.offset_color[j], dir[args.dir], args.sim)
						if x > -1 then
							args.ret.miss = false
							args.ret.missx = x
							args.ret.missy = y
							args.ret.missid = j - 1
							ret.missx = x
							ret.missy = y
							ret.missid = j - 1
							break
						else
							args.ret.miss = true
							ret.miss = args.ret.miss
						end
					end
					if args.ret.miss then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime

				if #args.first_color > 1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
				end

				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "多点颜色已消失: " .. args.pic
					else
						log = "多点颜色还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindMultiColor()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" or #args.first_color > 1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
				end
			end
		}
	}
end

--[=[
@fname:FindMultiColorEx
@cname:超级多点找色返回所有坐标
@format:[对象][x1, y1, x2, y2, ][第一点颜色, 偏移颜色组, ...][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级多点找色返回所有坐标
@note:超级多点找色返回所有坐标, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{ {id, x, y}, ..., "xy":{id:{ {x,y}, ...}, ...}, "id":{id, ...}, "counts":数量}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:第一点颜色, string, 可选, 要查找的16进制颜色, 格式为“BBGGRR”, 多个颜色用“|”隔开, 偏色使用“-”隔开, 比如”FFFFFF-101010|123456”, 后面必须跟偏移颜色组, 支持多参数, 详见示例
@arg:偏移颜色组, string, 可选, 相对与第一个颜色坐标点的偏移颜色组, 推荐用抓抓工具快速生成, 前面必须是第一点颜色，支持多参数, 详见示例
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindMultiColorEx(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local dir = { [0] = "左上", "中心", "右下", "左下", "右上", "上左", "上右", "下左", "下右" }
			dir["左上"], dir["中心"], dir["中间"], dir["右下"], dir["左下"], dir["右上"], dir["上左"], dir["上右"], dir["下左"], dir["下右"] =
			0, 1, 1, 2, 3, 4, 5, 6, 7, 8
			dir["lefttop"], dir["center"], dir["rightbottom"], dir["leftbottom"], dir["righttop"], dir["topleft"], dir["topright"], dir["bottomleft"], dir["bottomright"] =
			0, 1, 2, 3, 4, 5, 6, 7, 8

			args.first_color, args.offset_color = {}, {}
			local f = function(t)
				local numargs = {}
				local firstreg = "^%x%x%x%x%x%x[%-%|%x]*$"
				local offsetreg = "^%-?%d%d?%d?%d?%.?%d*%|%-?%d%d?%d?%d?.?%d*%|%x%x%x%x%x%x[%-%,%.%|%x]*$"
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match(firstreg) then
								table.insert(args.first_color, v)
							elseif v:match(offsetreg) then
								table.insert(args.offset_color, v)
							elseif dir[v:lower()] ~= nil then
								args.dir = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							local iscolor = 0
							for _, vv in pairs(v) do
								if type(vv) == "table" then
									if #vv == 2 and type(vv[1]) == "string" and type(vv[2]) == "string" and vv[1]:match(firstreg) and vv[2]:match(offsetreg) then
										iscolor = 1
									else
										iscolor = 0
										break
									end
								else
									if #v == 2 and type(v[1]) == "string" and type(v[2]) == "string" and v[1]:match(firstreg) and v[2]:match(offsetreg) then
										iscolor = 2
									else
										iscolor = 0
									end
									break
								end
							end

							if iscolor == 1 then
								for _, vv in pairs(v) do
									table.insert(args.first_color, vv[1])
									table.insert(args.offset_color, vv[2])
								end
							elseif iscolor == 2 then
								table.insert(args.first_color, v[1])
								table.insert(args.offset_color, v[2])
							elseif iscolor == 0 then
								args.ret = v
							end
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindMultiColorAttr.mt)

			args.times = 0
			local xyt = {}
			local isfind = false
			local fmc = function()
				local isfind = false
				for j = 1, #args.first_color do
					local xy = LuaAuxLib.FindMultiColorEx(args.x1, args.y1, args.x2, args.y2, args.first_color[j],
						args.offset_color[j], dir[args.dir], args.sim)
					if #xy > 0 then
						isfind = true
						for i = 1, #xy do
							xyt[#xyt + 1] = { j - 1, xy[i][1], xy[i][2] }
						end
					end
				end
				return isfind
			end

			local time = LuaAuxLib.GetTickCount()
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					isfind = fmc()
				until isfind

				if not (isfind) then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						isfind = fmc()
						if isfind then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					isfind = fmc()
					if isfind then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			end
			time = LuaAuxLib.GetTickCount() - time


			local getmulticolorrange = function(color)
				local t = zmm.Split(color, ",")
				local minX, minY, maxX, maxY = 0, 0, 0, 0
				for _, v in ipairs(t) do
					local pos = zmm.Split(v, "|")
					minX = minX > tonumber(pos[1]) and tonumber(pos[1]) or minX
					minY = minY > tonumber(pos[2]) and tonumber(pos[2]) or minY
					maxX = maxX < tonumber(pos[1]) and tonumber(pos[1]) or maxX
					maxY = maxY < tonumber(pos[2]) and tonumber(pos[2]) or maxY
				end

				return { maxX - minX, maxY - minY }
			end

			local excludemulticolor = function(pos, t)
				--                t.id, t.x, t.y = {}, {}, {}
				t.xy, t.id = {}, {}
				for k, v in pairs(pos) do
					if type(k) ~= "number" then
						t[k] = v
					else
						local isok = true
						for _, vv in ipairs(t) do
							local wh = getmulticolorrange(args.offset_color[v[1] + 1])
							if v[1] == vv[1] and math.abs(v[2] - vv[2]) < wh[1] and math.abs(v[3] - vv[3]) < wh[2] then
								isok = false
								break
							end
						end
						if isok then
							t[#t + 1] = v
							if t.xy[v[1]] == nil then
								t.xy[v[1]] = { { v[2], v[3] } }
								t.id[#t.id + 1] = v[1]
							else
								t.xy[v[1]][#t.xy[v[1]] + 1] = { v[2], v[3] }
							end
						end
					end
				end
			end


			local ret
			zmm.TableClear(args.ret)
			if isfind then
				excludemulticolor(xyt, args.ret)
				args.ret.counts = #args.ret
				args.ret.fun = "findmulticolorex"
				ret = zmm.Clone(args.ret)
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "找到数量=" ..
				#args.ret ..
				" 多点找色(只显示1个)=" ..
				args.first_color[1] ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 耗时=" ..
				time ..
				" 次数=" ..
				args.times ..
				" 相似度=" ..
				args.sim ..
				" 方向=" ..
				args.dir ..
				" 是否点击=" ..
				tostring(args.tap) .. " 偏移颜色(只显示1个)=" ..
				args.offset_color[1]:sub(1, 9) .. " 多点数量=" .. #args.offset_color
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n数量=" .. #args.ret
						_zmm.showlog(args.showlog, isfind, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, isfind, 0, log, args.line)
			end

			if args.tap and isfind then
				LuaAuxLib.Sleep(args.delaytap)
				for _, v in ipairs(ret) do
					if args.tapxy ~= "" then
						args.tapx, args.tapy, args.tapt = args.tapxy:match("^([%+%-]?%d+)%, *([%+%-]?%d+),? *(%d*)$")
						args.tapx = args.tapx:match("^[%+%-]") and (v[2] + tonumber(args.tapx)) or tonumber(args.tapx)
						args.tapy = args.tapy:match("^[%+%-]") and (v[3] + tonumber(args.tapy)) or tonumber(args.tapy)
						args.tapt = args.tapt == "" and _zmm.TapAttr.default.t or tonumber(args.tapt)
					else
						args.tapx, args.tapy, args.tapt = v[2], v[3], _zmm.TapAttr.default.t
					end
					zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
				end
			end

			if args.clear == -2 and isfind then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					for j = 1, #args.first_color do
						local x, y = LuaAuxLib.FindMultiColor(args.x1, args.y1, args.x2, args.y2, args.first_color[j],
							args.offset_color[j], dir[args.dir], args.sim)
						if x > -1 then
							args.ret.miss = false
							args.ret.missx = x
							args.ret.missy = y
							args.ret.missid = j - 1
							ret.missx = x
							ret.missy = y
							ret.missid = j - 1
							break
						else
							args.ret.miss = true
							ret.miss = args.ret.miss
						end
					end
					if args.ret.miss then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime

				LuaAuxLib.KeepReleaseScreenSnapshot(false)

				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "多点颜色已消失: " .. args.pic
					else
						log = "多点颜色还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, isfind, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindMultiColorEx()，错误信息：", errors)
			end
		},
		finally {
			function()
				LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
			end
		}
	}
end

--[=[
@fname:FindMultiColorTap
@cname:超级多点找色点击
@format:[对象][x1, y1, x2, y2, ][第一点颜色, 偏移颜色组, ...][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级多点找色点击
@note:与zm.FindMultiColor()类似, 区别在于本命令内置找到后点击. 由于查找与点击是两个动作组成, 为方便代码阅读理解, 故新增本命令
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:第一点颜色, string, 可选, 要查找的16进制颜色, 格式为“BBGGRR”, 多个颜色用“|”隔开, 偏色使用“-”隔开, 比如”FFFFFF-101010|123456”, 后面必须跟偏移颜色组, 支持多参数, 详见示例
@arg:偏移颜色组, string, 可选, 相对与第一个颜色坐标点的偏移颜色组, 推荐用抓抓工具快速生成, 前面必须是第一点颜色，支持多参数, 详见示例
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindMultiColorTap(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindMultiColor(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindMultiColorTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindMultiColorTable
@cname:超级多点找色遍历
@format:对象table
@tname:超级多点找色遍历
@note:与zm.FindMultiColor()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindMultiColor()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"000000","1|20|123456" true}, "怪物2":{10,20,30,40,"001011","55|22|ABCDEF"}}$n Dim ret = zm.FindMultiColorTable(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindMultiColorTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindMultiColor(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindMultiColorTable()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SetFindColor
@cname:设置FindColor默认参数
@format:属性表
@tname:设置zm.FindColor()默认参数 $1
@note:设置zm.FindColor()的参数默认值, 参数为键值对表, 改动后影响后面所有zm.FindColor()
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值, $n 例如{“sim”:0.9, “showlog”:”显示”}表示修改相似度默认为0.9, 日志输出默认为显示$n 详细属性如下:$n x1: 数值型, 查找范围左上角x坐标, 省略默认为0$n y1: 数值型, 查找范围左上角y坐标, 省略默认为0$n x2: 数值型, 查找范围右下角x坐标, 省略默认为0$n y2: 数值型, 查找范围右下角y坐标, 省略默认为0$n color: 字符串, 查找的十六进制颜色值, 默认为""$n dir: 字符串, 查找方向, 可选[“左上”,”中心”,”右上”,”左下”,”右下”]这几个值, 省略默认为"左上"$n sim: 数值型, 查找相似度, 省略默认为1.0$n autosim: 字符串, 智能相似度, 省略默认""$n timeout: 数值型, 查找超时时间, 省略默认为1毫秒$n delaytime: 数值型, 循环查找时间间隔, 省略默认为10毫秒$n tap: 布尔型, 找到后是否点击, 省略默认为false$n showlog: 字符串, 日志输出显示, 可选["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为"隐藏"$n ret: table, 参数返回值, 只能写数组变量名, 设置后, 若命令不带参数返回值, 则默认返回到此变量中, 省略默认为null
@exp:Dim IXYs() //预定义默认参数返回数组$n //设置默认参数返回值IXYs, 若找色命令不填写参数返回值, 则默认保存在该变量中$n //设置默认日志输出为显示状态, 后面所有 zm.FindColor() 都默认输出调试信息$n zm.SetFindColor {"ret":IXYs, "showlog":"显示"}
--]=]
_zmm.FindColorAttr = {}
_zmm.FindColorAttr.default = { x1 = 0, y1 = 0, x2 = 0, y2 = 0, color = "", dir = "左上", sim = 1.0, autosim = "",
	timeout = 1, delaytime = 10, delaytap = 0, tap = false, tapxy = "", showlog = "隐藏", line = "", note = "",
	clear = -1, cleartime = -2000, ret = {} }
_zmm.FindColorAttr.mt = { __index = _zmm.FindColorAttr.default }
function zmm.SetFindColor(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.FindColorAttr.default[k:lower()] ~= nil then
					_zmm.FindColorAttr.default[k:lower()] = v
				end
			end
			return _zmm.FindColorAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetFindColor()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindColor
@cname:超级找色
@format:[对象][x1, y1, x2, y2, ][颜色][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找色
@note:超级找色查找, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:颜色, string, 可选, 要查找的16进制颜色，格式为“BBGGRR”，多个颜色用“|”隔开，偏色使用“-”隔开，比如”FFFFFF-101010|123456”
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为1.0, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindColor(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local dir = { [0] = "左上", "中心", "右下", "左下", "右上", "上左", "上右", "下左", "下右" }
			dir["左上"], dir["中心"], dir["中间"], dir["右下"], dir["左下"], dir["右上"], dir["上左"], dir["上右"], dir["下左"], dir["下右"] =
			0, 1, 1, 2, 3, 4, 5, 6, 7, 8
			dir["lefttop"], dir["center"], dir["rightbottom"], dir["leftbottom"], dir["righttop"], dir["topleft"], dir["topright"], dir["bottomleft"], dir["bottomright"] =
			0, 1, 2, 3, 4, 5, 6, 7, 8

			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%x%x%x%x%x%x[%-%|%x]*$") then
								args.color = v
							elseif dir[v:lower()] ~= nil then
								args.dir = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindColorAttr.mt)

			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindColor(args.x1, args.y1, args.x2, args.y2,
						args.color, dir[args.dir], args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
				until args.ret.x > -1

				if args.ret.x == -1 then
					args.sim = args.simmax + args.simstep
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindColor(args.x1, args.y1, args.x2, args.y2,
							args.color, dir[args.dir], args.sim)
						args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
						if args.ret.x > -1 then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindColor(args.x1, args.y1, args.x2, args.y2,
						args.color, dir[args.dir], args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
					if args.ret.x > -1 then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
			end
			time = LuaAuxLib.GetTickCount() - time

			local strNames = zmm.Split(args.color, "|")
			if args.ret.id > -1 then
				local colorName = zmm.Split(strNames[args.ret.id + 1], "-")
				args.ret[4], args.ret.name = colorName[1], colorName[1]
			else
				args.ret[4], args.ret.name = "", ""
			end
			args.ret.fun = "findcolor"

			local ret
			if args.ret.x > -1 then
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, x = args.ret.x, y = args.ret.y,
					id = args.ret.id, name = args.ret.name, sim = args.sim, time = time, counts = args.times,
					fun = args.ret.fun }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 单点找色=" ..
				args.color ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 耗时=" ..
				time ..
				" 次数=" .. args.times .. " 相似度=" ..
				args.sim .. " 方向=" .. args.dir .. " 是否点击=" .. tostring(args.tap)
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				LuaAuxLib.Sleep(args.delaytap)
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					local x, y, id = LuaAuxLib.FindColor(args.x1, args.y1, args.x2, args.y2, args.color, dir[args.dir],
						args.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "颜色已消失: " .. args.pic
					else
						log = "颜色还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindColor()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" then
					LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
				end
			end
		}
	}
end

--[=[
@fname:FindColorEx
@cname:超级找色返回所有坐标
@format:[对象][x1, y1, x2, y2, ]颜色[, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找色返回所有坐标
@note:超级找色返回所有坐标, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{ {id, x, y}, ..., "xy":{id:{ {x,y}, ...}, ...}, "id":{id, ...}, "counts":数量}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:颜色, string, 可选, 要查找的16进制颜色，格式为“BBGGRR”，多个颜色用“|”隔开，偏色使用“-”隔开，比如”FFFFFF-101010|123456”
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindColorEx(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%x%x%x%x%x%x[%-%|%x]*$") then
								args.color = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.dir = args.dir and args.dir:lower()
				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.FindColorAttr.mt)

			if args.x1 == 0 and args.y1 == 0 and args.x2 == 0 and args.y2 == 0 then
				args.x2, args.y2 = LuaAuxLib.GetScreenInfo(0), LuaAuxLib.GetScreenInfo(1)
			end

			zmm.TableClear(args.ret)
			args.ret.id, args.ret.xy = {}, {}
			local fc = function()
				local x1, y1, x2, y2 = args.x1, args.y1, args.x2, args.y2
				local isfind = false
				repeat
					local x, y, id = LuaAuxLib.FindColor(x1, y1, x2, y2, args.color, 0, args.sim)
					if x > -1 then
						isfind = true
						args.ret[#args.ret + 1] = { id, x, y }
						if args.ret.xy[id] == nil then
							args.ret.xy[id] = { { x, y } }
							args.ret.id[#args.ret.id + 1] = id
						else
							args.ret.xy[id][#args.ret.xy[id] + 1] = { x, y }
						end
						local xx = x
						repeat
							local x, y, id = LuaAuxLib.FindColor(xx + 1, y1, x2, y1, args.color, 0, args.sim)
							if x > -1 then
								args.ret[#args.ret + 1] = { id, x, y }
								if args.ret.xy[id] == nil then
									args.ret.xy[id] = { { x, y } }
									args.ret.id[#args.ret.id + 1] = id
								else
									args.ret.xy[id][#args.ret.xy[id] + 1] = { x, y }
								end
								xx = x
							end
						until x == -1 or x + 1 > x2
						y1 = y + 1
					end
				until x == -1 or y1 > y2
				return isfind
			end

			local isfind = false

			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					isfind = fc()
				until isfind

				if not (isfind) then
					args.sim = args.simmax + args.simstep
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						isfind = fc()
						if isfind > -1 then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.times = args.times + 1
					isfind = fc()
					if isfind then
						break
					end
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
			end
			time = LuaAuxLib.GetTickCount() - time

			args.ret.fun = "findcolorex"
			local ret
			if isfind then
				--                _ZM_.gettapxyt(args)
				args.ret.counts = args.times
				ret = zmm.Clone(args.ret)
				--                ret = {args.ret.id, args.ret.x, args.ret.y, args.ret.name, x=args.ret.x, y=args.ret.y, id=args.ret.id, name=args.ret.name, sim=args.sim, time=time, counts=args.times}
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "找到数量=" ..
				#args.ret ..
				" 单点找色=" ..
				args.color ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 耗时=" .. time .. " 次数=" .. args.times .. " 相似度=" ..
				args.sim .. " 是否点击=" .. tostring(args.tap)
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n数量=" .. #args.ret
						_zmm.showlog(args.showlog, isfind, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, isfind, 0, log, args.line)
			end

			if args.tap and isfind then
				LuaAuxLib.Sleep(args.delaytap)
				for _, v in ipairs(args.ret) do
					if args.tapxy ~= "" then
						args.tapx, args.tapy, args.tapt = args.tapxy:match("^([%+%-]?%d+)%, *([%+%-]?%d+),? *(%d*)$")
						args.tapx = args.tapx:match("^[%+%-]") and (v[2] + tonumber(args.tapx)) or tonumber(args.tapx)
						args.tapy = args.tapy:match("^[%+%-]") and (v[3] + tonumber(args.tapy)) or tonumber(args.tapy)
						args.tapt = args.tapt == "" and _zmm.TapAttr.default.t or tonumber(args.tapt)
					else
						args.tapx, args.tapy, args.tapt = v[2], v[3], _zmm.TapAttr.default.t
					end
					zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
				end
				--                zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and isfind then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					local x, y, id = LuaAuxLib.FindColor(args.x1, args.y1, args.x2, args.y2, args.color, 0, args.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "颜色已消失: " .. args.pic
					else
						log = "颜色还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, isfind, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindColorEx()，错误信息：", errors)
			end
		},
		finally {
			function()
				LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
			end
		}
	}
end

--[=[
@fname:FindColorTap
@cname:超级找色点击
@format:[对象][x1, y1, x2, y2, ][颜色][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找色点击
@note:与zm.FindColor()类似, 区别在于本命令内置找到后点击. 由于查找与点击是两个动作组成, 为方便代码阅读理解, 故新增本命令
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:颜色, string, 可选, 要查找的16进制颜色，格式为“BBGGRR”，多个颜色用“|”隔开，偏色使用“-”隔开，比如”FFFFFF-101010|123456”
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为1.0, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindColorTap(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindColor(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindColorTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindColorTable
@cname:超级找色遍历
@format:对象table
@tname:超级找色遍历
@note:与zm.FindColor()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindColor()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"000000", true}, "怪物2":{10,20,30,40,"001011"}}$n Dim ret = zm.FindColorTable(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindColorTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindColor(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindColorTable()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SetFindStr
@cname:设置FindStr默认参数
@format:属性表
@tname:设置zm.FindStr()默认参数 $1
@note:设置zm.FindStr()的参数默认值, 参数为键值对表, 改动后影响后面所有zm.FindStr()
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值, $n 例如{“sim”:0.8, “showlog”:”显示”}表示修改相似度默认为0.8, 日志输出默认为显示$n 详细属性如下:$n x1: 数值型, 查找范围左上角x坐标, 省略默认为0$n y1: 数值型, 查找范围左上角y坐标, 省略默认为0$n x2: 数值型, 查找范围右下角x坐标, 省略默认为0$n y2: 数值型, 查找范围右下角y坐标, 省略默认为0$n color: 字符串, 文字颜色, 默认为"000000"$n dir: 字符串, 查找方向, 可选[“左上”,”中心”,”右上”,”左下”,”右下”]这几个值, 省略默认为"左上"$n sim: 数值型, 查找相似度, 省略默认为0.9$n autosim: 字符串, 智能相似度, 省略默认""$n timeout: 数值型, 查找超时时间, 省略默认为1毫秒$n delaytime: 数值型, 循环查找时间间隔, 省略默认为10毫秒$n tap: 布尔型, 找到后是否点击, 省略默认为false$n showlog: 字符串, 日志输出显示, 可选["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为"隐藏"$n ret: table, 参数返回值, 只能写数组变量名, 设置后, 若命令不带参数返回值, 则默认返回到此变量中, 省略默认为null
@exp:Dim IXYs() //预定义默认参数返回数组$n //设置默认参数返回值IXYs, 若找字命令不填写参数返回值, 则默认保存在该变量中$n //设置默认日志输出为显示状态, 后面所有 zm.FindStr() 都默认输出调试信息$n zm.SetFindStr {"ret":IXYs, "showlog":"显示"}
--]=]
_zmm.FindStrAttr = {}
_zmm.FindStrAttr.default = { x1 = 0, y1 = 0, x2 = 0, y2 = 0, color = "0000000", sim = 0.9, autosim = "", timeout = 1,
	delaytime = 10, delaytap = 0, tap = false, tapxy = "", showlog = "隐藏", line = "", note = "", clear = -1,
	cleartime = -2000, ret = {} }
_zmm.FindStrAttr.mt = { __index = _zmm.FindStrAttr.default }
function zmm.SetFindStr(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.FindStrAttr.default[k:lower()] ~= nil then
					_zmm.FindStrAttr.default[k:lower()] = v
				end
			end
			return _zmm.FindStrAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetFindStr()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindStr
@cname:超级找字
@format:[对象][x1, y1, x2, y2, ][文字][, 颜色][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 字库文件名][, 字库序号][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找字
@note:超级找字查找, 并识别是否点击, 详见帮助信息
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name,"miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开, 可以用$开头表示文字参数
@arg:颜色, string, 可选, 要文字的16进制颜色，格式为“BBGGRR”，多个颜色用“|”隔开，偏色使用“-”隔开，比如”FFFFFF-101010|123456”
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:字库文件名, string, 可选, 填写字库的文件名即可, 附件路径"Attachment:"可以省略不写
@arg:字库序号, string, 可选, 可选, 切换字库序号, 0号字库填写"#0", 以此类推, 当填写字库文件名, 不填字库序号时, 默认设置并使用9号字库
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStr(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%$.*$") then
								args.str = v
							elseif v:match("^%#%d$") then
								args.dictid = v
							elseif v:match("^%x%x%x%x%x%x[%-%|%x]*$") then
								args.color = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^.*%.txt$") then
								if v:match("[%:%/]") then
									args.dictfile = v
								else
									args.dictfile = "Attachment:" .. v
								end
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							elseif args.str == nil then
								args.str = v
							end

							if args.str == nil then
								args.str = v
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end
				_zmm.getrangesimtime(numargs, args)

				args.showlog = args.showlog and args.showlog:lower()
				if args.dictid then
					args.dictid = args.dictid:gsub("#", "")
					args.dictid = tonumber(args.dictid)
				end
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end
			setmetatable(args, _zmm.FindStrAttr.mt)

			if args.str == nil then
				traceprint(args.line, "zm.FindStr( )出错了, 原因:未找到参数文字, 解决方法:请给参数文字前面加$符号, 例如\"$查找的文字\"")
				return nil
			end
			args.str = args.str:gsub("%$", "", 1)

			if args.dictfile then
				if args.dictid == nil then
					args.dictid = 9
				end
				LuaAuxLib.SetDict(args.dictid, args.dictfile, 1)
				LuaAuxLib.UseDict(args.dictid)
			elseif args.dictid then
				LuaAuxLib.UseDict(args.dictid)
			end

			args.x1, args.y1 = _zmm.setScaleXY(args.x1, args.y1)
			args.x2, args.y2 = _zmm.setScaleXY(args.x2, args.y2)

			local time = LuaAuxLib.GetTickCount()
			args.times = 0
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindStr(args.x1, args.y1, args.x2, args.y2, args.str,
						args.color, args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
				until args.ret.x > -1

				if args.ret.x == -1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
							args.sim = args.simmax
						end
						args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindStr(args.x1, args.y1, args.x2, args.y2,
							args.str, args.color, args.sim)
						args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
						if args.ret.x > -1 then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					args.ret.x, args.ret.y, args.ret.id = LuaAuxLib.FindStr(args.x1, args.y1, args.x2, args.y2, args.str,
						args.color, args.sim)
					args.ret[1], args.ret[2], args.ret[3] = args.ret.id, args.ret.x, args.ret.y
					if args.ret.x > -1 then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
			end
			time = LuaAuxLib.GetTickCount() - time

			local strNames = {}
			for strName in (args.str .. "|"):gmatch("(.-)|") do
				table.insert(strNames, strName)
			end

			local ret
			args.ret[4], args.ret.name = strNames[args.ret.id + 1] or "", strNames[args.ret.id + 1] or ""
			args.ret.fun = "findstr"
			if args.ret.x > -1 then
				args.tapx, args.tapy = _zmm.setScaleXY(args.tapx, args.tapy, 1)
				args.ret.x, args.ret.y = _zmm.setScaleXY(args.ret.x, args.ret.y, 1)
				args.ret[2], args.ret[3] = args.ret.x, args.ret.y
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, x = args.ret.x, y = args.ret.y,
					id = args.ret.id, name = args.ret.name, sim = args.sim, time = time, counts = args.times,
					fun = args.ret.fun }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 找字=" ..
				args.str ..
				" 查找范围=" ..
				args.x1 ..
				"," ..
				args.y1 ..
				"," ..
				args.x2 ..
				"," ..
				args.y2 ..
				" 颜色" ..
				args.color ..
				" 耗时=" ..
				time ..
				" 次数=" ..
				args.times ..
				" 相似度=" ..
				args.sim ..
				" 是否点击=" ..
				tostring(args.tap) ..
				(args.dictid and " 字库序号=" .. args.dictid or "") ..
				(args.dictfile and " 字库文件名=" .. args.dictfile or "")
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				LuaAuxLib.Sleep(args.delaytap)
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					local x, y, id = LuaAuxLib.FindStr(args.x1, args.y1, args.x2, args.y2, args.str, args.color, args
					.sim)
					if x == -1 then
						args.ret.miss = true
						ret.miss = args.ret.miss
						break
					else
						args.ret.missx = x
						args.ret.missy = y
						args.ret.missid = id
						ret.missx = x
						ret.missy = y
						ret.missid = id
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime
				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "文字已消失: " .. args.pic
					else
						log = "文字还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStr()，错误信息：", errors)
			end
		},
		finally {
			function()
				if args.autosim ~= "" then
					LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
				end
			end
		}
	}
end

--[=[
@fname:FindStrTap
@cname:超级找字点击
@format:[对象][x1, y1, x2, y2, ][文字][, 颜色][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 字库文件名][, 字库序号][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级找字点击
@note:与zm.FindStr()类似, 区别在于本命令内置找到后点击. 由于查找与点击是两个动作组成, 为方便代码阅读理解, 故新增本命令
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:文字, string, 可选, 查找的文字内容, 多个文字用“|”隔开, 可以用$开头表示文字参数
@arg:颜色, string, 可选, 要文字的16进制颜色，格式为“BBGGRR”，多个颜色用“|”隔开，偏色使用“-”隔开，比如”FFFFFF-101010|123456”
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1
@arg:字库文件名, string, 可选, 填写字库的文件名即可, 附件路径"Attachment:"可以省略不写
@arg:字库序号, string, 可选, 可选, 切换字库序号, 0号字库填写"#0", 以此类推, 当填写字库文件名, 不填字库序号时, 默认设置并使用9号字库
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindStrTap(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.FindStr(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindStrTable
@cname:超级找字遍历
@format:对象table
@tname:超级找字遍历
@note:与zm.FindStr()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.FindStr()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"史莱姆","000000",true}, "怪物2":{10,20,30,40,"黑龙","123456"}}$n Dim ret = zm.FindStrTable(t) //遍历查找怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.FindStrTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindStr(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindStrTable()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SetCmpColorEx
@cname:设置CmpColorEx默认参数
@format:属性表
@tname:设置zm.CmpColorEx()默认参数 $1
@note:设置zm.CmpColorEx()的参数默认值, 参数为键值对表, 改动后影响后面所有zm.CmpColorEx()
@ret:参数列表, table, 返回设置后的参数列表, 失败返回null
@arg:属性表, table, 按照指定格式对表中的键值对进行赋值, $n 例如{“sim”:0.8, “showlog”:”显示”}表示修改相似度默认为0.8, 日志输出默认为显示$n 详细属性如下:$n sim: 数值型, 颜色相似度, 省略默认为0.9$n autosim: 字符串, 智能相似度, 省略默认""$n numsim: 数值型, 数量相似度, 省略默认为1.0$n timeout: 数值型, 对比超时时间, 省略默认为1毫秒$n delaytime: 数值型, 循环对比间隔时间, 省略默认为10毫秒$n tap: 布尔型, 对比成功后是否点击, 省略默认为false$n showlog: 字符串, 日志输出显示, 可选["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为"隐藏"$n ret: table, 参数返回值, 只能写数组变量名, 设置后, 若命令不带参数返回值, 则默认返回到此变量中, 省略默认为null
@exp:Dim IXYs() //预定义默认参数返回数组$n //设置默认参数返回值IXYs, 若多点找色命令不填写参数返回值, 则默认保存在该变量中$n //设置默认日志输出为显示状态, 后面所有 zm.CmpColorEx() 都默认输出调试信息$n zm.SetCmpColorEx {"ret":IXYs, "showlog":"显示"}
--]=]
_zmm.CmpColorExAttr = {}
_zmm.CmpColorExAttr.default = { color = {}, sim = 0.9, autosim = "", numsim = 1, timeout = 1, delaytime = 10, delaytap = 0,
	tap = false, tapxy = "", showlog = "隐藏", line = "", note = "", clear = -1, cleartime = -2000, ret = {} }
_zmm.CmpColorExAttr.mt = { __index = _zmm.CmpColorExAttr.default }
function zmm.SetCmpColorEx(t)
	return try {
		function()
			for k, v in pairs(t) do
				if type(k) == "string" and _zmm.CmpColorExAttr.default[k:lower()] ~= nil then
					_zmm.CmpColorExAttr.default[k] = v
				end
			end
			return _zmm.CmpColorExAttr.default
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SetCmpColorEx()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:CmpColorEx
@cname:超级多点比色
@format:[对象][颜色组, ...][, 颜色相似度[, 智能颜色相似度[, 颜色相似度步长]]][, 数量相似度][, 对比超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级多点比色
@note:超级多点比色, 并识别是否点击, 详见帮助信息
@ret:对比结果, table, 返回符合要求的第一个点坐标并保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 对比失败或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:颜色组, string, 可选, 要对比的颜色组内容, 格式为"x|y|bgr,...", 与抓抓上的多点比色里颜色描述相同, 支持多个颜色组参数, 找到任何一个就返回结果
@arg:颜色相似度, number, 可选, 对比的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能颜色相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:颜色相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:数量相似度, string, 可选, 表示一个颜色组中符合要求点的比例, 省略默认为1.0, 取值范围[0, 1.0], 越高越相似
@arg:对比超时, number, 可选, 最大对比耗时, 单位毫秒, 如果一次对比失败, 可以限时循环对比, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要对比成功后自动点击第一个点的坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续对比, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.CmpColorEx(...)
	local args = { ... }
	local iskeep = LuaAuxLib.GetKeepState()
	return try {
		function()
			args.color = {}
			local f = function(t)
				local numargs = {}
				for k, v in pairs(t) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match("^%d%d?%d?%d?%|%d%d?%d?%d?%|%x%x%x%x%x%x[%-%,%|%x]*$") then
								args.color[#args.color + 1] = v
							elseif _zmm.gShowLog[v:lower()] ~= nil then
								args.showlog = v
							elseif v:match("^%_%d+$") then
								args.line = v
							elseif v:match("^%@.*$") then
								args.note = v
							elseif v:match("^[%+%-]?%d+%, *[%+%-]?%d+,? *%d*$") then
								args.tapxy = v
							elseif v:match("^%d%.?%d*%-%d%.?%d*%-?%d?%.?%d*$") then
								args.autosim = v
							elseif v:match("^%d%.?%d*$") then
								args.numsim = tonumber(v) > 1 and 1 or tonumber(v)
							end
						elseif type(v) == "boolean" then
							args.tap = v
						elseif type(v) == "number" then
							if v >= 0 then
								table.insert(numargs, v)
							else
								if v == -1 or v == -2 then
									args.clear = v
								else
									args.cleartime = v
								end
							end
						elseif type(v) == "table" then
							args.ret = v
						end
					elseif type(k) == "string" then
						args[k:lower()] = v
					end
				end

				_zmm.getrangesimtime(numargs, args)

				args.showlog = args.showlog and args.showlog:lower()
			end

			if type(args[1]) == "table" then
				f(args[1])
				if #args > 1 then
					table.remove(args, 1)
					f(args)
				end
			else
				f(args)
			end

			setmetatable(args, _zmm.CmpColorExAttr.mt)

			local cmpcolor = function(color)
				local colors = zmm.Split(color, ",")
				local nums = 0
				for i = 1, #colors do
					local x, y, c = colors[i]:match("(%d+)|(%d+)|([%x|%-]*)")
					local n = LuaAuxLib.CmpColor(tonumber(x), tonumber(y), c, args.sim)
					if n > -1 then nums = nums + 1 end
				end
				return nums / #colors
			end

			args.times, args.ret.numsim = 0, 0
			args.ret.id, args.ret.x, args.ret.y = -1, -1, -1
			args.ret[1], args.ret[2], args.ret[3] = -1, -1, -1
			args.ret.name = ""

			local time = LuaAuxLib.GetTickCount()
			if args.autosim ~= "" then
				_zmm.getsimmaxminstep(args)

				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				LuaAuxLib.KeepReleaseScreenSnapshot(true)
				args.sim = args.simmax + args.simstep
				repeat
					args.times = args.times + 1
					args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
					if args.sim < args.simmin then
						break
					end
					for j = 1, #args.color do
						args.ret.numsim = cmpcolor(args.color[j])
						if args.ret.numsim >= args.numsim then
							args.ret.x, args.ret.y = args.color[j]:match("^(%d+)|(%d+)")
							args.ret.id, args.ret.name = j - 1, args.color[j]:match("^%d+|%d+|(%x+)")
							args.ret[1], args.ret[2], args.ret[3], args.ret[4] = args.ret.id, args.ret.x, args.ret.y,
								args.ret.name
							break
						end
					end
				until args.ret.numsim >= args.numsim

				if args.ret.x == -1 then
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					args.sim = args.simmax + args.simstep
					repeat
						args.times = args.times + 1
						args.sim = tonumber(string.format("%.4f", args.sim - args.simstep))
						if args.sim < args.simmin then
							args.sim = args.simmax
							LuaAuxLib.KeepReleaseScreenSnapshot(false)
							LuaAuxLib.KeepReleaseScreenSnapshot(true)
						end
						for j = 1, #args.color do
							args.ret.numsim = cmpcolor(args.color[j])
							if args.ret.numsim >= args.numsim then
								args.ret.x, args.ret.y = args.color[j]:match("^(%d+)|(%d+)")
								args.ret.id, args.ret.name = j - 1, args.color[j]:match("^%d+|%d+|(%x+)")
								args.ret[1], args.ret[2], args.ret[3], args.ret[4] = args.ret.id, args.ret.x, args.ret.y,
									args.ret.name
								break
							end
						end
						if args.ret.numsim >= args.numsim then
							break
						end
						LuaAuxLib.Sleep(args.delaytime)
					until (LuaAuxLib.GetTickCount() - time) > args.timeout
				end
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			else
				repeat
					args.times = args.times + 1
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					for j = 1, #args.color do
						args.ret.numsim = cmpcolor(args.color[j])
						if args.ret.numsim >= args.numsim then
							args.ret.x, args.ret.y = args.color[j]:match("^(%d+)|(%d+)")
							args.ret.id, args.ret.name = j - 1, args.color[j]:match("^%d+|%d+|(%x+)")
							args.ret[1], args.ret[2], args.ret[3], args.ret[4] = args.ret.id, args.ret.x, args.ret.y,
								args.ret.name
							break
						end
					end
					if args.ret.numsim >= args.numsim then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > args.timeout
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
			end
			time = LuaAuxLib.GetTickCount() - time

			args.ret.x, args.ret.y = tonumber(args.ret.x), tonumber(args.ret.y)
			args.ret[2], args.ret[3] = args.ret.x, args.ret.y
			args.ret.fun = "cmpcolorex"
			local ret
			if args.ret.x > -1 then
				_zmm.gettapxyt(args)
				ret = { args.ret.id, args.ret.x, args.ret.y, args.ret.name, id = args.ret.id, x = args.ret.x,
					y = args.ret.y, name = args.ret.name, sim = args.sim, time = time, times = args.times,
					fun = args.ret.fun }
			end

			if _zmm.gShowLog[args.showlog] then
				local log = "结果=" ..
				args.ret.id ..
				"," ..
				args.ret.x ..
				"," ..
				args.ret.y ..
				" 多点比色(只显示1个)=" ..
				args.color[1] ..
				" 耗时=" ..
				time ..
				" 次数=" ..
				args.times ..
				" 颜色相似度=" ..
				args.sim .. " 数量相似度=" ..
				args.numsim .. " 是否点击=" .. tostring(args.tap) .. " 多点数量=" .. #args.color
				if args.note ~= "" then
					log = "备注=" .. args.note .. " " .. log
					if args.note:match("^%@%@.*$") then
						local mlog = args.note .. "\n结果: " .. args.ret.id .. "," .. args.ret.x .. "," .. args.ret.y
						_zmm.showlog(args.showlog, args.ret.x > -1, 1, mlog, args.line)
					end
				end
				_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
			end

			if args.tap then
				LuaAuxLib.Sleep(args.delaytap)
				zmm.Tap(args.tapx, args.tapy, args.tapt, args.showlog, args.line, args.note)
			end

			if args.clear == -2 and args.ret.x > -1 then
				if args.tap then LuaAuxLib.Sleep(500) end
				args.ret.miss = false
				ret.miss = args.ret.miss
				local time = LuaAuxLib.GetTickCount()
				repeat
					LuaAuxLib.KeepReleaseScreenSnapshot(false)
					LuaAuxLib.KeepReleaseScreenSnapshot(true)
					for j = 1, #args.color do
						if cmpcolor(args.color[j]) >= args.numsim then
							args.ret.miss = false
							break
						else
							args.ret.miss = true
							ret.miss = args.ret.miss
						end
					end
					if args.ret.miss then
						break
					end
					LuaAuxLib.Sleep(args.delaytime)
				until (LuaAuxLib.GetTickCount() - time) > -args.cleartime

				LuaAuxLib.KeepReleaseScreenSnapshot(false)

				if _zmm.gShowLog[args.showlog] then
					local log
					if ret.miss then
						log = "多点比色已消失: " .. args.pic
					else
						log = "多点比色还存在: " .. args.pic
					end
					if args.note ~= "" then
						log = "备注=" .. args.note .. " " .. log
					end
					_zmm.showlog(args.showlog, args.ret.x > -1, 0, log, args.line)
				end
			else
				args.ret.miss = nil
			end

			return ret, args.ret.id, args.ret.x, args.ret.y, args.ret.name
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.CmpColorEx()，错误信息：", errors)
			end
		},
		finally {
			function()
				LuaAuxLib.KeepReleaseScreenSnapshot(iskeep)
			end
		}
	}
end

--[=[
@fname:CmpColorExTap
@cname:超级多点比色点击
@format:[对象][颜色组, ...][, 颜色相似度[, 智能颜色相似度[, 颜色相似度步长]]][, 数量相似度][, 对比超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级多点比色点击
@note:与zm.CmpColorEx()类似, 区别在于本命令内置对比成功后点击. 由于对比与点击是两个动作组成, 为方便代码阅读理解, 故新增本命令
@ret:对比结果, table, 返回符合要求的第一个点坐标并保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, 对比失败或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他参数, 则会对应覆盖对象数据, 省略默认为null
@arg:颜色组, string, 可选, 要对比的颜色组内容, 格式为"x|y|bgr,...", 与抓抓上的多点比色里颜色描述相同, 支持多个颜色组参数, 找到任何一个就返回结果
@arg:颜色相似度, number, 可选, 对比的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能颜色相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:颜色相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:数量相似度, number, 可选, 表示一个颜色组中符合要求点的比例, 省略默认为1.0, 取值范围[0, 1.0], 越高越相似
@arg:对比超时, number, 可选, 最大对比耗时, 单位毫秒, 如果一次对比失败, 可以限时循环对比, 省略默认为1
@arg:是否点击, boolean, 可选, 是否需要对比成功后自动点击第一个点的坐标, 支持[true, false]这两个值, 省略默认为false
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续对比, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.CmpColorExTap(...)
	local args = { ... }
	return try {
		function()
			args[#args + 1] = true
			return zmm.CmpColorEx(table.unpack(args))
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.CmpColorExTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:CmpColorExTable
@cname:超级多点比色遍历
@format:对象table
@tname:超级多点比色遍历
@note:与zm.CmpColorEx()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss}, ...}, 全部没找到或出错返回null
@arg:对象table, table, 把zm.CmpColorEx()的对象参数放入table数组中, 实现遍历多点找色
@exp:Dim t = {"怪物1":{"@史莱姆","12|432|000000,24|42|123456",true}, "怪物2":{"@黑龙","32|42|123456|ABEACD"}}$n Dim ret = zm.CmpColorExTable(t) //遍历比较怪物1和怪物2$n TracePrint zm.VarInfo(ret)
--]=]
function zmm.CmpColorExTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.CmpColorEx(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.CmpColorExTable()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindCmpPC
@cname:超级查找对比图色
@format:[对象][x1, y1, x2, y2, ][图色数据][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 是否点击][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级查找对比图色
@note:集成多点找色, 找图, 多点比色, 找色4大命令功能, 智能分析图色数据格式分配给不同命令调用, 适合新手记不住命令使用.
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss, "fun":fun}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他图色数据参数, 则会追加图色数据, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图色数据, string 或 table, 可选, 支持图片路径和颜色数据, 会根据参数格式类型依次分配给zm.FindMultiColor(), zm.FindPic(), zm.CmpColorEx(), zm.FindColor()命令调用,$n 任何一个图色数据找到后, 立即停止查找并返回结果. 注意, 不要填写找图命令中的偏色参数!$n 支持多个相同或不同的图色类型数据, 具体写法详见帮助例子.
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1. 注意, 如果存在多个不同类型的图色数据, 则每一种类型单独判断一次查找超时时间.
@arg:是否点击, boolean, 可选, 是否需要找到后自动点击该坐标, 支持[true, false]这两个值, 省略默认为false. 建议使用zm.FindCmpPCTap()命令代替此参数功能.
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindCmpPC(...)
	local args = { ... }
	return try {
		function()
			local tsreg = {}
			tsreg.multireg = { "^%x%x%x%x%x%x[%-%|%x]*$",
				"^%-?%d%d?%d?%d?%.?%d*%|%-?%d%d?%d?%d?.?%d*%|%x%x%x%x%x%x[%-%,%.%|%x]*$" }
			tsreg.colorreg = { "^%x%x%x%x%x%x[%-%|%x]*$" }
			tsreg.cmpreg = { "^%d%d?%d?%d?%|%d%d?%d?%d?%|%x%x%x%x%x%x[%-%,%|%x]*$" }
			tsreg.picreg = { "^.*%.png$", "^.*%.bmp$", "^.*%.jpg$" }

			local params = {}
			params.pics = {}
			params.multis = {}
			params.colors = {}
			params.cmps = {}

			local function getts(args)
				local ts = {}
				ts.pics = {}
				ts.multis = {}
				ts.colors = {}
				ts.cmps = {}
				for k, v in pairs(args) do
					if type(k) == "number" then
						if type(v) == "string" then
							if v:match(tsreg.picreg[1]) or v:match(tsreg.picreg[2]) or v:match(tsreg.picreg[3]) then
								ts.pics[#ts.pics + 1] = v
							elseif v:match(tsreg.multireg[1]) then
								ts.colors[#ts.colors + 1] = v
								if #ts.multis == 0 or #ts.multis[#ts.multis] == 2 then
									ts.multis[#ts.multis + 1] = { v }
								else
									ts.multis[#ts.multis] = { v }
								end
							elseif v:match(tsreg.multireg[2]) and #ts.multis ~= 0 and #ts.multis[#ts.multis] == 1 then
								table.remove(ts.colors, #ts.colors)
								ts.multis[#ts.multis][2] = v
							elseif v:match(tsreg.cmpreg[1]) then
								ts.cmps[#ts.cmps + 1] = v
							else
								table.insert(params, v)
							end
						elseif type(v) == "table" then
							if #v == 0 or (v.x ~= nil and v.y ~= nil) then
								params.ret = v
							else
								getts(v)
							end
						else
							table.insert(params, v)
						end
					elseif type(k) == "string" then
						params[k:lower()] = v
					end
				end
				if #ts.multis ~= 0 and #ts.multis[#ts.multis] == 1 then
					table.remove(ts.multis, #ts.multis)
				end

				for _, v in ipairs(ts.pics) do
					params.pics[#params.pics + 1] = v
				end
				for _, v in ipairs(ts.multis) do
					params.multis[#params.multis + 1] = v
				end
				for _, v in ipairs(ts.colors) do
					params.colors[#params.colors + 1] = v
				end
				for _, v in ipairs(ts.cmps) do
					params.cmps[#params.cmps + 1] = v
				end
			end

			getts(args)

			local ret
			if ret == nil and #params.multis ~= 0 then
				ret = zmm.FindMultiColor(params, table.unpack(params.multis))
			end
			if ret == nil and #params.pics ~= 0 then
				ret = zmm.FindPic(params, table.concat(params.pics, "|"))
			end
			if ret == nil and #params.cmps ~= 0 then
				ret = zmm.CmpColorEx(params, table.unpack(params.cmps))
			end
			if ret == nil and #params.colors ~= 0 then
				ret = zmm.FindColor(params, table.concat(params.colors, "|"))
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindCmpPC()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindCmpPCTap
@cname:超级查找对比图色点击
@format:[对象][x1, y1, x2, y2, ][图色数据][, 方向][, 相似度[, 智能相似度[, 相似度步长]]][, 查找超时][, 点击坐标][, 显示日志][, 输出行号][, 备注][, 等待消失][, 消失超时][, 参数返回值]
@tname:超级查找对比图色点击
@note:集成多点找色, 找图, 多点比色, 找色4大命令功能, 智能分析图色数据格式分配给不同命令调用并执行点击, 适合新手记不住命令使用.
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss, "fun":fun}, 没找到或出错返回null
@arg:对象, table, 可选, 把所有参数放入一个table中传入, 若后面跟有其他图色数据参数, 则会追加图色数据,, 省略默认为null
@arg:x1, number, 可选, 查找范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 查找范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 查找范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 查找范围的右下角y坐标, 省略默认为0
@arg:图色数据, string, 可选, 支持图片路径和颜色数据, 会根据参数格式类型依次分配给zm.FindMultiColor(), zm.FindPic(), zm.CmpColorEx(), zm.FindColor()命令调用,$n 任何一个图色数据找到后, 立即停止查找并返回结果. 注意, 不要填写找图命令中的偏色参数!$n 支持多个相同或不同的图色类型数据, 具体写法详见帮助例子.
@arg:方向, string, 可选, 从指定方向开始查找, 省略默认为"左上", 支持["左上","中心","右上","左下","右下","上左","上右","下左","下右"]这几个值
@arg:相似度, number, 可选, 对比查找的相似度小数, 省略默认为0.9, 取值范围[0, 1.0], 越高越相似
@arg:智能相似度, number, 可选, 与相似度组成组成智能相似计算范围, 适合图色变化大的情况, 省略默认取值与相似度相同, 取值范围[0, 1.0], 越高越相似
@arg:相似度步长, number, 可选, 智能相似度调节变化间隔, 必须写在相似度后面, 省略默认为0.05, 取值范围[0, 1.0]
@arg:查找超时, number, 可选, 最大查找耗时, 单位毫秒, 如果一次找不到, 可以限时循环查找, 省略默认为1. 注意, 如果存在多个不同类型的图色数据, 则每一种类型单独判断一次查找超时时间.
@arg:点击坐标, string, 可选, 点击修改后的坐标, 格式"±x,±y,t", 省略为找到的坐标, t是可选的, 表示点击后延时毫秒$n 例如"100,100"表示固定点击坐标100,100$n "+10,-10"表示偏移点击找到的x+10,y-10位置$n "10,10,2000"表示点击10,10坐标后延时2000毫秒$n 该参数会受zm.SetTap 设置超级点击影响
@arg:显示日志, string, 可选, 输出调试日志信息, 支持["显示", "隐藏", "显示找到", "显示没找到", "show", "hide", "showfind", "shownofind"]这几个值, 省略默认为”隐藏”
@arg:输出行号, string, 可选, 格式为"_"开头后跟行号, 比如"_10"表示第10行代码, 输出调试信息窗口显示第几行代码, 需开启显示日志才有效
@arg:备注, string, 可选, 格式为"@"开头后跟内容, 比如"@拾取屠龙刀", 输出调试信息窗口就会增加这条内容, 若连续两个"@@"则表示在浮窗中显示信息, 需要开启显示日志才有效
@arg:等待消失, number, 可选, 填写-1禁用或-2启用, -2表示找到后或点击后继续查找, 直到找不到或消失超时, 启用后可通过返回值的"miss"键名查看是否消失, 省略默认为-1
@arg:消失超时, number, 可选, 填写负数, 表示等待消失的超时时间, 单位毫秒, 注意必须填写负数, 不可填-1和-2, 省略默认为-2000
@arg:参数返回值, table, 可选, 只能传入数组变量名, 用于保存查找结果, 格式与命令返回值相同
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindCmpPCTap(...)
	local args = { ... }
	return try {
		function()
			args.tap = true
			return zmm.FindCmpPC(args)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindCmpPCTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:FindCmpPCTable
@cname:超级查找对比图色遍历
@format:对象table组
@tname:查找对比图色遍历
@note:与zm.FindCmpPC()类似, 参数只有一个对象数组, 可实现遍历参数的键值对象进行查找
@ret:查找结果, table, 找到返回结果保存到表中, 格式为{"success":找到数量, "fail":没找到数量, "键名":{id, x, y, name, "id":id, "x":x, "y":y, "name":name, "miss":miss, "fun":fun}, ...}, 全部没找到或出错返回null
@arg:对象table组, table, 把zm.FindCmpPC()的对象参数放入table数组中, 实现遍历查找对比图色
@exp://该命令用法多变, 请查看详细帮助信息
--]=]
function zmm.FindCmpPCTable(t)
	return try {
		function()
			local ret = { success = 0, fail = 0 }
			for k, v in pairs(t) do
				ret[k] = zmm.FindCmpPC(v)
				assert(k ~= "success" and k ~= "fail", "对象table不能有 success 或 fail 对象键名")
				if ret[k] then
					ret.success = ret.success + 1
				else
					ret.fail = ret.fail + 1
				end
			end
			if ret.success == 0 then return nil end
			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.FindCmpPCTable()，错误信息：", errors)
			end
		}
	}
end

function _zmm.SnapShotID()
	local IDs = {}
	return function(name)
		IDs[name] = IDs[name] + 1
		return IDs[name]
	end
end

--[=[
@fname:SnapShot
@cname:无限截图
@format:截图目录[, 文件名规则][, x1, y1, x2, y2][, 压缩比率]
@tname:无限截图
@note:通过每次截图自动生成不同文件名实现不覆盖文件, 无限截图功能.
@ret:文件路径, string, 返回本次截图得到的完整文件路径
@arg:截图目录, string, 要存放文件的目录路径, 例如"/sdcard/Pictures/"
@arg:文件名规则, string, 可选, 自定义文件名规则, $n表示内置递增序号, $d表示当前日期, $t表示当前时间, 省略默认为"$d$t-$n.png"或"$d$t-$n.jpg"
@arg:x1, number, 可选, 截取范围的左上角x坐标, 省略默认为0
@arg:y1, number, 可选, 截取范围的左上角y坐标, 省略默认为0
@arg:x2, number, 可选, 截取范围的右下角x坐标, 省略默认为0
@arg:y2, number, 可选, 截取范围的右下角y坐标, 省略默认为0
@arg:压缩比率, number, 可选, 0到100, 数值越大, 文件越小, 省略默认为0, 通过降低画质实现减少文件体积, 本参数仅支持按键IOS或按键安卓3.3.8以上版本
@exp:Dim 图片路径 = zm.SnapShot("/sdcard/Pictures/")
--]=]
function zmm.SnapShot(dir, ...)
	local args = { dir = dir, nums = {}, ... }
	return try {
		function()
			for _, v in ipairs(args) do
				if type(v) == "string" then
					args.file = v
				elseif type(v) == "number" then
					args.nums[#args.nums + 1] = v
				end
			end
			if #args.nums == 0 then
				args.x1, args.y1, args.x2, args.y2 = 0, 0, 0, 0
				args.ratio = nil
			elseif #args.nums == 1 then
				args.x1, args.y1, args.x2, args.y2 = 0, 0, 0, 0
				args.ratio = args.nums[1]
			elseif #args.nums == 4 then
				args.x1, args.y1, args.x2, args.y2 = args.nums[1], args.nums[2], args.nums[3], args.nums[4]
				args.ratio = nil
			elseif #args.nums == 5 then
				args.x1, args.y1, args.x2, args.y2 = args.nums[1], args.nums[2], args.nums[3], args.nums[4]
				args.ratio = args.nums[5]
			else
				error("参数错误")
				return nil
			end

			if args.dir:sub(-1) ~= "/" then
				args.dir = args.dir .. "/"
			end
			if args.file == nil or args.file == "" then
				if args.ratio then
					args.file = "$d$t-$n.jpg"
				else
					args.file = "$d$t-$n.png"
				end
			end

			if _zmm.GetSnapShotID == nil then
				_zmm.GetSnapShotID = _zmm.SnapShotID()
			end

			local reps = { d = os.date("%y%m%d"), t = os.date("%H%M%S"), n = _zmm.GetSnapShotID(args.dir .. args.file) }
			args.file = args.file:gsub("%$(%a)", reps)

			args.path = args.dir .. args.file

			if args.ratio then
				-- SnapShotEx
				LuaAuxLib.SnapShotEx(args.path, args.x1, args.y1, args.x2, args.y2, args.ratio)
			else
				-- SnapShot
				LuaAuxLib.SnapShot(args.path, args.x1, args.y1, args.x2, args.y2)
			end
			return args.path
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SnapShot()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------多线程---------------------------------------

--[=[
@fname:ThreadStatus
@cname:获取线程状态
@format:线程ID
@tname:获取线程 $1 的状态
@note:获取多线程的状态, 例如运行中, 被取消了等等
@ret:线程状态, string, 参数不是线程ID时返回null, 其他返回以下几种字符串$n pending 未启动$n running 运行中$n waiting 等待执行$n done 正常结束$n error 出错了$n cancelled 被强制停止了
@arg:线程ID, userdata, 启动线程后的返回值
@exp:Dim ThreadID$n ThreadID = Thread.Start(threadA)$n For 3$n     TracePrint zm.ThreadStatus(ThreadID)$n     Delay 1000$n Next$n Thread.Stop ThreadID$n TracePrint zm.ThreadStatus(ThreadID)$n Sub threadA()$n     For 5$n         TracePrint "执行多线程中..."$n         Delay 1000$n     Next$n End Sub
--]=]
function zmm.ThreadStatus(thread_handle)
	return try {
		function()
			if thread_handle then
				return thread_handle.status
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ThreadStatus()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ThreadWait
@cname:限时等待线程
@format:线程ID[, 等待毫秒]
@tname:等待 $1 线程 $2 毫秒
@note:暂停当前线程, 等待指定线程执行完毕或超时
@ret:无
@arg:线程ID, userdata, 启动线程后的返回值
@arg:等待毫秒, number, 可选，等待超时时间, 单位毫秒, 省略默认为null, 表示不限时
@exp://例子过长, 请查看命令帮助
--]=]
function zmm.ThreadWait(thread_handle, timeout)
	return try {
		function()
			if timeout ~= nil then
				timeout = timeout / 1000
			end
			thread_handle:join(timeout)
			return true
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ThreadWait()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------数据库操作---------------------------------------

--[=[
@fname:SQLiteCreateTbl
@cname:创建SQLite3数据表
@format:数据库路径, 数据表名, 字段名
@tname:在数据库 $1 中创建包含字段 $3 的表 $2
@note:创建SQLite3数据库的数据表, 若数据库文件不存在, 则自动创建
@ret:创建结果, boolean, 成功返回true, 失败返回false, 出错返回null
@arg:数据库路径, string, 数据库文件所在路径
@arg:数据表名, string, 要创建的数据表名
@arg:字段名, table, 要创建的字段名, 后面可跟数据类型, 例如{“id INTEGER”,”name TEXT”}
@exp:Dim ret = zm.SQLiteCreateTbl(GetSdcardDir() & "/_ZM_.db", "student", {"id INTEGER","name TEXT"})
--]=]
function zmm.SQLiteCreateTbl(DBpath, tbl, fields)
	return try {
		function()
			local sql, field
			local sqlite3 = require("sqlite3")
			for _, v in pairs(fields) do
				if field == nil then
					field = v
				else
					field = field .. "," .. v
				end
			end
			sql = string.format("CREATE TABLE %s (%s)", tbl, field)
			if sqlite3.complete(sql) == nil then return false end
			local db = sqlite3.open(DBpath)
			if db == nil then return false end
			if db:exec(sql) == sqlite3.OK then
				db:close()
				return true
			else
				db:close()
				return false
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SQLiteCreateTbl()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SQLiteDropTbl
@cname:删除SQLite3数据表
@format:数据库路径, 数据表名
@tname:在数据库 $1 中删除表 $2
@note:删除SQLite3数据库的数据表
@ret:删除结果, boolean, 成功返回true, 失败返回false, 出错返回null
@arg:数据库路径, string, 数据库文件所在路径
@arg:数据表名, string, 要删除的数据表名
@exp:Dim ret = zm.SQLiteDropTbl(GetSdcardDir() & "/_ZM_.db", "student")
--]=]
function zmm.SQLiteDropTbl(DBpath, tbl)
	return try {
		function()
			local sql
			local sqlite3 = require("sqlite3")
			sql = string.format("DROP TABLE %s", tbl)
			if sqlite3.complete(sql) == nil then return false end
			local db = sqlite3.open(DBpath)
			if db == nil then return false end
			if db:exec(sql) == sqlite3.OK then
				db:close()
				return true
			else
				db:close()
				return false
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.SQLiteDropTbl()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:SQLiteAddField
@cname:追加SQLite3字段
@format:数据库路径, 数据表名, 字段名
@tname:在数据库 $1  的表 $2中追加字段 $3
@note:追加SQLite3数据表的字段名, 若数据库不存在或字段名已存在, 则返回false
@ret:创建结果, boolean, 成功返回true, 失败返回false, 出错返回null
@arg:数据库路径, string, 数据库文件所在路径
@arg:数据表名, string, 要创建的数据表名
@arg:字段名, table, 要追加的字段名, 后面可跟数据类型, 例如{“id INTEGER”}, 暂时只支持一次添加一个字段
@exp:Dim ret = zm.SQLiteCreateTbl(GetSdcardDir() & "/_ZM_.db", "student", {"id INTEGER","name TEXT"})$n ret = zm.SQLiteAddField(GetSdcardDir() & "/_ZM_.db", "student", {"age INTEGER"})
--]=]
function zmm.SQLiteAddField(DBpath, tbl, fields)
	return try {
		function()
			local sql, field
			local sqlite3 = require("sqlite3")
			for _, v in pairs(fields) do
				if field == nil then
					field = v
				else
					field = field .. "," .. v
				end
			end
			sql = string.format("ALTER TABLE %s ADD COLUMN %s", tbl, field)
			if sqlite3.complete(sql) == nil then return false end
			local db = sqlite3.open(DBpath)
			if db == nil then return false end
			if db:exec(sql) == sqlite3.OK then
				db:close()
				return true
			else
				db:close()
				return false
			end
		end
	}
end

--[=[
@fname:RegExMatch
@cname:正则匹配
@format:源字符串, 正则表达式[, 返回结果]
@tname:在 $1 中正则匹配 $2
@note:Lua的正则匹配（也叫模式匹配）, 获取匹配成功的结果并保存到数组中
@ret:匹配结果, table, 成功返回匹配的结果保存到数组中, 失败返回null
@arg:源字符串, string, 待匹配查找的字符串内容
@arg:正则表达式, string, 匹配模式, 与Javascript或Perl等正则不同, 详见Lua匹配模式
@arg:返回结果, table, 可选, 只能填写数据类型为数组的变量, 匹配失败返回最大下标为-1的数组
@exp:Dim ret() //返回表参数, 可以省略该参数$n Dim s = "紫猫老师QQ:345911220, Email:345911220@qq.com"$n Dim data = zm.RegExMatch(s, "%d+", ret)$n TracePrint zm.VarInfo(data)
--]=]
function zmm.RegExMatch(str, pattern, t)
	return try {
		function()
			assert(str ~= nil, "参数1字符串内容不能为空")
			assert(pattern ~= nil, "参数2正则表达式不能为空")
			t = t or {}
			zmm.TableClear(t)
			--            local i = 1
			--            local ePos
			--            repeat
			--                local ta = {str:find(pattern, ePos)}
			--                ePos = ta[2]
			--                if ta[1] ~= nil then
			--                    t[i] = str:sub(ta[1], ta[2])
			--                    i=i+1
			--                    ePos = ePos + 1
			--                end
			--            until ta[1]==nil or ePos > str:len()

			local f = str:gmatch(pattern)
			while true do
				local tt = { f() }
				if #tt == 0 then
					break
				elseif #tt == 1 then
					t[#t + 1] = tt[1]
				else
					t[#t + 1] = tt
				end
			end

			--            for w in str:gmatch(pattern) do
			--                t[#t+1] = w
			--            end

			if #t ~= 0 then
				return t
			else
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RegExMatch()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RegExMatchSingle
@cname:单个正则匹配
@format:源字符串, 正则表达式[, 序号]
@tname:在 $1 中单个正则匹配 $2
@note:Lua的正则匹配（也叫模式匹配）, 获取指定序号结果返回字符串
@ret:匹配结果, string, 成功返回指定序号的匹配结果, 失败返回null
@arg:源字符串, string, 待匹配查找的字符串内容
@arg:正则表达式, string, 匹配模式, 与Javascript或Perl等正则不同, 详见Lua匹配模式
@arg:序号, number, 可选, 返回指定序号结果, 省略默认为1
@exp:Dim s = "紫猫老师QQ:345911220, 公开交流群:7333555"$n Dim data$n data = zm.RegExMatchSingle(s, "%d+")$n TracePrint "第1个结果", data$n data = zm.RegExMatchSingle(s, "%d+", 2)$n TracePrint "第2个结果", data$n data = zm.RegExMatchSingle(s, "%d+", 3)$n TracePrint "第3个结果", data
--]=]
function zmm.RegExMatchSingle(str, pattern, id)
	return try {
		function()
			assert(pattern ~= nil, "参数2正则表达式不能为空")
			local re = zmm.RegExMatch(str, pattern)
			if re then
				id = id or 1
				return re[id]
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RegExMatchSingle()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RegExMatchEx
@cname:正则子匹配
@format:源字符串, 正则表达式[, 返回结果]
@tname:在 $1 中正则子匹配 $2
@note:捕获子匹配结果, 并保存到二维数组中
@ret:匹配结果, table, 成功返回捕获的子匹配结果保存到二维数组中, 失败返回null
@arg:源字符串, string, 待匹配查找的字符串内容
@arg:正则表达式, string, 匹配模式, 必须含有子匹配, 与Javascript或Perl等正则不同, 详见Lua匹配模式
@arg:返回结果, table, 可选, 只能填写数据类型为数组的变量, 匹配失败返回最大下标为-1的数组
@exp:Dim s = "紫猫老师QQ:345911220, Email:345911220@qq.com"$n Dim data = zm.RegExMatchEx(s, "QQ:(%d+).-Email:(%w-@%w-%.%w+)")$n TracePrint zm.VarInfo(data)
--]=]
function zmm.RegExMatchEx(str, pattern, t)
	return try {
		function()
			assert(pattern ~= nil, "参数2正则表达式不能为空")
			t = t or {}
			zmm.TableClear(t)

			local f = str:gmatch(pattern)
			while true do
				local tt = { f() }
				if #tt == 0 then
					break
				end
				t[#t + 1] = tt
			end

			--            local i=1
			--            local ePos
			--            repeat
			--                local ta = {str:find(pattern, ePos)}
			--                ePos = ta[2]
			--                if ta[1] ~= nil then
			--                    t[i] = ta
			--                    table.remove(t[i],1)
			--                    table.remove(t[i],1)
			--                    i=i+1
			--                    ePos = ePos + 1
			--                elseif ta[1]-ta[2]==1 then
			--                    ePos = ePos + 2
			--                end
			--            until ta[1]==nil or ePos > str:len()
			if #t ~= 0 then
				return t
			else
				return nil
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RegExMatchEx()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RegExMatchExSingle
@cname:单个正则子匹配
@format:源字符串, 正则表达式[, 序号]
@tname:在 $1 中取单个正则子匹配 $2
@note:捕获子匹配结果, 根据序号返回内容
@ret:匹配结果, string, 成功返回捕获第一个指定序号的子匹配结果, 失败返回null
@arg:源字符串, string, 待匹配查找的字符串内容
@arg:正则表达式, string, 匹配模式, 必须含有子匹配, 与Javascript或Perl等正则不同, 详见Lua匹配模式
@arg:序号, number, 可选, 返回首个指定序号的子匹配结果, 省略默认为1
@exp:Dim s = "紫猫老师QQ:345911220, Email:345911220@qq.com"$n Dim data = zm.RegExMatchExSingle (s, "QQ:(%d+).-Email:(%w-@%w-%.%w+)")$n TracePrint "捕获第1个子匹配结果", data$n data = zm.RegExMatchExSingle (s, "QQ:(%d+).-Email:(%w-@%w-%.%w+)", 2)$n TracePrint "捕获第2个子匹配结果", data
--]=]
function zmm.RegExMatchExSingle(str, pattern, id)
	return try {
		function()
			assert(pattern ~= nil, "参数2正则表达式不能为空")
			local re = zmm.RegExMatchEx(str, pattern)
			if re then
				id = id or 1
				return re[1][id]
			end
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RegExMatchEx()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:RegExReplace
@cname:正则替换
@format:源字符串, 正则表达式, 替换内容[, 替换次数]
@tname:在 $1 中正则匹配 $2 结果并替换 $3
@note:对源字符串进行模式匹配并替换内容
@ret:新字符串, string, 成功返回模式匹配替换后的内容, 失败返回null
@arg:源字符串, string, 待匹配查找的字符串内容
@arg:正则表达式, string, 匹配模式, 与Javascript或Perl等正则不同, 详见Lua匹配模式
@arg:替换内容, string或table, 字符串表示替换后的内容, table表示以匹配结果为键名，从表中查找对应的键值替换
@arg:替换次数, number, 可选, 匹配成功后替换的次数, 省略默认为null, 表示全替换
@exp:Dim data = zm.RegExReplace("紫猫老师QQ:12345", "%d+", "345911220")
--]=]
function zmm.RegExReplace(str, pattern, repl, n)
	return try {
		function()
			return string.gsub(str, pattern, repl, n)
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.RegExReplace()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------UI函数---------------------------------------

--[=[
@fname:GetUIValue
@cname:读取UI控件值
@format:控件名
@tname:读取UI控件 $1 的值
@note:读取UI动态界面控件的值, 与UI.GetValue(), 本命令直接以(数值型/布尔型/字符串)形式返回结果
@ret:控件值, 任意类型, 由控件决定对应的数据类型, 返回控件值
@arg:控件名, string, UI界面的控件名
@exp:'创建一个名称为"layout1"的布局控件$n UI.NewLayout ("layout1")$n '以下例子为分别创建文字框、输入框、按钮等控件并使用获取GetValue它们的值$n UI.AddTextView("layout1", "文字框1", "姓名：")$n Dim 内容= zm.GetUIValue("文字框1")
--]=]
function zmm.GetUIValue(name)
	return try {
		function()
			return LuaAuxLib.Encode_GetJsonLib():decode(LuaAuxLib.UI_GetValue(name))[name]
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.GetUIValue()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------Element元素---------------------------------------


--ELEMENT.GETALL
--LuaAuxLib.GetUiElement()
--ELEMENT.GETALLINJSON
--LuaAuxLib.GetFullUiElement()
--ELEMENT.GETTEXT
--LuaAuxLib.GetElementText(%1)
--ELEMENT.FIND
--LuaAuxLib.FindUiElement(%1, %2)

--[[
[弃用]极速版: 通过ELEMENT.FIND查找
标准版: 通过ELEMENT.GETALL获取信息并过滤数据
完整版: ELEMENT.GETALLINJSON再次过滤查找

[弃用]find模式: class, text
getall模式: class, text, resource-id, left,right,up,bottom
getallinjson模式: index,class,text,resource-id,package,content-desc,checkable,checked,clickable,enable,focusable,focused,scrollable,long-clickable,password,selected,bounds,left,right,up,bottom, pos, children

参数:
元素特征, table, {属性名:属性值}
超时时间, number

]]
--[[
function zmm.ElementFind(element) --标准版getall
    local result
    local nodes = LuaAuxLib.GetUiElement()
    for _,node in ipairs(nodes) do
        for k, v in pairs(element) do
            if node[k] == v then
                result = node
            else
                result = nil
                break
            end
        end
        if result then
            break
        end
    end
    return result
end
--]]
--function _ZM_.Ele_getParams(params)
--    local nums = {upzero={}, lowzero={}}
--    local node = {nodes={},
--                    x1=0,y1=0,x2=0,y2=0,
--                    xpos=0,ypos=0,
--                    timeout=0,
--                    matchmode=-1,
--                    ischild=true}
--    --非对象参数处理
--    for k, v in pairs(params) do
--        if type(k) == "number" then
--            local vtype = type(v)
--            if vtype == "string" then
--                table.insert(node.nodes, v)
--            elseif vtype == "number" then
--                if v >= 0 then
--                    table.insert(nums.upzero, v)
--                else
--                    table.insert(nums.lowzero, v)
--                end
--            elseif vtype == "boolean" then
--                node.ischild = v
--            end
--        end
--    end
--    --处理正数
--    local upzerolen = #nums.upzero
--    node.x1,node.y1,node.x2,node.y2=0,0,0,0
--    node.xpos,node.ypos=0,0
--    node.timeout=0
--    if upzerolen == 1 then
--        node.timeout = nums.upzero[1]
--    elseif upzerolen == 2 then
--        node.xpos, node.ypos = nums.upzero[1], nums.upzero[2]
--    elseif upzerolen == 3 then
--        node.xpos, node.ypos, node.timeout = nums.upzero[1], nums.upzero[2], nums.upzero[3]
--    elseif upzerolen == 4 then
--        node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
--    elseif upzerolen == 5 then
--        node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
--        node.timeout = nums.upzero[5]
--    elseif upzerolen == 6 then
--        node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
--        node.xpos, node.ypos = nums.upzero[5], nums.upzero[6]
--    elseif upzerolen == 7 then
--        node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
--        node.xpos, node.ypos, node.timeout = nums.upzero[5], nums.upzero[6], nums.upzero[7]
--    elseif upzerolen ~= 0 then
--        traceprint("提醒，传入zm.ElementFind()命令中数值参数的数量有误，可能会对结果有影响。")
--    end
--    --处理负数
--    node.matchmode = -1
--    for _,v in pairs(nums.lowzero) do
--        if v == -1 or v == -2 or v == -3 then
--            node.matchmode = v
--        end
--    end

--    return node
--end

--function _ZM_.Ele_getNodeParams(params)
--    local nodes = {timeout=0}

--    if type(params[1]) == "table" then
--        for _, v in pairs(params) do
--            if type(v) == "table" then
--                local p = _ZM_.Ele_getParams(v)
--                table.insert(nodes, p)
--                if p.timeout > nodes.timeout then
--                    nodes.timeout = p.timeout
--                end
--            elseif type(v) == "number" and v > nodes.timeout then
--                nodes.timeout = v
--            end
--        end
--    else
--        local p = _ZM_.Ele_getParams(params)
--        table.insert(nodes, p)
--        nodes.timeout = p.timeout
--    end

--    return nodes
--end

--function _ZM_.Ele_ExpandNodes(ui, node, result)
--    -- nodes为LuaAuxLib.GetFullUiElement()结果
--    -- node为要找的元素参数集合, 含属性值, 范围, 坐标, 是否子节点等table数据
--    -- result存放结果, 格式{ find=-1|0|1 , node=nil|{}, index=-1 }, result.find=0表示未查找, result.find=-1表示不符合, result.find=1表示已找到

--    for uik, uiv in pairs(ui) do
--        if result.find == 1 then
--            return
--        end

--        result.index = result.index + 1
--        result.find = 0
--        if type(uik) == "number" and type(uiv) == "table" and not(node.ischild and uiv["children"]) then
--            if result.find ~= -1 and (node.x1~=0 or node.y1~=0 or node.x2~=0 or node.y2~=0) then
--                -- 处理范围
--                if node.x1 <= uiv.left and node.y1 <= uiv.top and node.x2 >= uiv.right and node.y2 >= uiv.bottom then
--                    result.find = 1
--                else
--                    result.find = -1
--                end
--            end

--            if result.find ~= -1 and (node.xpos ~= 0 or node.ypos ~= 0) then
--                -- 处理指定坐标
--                if node.xpos >= uiv.left and node.xpos <= uiv.right and node.ypos >= uiv.top and node.ypos <= uiv.bottom then
--                    result.find = 1
--                else
--                    result.find = -1
--                end
--            end


--            -- 处理元素属性比较
--            -- todo:matchmode处理
--            local temp = {
--                    txt = uiv["text"],
--                    res = uiv["resource-id"],
--                    cls = uiv.class,
--                    pkg = uiv.package,
--                    des = uiv["content-desc"],
--                }
--            for _, v in pairs(node.nodes) do
--                if result.find ~= -1 then
--                    if node.matchmode == -1 then
--                        if v == temp.txt then
--                            temp.txt = nil
--                            result.find = 1
--                        elseif v == temp.res then
--                            temp.res = nil
--                            result.find = 1
--                        elseif v == temp.cls then
--                            temp.cls = nil
--                            result.find = 1
--                        elseif v == temp.pkg then
--                            temp.pkg = nil
--                            result.find = 1
--                        elseif v == temp.des then
--                            temp.des = nil
--                            result.find = 1
--                        else
--                            result.find = -1
--                            break
--                        end
--                    elseif node.matchmode == -2 then
--                        if temp.txt and temp.txt:match(v) then
--                            temp.txt = nil
--                            result.find = 1
--                        elseif temp.res and temp.res:match(v) then
--                            temp.res = nil
--                            result.find = 1
--                        elseif temp.cls and temp.cls:match(v) then
--                            temp.cls = nil
--                            result.find = 1
--                        elseif temp.pkg and temp.pkg:match(v) then
--                            temp.pkg = nil
--                            result.find = 1
--                        elseif temp.des and temp.des:match(v) then
--                            temp.des = nil
--                            result.find = 1
--                        else
--                            result.find = -1
--                            break
--                        end
--                    end
--                end
--            end

--            -- 最终判断是否有找到
--            if result.find == 1 then
--                result.node = uiv
--            end
--        end

--        if result.find == 1 then
--            return
--        end
--        if uiv["children"] then
--            _ZM_.Ele_ExpandNodes(uiv["children"], node, result)
--        end
--    end
--end

--[=[
@fname:ElementRotation
@cname:设置元素旋转方向
@format:旋转方向[, x, y][, ...]
@tname:设置元素旋转方向 $1
@note:设备旋转后元素的坐标系与触摸的坐标系也许会不一样, 本命令将对最后一步得到元素中心坐标做旋转处理, 其他坐标例如范围之类不做处理. 当传入x,y坐标参数时, 仅对本次传入坐标转换, 不处理元素命令坐标转换.
@ret:table, 返回转换后的坐标数组
@arg:旋转方向, number, 设备的逆时针旋转方向, 默认为0, 表示未旋转, 支持[-1,0,1,2,3,90,180,270]这几个数值, -1表示自动判断旋转方向, 0表示未旋转, 1或90表示逆时针旋转90°, 2或180表示逆时针旋转180°, 3或270表示逆时针旋转270°
@arg:x, number, 可选, 要转换的x坐标, 必须与y坐标一起填写, 支持任意数量坐标
@arg:y, number, 可选, 要转换的y坐标, 必须与x坐标一起填写, 支持任意数量坐标
@exp://执行后对接下来所有紫猫插件的元素节点命令有效$n zm.ElementRotation(-1) //自动判断方向自动处理
@exp://仅对本次坐标转换, 不影响后面的元素节点命令$n Dim xy = zm.ElementRotation(-1, 100, 200, 300, 400)
--]=]
_zmm.ElementRotationNums = 0
function zmm.ElementRotation(dir, ...)
	local params = { ... }
	return try {
		function()
			local ElementRotationNums = 0
			local ret = {}
			if dir == -1 then
				dir = LuaAuxLib.GetScreenInfo(3)
			end
			if dir == 0 then
				ElementRotationNums = 0
			elseif dir == 1 or dir == 90 then
				ElementRotationNums = 3
			elseif dir == 2 or dir == 180 then
				ElementRotationNums = 0
			elseif dir == 3 or dir == 270 then
				ElementRotationNums = 3
			else
				ElementRotationNums = 0
			end
			if #params == 0 then
				_zmm.ElementRotationNums = ElementRotationNums
			else
				for i = 1, #params, 2 do
					if i + 1 > #params then
						break
					end
					for _ = 1, ElementRotationNums do
						params[i], params[i + 1] = table.unpack(LuaAuxLib.xyRotate(params[i], params[i + 1], 1))
					end
					table.insert(ret, params[i])
					table.insert(ret, params[i + 1])
				end
			end

			return ret
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFindTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ElementFind
@cname:超级查找元素
@format:[对象, ...][, 超时时间] | [x1,y1,x2,y2,][元素特征, ...][指定坐标x,指定坐标y,][是否最终元素,][匹配模式][,超时时间]
@tname:超级查找元素
@note:根据条件查找元素节点, 支持指定范围, 指定坐标, 指定元素属性, 正则模式匹配, 限时循环查找, 多组元素查找等高级功能, 详见文档说明. 注意数值参数虽然都可以省略不写, 但要符合 范围, 指定坐标, 超时时间 顺序
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:对象, table, 可选, 若要填写则必须写在最前面, 支持多个对象参数, 每个对象为或关系, 填写后将忽略除超时参数外的其他普通参数, 找到任何一个对象立即返回结果. 对象是指把查找一个元素用到的参数全部放到数组中, 然后用该数组做对象参数. 详见例子
@arg:x1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:x2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:元素特征, string, 可选, 抓抓上获取到的元素特征值, 支持text, resource-id, class, package, content-desc这5个特征值, 可自由填写0到5个特征, 无顺序要求, 同个对象每个特征之间为且关系, 内部忽略特征名, 只判断特征值, 详见例子
@arg:指定坐标x, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:指定坐标y, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:是否最终元素, boolean, 可选, 是否只获取没有子节点的元素, 省略默认为true, 表示只获取没有子节点的元素
@arg:匹配模式, number, 可选, 元素特征的匹配模式, -1表示直接比较是否相等的完整匹配模式, -2表示正则匹配是否符合的正则模式, 省略默认为-1
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n Dim node = zm.ElementFind("android.view.View", "com.jingdong.app.mall", "我的")$n TracePrint zm.VarInfo(node)$n If node Then $n 	Tap node["x"], node["y"]$n End If
--]=]
function zmm.ElementFind(...)
	local params = { ... }
	return try {
		function()
			local function _getParams(params)
				local nums = { upzero = {}, lowzero = {} }
				local node = {
					nodes = {},
					x1 = 0,
					y1 = 0,
					x2 = 0,
					y2 = 0,
					xpos = 0,
					ypos = 0,
					timeout = 0,
					matchmode = -1,
					ischild = true
				}
				--非对象参数处理
				for k, v in pairs(params) do
					if type(k) == "number" then
						local vtype = type(v)
						if vtype == "string" then
							table.insert(node.nodes, v)
						elseif vtype == "number" then
							if v >= 0 then
								table.insert(nums.upzero, v)
							else
								table.insert(nums.lowzero, v)
							end
						elseif vtype == "boolean" then
							node.ischild = v
						end
					end
				end
				--处理正数
				local upzerolen = #nums.upzero
				node.x1, node.y1, node.x2, node.y2 = 0, 0, 0, 0
				node.xpos, node.ypos = 0, 0
				node.timeout = 0
				if upzerolen == 1 then
					node.timeout = nums.upzero[1]
				elseif upzerolen == 2 then
					node.xpos, node.ypos = nums.upzero[1], nums.upzero[2]
				elseif upzerolen == 3 then
					node.xpos, node.ypos, node.timeout = nums.upzero[1], nums.upzero[2], nums.upzero[3]
				elseif upzerolen == 4 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
				elseif upzerolen == 5 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.timeout = nums.upzero[5]
				elseif upzerolen == 6 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.xpos, node.ypos = nums.upzero[5], nums.upzero[6]
				elseif upzerolen == 7 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.xpos, node.ypos, node.timeout = nums.upzero[5], nums.upzero[6], nums.upzero[7]
				elseif upzerolen ~= 0 then
					traceprint("提醒，传入zm.ElementFind()命令中数值参数的数量有误，可能会对结果有影响。")
				end
				--处理负数
				node.matchmode = -1
				for _, v in pairs(nums.lowzero) do
					if v == -1 or v == -2 or v == -3 then
						node.matchmode = v
					end
				end

				return node
			end

			local function getNodeParams(params)
				local nodes = { timeout = 0 }

				if type(params[1]) == "table" then
					for _, v in pairs(params) do
						if type(v) == "table" then
							local p = _getParams(v)
							table.insert(nodes, p)
							if p.timeout > nodes.timeout then
								nodes.timeout = p.timeout
							end
						elseif type(v) == "number" and v > nodes.timeout then
							nodes.timeout = v
						end
					end
				else
					local p = _getParams(params)
					table.insert(nodes, p)
					nodes.timeout = p.timeout
				end

				return nodes
			end

			local function _ExpandNodes(ui, node, result)
				-- nodes为LuaAuxLib.GetFullUiElement()结果
				-- node为要找的元素参数集合, 含属性值, 范围, 坐标, 是否子节点等table数据
				-- result存放结果, 格式{ find=-1|0|1 , node=nil|{}, index=-1 }, result.find=0表示未查找, result.find=-1表示不符合, result.find=1表示已找到

				for uik, uiv in pairs(ui) do
					if result.find == 1 then
						return
					end

					result.index = result.index + 1
					result.find = 0
					if type(uik) == "number" and type(uiv) == "table" and not (node.ischild and uiv["children"]) then
						if result.find ~= -1 and (node.x1 ~= 0 or node.y1 ~= 0 or node.x2 ~= 0 or node.y2 ~= 0) then
							-- 处理范围
							if node.x1 <= uiv.left and node.y1 <= uiv.top and node.x2 >= uiv.right and node.y2 >= uiv.bottom then
								result.find = 1
							else
								result.find = -1
							end
						end

						if result.find ~= -1 and (node.xpos ~= 0 or node.ypos ~= 0) then
							-- 处理指定坐标
							if node.xpos >= uiv.left and node.xpos <= uiv.right and node.ypos >= uiv.top and node.ypos <= uiv.bottom then
								result.find = 1
							else
								result.find = -1
							end
						end


						-- 处理元素属性比较
						local temp = {
							txt = uiv["text"],
							res = uiv["resource-id"],
							cls = uiv.class,
							pkg = uiv.package,
							des = uiv["content-desc"],
						}
						for _, v in pairs(node.nodes) do
							if result.find ~= -1 then
								if node.matchmode == -1 then
									if v == temp.txt then
										temp.txt = nil
										result.find = 1
									elseif v == temp.res then
										temp.res = nil
										result.find = 1
									elseif v == temp.cls then
										temp.cls = nil
										result.find = 1
									elseif v == temp.pkg then
										temp.pkg = nil
										result.find = 1
									elseif v == temp.des then
										temp.des = nil
										result.find = 1
									else
										result.find = -1
										break
									end
								elseif node.matchmode == -2 then
									if temp.txt and temp.txt:match(v) then
										temp.txt = nil
										result.find = 1
									elseif temp.res and temp.res:match(v) then
										temp.res = nil
										result.find = 1
									elseif temp.cls and temp.cls:match(v) then
										temp.cls = nil
										result.find = 1
									elseif temp.pkg and temp.pkg:match(v) then
										temp.pkg = nil
										result.find = 1
									elseif temp.des and temp.des:match(v) then
										temp.des = nil
										result.find = 1
									else
										result.find = -1
										break
									end
								end
							end
						end

						-- 最终判断是否有找到
						if result.find == 1 then
							result.node = uiv

							result.node.x = math.floor(uiv.left + uiv.pos[1])
							result.node.y = math.floor(uiv.top + uiv.pos[2])

							for _ = 1, _zmm.ElementRotationNums do
								result.node.x, result.node.y = table.unpack(LuaAuxLib.xyRotate(result.node.x,
									result.node.y, 1))
							end

							result.node.xy = { result.node.x, result.node.y }

							result.node.index = result.index
						end
					end

					if result.find == 1 then
						return
					end
					if uiv["children"] then
						_ExpandNodes(uiv["children"], node, result)
					end
				end
			end

			local cjson = require("cjson")
			local nodesparam = getNodeParams(params)
			local result = {}
			local timestart = LuaAuxLib.GetTickCount()
			repeat
				local ui = cjson.decode(LuaAuxLib.GetFullUiElement())
				for nodeK, nodeV in pairs(nodesparam) do
					if nodeK ~= "timeout" then
						result.index = -1
						result.find = 0
						result.node = nil
						_ExpandNodes(ui, nodeV, result)
						if result.node then
							result.node.id = nodeK
							break
						end
					end
				end
			until result.node or (LuaAuxLib.GetTickCount() - timestart) > nodesparam.timeout

			return result.node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFind()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ElementFindTap
@cname:超级查找元素并点击
@format:[对象, ...][, 超时时间] | [x1,y1,x2,y2,][元素特征, ...][指定坐标x,指定坐标y,][是否最终元素,][匹配模式][,超时时间]
@tname:超级查找元素并点击
@note:与 zm.ElementFind() 唯一区别是多了找到后立即点击功能, 点击受zm.SetTap()影响, 根据条件查找元素节点并点击, 支持指定范围, 指定坐标, 指定元素属性, 正则模式匹配, 限时循环查找, 多组元素查找等高级功能, 详见文档说明. 注意数值参数虽然都可以省略不写, 但要符合 范围, 指定坐标, 超时时间 顺序
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:对象, table, 可选, 若要填写则必须写在最前面, 支持多个对象参数, 每个对象为或关系, 填写后将忽略除超时参数外的其他普通参数, 找到任何一个对象立即返回结果. 对象是指把查找一个元素用到的参数全部放到数组中, 然后用该数组做对象参数. 详见例子
@arg:x1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:x2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:元素特征, string, 可选, 抓抓上获取到的元素特征值, 支持text, resource-id, class, package, content-desc这5个特征值, 可自由填写0到5个特征, 无顺序要求, 同个对象每个特征之间为且关系, 内部忽略特征名, 只判断特征值, 详见例子
@arg:指定坐标x, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:指定坐标y, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:是否最终元素, boolean, 可选, 是否只获取没有子节点的元素, 省略默认为true, 表示只获取没有子节点的元素
@arg:匹配模式, number, 可选, 元素特征的匹配模式, -1表示直接比较是否相等的完整匹配模式, -2表示正则匹配是否符合的正则模式, 省略默认为-1
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n If zm.ElementFindTap("android.view.View", "com.jingdong.app.mall", "我的") Then $n 	TracePrint "点击 京东-我的 成功"$n End If
--]=]
function zmm.ElementFindTap(...)
	local params = { ... }
	return try {
		function()
			local node = zmm.ElementFind(table.unpack(params))
			if node then
				zmm.Tap(node.x, node.y, "hide")
			end
			return node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFindTap()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ElementFindEx
@cname:超级查找元素返回全部
@format:[对象, ...][, 超时时间] | [x1,y1,x2,y2,][元素特征, ...][指定坐标x,指定坐标y,][是否最终元素,][匹配模式][,超时时间]
@tname:超级查找元素返回全部
@note:根据条件查找元素节点返回全部元素, 支持指定范围, 指定坐标, 指定元素属性, 正则模式匹配, 限时循环查找, 多组元素查找等高级功能, 详见文档说明. 注意数值参数虽然都可以省略不写, 但要符合 范围, 指定坐标, 超时时间 顺序
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:对象, table, 可选, 若要填写则必须写在最前面, 支持多个对象参数, 每个对象为或关系, 填写后将忽略除超时参数外的其他普通参数, 找到任何一个对象立即返回结果. 对象是指把查找一个元素用到的参数全部放到数组中, 然后用该数组做对象参数. 详见例子
@arg:x1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:x2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:元素特征, string, 可选, 抓抓上获取到的元素特征值, 支持text, resource-id, class, package, content-desc这5个特征值, 可自由填写0到5个特征, 无顺序要求, 同个对象每个特征之间为且关系, 内部忽略特征名, 只判断特征值, 详见例子
@arg:指定坐标x, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:指定坐标y, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:是否最终元素, boolean, 可选, 是否只获取没有子节点的元素, 省略默认为true, 表示只获取没有子节点的元素
@arg:匹配模式, number, 可选, 元素特征的匹配模式, -1表示直接比较是否相等的完整匹配模式, -2表示正则匹配是否符合的正则模式, 省略默认为-1
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n Dim node = zm.ElementFind("android.view.View", "com.jingdong.app.mall", "我的")$n TracePrint zm.VarInfo(node)$n If node Then $n 	Tap node["x"], node["y"]$n End If
--]=]
function zmm.ElementFindEx(...)
	local params = { ... }
	return try {
		function()
			local function _getParams(params)
				local nums = { upzero = {}, lowzero = {} }
				local node = {
					nodes = {},
					x1 = 0,
					y1 = 0,
					x2 = 0,
					y2 = 0,
					xpos = 0,
					ypos = 0,
					timeout = 0,
					matchmode = -1,
					ischild = true
				}
				--非对象参数处理
				for k, v in pairs(params) do
					if type(k) == "number" then
						local vtype = type(v)
						if vtype == "string" then
							table.insert(node.nodes, v)
						elseif vtype == "number" then
							if v >= 0 then
								table.insert(nums.upzero, v)
							else
								table.insert(nums.lowzero, v)
							end
						elseif vtype == "boolean" then
							node.ischild = v
						end
					end
				end
				--处理正数
				local upzerolen = #nums.upzero
				node.x1, node.y1, node.x2, node.y2 = 0, 0, 0, 0
				node.xpos, node.ypos = 0, 0
				node.timeout = 0
				if upzerolen == 1 then
					node.timeout = nums.upzero[1]
				elseif upzerolen == 2 then
					node.xpos, node.ypos = nums.upzero[1], nums.upzero[2]
				elseif upzerolen == 3 then
					node.xpos, node.ypos, node.timeout = nums.upzero[1], nums.upzero[2], nums.upzero[3]
				elseif upzerolen == 4 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
				elseif upzerolen == 5 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.timeout = nums.upzero[5]
				elseif upzerolen == 6 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.xpos, node.ypos = nums.upzero[5], nums.upzero[6]
				elseif upzerolen == 7 then
					node.x1, node.y1, node.x2, node.y2 = nums.upzero[1], nums.upzero[2], nums.upzero[3], nums.upzero[4]
					node.xpos, node.ypos, node.timeout = nums.upzero[5], nums.upzero[6], nums.upzero[7]
				elseif upzerolen ~= 0 then
					traceprint("提醒，传入zm.ElementFind()命令中数值参数的数量有误，可能会对结果有影响。")
				end
				--处理负数
				node.matchmode = -1
				for _, v in pairs(nums.lowzero) do
					if v == -1 or v == -2 or v == -3 then
						node.matchmode = v
					end
				end

				return node
			end

			local function getNodeParams(params)
				local nodes = { timeout = 0 }

				if type(params[1]) == "table" then
					for _, v in pairs(params) do
						if type(v) == "table" then
							local p = _getParams(v)
							table.insert(nodes, p)
							if p.timeout > nodes.timeout then
								nodes.timeout = p.timeout
							end
						elseif type(v) == "number" and v > nodes.timeout then
							nodes.timeout = v
						end
					end
				else
					local p = _getParams(params)
					table.insert(nodes, p)
					nodes.timeout = p.timeout
				end

				return nodes
			end

			local function _ExpandNodesEx(ui, node, result)
				-- nodes为LuaAuxLib.GetFullUiElement()结果
				-- node为要找的元素参数集合, 含属性值, 范围, 坐标, 是否子节点等table数据
				-- result存放结果, 格式{ find=-1|0|1 , node=nil|{}, index=-1 }, result.find=0表示未查找, result.find=-1表示不符合, result.find=1表示已找到

				for uik, uiv in pairs(ui) do
					--                    if result.find == 1 then
					--                        return
					--                    end

					result.index = result.index + 1
					result.find = 0
					if type(uik) == "number" and type(uiv) == "table" and not (node.ischild and uiv["children"]) then
						if result.find ~= -1 and (node.x1 ~= 0 or node.y1 ~= 0 or node.x2 ~= 0 or node.y2 ~= 0) then
							-- 处理范围
							if node.x1 <= uiv.left and node.y1 <= uiv.top and node.x2 >= uiv.right and node.y2 >= uiv.bottom then
								result.find = 1
							else
								result.find = -1
							end
						end

						if result.find ~= -1 and (node.xpos ~= 0 or node.ypos ~= 0) then
							-- 处理指定坐标
							if node.xpos >= uiv.left and node.xpos <= uiv.right and node.ypos >= uiv.top and node.ypos <= uiv.bottom then
								result.find = 1
							else
								result.find = -1
							end
						end


						-- 处理元素属性比较
						local temp = {
							txt = uiv["text"],
							res = uiv["resource-id"],
							cls = uiv.class,
							pkg = uiv.package,
							des = uiv["content-desc"],
						}
						for _, v in pairs(node.nodes) do
							if result.find ~= -1 then
								if node.matchmode == -1 then
									if v == temp.txt then
										temp.txt = nil
										result.find = 1
									elseif v == temp.res then
										temp.res = nil
										result.find = 1
									elseif v == temp.cls then
										temp.cls = nil
										result.find = 1
									elseif v == temp.pkg then
										temp.pkg = nil
										result.find = 1
									elseif v == temp.des then
										temp.des = nil
										result.find = 1
									else
										result.find = -1
										break
									end
								elseif node.matchmode == -2 then
									if temp.txt and temp.txt:match(v) then
										temp.txt = nil
										result.find = 1
									elseif temp.res and temp.res:match(v) then
										temp.res = nil
										result.find = 1
									elseif temp.cls and temp.cls:match(v) then
										temp.cls = nil
										result.find = 1
									elseif temp.pkg and temp.pkg:match(v) then
										temp.pkg = nil
										result.find = 1
									elseif temp.des and temp.des:match(v) then
										temp.des = nil
										result.find = 1
									else
										result.find = -1
										break
									end
								end
							end
						end

						-- 最终判断是否有找到
						if result.find == 1 then
							--                            result.node = uiv
							table.insert(result.node, uiv)
							result.node[#result.node].x = math.floor(uiv.left + uiv.pos[1])
							result.node[#result.node].y = math.floor(uiv.top + uiv.pos[2])
							for _ = 1, _zmm.ElementRotationNums do
								result.node[#result.node].x, result.node[#result.node].y = table.unpack(LuaAuxLib
								.xyRotate(result.node[#result.node].x, result.node[#result.node].y, 1))
							end
							result.node[#result.node].xy = { result.node[#result.node].x, result.node[#result.node].y }

							result.node[#result.node].index = result.index
						end
					end

					--                    if result.find == 1 then
					--                        return
					--                    end
					if uiv["children"] then
						_ExpandNodesEx(uiv["children"], node, result)
					end
				end
			end

			local cjson = require("cjson")
			local nodesparam = getNodeParams(params)
			local result = {}
			local timestart = LuaAuxLib.GetTickCount()
			repeat
				local ui = cjson.decode(LuaAuxLib.GetFullUiElement())
				for nodeK, nodeV in pairs(nodesparam) do
					if nodeK ~= "timeout" then
						result.index = -1
						result.find = 0
						result.node = {}
						_ExpandNodesEx(ui, nodeV, result)
						if #result.node > 0 then
							result.node.id = nodeK
							result.node.counts = #result.node
							break
						end
					end
				end
			until #result.node > 0 or (LuaAuxLib.GetTickCount() - timestart) > nodesparam.timeout

			return result.node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFind()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ElementFindExTap
@cname:超级查找元素返回全部并点击
@format:[对象, ...][, 超时时间] | [x1,y1,x2,y2,][元素特征, ...][指定坐标x,指定坐标y,][是否最终元素,][匹配模式][,超时时间]
@tname:超级查找元素返回全部并点击
@note:与 zm.ElementFindExTap() 唯一区别是多了找到后立即点击所有点功能, 点击受zm.SetTap()影响, 每次点击间隔内置250毫秒, 根据条件查找元素节点并点击, 支持指定范围, 指定坐标, 指定元素属性, 正则模式匹配, 限时循环查找, 多组元素查找等高级功能, 详见文档说明. 注意数值参数虽然都可以省略不写, 但要符合 范围, 指定坐标, 超时时间 顺序
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:对象, table, 可选, 若要填写则必须写在最前面, 支持多个对象参数, 每个对象为或关系, 填写后将忽略除超时参数外的其他普通参数, 找到任何一个对象立即返回结果. 对象是指把查找一个元素用到的参数全部放到数组中, 然后用该数组做对象参数. 详见例子
@arg:x1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y1, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:x2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:y2, number, 可选, 查找范围参数, 元素节点必须完全在范围内, 省略默认为0, 范围0,0,0,0表示全屏
@arg:元素特征, string, 可选, 抓抓上获取到的元素特征值, 支持text, resource-id, class, package, content-desc这5个特征值, 可自由填写0到5个特征, 无顺序要求, 同个对象每个特征之间为且关系, 内部忽略特征名, 只判断特征值, 详见例子
@arg:指定坐标x, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:指定坐标y, number, 可选, 直接获取指定坐标上的元素特征串, 省略默认为0, 指定坐标0,0表示不使用本参数
@arg:是否最终元素, boolean, 可选, 是否只获取没有子节点的元素, 省略默认为true, 表示只获取没有子节点的元素
@arg:匹配模式, number, 可选, 元素特征的匹配模式, -1表示直接比较是否相等的完整匹配模式, -2表示正则匹配是否符合的正则模式, 省略默认为-1
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n If zm.ElementFindTap("android.view.View", "com.jingdong.app.mall", "我的") Then $n 	TracePrint "点击 京东-我的 成功"$n End If
--]=]
function zmm.ElementFindExTap(...)
	local params = { ... }
	return try {
		function()
			local node = zmm.ElementFindEx(table.unpack(params))
			if node then
				for _, v in ipairs(node) do
					zmm.Tap(v.x, v.y, "hide")
					zmm.Delay(250)
				end
			end
			return node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFindTap()，错误信息：", errors)
			end
		}
	}
end

_zmm.ElementKeys = {
	index = "index",
	id = "index",
	text = "text",
	txt = "text",
	class = "class",
	cls = "class",
	package = "package",
	pkg = "package",
	checkable = "checkable",
	checked = "checked",
	clickable = "clickable",
	enabled = "enabled",
	focusable = "focusable",
	focused = "focused",
	scrollable = "scrollable",
	password = "password",
	selected = "selected",
	bounds = "bounds",
	range = "bounds",
	left = "left",
	x1 = "left",
	top = "top",
	y1 = "top",
	right = "right",
	x2 = "right",
	bottom = "bottom",
	y2 = "bottom",

	-- 以下自定义参数
	width = "width",
	w = "width",
	height = "height",
	h = "height",
	size = "size",
	s = "size",
	x = "x",
	y = "y",                              --元素中心坐标
	xy = "xy",                            --元素中心坐标

	-- 以下非节点属性
	ischild = "ischild",
	matchmode = "matchmode",
	m = "matchmode",
	timeout = "timeout",
	t = "timeout"
}
_zmm.ElementKeys["resource-id"], _zmm.ElementKeys["res"] = "resource-id", "resource-id"
_zmm.ElementKeys["content-desc"], _zmm.ElementKeys["des"] = "content-desc", "content-desc"
_zmm.ElementKeys["long-clickable"], _zmm.ElementKeys["lck"] = "long-clickable", "long-clickable"


--[=[
@fname:ElementFindS
@cname:超级查找元素(专业版)
@format:元素对象[, ...][, 超时时间]
@tname:超级查找元素
@note:根据元素对象条件查找元素节点, 不支持除超时时间外的普通参数, 与zm.ElementFind()区别是支持指定元素节点名与元素节点值
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:元素对象, table, 以键值对表形式存放元素名与元素值, 详细格式见例子, 支持多个对象参数, 每个对象为或关系
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n Dim node = zm.ElementFindS({"text":"微信", "class":"android.widget.TextView"})$n TracePrint zm.VarInfo(node)$n If node Then $n 	Tap node["x"], node["y"]$n End If
--]=]
function zmm.ElementFindS(...)
	local params = { ... }
	return try {
		function()
			local function _getParams(params)
				local node = {
					keystr = {},
					keybool = {},
					offsetlen = 5,
					timeout = 0,
					matchmode = -1,
					ischild = true
				}

				for k, v in pairs(params) do
					if type(k) == "string" then
						if _zmm.ElementKeys[k] then
							node[_zmm.ElementKeys[k]] = v
							if _zmm.ElementKeys[k] == "text" or _zmm.ElementKeys[k] == "class" or _zmm.ElementKeys[k] == "resource-id" or _zmm.ElementKeys[k] == "package" or _zmm.ElementKeys[k] == "content-desc" then
								node.keystr[_zmm.ElementKeys[k]] = v
							elseif _zmm.ElementKeys[k] == "checkable" or _zmm.ElementKeys[k] == "checked" or _zmm.ElementKeys[k] == "clickable" or _zmm.ElementKeys[k] == "enabled" or _zmm.ElementKeys[k] == "focusable" or _zmm.ElementKeys[k] == "focused" or _zmm.ElementKeys[k] == "scrollable" or _zmm.ElementKeys[k] == "password" or _zmm.ElementKeys[k] == "selected" or _zmm.ElementKeys[k] == "long-clickable" then
								node.keybool[_zmm.ElementKeys[k]] = v
							end
						elseif node[k] then
							node[k] = v
						end
					end
				end

				if node.index then
					node.maxid = 0
					local tp = type(node.index)
					if tp == "number" then
						node.maxid = node.index
					elseif tp == "table" then
						for _, v in ipairs(node.index) do
							local tp = type(v)
							if tp == "number" then
								if v > node.maxid then
									node.maxid = v
								end
							elseif tp == "table" then
								if v[2] > node.maxid then
									node.maxid = v[2]
								end
							end
						end
					end
				end

				if node.bounds then
					local pattern = "(%d+)[^%d]+(%d+)[^%d]+(%d+)[^%d]+(%d+)"
					if type(node.bounds) == "string" then
						node.bounds = { string.match(node.bounds, pattern) }
					elseif type(node.bounds) ~= "table" then
						error("bounds必须是字符串或数组")
					end
					for k, v in ipairs(node.bounds) do
						if type(v) == "string" then
							if v:match(pattern) then
								node.bounds[k] = { v:match(pattern) }
								for i, n in ipairs(node.bounds[k]) do
									node.bounds[k][i] = tonumber(n)
								end
							else
								node.bounds[k] = tonumber(v)
							end
						end
					end
				end

				return node
			end

			local function getNodeParams(params)
				local nodes = { timeout = 0 }

				for _, v in pairs(params) do
					if type(v) == "table" then
						local p = _getParams(v)
						table.insert(nodes, p)
						if p.timeout > nodes.timeout then
							nodes.timeout = p.timeout
						end
					elseif type(v) == "number" and v > nodes.timeout then
						nodes.timeout = v
					end
				end

				return nodes
			end

			local check = {}
			--id = 30 表示index是30
			--id = {22,66} 表示index是22或者66
			--id = {{10,20}} 表示id范围是10到20
			--id = {{10,20},{55,96},33} 表示id范围是[10,20]或者[55,96]或33
			function check.index(id, lvl)
				local tp = type(id)
				if tp == "number" then
					if id == lvl then
						return 1
					end
				elseif tp == "table" then
					for _, v in ipairs(id) do
						local tp = type(v)
						if tp == "number" then
							if v == lvl then
								return 1
							end
						elseif tp == "table" then
							if lvl >= v[1] and lvl <= v[2] then
								return 1
							end
						end
					end
				end
				return -1
			end

			--strings = "微信" 表示唯一比较微信
			--strings = {"微信", "淘宝"} 表示微信或淘宝
			function check.str(paramstr, uistr, mode)
				local diff = function(paramstr, uistr, mode)
					if mode == -1 then
						return paramstr == uistr
					elseif mode == -2 then
						return uistr:match(paramstr)
					end
				end


				local tp = type(paramstr)
				if tp == "string" then
					if diff(paramstr, uistr, mode) then
						return 1
					end
				elseif tp == "table" then
					for _, v in ipairs(paramstr) do
						if diff(v, uistr, mode) then
							return 1
						end
					end
				end
				return -1
			end

			function check.bounds(argbounds, left, top, right, bottom, offset)
				local tp = type(argbounds[1])
				if tp == "number" then
					if argbounds[1] - offset <= left and argbounds[2] - offset <= top and argbounds[3] + offset >= right and argbounds[4] + offset >= bottom then
						return 1
					end
				elseif tp == "table" then
					for _, v in ipairs(argbounds) do
						if v[1] - offset <= left and v[2] - offset <= top and v[3] + offset >= right and v[4] + offset >= bottom then
							return 1
						end
					end
				end
				return -1
			end

			function check.size(size, left, top, right, bottom, offset)
				local tp = type(size[1])
				if tp == "number" then
					if math.abs(right - left - size[1]) <= offset and math.abs(bottom - top - size[2]) <= offset then
						return 1
					end
				elseif tp == "table" then
					for _, v in ipairs(size) do
						if math.abs(right - left - v[1]) <= offset and math.abs(bottom - top - v[2]) <= offset then
							return 1
						end
					end
				end
				return -1
			end

			function check.xy(xy, left, top, right, bottom, offset)
				local tp = type(xy[1])
				if tp == "number" then
					if left - offset <= xy[1] and xy[1] <= right + offset and top - offset <= xy[2] and xy[2] <= bottom + offset then
						return 1
					end
				elseif tp == "table" then
					for _, v in ipairs(xy) do
						if left - offset <= v[1] and v[1] <= right + offset and top - offset <= v[2] and v[2] <= bottom + offset then
							return 1
						end
					end
				end
				return -1
			end

			local function _ExpandNodes(ui, node, result)
				-- nodes为LuaAuxLib.GetFullUiElement()结果
				-- node为要找的元素参数集合, 含属性值, 范围, 坐标, 是否子节点等table数据
				-- result存放结果, 格式{ find=-1|0|1 , node=nil|{}, index=-1 }, result.find=0表示未查找, result.find=-1表示不符合, result.find=1表示已找到

				for uik, uiv in pairs(ui) do
					if result.find == 1 or (node.maxid and node.maxid == result.index and result.find == -1) then
						return
					end

					result.index = result.index + 1
					result.find = 0
					if type(uik) == "number" and type(uiv) == "table" and not (node.ischild and uiv["children"]) then
						-- 处理id
						if result.find ~= -1 and node.index then
							result.find = check.index(node.index, result.index)
						end

						-- 处理string集合
						if result.find ~= -1 then
							for kstr, vstr in pairs(node.keystr) do
								if result.find ~= -1 then
									result.find = check.str(vstr, uiv[kstr], node.matchmode)
								end
							end
						end

						-- 处理boolean集合
						if result.find ~= -1 then
							for kbool, vbool in pairs(node.keybool) do
								if result.find ~= -1 then
									result.find = (vbool == uiv[kbool]) and 1 or -1
								end
							end
						end

						-- 处理范围
						-- bounds格式为:{范围} 或{{范围}, {范围}}
						if result.find ~= -1 and node.bounds then
							result.find = check.bounds(node.bounds, uiv.left, uiv.top, uiv.right, uiv.bottom,
								node.offsetlen)
						end


						-- 处理形状
						-- size格式为:{w, h} 或{{w,h},{w,h}}
						if result.find ~= -1 and node.size then
							result.find = check.size(node.size, uiv.left, uiv.top, uiv.right, uiv.bottom, node.offsetlen)
						end

						-- 处理坐标
						-- xy格式为:{x, y} 或{{x,y},{x,y}}
						if result.find ~= -1 and node.xy then
							result.find = check.xy(node.xy, uiv.left, uiv.top, uiv.right, uiv.bottom, node.offsetlen)
						end


						-- 最终判断是否有找到
						if result.find == 1 then
							result.node = uiv

							result.node.x = math.floor(result.node.left + result.node.pos[1])
							result.node.y = math.floor(result.node.top + result.node.pos[2])
							for _ = 1, _zmm.ElementRotationNums do
								result.node.x, result.node.y = table.unpack(LuaAuxLib.xyRotate(result.node.x,
									result.node.y, 1))
							end
							result.node.xy = { result.node.x, result.node.y }

							result.node.index = result.index
						end
					end

					if result.find == 1 or (node.maxid and node.maxid == result.index and result.find == -1) then
						return
					end
					if uiv["children"] then
						_ExpandNodes(uiv["children"], node, result)
					end
				end
			end

			local cjson = require("cjson")
			local nodesparam = getNodeParams(params)
			local result = {}
			local timestart = LuaAuxLib.GetTickCount()
			repeat
				local ui = cjson.decode(LuaAuxLib.GetFullUiElement())
				for nodeK, nodeV in pairs(nodesparam) do
					if nodeK ~= "timeout" then
						result.index = -1
						result.find = 0
						result.node = nil
						_ExpandNodes(ui, nodeV, result)
						if result.node then
							result.node.id = nodeK
							break
						end
					end
				end
			until result.node or (LuaAuxLib.GetTickCount() - timestart) > nodesparam.timeout

			return result.node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFindS()，错误信息：", errors)
			end
		}
	}
end

--[=[
@fname:ElementFindSTap
@cname:超级查找元素并点击(专业版)
@format:元素对象[, ...][, 超时时间]
@tname:超级查找元素
@note:与 zm.ElementFindS() 唯一区别是多了找到后立即点击功能, 点击受zm.SetTap()影响, 根据元素对象条件查找元素节点, 不支持除超时时间外的普通参数, 与zm.ElementFind()区别是支持指定元素节点名与元素节点值
@ret:元素节点, table, 找到返回任何符合的元素节点后, 立即返回详细信息表, 没找到或超时则返回null, 详细信息表除了有抓抓上的元素信息外, 还新增如下成员{"x":x, "y":y, "xy":[x,y], "name":name}, 其中x和y表示元素中心坐标, name表示对象参数名字
@arg:元素对象, table, 以键值对表形式存放元素名与元素值, 详细格式见例子, 支持多个对象参数, 每个对象为或关系
@arg:超时时间, number, 可选, 最大查找时间, 如果每个对象内都有超时时间的话, 将使用最大的一个时间
@exp://更多例子请看帮助文档$n Dim node = zm.ElementFindSTap({"text":"微信", "class":"android.widget.TextView"})$n TracePrint zm.VarInfo(node)
--]=]
function zmm.ElementFindSTap(...)
	local params = { ... }
	return try {
		function()
			local node = zmm.ElementFindS(table.unpack(params))
			if node then
				zmm.Tap(node.x, node.y, "hide")
			end
			return node
		end,
		catch {
			function(errors)
				traceprint("发生运行时错误！错误代码：zm.ElementFindSTap()，错误信息：", errors)
			end
		}
	}
end

----
---------------------------------------初始化函数---------------------------------------

function _zmm.Init()
	local json = LuaAuxLib.URL_OperationGet(
	_zmm._URL_ .. "?ver=" .. _zmm._VER_ .. "&version=" .. _zmm._VERSION_ .. "&week=" .. os.date("%W"), 3)
	local t
	if json == nil or json == "" or json:match("成功") == nil then
		--traceprint("未获取到数据")
	else
		t = LuaAuxLib.Encode_GetJsonLib():decode(json)
		if t then
			_zmm._URL_DOWNLOAD_ = t.download or _zmm._URL_DOWNLOAD_  -- 插件下载地址
			local site          = _zmm._URL_analytics_:match("https?://[^/]*/")
			if t.droplist and t.droplist:match(site) then
				_zmm._URL_analytics_ = t.analytics
			end

			_zmm._QQ_GROUP_    = t.qqgroup or _zmm._QQ_GROUP_      -- 交流QQ群
			_zmm._NEW_VERSION_ = t.version or _zmm._NEW_VERSION_   -- 交流QQ群
			_zmm._NEW_VER_     = t.ver or _zmm._NEW_VER_           -- 交流QQ群
			_zmm._INFO_        = t.info or _zmm._INFO_
			--traceprint("_NEW_VER_", t.ver)
			--traceprint("_NEW_VERSION_", t.version)
			local json         = _zmm.readzmconfig()
			if json then
				json.newversion = _zmm._NEW_VERSION_
				json.info = _zmm._INFO_
				json.newver = _zmm._NEW_VER_
				_zmm.writezmconfig(json)
			end
		end
	end

	_zmm.Init = function()
	end
	return t
end

--[=[
@fname:Init
@cname:初始化插件环境
@format:无
@tname:初始化紫猫插件环境
@note:初始化插件环境与各种函数库, 只需要执行一次
@ret:无
@arg:无
@exp:zm.Init()
--]=]
function zmm.Init(pwd)
	if pwd == "zimaoxy.com" then
		_zmm._VIP_ = true
	end
	-- 恢复当前目录, 解决释放插件到plugin失败问题
	os.execute("cd " .. __MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__)
	traceprint("▁▂▃▄▅▆▇█  欢迎使用紫猫插件V" ..
	_zmm._VERSION_ .. "版，QQ交流群" .. _zmm._QQ_GROUP_ .. "  █▇▆▅▄▃▂▁")
	traceprint("正在初始化运行环境与各种函数库中...")
	_zmm.CreatePluginDir()
	zmm.RndInitSeed()
	zmm.InitBase64()
	_zmm.InitLockBox()
	traceprint("************  紫猫学院( https://zimaoxy.com/ )：插件已初始化完毕  ************")
	_zmm.analytics()
	zmm.Init = function()
	end
	return true
end

--[=[
@fname:Test
@cname:紫猫老师专用测试函数
@format:无
@tname:紫猫老师专用测试函数
@note:插件测试函数, 请勿调用
@ret:无
@arg:无
@exp:无
--]=]
function zmm.Test(mode, wd)
	--    traceprint(string.find("12", "%?"))
	traceprint("紫猫老师专用测试函数, 无实际功能, 请勿调用")
	if mode == 345911220 then
		traceprint("==============================================")
		if wd then
			if wd == "LuaAuxLib" then
				for k, v in pairs(LuaAuxLib) do
					traceprint("LuaAuxLib", k, v, type(v))
					if type(v) == "table" then
						for kk, vv in pairs(v) do
							traceprint("table", k, kk, vv, type(vv))
						end
					end
				end
			else
				for k, v in pairs(LuaAuxLib) do
					if wd then
						if k:match(wd) then
							traceprint("LuaAuxLib", k, v, type(v))
						end
					end
				end
			end
		else
			for k, v in pairs(_G) do
				traceprint("_G", k, v, type(v))
				if type(v) == "table" then
					for kk, vv in pairs(v) do
						traceprint("table", k, kk, vv, type(vv))
					end
				end
			end
		end
	end
end

--[[
挂载curl
If shanhai.Mount("/system") Then
    TracePrint "系统目录挂载成功"
    PutAttachment "/system/bin/", "curl"
    PutAttachment "/system/lib/", "libcurl.so"
    Call shanhai.Chmod("/system/bin/curl", 2)
    Call shanhai.Chmod("/system/lib/libcurl.so", 2)
Else
    TracePrint "系统目录挂载失败"
End If
--]]
zm = QMPlugin
QMPlugin = null
