QMPlugin = {}
local xc = {}
QMPlugin = xc -- 所有表xc里面的键名都会被作为插件函数使用


-- 创建一个空的 table
local hashMap = {}

-- 内部函数
local function try(block)
    local tablejoin = function(...)
        local result = {}
        for _, t in ipairs({ ... }) do
            if type(t) == "table" then
                for k, v in pairs(t) do
                    if type(k) == "number" then
                        table.insert(result, v)
                    else
                        result[k] = v
                    end
                end
            else
                table.insert(result, t)
            end
        end
        return result
    end


    -- get the try function
    local try = block[1]
    assert(try)

    -- get catch and finally functions
    local funcs = tablejoin(block[2] or {}, block[3] or {})

    -- try to call it
    local result_error = {}
    local results = { pcall(try) }
    if not results[1] then
        -- run the catch function
        if funcs and funcs.catch then
            result_error = { funcs.catch(results[2]) }
        end
    end

    -- run the finally function
    if funcs and funcs.finally then
        local result_fin = { funcs.finally(table.unpack(results)) }
        if #result_fin > 0 then
            return table.unpack(result_fin)
        end
    end

    -- ok?
    if results[1] and #results > 1 then
        return table.unpack(results, 2, #results)
    else
        if #result_error > 0 then
            return table.unpack(result_error)
        else
            return nil
        end
    end
end
local function catch(block)
    -- get the catch block function
    return { catch = block[1] }
end
local function finally(block)
    -- get the finally block function
    return { finally = block[1] }
end
local function traceprint(...)
    if QMPlugin then
        print(...)
    else
        local line = select(1, ...)
        if type(line) == "string" and line:match("^%_%d+$") then
            LuaAuxLib.TracePrint(line .. ":", table.concat({ ... }, " ", 2, select("#", ...)))
        elseif line == ":" and #{ ... } > 1 then
            LuaAuxLib.TracePrint(":", table.concat({ ... }, " ", 2, select("#", ...)))
        else
            LuaAuxLib.TracePrint(":", "xc插件：", ...)
        end
    end
end
function xc._tabledump(t)
    local str = "{"
    local kk = function(k)
        if type(k) == "string" then
            return "\034" .. k .. "\034"
        else
            return tostring(k)
        end
    end

    for k, v in pairs(t) do
        if type(v) == "table" and k ~= "__index" then
            str = str .. kk(k) .. ":" .. xc._tabledump(v)
        elseif type(v) == "string" then
            str = str .. kk(k) .. ":" .. "\034" .. v .. "\034" .. ","
        else
            str = str .. kk(k) .. ":" .. tostring(v) .. ","
        end
    end
    str = #str > 1 and str:sub(1, -2) .. "}," or str .. "},"
    return str
end

-- function xc.tabledump(t)
-- 	return xc._tabledump(t):sub(1, -2)
-- end
--[[
@fname:VarInfo
@cname:获取参数信息
@tname:获取 $1 等参数的详细信息
@format:[参数1][,参数2...]
@note:获取参数的长度, 数据类型与值, 可以直接获取表的值
@ret:详细信息, 字符串, 得到所有参数的详细信息内容
@arg:参数, 任意类型, 可选, 不限参数数量, 表示要获取信息的参数
@exp:TracePrint zm.VarInfo("Hello")        //输出数据类型, 长度, 字符串内容
@exp:TracePrint zm.VarInfo({1,2,3,"紫猫":"学院"})  //输出table的长度与详细内容
--]]
function xc.varInfo(...)
    -- 防止无法获取nil参数
    local paramCount = select("#", ...)
    local varType, printStr, t = "", "", {}
    for i = 1, paramCount do
        local v = select(i, ...)
        try {
            function()
                local jq = function(v)
                    if #v > 65536 then
                        v = v:sub(1, 65536) .. "...(紫猫插件:由于内容长度超过65536,请使用其他方式输出查看.)"
                    end
                    return v
                end

                varType = type(v)
                if varType == "table" then
                    try {
                        function()
                            printStr = "【" ..
                                varType .. " " .. tostring(#v) .. "】" .. jq(LuaAuxLib.Encode_GetJsonLib():encode(v))
                        end,
                        catch {
                            try {
                                function()
                                    printStr = "【" .. varType .. " " .. tostring(#v) .. "】" .. jq(xc.tabledump(v))
                                end
                            }
                        }
                    }
                elseif varType == "number" or varType == "string" then
                    printStr = "【" .. varType .. " " .. tostring(#tostring(v)) .. "】" .. jq(tostring(v))
                elseif varType == "boolean" or varType == "null" then
                    printStr = "【" .. varType .. "】" .. tostring(v)
                else
                    printStr = "【" .. varType .. "】 unknown"
                end
                table.insert(t, #t + 1, printStr)
            end,
            catch {
                function(errors)
                    traceprint("发生运行时错误！错误代码：zm.VarInfo()，错误信息：", errors)
                end
            }
        }
    end
    printStr = table.concat(t, ", ")
    return printStr
end

--[=[
@cname:创建一个空的map
@arg: name(string),必选,map名称
@ret: name(string),map名称
--]=]
function xc.newMap(name)
    hashMap[name] = {}
    return name;
end

--[=[
@cname:获取map
@arg: name(string),必选,map名称
@ret: (any) map对象
--]=]
function xc.getMap(name)
    return hashMap[name]
end

--[=[
@cname:向指定map中存放值
@arg: name(string),必选,map名称
@arg: key(string,number),必选,键值
@arg: value(any),必选,参访值
--]=]
function xc.mapPut(name, key, value)
    hashMap[name][key] = value
end

--[=[
@cname:向指定map中存放值
@arg: name(string),必选,map名称
@arg: table(string,number),必选,map
--]=]
function xc.putMap(name, table)
    for key, value in pairs(table) do
        hashMap[name][key] = value
    end
end

--[=[
@cname:从map中取值
@arg: name(string),必选,map名称
@arg: key(string,number),必选,键值
@ret: (any) 对应map的key的值
--]=]
function xc.mapGet(name, key)
    return hashMap[name][key]
end

--[=[
@cname:将字符串解析成表
@arg: data(string),xml格式的字符串
@ret: (table) 解析成对于的table表结构
--]=]
function xc.ParseByXmlStr(data)
    local xfile = xml.eval(data)
    local ret
    if xfile ~= nil then
        ret = xfile
    else
        ret = ""
    end
    return ret
end

--[=[
@cname:找按钮节点
@arg: table(table),必选,节点表
@arg: type(string),必选,节点类型：text、id
@ret: (table) 按钮的坐标
--]=]
function xc.findBtn(table, type, name)
    for k, v in ipairs(table) do
        if v == nil then
        elseif v[type] == name then
            local xy = v["bounds"]
            if xy == nil or xy == "" then
                return nil;
            end
            local x1, y1, x2, y2
            local is = true
            for x, y in xy:gmatch("%[(%d+),(%d+)%]") do
                if is then
                    x1 = tonumber(x)
                    y1 = tonumber(y)
                    is = false
                else
                    x2 = tonumber(x)
                    y2 = tonumber(y)
                end
            end
            return { (x1 + x2) / 2, (y1 + y2) / 2 }
        else
            -- traceprint(v[type])
            local ret = xc.findBtn(v, type, name)
            if ret ~= nil and ret ~= "" then
                return ret
            end
        end
    end
end

--[=[
@cname:将字符串中的数字提取出来拼装在一起
@arg: data(string),必选,字符串
@ret: (number) 数字
--]=]
function xc.str2number(data)
    local str = data
    local numStr = ""
    -- 遍历字符串的每个字符
    for i = 1, #str do
        local char = string.sub(str, i, i)
        -- 如果字符是数字，将其添加到numStr中
        if tonumber(char) then
            numStr = numStr .. char
        end
    end
    -- 将拼接好的numStr转换为数字
    return (tonumber(numStr) or 0)
end

--[=[
@cname:遍历按键精灵中的库函数
--]=]
function xc.scanLuaAuxLib()
    local text = "";
    try {
        function()
            for k, v in pairs(LuaAuxLib) do
                -- traceprint("名称: " .. k, "类型: " .. type(v))
                text = text .. k .. " "
            end
            traceprint(text)
        end,
        catch {
            function(errors)
                traceprint("发生运行时错误！错误代码：scanLuaAuxLib(),错误信息：", errors)
            end
        }
    }
end
