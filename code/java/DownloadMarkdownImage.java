package code.java;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadMarkdownImage {
    public static void main(String[] args) {
        String filePath = "D:/my_library/linux/linux文件权限.md"; // 指定md文件路径
        String dirPath = "D:/my_library/resources"; // 指定目录路径
        // 创建正则表达式匹配Markdown中的图片地址
        Pattern pattern = Pattern.compile("!\\[[^\\]]*\\]\\((?<url>[^\\)]+)\\)");
        try {
            // 读取md文件内容
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            String markdown = sb.toString();
            reader.close();
            Matcher matcher = pattern.matcher(markdown);
            // 下载并保存匹配到的图片地址，并替换对应的Markdown中的图片地址
            while (matcher.find()) {
                String imageUrl = matcher.group("url");
                String localImagePath = saveImageToPath(imageUrl, dirPath); // 下载并保存图片，返回本地图片地址
                markdown = markdown.replace(imageUrl, localImagePath); // 替换Markdown中的图片地址
            }
            // 将替换后的Markdown内容写入文件中
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(markdown);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载并保存图片到指定目录
     * 
     * @param imageUrl 图片URL地址
     * @param dirPath  保存图片的目录路径
     * @return 本地图片地址
     * @throws IOException
     */
    public static String saveImageToPath(String imageUrl, String dirPath) throws IOException {
        URL url = new URL(imageUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(5000);
        InputStream inputStream = conn.getInputStream();
        String fileName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1); // 获取图片地址中的最后一部分作为图片名称
        String localImagePath = dirPath + "/" + fileName; // 本地图片地址
        FileOutputStream fos = new FileOutputStream(localImagePath);
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }
        fos.close();
        inputStream.close();
        return localImagePath;
    }
}