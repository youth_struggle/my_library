package code.java;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFileLine {

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream("C:\\Users\\26802\\Desktop\\新建 文本文档.txt"), "UTF-8"))) {
            String line;
            int i = -1;
            int n = -1;
            while ((line = br.readLine()) != null) {
                if (line.contains("识别库")) {
                    line = line.replaceAll("\\(\\d+\\)", "(" + i + ")");
                    i++;
                } else if (line.contains("数字")) {
                    line = line.replaceAll("\\(\\d+\\)", "(" + n + ")");
                    n++;
                }
                // 处理每行数据
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
