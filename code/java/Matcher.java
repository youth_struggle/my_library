package code.java;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.Map;

public class Matcher {
    public static void main(String[] args) {
        LocalDateTime startDateTime = LocalDateTime.of(2022, 1, 1, 0, 0, 0);
        LocalDateTime endDateTime = LocalDateTime.of(2022, 1, 10, 0, 0, 0);
        Map<LocalDate, int[]> map = new LinkedHashMap<>();
        LocalDate currentDate = startDateTime.toLocalDate();
        while (!currentDate.isAfter(endDateTime.toLocalDate())) {
            int[] hours = new int[24];
            LocalDateTime currentDateTime = LocalDateTime.of(currentDate, LocalTime.MIDNIGHT);
            while (!currentDateTime.isAfter(endDateTime)) {
                int hour = currentDateTime.getHour();
                hours[hour]++;
                currentDateTime = currentDateTime.plus(1, ChronoUnit.HOURS);
            }
            map.put(currentDate, hours);
            currentDate = currentDate.plus(1, ChronoUnit.DAYS);
        }
        System.out.println("Map: " + map.toString());
    }
}
