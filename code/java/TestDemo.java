package code.java;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\xiechao\\Desktop\\output.txt");
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream("C:\\Users\\xiechao\\Desktop\\test.txt"), "UTF-8"))) {
            String line;
            int i = 35;
            int n = 165;
            while ((line = br.readLine()) != null) {
                String[] arr = line.split(",");
                StringBuilder app = new StringBuilder();
                for (String str : arr) {
                    if (str.contains("'Oth")) {
                        str = String.format("('Oth%03d'", i);
                        i++;
                    } else if (str.equals(" 165")) {
                        str = n + "";
                        n++;
                    }
                    app.append(str).append(",");
                }
                bw.write(app.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bw.close();
            fw.close();
        }
    }
}
