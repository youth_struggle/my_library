package code.java;

public class SortDemo {

    public static void main(String[] args) {
        int[] arr = { 10, 6, 13, 22, 99, 3, 6, 4, 3, 1, 87, 32, 89 };
        // System.out.println(Arrays.toString(bubble(arr)));
        // System.out.println(Arrays.toString(choose(arr)));
        // System.out.println(Arrays.toString(quick(arr, 0, arr.length - 1)));
        int[] list = { 1, 2, 3, 4, 5, 6, 7 };
        
        System.out.println(binary(list, 7, 0, arr.length - 1));

    }

    /*
     * 冒泡算法：
     * 
     * 比较相邻的元素。如果第一个比第二个大，就交换它们两个；
     * 对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对，这样在最后的元素应该会是最大的数；
     * 针对所有的元素重复以上的步骤，除了最后一个；
     * 重复步骤1~3，直到排序完成。
     */
    public static int[] bubble(int[] list) {
        int size = list.length;
        int tem = 0;
        for (int i = size - 1; i > 0; i--) {
            for (int j = 0; j < size - 1; j++) {
                if (list[j] > list[j + 1]) {
                    tem = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = tem;
                }
            }
        }
        return list;
    }

    /*
     * 选择排序：
     * 
     * 在未排序序列中找到最小（大）元素，存放到排序序列的起始位置
     * 从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。
     * 重复第二步，直到所有元素均排序完毕。
     */

    public static int[] choose(int[] list) {
        int length = list.length;
        int tem = 0;
        int index = 0;

        for (int i = 0; i < length; i++) {
            tem = list[i];
            boolean is = false;
            for (int j = i + 1; j < length; j++) {
                if (tem > list[j]) {
                    tem = list[j];
                    index = j;
                    is = true;
                }
            }
            // 值做交换
            if (is) {
                list[index] = list[i];
                list[i] = tem;
            }
        }
        return list;
    }

    /*
     * 插入排序：
     * 把待排序的数组分成已排序和未排序两部分，初始的时候把第一个元素认为是已排好序的。
     * 从第二个元素开始，在已排好序的子数组中寻找到该元素合适的位置并插入该位置。
     * 重复上述过程直到最后一个元素被插入有序子数组中。
     */
    public static int[] insert(int[] list) {
        int length = list.length;
        int tem = 0;
        for (int i = 0; i < length - 1; i++) {
            int j = i;
            while (j > 0 && list[j] > list[j + 1]) {
                tem = list[j];
                list[j] = list[i + 1];
                list[j + 1] = tem;
                j--;
            }
        }
        return list;
    }

    /*
     * 快排：
     * 从数列中挑出一个元素，称为"基准"（pivot），
     * 重新排序数列，所有比基准值小的元素摆放在基准前面，所有比基准值大的元素摆在基准后面（相同的数可以到任何一边）。在这个分区结束之后，
     * 该基准就处于数列的中间位置。这个称为分区（partition）操作。
     * 递归地（recursively）把小于基准值元素的子数列和大于基准值元素的子数列排序。
     */

    private static int[] quick(int[] arr, int low, int high) {
        if (low >= high)
            return arr;
        int pivot = partition(arr, low, high); // 将数组分为两部分
        quick(arr, low, pivot - 1); // 递归排序左子数组
        quick(arr, pivot + 1, high); // 递归排序右子数组
        return arr;
    }

    private static int partition(int[] arr, int low, int high) {
        int pivot = arr[low]; // 基准
        while (low < high) {
            while (low < high && arr[high] >= pivot)
                --high;
            arr[low] = arr[high]; // 交换比基准大的记录到左端
            while (low < high && arr[low] <= pivot)
                ++low;
            arr[high] = arr[low]; // 交换比基准小的记录到右端
        }
        // 扫描完成，基准到位
        arr[low] = pivot;
        // 返回的是基准的位置
        return low;
    }

    public static int binary(int[] list, int val, int left, int right) {
        if (left == right)
            return left;

        int index = (left + right) / 2;
        if (list[index] > val) {// 查左方
           return binary(list, val, 0, index - 1);
        } else if (list[index] == val) {
            return index;
        } else {// 查右方
            return binary(list, val, index + 1, right);
        }
    }
}
