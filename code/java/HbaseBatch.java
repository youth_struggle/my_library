package code.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.security.User;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseBatch {
    
    public static void main(String[] args) {
        batchInsertFromCsv();
    }

    /*
     * 读取csv文件，批量插入到hbase中
     */
    public static void batchInsertFromCsv() {
        // 表名
        String HBASE_TABLE_NAME = "DTSW:dm_wirelesscover_gridmr_rsrp_d";
        // 列族
        String COLUMN_FAMILY = "cf1";
        // 每次批量插入的数量
        int BATCH_SIZE = 500;
        // 读取文件路径
        String csvFilePath = "C:\\Users\\xiechao\\Desktop\\mr栅格\\dm_wirelesscover_gridmr_rsrp_hbase_d.csv";
        // 创建hbase的配置
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "172.18.0.27:2181");
        conf.set("hbase.rootdir", "/hbase");
        conf.set("zookeeper.znode.parent", "/hbase");
        conf.set("hbase.user", "mr");
        // hbase使用的用户名
        UserGroupInformation userGroupInformation = UserGroupInformation.createRemoteUser(conf.get("hbase.user"));

        try (Connection connection = ConnectionFactory.createConnection(conf, User.create(userGroupInformation));
                Table table = connection.getTable(TableName.valueOf(HBASE_TABLE_NAME));
                BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
            String line;
            List<Put> putList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                String rowKey = values[0];
                Put put = new Put(Bytes.toBytes(rowKey));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY), Bytes.toBytes("value"), Bytes.toBytes(values[1]));
                putList.add(put);
                if (putList.size() > 0 && putList.size() % BATCH_SIZE == 0) {
                    table.put(putList);
                    System.out.println("插入数据条数：" + putList.size());
                    putList.clear();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
