package code.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {
       List<String> list= new ArrayList<String>();
       list.add("str");
       list.add("str1");
       list.add("str2");
       list.add("str3");
       List<String> collect = list.stream().filter(e->e.equals("str")).collect(Collectors.toList());
       System.out.println(Arrays.toString(collect.toArray()));
    }

    public void test() {
        String sql = "SELECT round( CAST ( radio_succconn_rate_qci1 AS NUMERIC )/radio_succconn_rate_qci1_xcd * 100, 2 ), radio_succconn_rate_qci1_xcd FROM my_table";
        String pattern = "\\b[a-zA-Z_][a-zA-Z0-9_]*(?=[^()]*\\))";
        Pattern p = Pattern.compile(pattern);
        java.util.regex.Matcher m = p.matcher(sql);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String columnName = m.group();
            m.appendReplacement(sb, columnName.replace("radio_succconn_rate_qci1", "radio_succconn_rate_qci1_agg"));
        }
        m.appendTail(sb);
        String newSql = sb.toString();
        System.out.println(newSql);
    }
}
