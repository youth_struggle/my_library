package code.java;

public class ThreadDemo {
    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();

        Thread a = new Thread(() -> {
            synchronized (lock1) {
                System.out.println("a线程1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock2) {
                    System.out.println("a线程2");
                }
            }

        });

        Thread b = new Thread(() -> {
            synchronized (lock2) {
                System.out.println("b线程1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock1) {
                    System.out.println("b线程2");
                }
            }

        });

        a.start();
        b.start();

        // try {
        // a.join();
        // b.join();
        // } catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
    }
}
