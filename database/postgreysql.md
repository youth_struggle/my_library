## 相关函数
### coalesce：函数用于返回第一个非空表达式的值。如果所有表达式都为空，则 COALESCE 函数返回 NULL

  ~~~ sql
  COALESCE(expression1, expression2, ..., expressionN)
  ~~~

### nullif：如果这两个参数的值相等，则返回`NULL`，否则返回第一个参数的值。

  ~~~ sql
  NULLIF(expression1, expression2)
  ~~~

### 创建函数，判断字符串是否为数值

  ~~~ sql
  CREATE OR REPLACE FUNCTION "public"."is_numeric"("txtstr" varchar)
      RETURNS "pg_catalog"."bool" AS $BODY$BEGIN
      RETURN txtStr ~ '^([0-9]+[.]?[0-9]*|[.][0-9]+)$' ;
  
  END
  $BODY$
      LANGUAGE plpgsql VOLATILE
                       COST 100;
  ~~~

### 使用正则判断字段是否为数值

  ~~~ sql
  select '123131' ~ '^([0-9]+[.]?[0-9]*|[.][0-9]+)$'; #返回true
  ~~~

### 获取当前时间的前一天
  ~~~ sql
  select (current_date - interval '1 day')  # 格式：yyyy-MM-dd HH:mm:ss
  select (current_date - interval '1 day')::date  # 格式：yyyy-MM-dd
  ~~~

### 将日期2023-03-06转换成 3-6这种格式
  ~~~ sql
  to_char( cast( '2023-03-06' AS date ), 'fmmm-fmdd' )
  ~~~

### 场景：统计城市维度下，投诉的用户数，其中一个字段是记录了投诉用户的电话号码使用|分隔
  ~~~ sql
    select city_id                 cityId,
           count(distinct msisdn)  complaintUserNums,
    from dm_lte_user_city_stat_d
                cross join
            lateral unnest(string_to_array(complaint_msisdn, '|')) as msisdn
    where p_date >= '2023-09-25'
         and p_date <= '2023-09-25'
    group by city_id
  ~~~
### 统计非连续时间段，指标的周环比
  > 非连续时间段，需要使用一张临时表，这张表时间连续的，然后关联这张表，使用窗口函数来计算环比
  ~~~ sql
    select 
        "date",
        round(("currentNums"-"previousNums")*100.0/"previousNums") "ratio"
    from
    (select 
    t1."date",
    t2."currentNums",
    lag(t2."currentNums") over(order by t2."date") "previousNums"
    from
    (select generate_series('开始时间','结束时间','1 week') "date") t1
    left join dm_lte_user_area_stat_w t2 on t1."date"=t2."date") t
  ~~~

### 分组查询后，对结果中未分组的字段通过逗号相隔，聚合成一个字段
  ~~~ sql
  SELECT group_id, string_agg(name, ',') AS names
  FROM your_table
  GROUP BY group_id
  ~~~

### 对分组查询结果中，未分组字段去重后使用逗号拼接
  ~~~ sql
  SELECT group_id, array_to_string(array_agg(distinct name), ',') AS names
  FROM your_table
  GROUP BY group_id
  ~~~

### 插入数据时忽略主键冲突（冲突的数据不插入）
  ~~~ sql
  insert ignore into my_table(id,age) values (1,2)
  ~~~

### 将date(2023-09-12)和hour(3)组装成(2023-09-12 03:00:00)，使用0填充缺省位
~~~ sql
select p_date || ' ' || lpad(p_hour :: varchar,2,'0')
~~~

## postgis相关
### 什么是几何对象空间参考标识符（SRID）
> 是一个用于唯一标识空间参考系统的整数值或字符串,空间参考系统定义了一组坐标系统和转换规则。如经纬度坐标系（WGS84，SRID 4326），投影坐标系（UTM，SRID 32618）等


### 判断点面相交
~~~ sql
select * from geom_table where st_intersects(st_geomfromtext(concat('point(', 81.3897672787959, ' ', 40.4184700867551, ')'), 4326), st_geomfromtext(st_astext(geom),4326))
~~~
> st_astext：将geom类型数据转换成字符类型表示方式。如point(1,2)  
> st_geomfromtext：将文本表示的几何对象转换为几何类型。它接受一个带有几何信息的文本字符串作为输入，并返回相应的几何对象。  
> st_intersects：用于判断两个几何对象是否相交


### 重设表的自增主键
~~~ sql
select setval('"dm_dqm_warning_list_id_seq"',(select max(id) from dm_dqm_warning_list));
-- dm_dqm_warning_list为表名，id为自增的键
~~~


### 取指定时间所在周的最后一天
~~~ sql
select to_char(to_date(to_char('2024-03-07'::date,'yyyyww'),'yyyyww') + interval '6 day','yyyy-MM-dd');
~~~

### 查询天、周、月、季、年维度范围条件
~~~ xml
<choose>
    <when test="param.dateType=='day'">
        and create_time between #{param.startDate} and #{param.endDate}
    </when>
    <when test="param.dateType=='week'">
        and to_char(create_time::date,'yyyy-ww') between #{param.startDate} and to_char(#{param.endDate}::date - interval '13 week','yyyy-ww')
    </when>
    <when test="param.dateType=='month'">
        and to_char(create_time::date,'yyyy-mm') between #{param.startDate} and to_char(#{param.endDate}::date - interval '12 month','yyyy-ww')
    </when>
    <when test="param.dateType=='quarter'">
        and to_char(create_time::date,'yyyy-q') between #{param.startDate} and to_char(#{param.endDate}::date - interval '3 month','yyyy-q')
    </when>
    <when test="param.dateType=='year'">
        and to_char(create_time::date,'yyyy') between #{param.startDate} and to_char(#{param.endDate}::date - interval '9 year','yyyy')
    </when>
</choose>
~~~