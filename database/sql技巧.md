# 1. left join左连接
* 左连接主表时，是以主表的数据为主，从表只是补充一些信息内容，所以关联的从表不能去影响主表的数据条数（假设主表有5条数据，不能因为左连接关联从表而变成8条数据），这就要保证左连接的on条件，在从表中是唯一的（比如on a.bank_no=b.bank_no and a.ods_date=b.ods_date,那么bank_no和ods_date组合在从表中是唯一的索引）。

# 2. mysql的深度分页
~~~ sql
select name,code from student limit 1000000,20
~~~
这个时候，mysql 会查出来 1000020 条数据，抛弃 1000000 条，如此大的数据量，速度一定快不起来。

优化：
~~~ sql
select name,code from student where id>1000000  limit 20
~~~
这样，mysql 会走主键索引，直接连接到 1000000 处，然后查出来 20 条数据



##  3. 统计比率问题

统计表中的两种条件的总数的比例可以使用下面的写法。这样得到的结果是错误的，应为int/int 结果还是int型

```sql
select count(case when name='tom' then 1 end)/count(name) as rate
from 'table_name'
```

## 4. 查看mybatis在执行过程中，生成的动态sql和参数映射
>> 可以到org.apache.ibatis.executor.CachingExecutor.query方法，找到BoundSql boundSql = ms.getBoundSql(parameterObject);这句代码查看。