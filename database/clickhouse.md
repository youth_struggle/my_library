# clickhouse、ck

### 在clickhouse中多数据源查询
~~~ sql
CREATE table dm_loadanalysis_predict_cell_confidence_stat_d ON CLUSTER cluster_2shard_2replicas
    ENGINE = PostgreSQL('172.18.2.41:5732', 'wnos','dm_loadanalysis_predict_cell_confidence_stat_d', 'postgres', 'DITO@-Pg-2022!311');
~~~

### 在ck上创建表例子
~~~ sql
create table aggr_predict_highload_cell_d(
    id integer,
    name varchar(255)
    money double
) 
engine = MergeTree() -- 使用默认存储引擎，适合批量插入、查询
partition by p_date -- 设置分区字段
order by (predict_date);-- 排序字段
~~~

### ck获取当前时间的前一天
~~~ sql
toString(toDate(now()- interval 1 day))
~~~

### 将日期2023-03-06转换成 3-6这种格式
  ~~~ sql
  concat(toString(toMonth(toDate('2023/1/2'))),'-',toString(toDayOfMonth(toDate('2023/1/2'))))
  ~~~

### 当ck中两张表关联查询时，如果查询字段在两张表中都有，但是未指定是哪张表，则ck默认以第一张关联表的字段为主，此场景只适用于两张表。
~~~ sql
select id, cell_name, area_name, city_name, associated, p_date -- 默认以a表的字段为主
from demo_join_a a
         inner join demo_join_b b on a.associated = b.associated;
~~~

