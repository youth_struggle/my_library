## HBase逻辑存储结构

![img](https://pic2.zhimg.com/v2-cf4903a44a73a7faa52c8f2857e367ed_r.jpg)

如上图，最左边的row_key是系统自带的一列，name、city等列是自己创建的，可以将多个列按照某个规则划分到列族，如上图中将个人信息列的name、city、phone划分到personal_info列族，tel、address等列存储到office_info列族。所以不同列可以划分到不同的列族。同理，不同的row_key也可以划分到不同的Region中。

> 列族是竖着划分的，region是横着划分的。Region是Hbase中分布式存储和负载均衡的最小单元。Hbase中的表是根据RowKey的值竖直分割成所谓的region。一个hbase表由一个或多个region组成。当数据量达到阈值时，一个region会分裂成多个新的region。Hbase支持多种切分触发策略。

## HBase物理存储结构

![img](https://pic3.zhimg.com/v2-216751979c2994ce5f31547d768d9e92_r.jpg)

如上图所示，在实际存储时，每个row_key和列的交集位置的数据在存储时是按行存储的，如红框中的张三在存储时需要指定行键(row*_*key)、column_family(列族)*、*column_qualifier(列名)、时间戳(TimeStamp)、类型(Type)、具体值(value)。而逻辑结构中的空单元格，底层实际根本不存储。

> 如果要**修改**数据，比如手机号变动，需要提交更新的时间戳，和对应的数据，如上图中t4比t3更接近当前时刻，说明t4时刻提交了新数据，**查询**时返回t4时刻的数据就好。
>
> 类型是put表明该数据是提交来的，而当**删除**数据时，类型是delete，此时在查询时，如果类型del对应的时间戳比put对应的时间戳更新，说明该数据已经删除。

上面的两条，既然有了新数据或者已经标记删除了数据，那么原来的数据不应该继续存在，hbase有合并机制。

## HBase创建表
~~~ sql
create 'table_name','cf','cf1'; -- 创建列族名为cf、cf1的表
create 'table_name',{name=>'cf',version=>1} -- 创建的表每个单元格的版本数为1
-- 在HBase中，列名是动态的，你可以在插入数据时指定新的列名，而不需要提前在表定义中指定所有的列名
put 'my_table', 'row1', 'cf:col1', 'value1'
~~~ 

### HBase常用命令

```sql
# 查询指定表的建表语句
describe 'table_name'
# 查询hbase中的所有表
list
# 扫描表中所有数据
scan 'table_name'
# 获取指定表指定rowkey数据
get 'my_table', 'rowkey'
# 前缀匹配扫描rowkey
scan 'my_table', {ROWPREFIXFILTER => 'prefixRowkey',LIMIT=>30}
# 向hbase插入或修改数据
put 'my_table', 'row1', 'cf:col1', 'value1'
# 删除指定rowkey下所有列族和列限定符的数据
deleteall 'my_table', 'row1'
# 删除指定rowkey下指定列族的所有列限定符的数据
delete 'my_table', 'row1', 'cf'
```

### spring boot封装hbase的常见api有哪些，分别有什么作用

- put(String tableName, Put put)：向指定的表中插入一行数据。

- put(List<Put> puts)：向指定的表中批量插入多行数据。

- get(String tableName, Get get)：根据指定的行键获取一行数据。

- get(String tableName, Get get, RowMapper<T> mapper)：根据指定的行键获取一行数据，并使用指定的RowMapper将数据映射为对象。

- get(String tableName, List<Get> gets)：根据指定的多个行键获取多行数据。

- get(String tableName, List<Get> gets, RowMapper<List<T>> mapper)：根据指定的多个行键获取多行数据，并使用指定的RowMapper将数据映射为对象列表。

- delete(String tableName, Delete delete)：删除指定表中的一行数据。

- delete(String tableName, List<Delete> deletes)：删除指定表中的多行数据。

- exists(String tableName, Get get)：判断指定行键的数据是否存在。

- exists(String tableName, List<Get> gets)：判断指定多个行键的数据是否存在。

- find(String tableName, Scan scan, RowMapper<T> mapper)：根据指定的扫描条件查询数据，并使用指定的RowMapper将数据映射为对象。

- find(String tableName, Scan scan, ResultsExtractor<T> action)：根据指定的扫描条件查询数据，并使用指定的ResultsExtractor处理查询结果。

- execute(String tableName, TableCallback<T> action)：在指定的表上执行自定义的操作，并返回操作结果。
除了以上常用的API接口，HbaseTemplate还提供了其他一些用于管理HBase表结构和连接的方法，如createTableIfNotExists、deleteTable、getConnection等。
通过HbaseTemplate提供的API接口，我们可以方便地进行HBase数据库的增删改查操作，同时还能够利用Spring的特性进行事务管理和异常处理。


