## 1. 相关子查询
查询语句有两个，一个是父类即外层sql，一个是子类即条件sql。父类查询结果中一个列属性作为参数，作为子类查询sql中的查询条件，子类查询结果的列作为父类sql查询条件。

>列举：查询本部门最高工资的员工
~~~ sql
select * from emp e where sal>=(select max(sal) from emp where deptno=e.deptno) order by deptno
~~~

## 2. mysql的事务隔离级别
举列子：事务T1和事务T2两个事务并发执行
1. read unconmmitted 读未提交：事务中的修改对其他事务是可见的，如T1事务操作了修改了数据但未提交事务，此时T2事务可以看见事务T1修改的数据。
2. read conmmitted 读已提交：oracle默认此级别，事务T1修改了数据但未提交，事务T2读取了这条数据，但数据是原来并未修改的值。
3. repeatable read 重复读：Mysql默认此级别，事务T1修改数据事务T2读取数据，无论事务T1是否提交，事务T2读取的数据都是一样的。
4. serializable 串行化：使事务串行执行，即事务T1和事务T2按照先后依次执行。
>**脏读**：一个事务读取了另一个事务未提交的数据。（读未提交）  

>**不可重复读**：一个事务内多次读取同一条数据结果都不一样（事务A读取数据事务B修改的数据，提交前和提交后数据不一致）。
幻读：一个事务读取到另一个事务修改的数据，造成前后读取不一样。

>[mysql可重复读实现机制](https://zhuanlan.zhihu.com/p/166152616)

## 3. mysql忘记密码
~~~ sql
1. 停掉服务：service mysql stop
2. 跳过权限表：mysqld_safe --skip-grant-tables &
3. 可以直接登录了
~~~
## 4. 设置远程登录权限
~~~ sql
1. use mysql;
2. select host, user, authentication_string, plugin from user; 
3. GRANT ALL ON *.* TO 'root'@'%' identified by "123456";
4. flush privileges;
~~~

