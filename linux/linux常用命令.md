## 将文件夹复制到指定目录
~~~ shell
cp -r /www/wwwwroot/kodbox/files /www/wwwwroot/kodbox/data
~~~

## 在当前目录查找指定文件
~~~ shell
find . -name "*.txt" -o -name "*.pdf"
~~~

## 查看网卡驱动
~~~ shell
nmcli device status
~~~

## 重启网络配置
~~~ shell
service network restart
~~~

## 修改linux主机名
~~~ shell
vi /etc/hostname
# 在文件中输入主机名，如：node01
~~~

## linux基础命令失效
~~~ shell 
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
~~~