# 1、exFAT格式无法在linux上挂载
[安装教程](https://blog.csdn.net/weixin_46396833/article/details/122091828)
依次执行以下命令：
~~~ shell
sudo yum install epel-release
sudo rpm -v --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
sudo rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
sudo yum install exfat-utils fuse-exfat
~~~
# 2、挂载USB磁盘到系统
~~~ shell
fdisk -l # 查看usb是否被识别
lsblk # 查看u盘数据位置
mount /dev/sdb1 /home/data # 将sdb1挂载到对应的目录
umount /home/date # 卸载挂载点
~~~

# 3. 将两个分区合成一个
[教程](https://blog.csdn.net/d1240673769/article/details/113999873)
~~~ shell
fdisk -l # 查看所有分区
df -lh # 查看磁盘分区挂在情况
df -T # 查看磁盘的文件系统类型
# 创建pv
pvcreate /dev/sdb #硬盘1
pvcreate /dev/sdc #硬盘2
# 创建vg
vgcreate lvm_data /dev/sdb
# 扩展vg
vgextend lvm_data /dev/sdc
# 创建lv
# lvcreate -l[自定义分区大小] -n[自定义分区名称] [vg名称]
# lvcreate -l 10.0G -n vg_data lvm_data # 创建10G的分区
lvcreate -l 100%VG -n vg_data lvm_data
# 格式化分区
mkfs -t ext4 /dev/lvm_data/vg_data
# 挂载分区
mount /dev/lvm_data/vg_data /data
# 卸载挂载分区
umount /data
# 设置开机加载
在/etc/fstab文件末尾添加如下行：
/dev/lvm_data/vg_data	/data 	ext4	defaults	0 0
# 杀死占用改目录的所有进程序
fuser -km 目录
~~~
