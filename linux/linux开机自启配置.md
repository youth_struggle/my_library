1. 在/usr/lib/systemd/system 目录下新建一个文件，此处以frp为例，文件名为frp.service。
2. 文件的内容为：其中的ExecStart的值为你的程序和启动时的配置。
3. 设置开启自启动：systemctl daemon-reload 、systemctl enable frp。
4. 启动程序：systemctl start frp。
5. 查看frp是否启动：ps aux | grep frp。
   
~~~ shell
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/home/frpc/frpc -c /home/frpc/frpc.ini
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true
StandardOutput=syslog
StandardError=inherit

[Install]
WantedBy=multi-user.target
~~~

## linux连接wifi设置

```shell
sudo nmcli device wifi list # 扫描wifi
sudo nmcli device wifi connect [SSID] password [password] # 连接wifi
nmcli connection show --active # 查看当前wifi连接情况
```

