# 1. 容器的使用
~~~ shell
# 拉取镜像
docker pull ubuntu
# 运行容器,-i:交互式操纵，-t:终端。使用exit推出容器终端
docker run -it ubuntu /bin/bash
# 后台运行容器，-d:指定容器的运行模式
docker run -itd --name ubuntu-test ubuntu /bin/bash
# 进入指定的容器,退出不会停止容器
docker exec -it 容器id /bin/bash
# 将docker容器中的文件拷贝出来
docker cp 容器id:容器内文件路径 目标路径
# 容器启动、停止、重启
docker stop <容器 ID>
docker start <容器 ID> 
docker restart <容器 ID>
docker rm -f <容器 ID>
# 容器导入导出，导入可以是目录或者url
docker export 1e560fca3906 > ubuntu.tar
docker import http://example.com/exampleimage.tgz example/imagerepo
# 查看容器的标准日志，实时查看
docker logs -f <容器ID>
# 在运行的容器内使用 apt-get update 命令进行更新。
apt-get update
# 查看容器
docker ps [options]  
-a :显示所有的容器，包括未运行的。

-f :根据条件过滤显示的内容。

--format :指定返回值的模板文件。

-l :显示最近创建的容器。

-n :列出最近创建的n个容器。

--no-trunc :不截断输出。

-q :静默模式，只显示容器编号。

-s :显示总的文件大小。
# 查看容器运行实时情况
docker stats
# 启动容器时将容器和宿主机使用相同的网络环境，这样容器里的应用不用单独做端口映射,只需在启动命令中加下面参数
--net host
~~~
# 2. 镜像的使用
~~~ shell
# 在docker hub上搜索镜像
docker search httpd
~~~
## 2.1 镜像构建
1. 创建Dockerfile，具体文件参考网络
2. 通过命令docker bulid -t <容器名> <Dockerfile路径>
3. 设置镜像标签docker tag <镜像id> <标签名>
# 3. docker run 时的一些参数解释
-d:后台运行

-p:将容器内部使用的网络端口随机映射到我们使用的主机上。例：0.0.0.0:3306->3306，前面的是服务器的ip:port，后一个是docker里面的端口

-t: 终端，支持终端登录

-i: 交互式操作。

--name：为容器命名

-h：设定容器的主机名，它会被写到容器内的 /etc/hostname 和 /etc/hosts。

-v：给容器挂载存储卷，挂载到容器的某个目录。例：-v `pwd`:/root。前面的是宿主机（服务器）的目录，后一个是容器中的目录。