~~~ shell
    cd /etc/sysconfig/network-scripts/
    ifconfig #查看当前使用到的网卡
    vi ifcfg-wlp2s0 # 根据上一步查看的网卡，编辑对于的配置，如果没配置，可以复制一份其他网卡的配置，并修改成对应网卡的名称
    systemctl restart network
~~~
ifcfg-enp3s0文件内容,wifi一般修改ifcfg-wifi名称的文件
~~~ shell
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static #修改为静态
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp3s0
UUID=0732b90b-4618-419a-86d0-61a6ae038a1f # 如果是复制的文件则这里的id要保证唯一
DEVICE=enp3s0
ONBOOT=yes #是否开启网卡

# 以下是新增内容
IPADDR=192.168.1.11 # 静态内网ip
NETMASK=255.255.255.0 # 子网掩码
GAETWAY=192.168.1.1 # 这个ip是路由器的ip
DNS1=114.114.114.114 # dns
~~~
