## hadoop集群安装
> 对于大数据集群提前进行主机名、IP地址、主次服务的规划：NN（nameNode）DN(datanNode) SN(secondNameNode)   
node01 ：192.168.199.80 NN DN  
node02 ：192.168.199.81 DN  
node03 ：192.168.199.82 DN SN  

### 常用软件安装
~~~ shell
sudo yum install net-tools
sudo yum install vim
sudo vim /etc/vimrc # 使vim命令的tab键是4个空格
# 添加  set ts=4
~~~

### 系统准备
1. 关闭防火墙
~~~ shell
sudo systemctl stop firewalld.service
sudo systemctl disable firewalld.service
~~~

2. 关闭selinux
~~~ shell
sudo vim /etc/selinux/config
# 修改为 SELINUX=disabled
~~~

3. 上传安装包
~~~ shell
sudo rpm -ivh jdk-8u281-linux-x64.rpm
sudo tar -zxvf hadoop-2.7.1.tar.gz -C /opt/
~~~

4. 添加环境变量
~~~ shell
sudo vim /etc/profile.d/hadoop.sh # 顺便创建文件
# export HADOOP_HOME=/opt/hadoop-2.7.1
# export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
source /etc/profile.d/hadoop.sh # 生效
~~~

5. 改变hadoop文件权限（如果不是在root用户下搭建）
~~~ shell
cd /opt
sudo chown -R huser:huser /opt/hadoop-2.7.1
~~~

6. 创建HDFS的NN和DN工作主目录
~~~ shell
sudo mkdir /var/big_data
sudo chown -R huser:huser /var/big_data
~~~

### hadoop系统配置
> hadoop的配置都在安装目录的etc下

1. 为Hadoop提供JAVA解释器路径信息，主要目的是解决远程访问hadoop时候JAVA_HOME无法继承的问题
~~~shell
vim hadoop-env.sh
# 添加 export JAVA_HOME=/usr/java/default
# export HDFS_NAMENODE_USER=root
# export HDFS_DATANODE_USER=root
# export HDFS_SECONDARYNAMENODE_USER=root
# export YARN_RESOURCEMANAGER_USER=root
# export YARN_NODEMANAGER_USER=root
~~~

2. 为Yarn任务、资源管理器提供Java运行环境
~~~shell
vim yarn-env.sh
# export JAVA_HOME=/usr/java/default
~~~

3. core-site.xml：配置HDFS主节点信息、持久化和数据文件的主目录
~~~shell
vim core-site.xml
# <configuration>
#	<property>
#        <name>fs.defaultFS</name>
#        <value>hdfs://node01:9000</value>
#    </property>
#	<property>
#        <name>hadoop.tmp.dir</name>
#        <value>/home/bigdata</value>
#    </property>
#    <property>
#        <name>hadoop.http.staticuser.user</name>
#        <value>root</value>
#    </property>
#</configuration>
~~~

4. hdfs-site.xml：配置HDFS默认的数据存放策略
~~~ shell
vim hdfs-site.xml
#<configuration>
#	<property>
#        <name>dfs.replication</name>
#        <value>2</value>
#    </property>
#	<property>
#        <name>dfs.namenode.secondary.http-address</name>
#        <value>node03:50090</value>
#    </property>
#    <property>
#      <name>dfs.webhdfs.enabled</name>
#      <value>true</value>
#    </property>
#</configuration>
~~~

5. mapred-site.xml：配置mapreduce任务调度策略
~~~ shell
vim mapred-site.xml
#<configuration>
#	<property>
#        <name>mapreduce.framework.name</name>
#        <value>yarn</value>
#    </property>
#	<property>
#	    <name>yarn.app.mapreduce.am.env</name>
#	    <value>HADOOP_MAPRED_HOME=$HADOOP_HOME</value>
#	</property>
#	<property>
#	    <name>mapreduce.map.env</name>
#	    <value>HADOOP_MAPRED_HOME=$HADOOP_HOME</value>
#	</property>
#	<property>
#	    <name>mapreduce.reduce.env</name>
#	    <value>HADOOP_MAPRED_HOME=$HADOOP_HOME</value>
#	</property>
#</configuration>
~~~

6. 配置datanode节点信息
~~~shell
vim workers
# node01
# node02
# node03
~~~

### 修改集群中主机host
~~~ shell
sudo vim /etc/hosts
# 192.168.199.80  node01
# 192.168.199.81  node02
# 192.168.199.82  node03
# 注意屏蔽或删除上面的127.0.0.1的信息
~~~

### 克隆虚拟机
> 克隆后，修改node02、node03的IP和主机名
~~~shell
sudo vim /etc/sysconfig/networ-scripts/ifcfg-ens33
# 修改为静态ip
# BOOTPROTO="static"
# ONBOOT=yes 
# IPADDR="xxx.xxx.xxx.xxx"
# NETMASK="255.255.255.1"
# GATEWAY="192.168.1.1"
# DNS1="8.8.8.8"
sudo systemctl restart network # 重置网络
sudo vim /etc/hostname # node01、node02、node03
~~~

### 配置集群的ssh免密
在3台机器上执行产生自己的公钥：
~~~shell
ssh-keygen -t rsa
ssh-copy-id node01
ssh-copy-id node02
ssh-copy-id node03
~~~

### 启动hadoop,在node01上
1. 格式化hdfs
~~~shell
hdfs namenode -format
start-all.sh
stop-all.sh
~~~

## sprk集群部署

1. 上传安装包
~~~shell
tar xzvf spark-2.2.0-bin-hadoop2.7.tgz
~~~

2. 修改对应配置文件
~~~ shell
cd /export/servers/spark/conf
cp spark-env.sh.template spark-env.sh
vi spark-env.sh
# export JAVA_HOME=/export/servers/jdk1.8.0
# export SPARK_MASTER_HOST=node01  //主节点
# export SPARK_MASTER_PORT=7077  //运行端口
~~~

3. 修改slaves配置文件
~~~ shell
cd /export/servers/spark/conf
vi workers
# node02
# node03
~~~

4. 分发到三台虚拟机上
5. 启动集群，在spark安装目录下
~~~ shell
sbin/start-all.sh
~~~

## 集群分发脚本

1. 创建脚本文件xsync.sh
~~~shell
#!/bin/bash

#1. 判断参数个数
if [ $# -lt 1 ]
then
        echo NOT Enough Argument!
        exit;
fi

#2. 遍历集群所有机器
for host in node2 node3 node4
do
        echo ========== $host =========
        #3. 遍历所有目录, 挨个发送
        for file in $@
        do
           #4. 判断文件是否存在
           if [ -e $file ]
                then
                    #5.获取父目录
                    pdir=$(cd -P $(dirname $file); pwd)
                    #6.获取当前文件的名称
                    fname=$(basename $file)
                    ssh $host "mkdir -p $pdir"
                    rsync -av $pdir/$fname $host:$pdir           
                else
                    echo $file does not exists!
           fi
        done
done
~~~

2. 给脚本赋权
~~~shell
chmod +x xsync.sh
~~~

3.将脚本加入到环境变量

## 同时在多台服务器上执行相同命令
~~~shell
#!/bin/bash
#在集群的所有机器上批量执行同一条命令
if(($#==0))
then
	echo 请输入您要操作的命令！
	exit
fi

echo 要执行的命令是$*

#循环执行此命令
for host in node2 node3 node4
do
	echo ---------------------$host-----------------
	ssh $host $*
done
~~~