# 参考地址
[安装教程](https://www.cnblogs.com/wqp001/p/14709997.html)
## 1.拉取镜像
aria2-pro的镜像名：p3terx/aria2-pro
ariaNg的镜像名：p3terx/ariang   # 用来管理下载的web界面

## 2.镜像启动命令

### 1. aria2镜像启动命令
~~~ shell
docker run -d \
--name aria2 \
--restart unless-stopped \
--log-opt max-size=1m \
-e PUID=$UID \
-e PGID=$GID \
-e UMASK_SET=022 \
-e RPC_SECRET=xiechao \ # rpc通信密钥
-e RPC_PORT=6800 \
-e LISTEN_PORT=6888 \
-p 16800:6800 \
-p 16888:6888 \
-p 16888:6888/udp \
-v /www/wwwroot/kodbox/data/files/storage/download:/downloads \ # 下载文件映射到服务器上的目录
p3terx/aria2-pro
~~~

### 2. ariaNg镜像启动命令
~~~ shell
docker run -d \
--name ariang \
--log-opt max-size=1m \
--restart unless-stopped \
-p 16880:6880 \
p3terx/ariang
~~~

### 3.如何配置frpc端口映射
~~~ shell
[aria2]
type = tcp
local_ip = 127.0.0.1
local_port = 16880
remote_port = 16880

[aria2_rpc]
type = tcp
local_ip = 127.0.0.1
local_port = 16800
remote_port = 16800
~~~

### 4.访问
地址：192.168.1.11:16800
![Alt text](image.png)
