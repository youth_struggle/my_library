## 什么是conda
> python环境下的一个包管理工具，类似于Java中的maven，但又别于maven。Conda允许用户创建多个独立的Python环境，在每个环境中安装和管理不同版本的软件包
### miniconda镜像源切换(windows)
- 先执行conda config --set show_channel_urls yes命令生成在c盘用户目录下的.condarc文件。如果conda命令无法使用需要配置环境变量，配置路径/conda安装路径/Scrips/
- 编辑.condarc文件，添加下面内容
~~~ yml
channels:
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/menpo/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/
  - defaults
show_channel_urls: true
~~~
- 运行命令清楚缓存 conda clean -i 