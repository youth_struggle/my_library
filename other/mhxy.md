## 数据结构设计
~~~json
{
    "name":"",
    "type":"",
    "range":"",
    "target":"",
    "delta":"",
    "dir":0,
    "sim":0.8,
    "mode":"1,2,3",
    "tap":"x+12,y+22"
}
~~~

## 代码设计
1. 创建一个**DataManarger**，负责数据拉取和数据解析，将数据解析成name为key，自身为value的数据结构
2. 创建一个找图找字工具类，更具传入数据匹配执行逻辑。