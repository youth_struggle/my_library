# git 在使用中遇到的一些问题
* 查看本地未推送的commit：git log branch_name ^origin/branch_name
* 撤销提交但是未推送的代码
  1. git reset --soft<commit id>：撤销git commit，不撤销git add，保留编辑器改动代码
  2. git reset --hard<commit id>：撤销git commit，撤销git add，删除编辑器改动代码
  3. <commit id>可以换成HEAD^(或者HEAD~1)最新一次提交。HEAD^^(或者HEAD~2)上上一个版本

## 1. 本地无法拉去代码，有冲突解决办法
1. 以服务器的版本为准，覆盖掉本地的更改 
     ~~~ shell
      git stash
      git pull
      git stash pop
     ~~~


## 2. 本地在开发新功能，但是有个bug有修改，但是在提交时只想提交修改bug的代码，在不开新分支的情况下可以使用git stash
git stash：会把所有未提交的修改（包括暂存的和非暂存的）都保存起来，用于后续恢复当前工作目录。所以在修改代码时，我们可以先将开发到一半的代码暂存起来，然后修改bug，修改完并推送后将原来的代码回复回来。
1. git stash <save "message"> #存放开发的代码，并命名
2. git stash pop #恢复之前的代码，并删除stash列表中的信息,git stash apply 应用但不删除

* git stash push -m message 文件 #暂存指定文件
* git stash list # 查看stash列表
* git stash clear #清空stash列表
* git stash drop #删除stash指定的暂存

## 3. 查看在开发过程中提交了多少代码行数
~~~ shell
git log --author=xie.chao --since=2024-04-02 --until=2024-04-27 --format='%aN' | sort -u | while read name; do echo -en "$name\t"; git log --author="$name" --pretty=tformat: --numstat | grep "\(.html\|.java\|.xml\|.properties\|.yml\)$" | awk '{ add += $1; subs += $2; loc += $1 - $2 } END { printf "added lines: %s, removed lines: %s, total lines: %s\n", add, subs, loc }' -; done
~~~

### 4. 代码已提交并推送到了远程仓库，需要回退版本如何操作。
> 如果提交了三次代码，并且已推送到了远程仓库。现在我需要将最近的两个版本的代码回退掉，只要第一次提交的代码。

~~~ shell
  git reset --hard HEAD~3
  git commit -m代码回退
  git push -f # 本地的版本低于远程仓库的版本，只能强制覆盖代码，危险操作，最新两个版本的代码将丢弃
~~~
