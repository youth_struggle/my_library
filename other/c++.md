# #define
宏定义命令，把一个标识符设置为预处理变量，用来表示一个字符串，该标识符称为“宏名”。
在编写源程序时，宏定义中的字符串都可由“宏名”去替换；而对源程序进行编译预处理时，预处理程序先将程序中所有出现的“宏名”都用宏定义中的字符串去代换，然后再进行编译。
有两种形式：
1. 带参数
#define SYSTEM    //定义一个宏名为SYSTEM,但没有值  
#define  PI  3.14159   //定义宏名PI为常数3.14159  
#define  M  (a+b)      //定义宏名M为表达式(a+b)  

2. 不带参数
#define MUL(x, y)  ((x)*(y))     
n = MUL(3,4)  // 宏调用，经预处理宏展开后的语句为n = ((3)*(4)) 

# #ifndef
条件指示符#ifndef的最主要目的是防止头文件的重复包含和编译。
例如要编写头文件test.h，在头文件开头写上两行：

#ifndef _TEST_H //此处返回为true则不在编译下面的代码  
#define _TEST_H //一般是文件名的大写  
头文件结尾写上一行：  
#endif  
这样一个工程文件里同时包含两个test.h时，就不会出现重定义的错误了。

# const char *str==char const *str
str指向的内容是个常量，站在str的角度执行**内容是常量**，不能更改内容，但是如果指向的是个字符数组，站在数组的角度是可以更改内容的。以下示例：
~~~ c++
const char* p;
char str[]="hello";
p=str;
str[1]='s';//可以更改
p[1]='s';//不能更改，该行会报错
std::cout<<p;
~~~
# char* const str
定义一个指向字符的指针常量，即str的**指向不能被修改**，但是可以修改指针所指向的内容。
~~~ c++
char str[]="hello";
char* const p=str;//在定义指针常量时必须给默认值
str[1]='s';//可以更改
p++;//不能更改，该行会报错,不能修改指向
std::cout<<p;
~~~

# char & func(const char &str)
参数传入的是一个引用，相较与值传递更加节省内存和提高效率。值传递需要复制数据然后传入到函数中，引用则不会。加上const后函数内就不能更改str的值了。同理，返回的是引用，则不用复制返回的数据，而是直接返回这个数据的引用。

# void * 指针
[参考地址](https://blog.csdn.net/W_H_M_2018/article/details/111120624)  
函数的返回值或者参数是void* 则说明返回或者参数是个任意的指针类型，相当与泛型指针

# virtual 修饰接口
表明该函数是个虚函数，可以被重写。

# inline 修饰函数
表明该函数是个内联函数，可以在头文件中直接有简单的实现。

# func(param1,param2){}: a:b 中函数表达的意思
~~~ c++
func(param1,param2){
    param1=a;
    param2=b
}
~~~

# class A : public B 的含义
表示类A继承自类B

# func() const 的含义
表明该类成员函数无法修改该类的成员属性，因此静态成员函数后面不能加const。

# CMake使用
## 关键字
- project(hello c cxx)：指定工程名为hello，并且支持语言是c和c++，默认定义了两个变量PROJECT_BINARY_DIR、PROJECT_SOURCE_DIR。
- set(SRC_LIST main.cpp t1.cpp)：用来显示的指定变量，多个用空格隔开。
- message：message(SEND_ERROR,"产生错误，生成过程被跳过")、message(STATUS,"输出前缀-的信息")、message(FATAL_ERROR,"立即终止所有cmake过程")。
- add_executable(hello,${SRC_LIST})：生成可执行文件hello，源文件读取变量SRC_LIST中的内容。

# vscode 配置c/c++运行环境
1. 需要先安装MinGW的编译器
2. vscode需要生成两个文件，tasks.json、launch.json。
   - tasks.json：ctrl+shift+p->输入配置默认生成任务->修改option.cwd属性的值的路径为MinGW/bin
   - launch.json：修改program为${fileDirname}\\${fileBasenameNoExtension}.exe，修改miDebuggerPath为MinGW/bin/gdb.exe，如果没有区MinGW目录下打开控制台执行mingw-get install gdb命令安装。


# 小记
- 头文件中的变量申明使用extern、const、static
//1.static:在头文件中使用static定义的变量，那么这个变量只试用于引用改头文件的源文件，其他引用该头文件的源文件中的变量不受其影响。
//2.extern：使用该关键字申明的变量是全局变量，作用于整个程序的生命周期。
//3.const: 保护被修饰的成员，防止意外修改，增强程序的健壮性。可以和上面的两个关键词组合使用。
- 内联函数为啥可以多次定义
- 条件编译将头文件的循环引用和多次引用解决：#ifdef、#ifndef
- 不同的头文件包含了相同函数名的不同定义，并且在同一个程序中引用，会导致函数名相同的问题。c++的解决办法是命名空间
- 内联函数、普通函数、宏函数
//1.普通函数：普通函数的调用会涉及压栈和出栈的操作，会有小的开销。但逻辑复杂的函数可以使用这种方式
//2.宏函数：直接在使用宏函数的地方进行字符的替换，编译器不对其检查。使用场景是非常简单的函数可以这样使用
//3.内联函数：也是对使用的地方直接进行函数实现的替换，但是编译器要其进行检查。场景：相对不复杂的函数，缺点可能造成代码膨胀。
- 在头文件中定义内联函数：这样多个源文件引用这个头文件时，都可以进行内联展开（预编译阶段会替换include）。关键点：内联展开之前必须要有这个函数的定义。
