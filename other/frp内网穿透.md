# frp内网穿透配置
## 1. frp的linux安装包
[linux_amd64_frp](/resources/frp_0.33.0_linux_amd64.tar.gz)

## 2. 服务端配置
~~~c
//1.在服务端解压
tar -zxvf 文件名
//2.进入解压文件夹，修改配置文件
vi frps.ini

[common]
server_addr = 127.0.0.1
bind_port = 7000 #与客户端绑定的进行通信的端口
vhost_http_port = 80 #访问客户端web服务自定义的端口号
//3.启动服端
nohup ./frps -c frps.ini &
~~~

## 3. 客户端配置
~~~ c
//1.在客户端解压
tar -zxvf 文件名
//2.进入解压文件夹，修改配置文件
vi frpc.ini

[common]
server_addr = 服务端ip
server_port = 7000

[ssh]
type = tcp
local_ip = 127.0.0.1           
local_port = 22
remote_port = 6000
    
[web]
type = http # 选择的类型，如果你是tcp就写tcp
local_port = 80
custom_domains = 服务端ip:端口
//3.启动
nohup ./frpc -c frpc.ini &
~~~