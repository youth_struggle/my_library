# MarkDown语法演练
## 代码块
~~~java
pullic void static main(String args[]){
    System.out.println("hello world");
}
~~~
## 图片  
![图片](https://img-blog.csdnimg.cn/9cfa5d6525f84f70b08c4a4dfa8fea99.png)

## 连接
[这是个连接](www.baidu.com)
## 分割符
---
## 特殊线条
~~删除线~~，<u>带下划线文本</u>
## 字体
*斜体*，**粗体**，***粗斜体***
## 列表
### 无序列表
* 第一项
* 第二项
### 有序列表
1. 有序列表1
2. 有序列表2
## 区块
> 区块  
> 1.使用列表
>  * 无序列表
## 单行代码
`printf()` 函数  

## 表格
|  表头  |  表尾  |
|  :----  |  ----:  |
|  单元格  | 位置  |

|  名称  |	关键字  |  调用的js  |  说明  |

|  :----:|  :----:|  :----:|  :----:|  
|AppleScript  |	applescript  |  shBrushAppleScript.js  | 
ActionScript 3.0	actionscript3 , as3	shBrushAS3.js	 
Shell	bash , shell	shBrushBash.js	 
ColdFusion	coldfusion , cf	shBrushColdFusion.js	 
C	cpp , c	shBrushCpp.js	 
C#	c# , c-sharp , csharp	shBrushCSharp.js	 
CSS	css	shBrushCss.js	 
Delphi	delphi , pascal , pas	shBrushDelphi.js	 
diff&patch	diff patch	shBrushDiff.js	用代码版本库时,遇到代码冲突,其语法就是这个
Erlang	erl , erlang	shBrushErlang.js	 
Groovy	groovy	shBrushGroovy.js	 
Java	java	shBrushJava.js	 
JavaFX	jfx , javafx	shBrushJavaFX.js	 
JavaScript	js , jscript , javascript	shBrushJScript.js	 
Perl	perl , pl , Perl	shBrushPerl.js	 
PHP	php	shBrushPhp.js	 
text	text , plain	shBrushPlain.js	就是普通文本
Python	py , python	shBrushPython.js	 
Ruby	ruby , rails , ror , rb	shBrushRuby.js	 
SASS&SCSS	sass , scss	shBrushSass.js	 
Scala	scala	shBrushScala.js	 
SQL	sql	shBrushSql.js	 
Visual Basic	vb , vbnet	shBrushVb.js	 
XML	xml , xhtml , xslt , html	shBrushXml.js	 
Objective C	objc , obj-c	shBrushObjectiveC.js	 
F#	f# f-sharp , fsharp	shBrushFSharp.js	 
xpp	xpp , dynamics-xpp	shBrushDynamics.js	 
R	r , s , splus	shBrushR.js	 
matlab	matlab	shBrushMatlab.js	 
swift	swift	shBrushSwift.js	 
GO	go , golang	shBrushGo.js

## 引入视频、音乐、文件连接
<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=1488737309&auto=1&height=66"></iframe>

<iframe src="//player.bilibili.com/player.html?aid=59317437&bvid=BV1Pt411G7qh&cid=103365806&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
