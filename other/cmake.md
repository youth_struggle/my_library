# cmake学习
## window和linux编译后的文件格式

示例：[cmake编译libpng](https://www.shuzhiduo.com/A/QV5Zg1x7dy/)

- 动态库的后缀：在Linux下后缀为.so，在win系统下后缀为.dll。
- 静态库的后缀：在Linux下后缀为.a，在win系统下后缀为.lib。
- 可执行文件后缀：在Linux下后缀为.out，在win系统下后缀为.exe。


## g++|gcc编译器常用命令
- 预编译：g++ -E mian.cpp -o main.i
- 编译：g++ -S mian.i -o main.s
- 汇编：g++ -c mian.s -o main.o
-   链接：g++ mian.cpp -o main.i

### g++|gcc重要编译参数
- -g：产生带调试信息的可执行文件可以被gdb调试。g++ -g test.cpp -o test
- -O[n]：告诉编译器自行优化代码，n常为0~3。g++ test.cpp -O2 -o test
- -l和-L：指定库文件|指定库路径，链接的文件或路径。g++ test.cpp -lglog -o test 或 g++ test.cpp -L/home/user/lib -o test
- -I：头文件路径。g++ -I/myinclude test.cpp  -o test
- -Wall和-w：打印编译过程警告信息|关闭信息：g++ -Wall test.cpp
- -std=c++11：设置编译标准。g++ -std=c++11 test.cpp -o test
- -DDEBUG：输出源码中debug宏的调试信息。
### g++|gcc生成静态库和动态库

1. 静态库

      ```shell
      g++ -c swap.cpp  -I../include -o swap.o # 汇编生成swap.o文件
      ar rs libswap.a swap.o # 生成静态库文件
      g++ main.cpp -Iinclude -Lsrc -lswap -o static_main # 链接生成可执行程序
      ```

2. 动态库

    ~~~shell
    ```shell
     g++ swap.cpp -I../include -fPIC -shared -o swap.so # 生成动态库
     ## 上面这句等价于
     swap.cpp -I../include -fPIC
     swap.cpp -shared -o swap.so
     ## 上面两句
     g++ main.cpp -Iinclude -Lsrc -lswap -o shared_main # 链接动态库生成可执行文件
    ```
    ~~~

 3. 执行可执行程序

        ```shell
        # 如果动态库不和主程序在同一目录，需要指定库路径
        LD_LIBRARY_PATH=src ./main
        ```


## CmakeList.txt中常用配置

- cmake_minimum_required(VERSION 2.8.3)：cmake最小版本要求
- project(HELLOE)：工程名
- set(SRC main.cpp hello.cpp)：显示定义变量
- aux_source_directory(. SRC)：定义SRC变量，其值为当前目录下所有源文件
- include_directories(/usr/include/ ./include)：向工程添加特定的文件搜索路径，类似g++ -I
- link_directories(/usr/lib ./lib)：向工程添加多个特定的库文件路径，类似g++ -L
- add_library(hello SHARED ${SRC}):生成库文件，通过变量SRC生成libhello.so共享库。
- add_complie_options(-Wall -std=c++11 -o2)：添加编译参数
- add_excutable(main main.cpp src/ hello.cpp)：生成可执行文件
- target_link_libraries(main hello)：将hello动态库链接到可执行文件main中，类似编译时-lmath
- add_subdirectory(src)：添加src子目录，src中有个CmakeLists.txt

## 常用变量

- CMAKE_C_FLAGS、CMAKE_CXX_FLAGS：set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")在CMAKE_CXX_FLAGS 变量后追加-std=c++11
- set(CMAKE_BUILD_TYPE Debug)：设置编译类型为debug
- CMAKE_BINARY_DIR、PROJECT_BINARY_DIR、BINARY_DIR：三者含义基本一致，指当前编译工程目录。

|               变量                |                    内容                    |                             备注                             |
| :-------------------------------: | :----------------------------------------: | :----------------------------------------------------------: |
|      EXECUTABLE_OUTPUT_PATH       |             可执行文件输出目录             |     set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)      |
|  CMAKE_ARCHIVE_OUTPUT_DIRECTORY   |             设置静态库输出目录             | set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)  |
|  CMAKE_LIBRARY_OUTPUT_DIRECTORY   |            编译时lib库输出目录             | set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)  |
|  CMAKE_RUNTIME_OUTPUT_DIRECTORY   |               动态库输出目录               | set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)  |
|        PROJECT_BINARY_DIR         |                编译输出目录                |                如${PROJECT_SOURCE_DIR}/build                 |
|        PROJECT_SOURCE_DIR         |                  代码目录                  | 如果直接在代码目录下cmake，那么此变量与PROJECT_BINARY_DIR内容相同 |
|        CMAKE_INCLUDE_PATH         |               包含头文件目录               |                     环境变量,非cmake变量                     |
|        CMAKE_LIBRARY_PATH         |                 链接库目录                 |                           环境变量                           |
|     CMAKE_CURRENT_SOURCE_DIR      |             当前Cmake代码目录              |              当前处理的CMakeLists.txt所在的路径              |
|     CMAKE_CURRENT_BINARY_DIR      |               target编译目录               |                                                              |
|      CMAKE_CURRENT_LIST_FILE      | 输出调用这个变量的CMakeLists.txt的完整路径 |                                                              |
|      CMAKE_CURRENT_LIST_LINE      |            输出这个变量所在的行            |                                                              |
|         CMAKE_MODULE_PATH         |       定义自己的cmake模块所在的路径        |                                                              |
|         CMAKE_MODULE_PATH         |        cmake查找cmake模块文件的目录        |           find_package(…)时，在此变量的目录内查找            |
|           PROJECT_NAME            |     返回通过PROJECT指令定义的项目名称      |                                                              |
| CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS |       用来控制IF ELSE语句的书写方式        |                                                              |

## Cmake编译c++文件过程

```shell
mkdir build # 创建一个build目录
cd build # 进入到build目录
cmake .. # 生成编译过程文件
make # 编译
```

> 当修改了其中某个c++源文件，可以直接在build目录下执行make重新编译，编译器只会编译已修改的源文件。

## cmake编译命令参数

- -G：指定编译器 > cmake -G "MinGW Makefiles"

## 使用MinGW和CMake在VSCode中进行C++调试

> 注意：生成可调试的程序需要在cmakelist.txt中添加set(CMAKE_BUILD_TYPE Debug)。

1. 配置tasks.json

   > `task.json`文件用于配置任务，包括编译、运行和清理等操作。在VSCode中打开终端，使用快捷键`Ctrl+Shift+P`打开命令面板，在命令面板中选择`Tasks: Configure Task`，然后选择`Create tasks.json file from template`，并选择`Others`模板。

   ```json
   {
       "version": "2.0.0",
       "options": {
           "cwd": "${workspaceFolder}/build"
       },
       "tasks": [
           {
               "label": "cmake",
               "type": "shell",
               "command": "cmake -G 'MinGW Makefiles' -DCMAKE_CXX_FLAGS=-std=c++11 ..",
               "group": "build",
               "problemMatcher": "$msCompile"
           },
           {
               "label": "make",
               "group": {
                   "kind": "build",
                   "isDefault": true
               },
               "command": "mingw32-make.exe",
               "args": []
           },
           {
               "label": "Build",
               "dependsOrder": "sequence",
               "dependsOn": [
                   "cmake",
                   "make"
               ]
           }
       ]
   }
   ```

   

2. 配置lauch.json

   > `launch.json`文件用于配置调试器，包括调试器类型、调试器路径、程序参数等。在VSCode中打开Debug视图，点击齿轮按钮，选择"C++(GDB/LLDB)"环境，并配置`launch.json`文件。

   ```json
   {
       // 使用 IntelliSense 了解相关属性。 
       // 悬停以查看现有属性的描述。
       // 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
       "version": "0.2.0",
       "configurations": [
           {
               "name": "(gdb) 启动",
               "type": "cppdbg",
               "request": "launch",
               "program": "${workspaceFolder}/build/main.exe", // 指定调试程序路径
               "args": [],
               "stopAtEntry": false,
               "cwd": "${fileDirname}",
               "environment": [],
               "externalConsole": false,
               "MIMode": "gdb",
               "miDebuggerPath": "D:/development/MinGW/bin/gdb.exe",// gdb路径
               "setupCommands": [
                   {
                       "description": "为 gdb 启用整齐打印",
                       "text": "-enable-pretty-printing",
                       "ignoreFailures": true
                   },
                   {
                       "description": "将反汇编风格设置为 Intel",
                       "text": "-gdb-set disassembly-flavor intel",
                       "ignoreFailures": true
                   }
               ],
               "preLaunchTask": "Build"// 启动调试的前置任务
           }
       ]
   }
   ```

   

3. 

