# imread(const String& filename, int flags)
1. filename:图像路径
2. flags:读取方式
    * -1：原图不做任何改变
    *  0：灰度图方式
    *  1：RGB方式读取

# 图像基础（大小、深度、通道）
1. 大小：图片的大小
2. 深度：单个通道的取值大小，占用二进制位数/bit位。
3. 通道：目前最多有4通道，除RGB外多了个A通道，即透明度。
opencv的mat类表示：CV_<bit_depth>(S|U|F)C<number_of_channels>
S = 符号整型  U = 无符号整型  F = 浮点型  
E.g.：  
CV_8UC1 是指一个8位无符号整型单通道矩阵,  
CV_32FC2是指一个32位浮点型双通道矩阵。  

# 矩阵->图像的理解
矩阵的理解：
一维数组：[1,2,3] 由点组成 step[0]
二维数组：[[1,2,3],[2,3,4],[3,5,6]] 由线组成 step[1]
三维数组：[[[1,2,3],[2,3,4]],[[3,4,5],[4,5,6]]] 由面组成 step[2]

矩阵->图像的理解：
比如常见的RGB彩色图是三通道的，可以表示为：
[[[R,G,B],[R1,G1,B1],[R2,G2,B2]],
[[R3,G3,B3],[R4,G4,B4],[R5,G5,B5]],
[[R6,G6,B6],[R7,G7,B7],[R8,G8,B8]]]
在矩阵中最小单位**点**->在图像中则表示单个通道的值
在矩阵中**线**->在图像中则表示单个像素点（1*3）
在矩阵中**面**->在图像中则表示一行像 
在opencv中Mat对象的data是存储的图像的指针，在本例中指向的是R(0,0,0)的地址。所以假设我要计算R5(0,1,2)的地址：
r5_addr=m.data+m.step[0]*0+m.step[1]*1+m.step[2]*2 
其中的step就是每个维度的字节数

# Android中使用opencv

[安卓使用opencv参考链接](https://www.jb51.net/article/202708.htm)（不需要创建jniLibs文件夹）



# opencv使用中的疑问

## opencv默认的灰度处理是怎么处理的？

默认通过一个灰度转换公式实现灰度计算：cvtColor(img,COLOR_BGR2GRAY)

![img](https://img-blog.csdn.net/20180327214135426?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MTY5NTU2NA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

## 那它又是怎么将图像二值化的呢？

1. cv.threshold(gray,125,255,THRESH_BINARY)用来实现阈值分割，ret是return value缩写，代表当前的阈值，暂时不用理会。函数有4个参数：
   参数1：要处理的原图，一般是灰度图
   参数2：设定的阈值
   参数3：最大阈值，一般为255
   参数4：阈值的方式，主要有5种。THRESH_BINARY（二值化）|THRESH_BINARY_INV（二值化反转）
2. 固定阈值是在整幅图片上应用一个阈值进行分割，它并不适用于明暗分布不均的图片。 cv.adaptiveThreshold()自适应阈值会每次取图片的一小部分计算阈值，这样图片不同区域的阈值就不尽相同。它有5个参数：
   参数1：要处理的原图
   参数2：最大阈值，一般为255
   参数3：小区域阈值的计算方式
   ADAPTIVE_THRESH_MEAN_C：小区域内取均值
   ADAPTIVE_THRESH_GAUSSIAN_C：小区域内加权求和，权重是个高斯核
   参数4：阈值方式（跟前面讲的那5种相同）
   参数5：小区域的面积，如11就是11*11的小块
   参数6：最终阈值等于小区域计算出的阈值再减去此值
3. 大津算法，也被称作最大类间方差法，是一种自动确定[二值化](https://so.csdn.net/so/search?q=二值化&spm=1001.2101.3001.7020)阈值的算法，threshold(gray, gray_binary, 0, 255, THRESH_BINARY + THRESH_OTSU);
