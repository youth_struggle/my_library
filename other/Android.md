# 安卓开发

## 1、ARouter使用

##  2、继承类

- [Application](https://www.cnblogs.com/renqingping/archive/2012/10/24/Application.html)类：在Android中，可以通过继承Application类来实现应用程序级的全局变量，这种全局变量方法相对静态类更有保障，直到应用的所有Activity全部被destory掉之后才会被释放掉。

# 3、布局

## 3.1、线性布局

![image-20221229102911685](http://101.43.99.219:1314/file/download?path=/markdown/image-20221229102911685.png)

## 3.2、相对布局

> 相对布局，主要设置参考视图来确定自己的布局位置，可以相对父容器视图，或者相对同级视图

![image-20221229104248441](http://101.43.99.219:1314/file/download?path=/markdown/image-20221229104248441.png)

## 3.3、网格布局

> 子视图默认是从左往右，从上往下的布局方式

![image-20221229105129480](http://101.43.99.219:1314/file/download?path=/markdown/image-20221229105129480.png)



## Activity

![image-20221229170032003](http://101.43.99.219:1314/file/download?path=/markdown/image-20221229170032003.png)

## activity的加载模式

![image-20221230093217271](http://101.43.99.219:1314/file/download?path=/markdown/image-20221230093217271.png)

**模式详解：**

------

### standard模式：

标准启动模式，也是activity的默认启动模式。在这种模式下启动的activity可以被多次实例化，即在同一个任务中可以存在多个activity的实例，每个实例都会处理一个Intent对象。如果Activity A的启动模式为standard，并且A已经启动，在A中再次启动Activity A，即调用startActivity（new Intent（this，A.class）），会在A的上面再次启动一个A的实例，即当前的桟中的状态为A-->A。

------

### singleTop模式：

如果一个以singleTop模式启动的Activity的实例已经存在于任务栈的栈顶， 那么再启动这个Activity时，不会创建新的实例，而是重用位于栈顶的那个实例， 并且会调用该实例的**onNewIntent()**方法将Intent对象传递到这个实例中。 举例来说，如果A的启动模式为singleTop，并且A的一个实例已经存在于栈顶中， 那么再调用startActivity（new Intent（this，A.class））启动A时， 不会再次创建A的实例，而是重用原来的实例，并且调用原来实例的onNewIntent()方法。 这时任务栈中还是这有一个A的实例。如果以singleTop模式启动的activity的一个实例 已经存在与任务栈中，但是不在栈顶，那么它的行为和standard模式相同，也会创建多个实例。

------

### singleTask模式：

只允许在系统中有一个Activity实例。如果系统中已经有了一个实例， 持有这个实例的任务将移动到顶部，同时intent将被通过onNewIntent()发送。 如果没有，则会创建一个新的Activity并置放在合适的任务中。

官方文档中提到的一个问题：

> 系统会创建一个新的任务，并将这个Activity实例化为新任务的根部（root） 这个则需要我们对taskAffinity进行设置了，使用taskAffinity后的解雇：

------

------

### singleInstance模式

保证系统无论从哪个Task启动Activity都只会创建一个Activity实例,并将它加入新的Task栈顶 也就是说被该实例启动的其他activity会自动运行于另一个Task中。 当再次启动该activity的实例时，会重用已存在的任务和实例。并且会调用这个实例 的onNewIntent()方法，将Intent实例传递到该实例中。和singleTask相同， 同一时刻在系统中只会存在一个这样的Activity实例。



## [Intent](https://www.runoob.com/w3cnote/android-tutorial-intent-base.html)

- **显式Intent**：通过组件名指定启动的目标组件,比如startActivity(new Intent(A.this,B.class)); 每次启动的组件只有一个~
- **隐式Intent**:不指定组件名,而指定Intent的Action,Data,或Category,当我们启动组件时, 会去匹配AndroidManifest.xml相关组件的Intent-filter,逐一匹配出满足属性的组件,当不止一个满足时, 会弹出一个让我们选择启动哪个的对话框~

> 界面跳转A->B可以通过显示或者隐式的意图来传递数据，但如果是界面跳转回来呢？B->A：

```java
//A界面
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button routerButton = findViewById(R.id.router);
        TextView viewById = findViewById(R.id.text_view);
        //界面返回注册监听器
        ActivityResultLauncher<Intent> register = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result != null) {
                Intent intent = result.getData();
                if (intent != null && result.getResultCode() == Activity.RESULT_OK) {
                    Bundle bundle = intent.getExtras();
                    String resp = bundle.getString("resp");
                    viewById.setText(resp);
                }
            }
        });
        //跳转按钮时间监听
        routerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Demo2.class);
                Bundle bundle = new Bundle();
                bundle.putString("request", "在吗，睡了吗？");
                intent.putExtras(bundle);
                register.launch(intent);
            }
        });
    }
}
//B界面
public class Demo2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo2);
        Button btn = findViewById(R.id.btn_click);
        TextView text = findViewById(R.id.text_view);
        //从A界面跳转过来的数据包
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String reques = bundle.getString("request");
        text.setText(reques);
        //点击按钮跳转回去
        btn.setOnClickListener(v -> {
            Intent inten = new Intent();
            Bundle bundle1 = new Bundle();
            bundle1.putString("resp", "在的，今晚我爸妈不在");
            inten.putExtras(bundle1);
            setResult(Activity.RESULT_OK, inten);
            finish();
        });
    }
}
```

